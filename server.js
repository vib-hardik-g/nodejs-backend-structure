const express = require("express");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const dotenv = require("dotenv");
const useragent = require("express-useragent");

dotenv.config();

const app = express();
const path = require("path");
const {
  createSuperAdmin,
  feedCountryStateCity,
  createTimeTableType,
  createAssessRights,
} = require("./app/dataFeed/feedData");

app.use(useragent.express());

app.use("/public", express.static(path.resolve(__dirname, "app/public")));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(fileUpload());

app.use((req, res, next) => {
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST, PUT, OPTIONS, DELETE, GET"
  );
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});
app.use(function returnOps(req, res, next) {
  if (req.method === "OPTIONS") return res.status(200).send("OK").end();
  next();
});

(async function () {
  console.log("DATA FEEDING ::::: STARTED");
  createSuperAdmin();
  feedCountryStateCity();
  createTimeTableType();
  createAssessRights();
  console.log("DATA FEEDING ::::: FINISHED");
})();

app.use(require("./app/routes"));

// set port, listen for requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
