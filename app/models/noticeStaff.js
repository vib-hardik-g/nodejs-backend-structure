const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const noticeStaff = sequelize.define('noticeStaff', {
        noticeType: {
            type: Sequelize.ENUM('Class', 'Subject'),
            defaultValue: 'Class'
        },
        title: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Title cannot be empty"
                }
            }
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        noticeDocument: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    noticeStaff.associate = (models) => {
        noticeStaff.hasMany(models.noticeStaffType, { foreignKey: "noticeStaffId" });
        noticeStaff.belongsTo(models.document, { foreignKey: "noticeDocument" });
        noticeStaff.belongsTo(models.institution, { foreignKey: "instituteId" });
    }

    return noticeStaff;
};