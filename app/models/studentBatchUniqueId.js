const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentBatchUniqueId = sequelize.define('studentBatchUniqueId', {
        studentId: {
            type: Sequelize.INTEGER,
        },
        instituteId: {
            type: Sequelize.INTEGER,
        },
        uniqueId: {
            type: Sequelize.INTEGER,
            unique: {
                args: true,
                msg: 'Unique id already exists',
                fields: [sequelize.col('uniqueId')]
            },
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    studentBatchUniqueId.associate = (models) => {
        studentBatchUniqueId.belongsTo(models.student, { foreignKey: "studentId" });
        studentBatchUniqueId.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return studentBatchUniqueId;
}