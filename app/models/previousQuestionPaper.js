const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const previousQuestionPaper = sequelize.define(
        'previousQuestionPaper', {
            classId:{
                type: Sequelize.INTEGER,
            },
            examTypeId:{
                type: Sequelize.INTEGER,
            },
            subjectId:{
                type: Sequelize.INTEGER,
            },
            questionPaperName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Question Paper Name cannot be empty"
                    }
                }
            },
            questionPaperPath: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Question Paper Path cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    previousQuestionPaper.associate = (models) => {
        previousQuestionPaper.belongsTo(models.class, { foreignKey: "classId" });
        previousQuestionPaper.belongsTo(models.examType, { foreignKey: "examTypeId" });
        previousQuestionPaper.belongsTo(models.subjectTopicBatchMapping, { foreignKey: "subjectId" });
    }
    return previousQuestionPaper;
}