const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const timeTableDetail = sequelize.define(
        'timeTableDetail', {
            timeTableId: {
                type: Sequelize.INTEGER,
            },
            subjectId: {
                type: Sequelize.INTEGER,
            },
            staffId: {
                type: Sequelize.INTEGER,
            },
            centerId: {
                type: Sequelize.INTEGER,
            },
            day: {
                type: Sequelize.ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Day cannot be empty"
                    }
                }
            },
            room: {
                type: Sequelize.STRING(50),
                defaultValue: ''
            },
            startTime: {
                type: Sequelize.STRING(10),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Start Time cannot be empty"
                    }
                }
            },
            endTime: {
                type: Sequelize.STRING(10),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "End Time cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    timeTableDetail.associate = (models) => { 
        timeTableDetail.belongsTo(models.timeTable, { foreignKey: 'timeTableId' });
        timeTableDetail.belongsTo(models.staff, { foreignKey: 'staffId' });
        timeTableDetail.belongsTo(models.subject, { foreignKey: 'subjectId' });
        timeTableDetail.belongsTo(models.center, { foreignKey: 'centerId' });
    }
    return timeTableDetail;
}