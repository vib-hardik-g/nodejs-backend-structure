const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcementStaffType = sequelize.define('announcementStaffType', {
        announcementStaffId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    announcementStaffType.associate = (models) => {
        announcementStaffType.belongsTo(models.announcementStaff, { foreignKey: "announcementStaffId" });
        announcementStaffType.belongsTo(models.class, { foreignKey: "classId" });
        announcementStaffType.belongsTo(models.subject, { foreignKey: "subjectId" });
    }

    return announcementStaffType;
};