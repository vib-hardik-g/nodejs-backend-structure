const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userModuleMapping = sequelize.define(
        'userModuleMapping', {
        userId: {
            type: Sequelize.INTEGER,
        },
        moduleId: {
            type: Sequelize.INTEGER,
        },
        subModuleId: {
            type: Sequelize.INTEGER,
        },
        permission: {
            type: Sequelize.BOOLEAN,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    userModuleMapping.associate = (models) => {
        userModuleMapping.belongsTo(models.user, { foreignKey: "userId" });
        userModuleMapping.belongsTo(models.module, { foreignKey: "moduleId" });
    };
    
    return userModuleMapping;
}