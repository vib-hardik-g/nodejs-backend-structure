const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subTopic = sequelize.define('subTopic', {
        topicDetailsId: {
            type: Sequelize.INTEGER,
        },
        subTopicName: {
            type: Sequelize.STRING,
        },
        allotedHours: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    subTopic.associate = (models) => {
        subTopic.belongsTo(models.topicDetails, { foreignKey: "topicDetailsId" });
        subTopic.hasMany(models.staffProgressReport, { foreignKey: "subTopicId" });
    }
    return subTopic;
}