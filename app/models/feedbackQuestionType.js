const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackQuestionType = sequelize.define(
        'feedbackQuestionType', {
            feedbackType: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Feedback Type cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    return feedbackQuestionType;
}