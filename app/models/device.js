const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const device = sequelize.define(
        'device', {
        userId: {
            type: Sequelize.INTEGER,
        },
        deviceName: {
            type: Sequelize.STRING,
        },
        deviceMake: {
            type: Sequelize.STRING,
        },
        deviceModel: {
            type: Sequelize.STRING,
        },
        deviceToken: {
            type: Sequelize.STRING,
        },
        platform: {
            type: Sequelize.STRING,
        },
        systemVersion: {
            type: Sequelize.STRING,
        },
        deviceId: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    device.associate = (models) => {
        device.belongsTo(models.user, { foreignKey: "userId" });
    }
    return device;

}