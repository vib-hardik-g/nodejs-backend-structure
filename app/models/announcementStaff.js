const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcementStaff = sequelize.define('announcementStaff', {
        announcementType: {
            type: Sequelize.ENUM('Class', 'Subject'),
            defaultValue: 'Class',
            allowNull: false
        },
        title: {
            type: Sequelize.STRING(255),
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Title cannot be empty"
                }
            }
        },
        description:{
            type:Sequelize.STRING(300),
            allowNull: false
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        announcementDocument: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    announcementStaff.associate = (models) => {
        announcementStaff.hasMany(models.announcementStaffType, { foreignKey: "announcementStafftId" });
        announcementStaff.belongsTo(models.document, { foreignKey: "announcementDocument" });
        announcementStaff.belongsTo(models.institution, { foreignKey: "instituteId" });
    }

    return announcementStaff;
};