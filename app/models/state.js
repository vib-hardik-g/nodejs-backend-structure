const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const state = sequelize.define(
        'state', {
            stateName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "State Name cannot be empty"
                    }
                }
            },
            countryId: Sequelize.INTEGER,
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    state.associate = (models) => {
        state.belongsTo(models.country, {foreignKey: "countryId" });
        state.hasMany(models.city, {foreignKey: "stateId" });
    }
    return state;
}