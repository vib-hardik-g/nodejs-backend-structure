const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const noticeStudentType = sequelize.define('noticeStudentType', {
        noticeStudentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        groupId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    noticeStudentType.associate = (models) => {
        noticeStudentType.belongsTo(models.noticeStudent, { foreignKey: "noticeStudentId" });
        noticeStudentType.belongsTo(models.class, { foreignKey: "classId" });
        noticeStudentType.belongsTo(models.batch, { foreignKey: "batchId" });
        noticeStudentType.belongsTo(models.group, { foreignKey: "groupId" });
    }

    return noticeStudentType;
};