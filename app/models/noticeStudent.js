const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const noticeStudent = sequelize.define('noticeStudent', {
        noticeType: {
            type: Sequelize.ENUM('Class', 'Batch', 'Group'),
            defaultValue: 'Class'
        },
        title: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Title cannot be empty"
                }
            }
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        noticeDocument: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    noticeStudent.associate = (models) => {
        noticeStudent.hasMany(models.noticeStudentType, { foreignKey: "noticeStudentId" });
        noticeStudent.belongsTo(models.document, { foreignKey: "noticeDocument" });
        noticeStudent.belongsTo(models.institution, { foreignKey: "instituteId" });
    }

    return noticeStudent;
};