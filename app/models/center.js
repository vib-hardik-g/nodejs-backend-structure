const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const center = sequelize.define(
        'center', {
            centerName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Center Name cannot be empty"
                    }
                }
            },
            instituteId:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    center.associate = (models) => {
        center.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return center;
}