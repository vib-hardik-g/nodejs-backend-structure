const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institutionEmail = sequelize.define(
        'institutionEmail', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            emailType: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Email Type cannot be empty"
                    }
                }
            },
            emailId: {
                type: Sequelize.STRING(50),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Email Id cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    institutionEmail.associate = (models) => {
        institutionEmail.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return institutionEmail;
}