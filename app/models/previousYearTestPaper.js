const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const previousYearTestPaper = sequelize.define('previousYearTestPaper', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        examTypeId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        documentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        year: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        isRePublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        rePublishedAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    previousYearTestPaper.associate = (models) => {
        previousYearTestPaper.belongsTo(models.document, { foreignKey: "documentId" });
        previousYearTestPaper.belongsTo(models.subject, { foreignKey: "subjectId" });
        previousYearTestPaper.belongsTo(models.class, { foreignKey: "classId" });
        previousYearTestPaper.belongsTo(models.examType, { foreignKey: "examTypeId" });
    }

    return previousYearTestPaper;
}