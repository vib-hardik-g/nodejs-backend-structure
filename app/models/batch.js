const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const batch = sequelize.define(
        'batch', {
            batchName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Batch Name cannot be empty"
                    }
                }
            },
            classId:{
                type: Sequelize.INTEGER,
            },
            instituteId:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt:{
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            }
        }
    );
    batch.associate = (models) => {
        batch.belongsTo(models.class, {foreignKey: "classId" });
        batch.belongsTo(models.institution, { foreignKey: "instituteId" });

        batch.hasMany(models.batchSubjectMapping, { foreignKey: "batchId", as: 'subjects' });
    }
    return batch;
}