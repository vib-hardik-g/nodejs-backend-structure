const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffAbsentee = sequelize.define(
        'staffAbsentee', {
            staffId: {
                type: Sequelize.INTEGER,
            },
            absenceType: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Subject Name cannot be empty"
                    }
                }
            },
            date: {
                type: Sequelize.DATEONLY,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    staffAbsentee.associate = (models) => {
        staffAbsentee.belongsTo(models.staff, { foreignKey: "staffId"  });
    }
    return staffAbsentee;
}