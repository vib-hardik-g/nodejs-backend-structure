const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const questionBank = sequelize.define(
        'questionBank', {
            instituteId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            classId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            subjectId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            topicDetailsId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            generatedTemplate:{
                type: Sequelize.INTEGER,
            },
            uploadedTemplate:{
                type: Sequelize.INTEGER,
            },
            generatedPDF:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    questionBank.associate = (models) => {
        questionBank.belongsTo(models.class, { foreignKey: "classId" });
        questionBank.belongsTo(models.subject, { foreignKey: "subjectId" });
        questionBank.belongsTo(models.topicDetails, { foreignKey: "topicDetailsId" });
        questionBank.hasMany(models.questionBankQuestion, { foreignKey: "questionBankId" });
        questionBank.belongsTo(models.document, { as: 'templateGenerated', foreignKey: "generatedTemplate" });
        questionBank.belongsTo(models.document, { as: 'templateUploaded', foreignKey: "uploadedTemplate" });
        questionBank.belongsTo(models.document, { as: 'pdfGenerated', foreignKey: "generatedPDF" });
    }
    return questionBank;
}