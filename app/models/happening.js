const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const happening = sequelize.define(
        'happening', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        studentId: {
            allowNull: true,
            type: Sequelize.INTEGER,
            defaultValue: null,
        },
        staffId: {
            allowNull: true,
            type: Sequelize.INTEGER,
        },
        happening: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Happening cannot be empty"
                }
            }
        },
        happeningDescription: {
            type: Sequelize.TEXT,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Happening Description cannot be empty"
                }
            }
        },
        happeningLikeCount: {
            type: Sequelize.INTEGER,
        },
        happeningCommentCount: {
            type: Sequelize.INTEGER,
        },
        happeningThumnbnailImage: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    happening.associate = (models) => {
        happening.belongsTo(models.student, { foreignKey: "studentId" });
        happening.belongsTo(models.staff, { foreignKey: "staffId" });
        happening.belongsTo(models.institution, { foreignKey: "instituteId" });
        happening.belongsTo(models.document, { foreignKey: "happeningThumnbnailImage", as:'thumnbnailImage' });
        happening.hasMany(models.happeningComment, { foreignKey: "happeningId", as: "comments" });
        happening.hasMany(models.happeningAttachment, { foreignKey: "happeningId", as: "attachments" });
    }
    return happening;
}