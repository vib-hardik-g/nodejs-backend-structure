const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userStaffMapping = sequelize.define(
        'userStaffMapping', {
            userId: {
                type: Sequelize.INTEGER,
            },
            staffId: {
                type: Sequelize.INTEGER,
            },
            instituteId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    userStaffMapping.associate = (models) => {
        userStaffMapping.belongsTo(models.user, { foreignKey: "userId" });
        userStaffMapping.belongsTo(models.staff, { foreignKey: "staffId" });
        userStaffMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return userStaffMapping;
}