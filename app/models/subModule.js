const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subModule = sequelize.define(
        'subModule', {
            moduleId: {
                type: Sequelize.INTEGER
            },
            subModuleName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Sub Module Name cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    subModule.associate = (models) => {
        subModule.belongsTo(models.module, { foreignKey: "moduleId", as: "module" });
    }
    return subModule;
}