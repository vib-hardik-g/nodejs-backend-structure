const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackTemplateCategory = sequelize.define('feedbackTemplateCategory', {
        feedbackTemplateId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        categoryName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackTemplateCategory.associate = (models) => {
        feedbackTemplateCategory.belongsTo(models.feedbackTemplate, { foreignKey: "feedbackTemplateId" });
        feedbackTemplateCategory.hasMany(models.feedbackTemplateBenchmark, { foreignKey: "feedbackTemplateCategoryId" });
        feedbackTemplateCategory.hasMany(models.feedbackTemplateCategoryParameter, { foreignKey: "feedbackTemplateCategoryId" });
    }
    return feedbackTemplateCategory;
}