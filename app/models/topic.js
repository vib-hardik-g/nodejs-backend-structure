const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const topic = sequelize.define('topic', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });
    topic.associate = (models) => {
        topic.belongsTo(models.class, { foreignKey: "classId" });
        topic.belongsTo(models.institution, { foreignKey: "instituteId" });
        topic.belongsTo(models.subject, { foreignKey: "subjectId" });
        topic.hasMany(models.topicDetails, { foreignKey: "topicId" });

    }
    return topic;
}