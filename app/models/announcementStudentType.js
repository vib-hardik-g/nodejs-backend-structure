const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcementStudentType = sequelize.define('announcementStudentType', {
        announcementStudentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        groupId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    announcementStudentType.associate = (models) => {
        announcementStudentType.belongsTo(models.announcementStudent, { foreignKey: "announcementStudentId" });
        announcementStudentType.belongsTo(models.class, { foreignKey: "classId" });
        announcementStudentType.belongsTo(models.batch, { foreignKey: "batchId" });
        announcementStudentType.belongsTo(models.group, { foreignKey: "groupId" });
    }

    return announcementStudentType;
};