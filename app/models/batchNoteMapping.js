const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const batchNoteMapping = sequelize.define(
        'batchNoteMapping', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        noteId: {
            type: Sequelize.INTEGER,
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    batchNoteMapping.associate = (models) => {
        batchNoteMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        batchNoteMapping.belongsTo(models.note, { foreignKey: "noteId" });
        batchNoteMapping.belongsTo(models.batch, { foreignKey: "batchId" });
    }
    return batchNoteMapping;
}