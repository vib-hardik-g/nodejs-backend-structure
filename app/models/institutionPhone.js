const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institutionPhone = sequelize.define(
        'institutionPhone', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            phoneNumber: {
                type: Sequelize.STRING(15),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Phone Number cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    institutionPhone.associate = (models) => {
        institutionPhone.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return institutionPhone;
}