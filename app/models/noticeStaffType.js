const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const noticeStaffType = sequelize.define('noticeStaffType', {
        noticeStaffId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    noticeStaffType.associate = (models) => {
        noticeStaffType.belongsTo(models.noticeStaff, { foreignKey: "noticeStaffId" });
        noticeStaffType.belongsTo(models.class, { foreignKey: "classId" });
        noticeStaffType.belongsTo(models.subject, { foreignKey: "subjectId" });
    }

    return noticeStaffType;
};