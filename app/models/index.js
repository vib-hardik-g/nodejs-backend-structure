'use strict';

var fs = require('fs');
var path = require('path');
var Connection = require('../../config/connection');
var basename = path.basename(module.filename);
var sequelize = Connection.sequelize;
//Connection.sequelize.start();

var db = {};
// console.log(fs
//   .readdirSync(__dirname)
//   .filter(function (file) {
//      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
//   }))
fs
   .readdirSync(__dirname)
   .filter(function (file) {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
   })
   .forEach(function (file) {
   //   console.log(file)
    //   var model = sequelize['import'](path.join(__dirname, file));
      const model = require(path.join(__dirname, file))(sequelize, sequelize.DataTypes)
      // console.log("model", model)
      db[model.name] = model;
   });

Object.keys(db).forEach(function (modelName) {
   if (db[modelName].associate) {
      db[modelName].associate(db);
   }
});

//Export the db Object
db.sequelize = sequelize;

module.exports = db;