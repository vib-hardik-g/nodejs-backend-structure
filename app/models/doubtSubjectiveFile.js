const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubtSubjectiveFile = sequelize.define(
        'doubtSubjectiveFile', {
            doubtId:{
                type: Sequelize.INTEGER,
            },
            doubtFile: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubtSubjectiveFile.associate = (models) => {
        doubtSubjectiveFile.belongsTo(models.doubt, { foreignKey: "doubtId" });
        doubtSubjectiveFile.belongsTo(models.document, { foreignKey: "doubtFile" });
    }
    return doubtSubjectiveFile;
}