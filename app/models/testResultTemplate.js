const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const testResultTemplate = sequelize.define('testResultTemplate', {
        examTypeId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        documentId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        isPublishedForStudent: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedForStudentAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        isPublishedForStaff: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedForStaffAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        isRepublishedForStaff: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        republishedForStaffAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        isRepublishedForStudent: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        republishedForStudentAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
    });

    testResultTemplate.associate = (models) => {
        testResultTemplate.belongsTo(models.institution, { foreignKey: "instituteId" });
        testResultTemplate.belongsTo(models.batch, { foreignKey: "batchId" });
        testResultTemplate.belongsTo(models.examType, { foreignKey: 'examTypeId' });
        testResultTemplate.belongsTo(models.document, { foreignKey: 'documentId' });

        testResultTemplate.hasMany(models.testResultTemplateSubject, { foreignKey: 'testResultTemplateId' });
    }
    return testResultTemplate;
}