const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentRequestAttachment = sequelize.define(
        'studentRequestAttachment', {
            studentRequestId:{
                type: Sequelize.INTEGER,
            },
            requestAttachment: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    studentRequestAttachment.associate = (models) => {
        studentRequestAttachment.belongsTo(models.studentRequest, { foreignKey: "studentRequestId" });
        studentRequestAttachment.belongsTo(models.document, { foreignKey: "requestAttachment" });
    }
    return studentRequestAttachment;
}