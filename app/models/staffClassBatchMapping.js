const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffClassBatchMapping = sequelize.define('staffClassBatchMapping', {
        staffClassMappingId: {
            type: Sequelize.INTEGER,
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    staffClassBatchMapping.associate = (models) => {
        staffClassBatchMapping.belongsTo(models.staffClassMapping, { foreignKey: "staffClassMappingId" });
        staffClassBatchMapping.belongsTo(models.batch, { foreignKey: "batchId" });
        staffClassBatchMapping.hasMany(models.staffClassBatchSubjectMapping, { foreignKey: "staffClassBatchMappingId" });
    }
    return staffClassBatchMapping;
}