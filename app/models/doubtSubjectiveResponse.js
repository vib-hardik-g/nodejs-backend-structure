const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubtSubjectiveResponse = sequelize.define(
        'doubtSubjectiveResponse', {
            doubtId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            staffId:{
                type: Sequelize.INTEGER,
            },
            response: {
                type: Sequelize.TEXT,
            },
            responseLikeCount:{
                type: Sequelize.INTEGER,
            },
            correct:{
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubtSubjectiveResponse.associate = (models) => {
        doubtSubjectiveResponse.belongsTo(models.student, { foreignKey: "studentId" });
        doubtSubjectiveResponse.belongsTo(models.staff, { foreignKey: "staffId" });
        doubtSubjectiveResponse.belongsTo(models.doubt, { foreignKey: "doubtId" });
    }
    return doubtSubjectiveResponse;
}