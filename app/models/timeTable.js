const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const timeTable = sequelize.define(
        'timeTable', {
            timeTableTypeId: {
                type: Sequelize.INTEGER,
            },
            batchId: {
                type: Sequelize.INTEGER,
            },
            instituteId: {
                type: Sequelize.INTEGER,
            },
            year: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            publishStatus:{
                type: Sequelize.ENUM('Pending', 'Published', 'Reuploaded', 'Republished'),
                defaultValue: 'Pending',
            }
        }
    );
    timeTable.associate = (models) => { 
        timeTable.hasMany(models.timeTableDetail, { foreignKey: 'timeTableId' });
        timeTable.belongsTo(models.timeTableType, { foreignKey: 'timeTableTypeId' });
        timeTable.belongsTo(models.batch, { foreignKey: 'batchId' });
        timeTable.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return timeTable;
}