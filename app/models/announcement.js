const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcement = sequelize.define(
        'announcement', {
            announcementTypeId:{
                type: Sequelize.INTEGER,
            },
            announcementStaffId:{
                type: Sequelize.INTEGER,
            },
            announcementApproverId:{
                type: Sequelize.INTEGER,
            },
            announcement: {
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Aannouncement cannot be empty"
                    }
                }
            },
            announcemnetStatus: {
                type: Sequelize.STRING,
            },
            announcementApprovedDate:{
                type: Sequelize.DATE,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    announcement.associate = (models) => {
        announcement.belongsTo(models.staff, {foreignKey: "announcementStaffId"});
        announcement.belongsTo(models.staff, {foreignKey: "announcementApproverId"});
        announcement.belongsTo(models.announcementType, {foreignKey: "announcementTypeId"});
    }
    return announcement;
}