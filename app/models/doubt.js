const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubt = sequelize.define(
        'doubt', {
            instituteId:{
                type: Sequelize.INTEGER,
                allowNull: false
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            staffId:{
                type: Sequelize.INTEGER,
            },
            subjectId:{
                type: Sequelize.INTEGER,
            },
            topicDetailsId:{
                type: Sequelize.INTEGER,
            },
            typeOfQuestion:{
                type: Sequelize.ENUM('subjective', 'objective'),
            },
            title: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Title cannot be empty"
                    }
                }
            },
            question: {
                type: Sequelize.TEXT,
            },
            likeCount: {
                type: Sequelize.INTEGER,
            },
            resolved: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubt.associate = (models) => {
        doubt.belongsTo(models.institution, { foreignKey: "instituteId" });
        doubt.belongsTo(models.student, { foreignKey: "studentId" });
        doubt.belongsTo(models.staff, { foreignKey: "staffId" });
        doubt.belongsTo(models.subject, { foreignKey: "subjectId" });
        doubt.belongsTo(models.topicDetails, { foreignKey: "topicDetailsId" });
        doubt.belongsTo(models.subTopic, { foreignKey: "subTopicId" });
    }
    return doubt;
}