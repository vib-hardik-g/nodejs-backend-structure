const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcementType = sequelize.define(
        'announcementType', {
            announcementType: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Aannouncement Type cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    announcementType.associate = (models) => {
        announcementType.hasMany(models.announcement, {foreignKey: "announcementTypeId"});
    }
    return announcementType;
}