const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const notification = sequelize.define(
        'notification', {
            userId: {
                type: Sequelize.INTEGER,
            },
            notificationType:{
                type: Sequelize.ENUM('0','1'),
            },
            notification:{
                type: Sequelize.STRING,
            },
            deepLink:{
                type: Sequelize.STRING,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    notification.associate = (models) => {
        notification.belongsTo(models.user, { foreignKey: "userId" });
    }
    return notification;
}