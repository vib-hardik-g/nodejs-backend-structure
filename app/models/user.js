const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define(
        'user', {
        userTypeId: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "User Type Id cannot be empty"
                }
            }
        },
        email: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Email cannot be empty"
                }
            }
        },
        password: {
            type: Sequelize.STRING,
        },
        userName: {
            type: Sequelize.STRING
        },
        notificationStatus: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        lastLogin: {
            type: Sequelize.DATE
        },
        designation: {
            type: Sequelize.STRING,
        },otp: {
            type: Sequelize.INTEGER,
        }
    }
    );
    user.associate = (models) => {
        user.hasMany(models.userStudentMapping, { foreignKey: "userId" });
        user.hasMany(models.userStaffMapping, { foreignKey: "userId" });
        user.hasMany(models.userModuleMapping, { foreignKey: "userId" });
    }
    return user;
}