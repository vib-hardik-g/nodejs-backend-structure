const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const city = sequelize.define(
        'city', {
            cityName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "City Name cannot be empty"
                    }
                }
            },
            stateId: Sequelize.INTEGER,
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    city.associate = (models) => {
        city.belongsTo(models.state, {foreignKey: "stateId" });
    }
    return city;
}