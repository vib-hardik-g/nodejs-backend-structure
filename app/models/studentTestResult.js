const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const studentTestResult = sequelize.define('studentTestResult', {
        testResultTemplateId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        studentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        marks: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    studentTestResult.associate = (models) => {
        studentTestResult.belongsTo(models.testResultTemplate, { foreignKey: "testResultTemplateId" });
        studentTestResult.belongsTo(models.student, { foreignKey: "studentId" });
        studentTestResult.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return studentTestResult;
}