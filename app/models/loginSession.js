const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const loginSession = sequelize.define(
        'loginSession', {
            userId: {
                type: Sequelize.INTEGER,
            },
            token: {
                type: Sequelize.STRING,
            },
            isMobile: {
                type: Sequelize.BOOLEAN
            },
            isDesktop: {
                type: Sequelize.BOOLEAN
            },
            browser: {
                type: Sequelize.STRING,
            },
            version: {
                type: Sequelize.STRING,
            },
            os: {
                type: Sequelize.STRING,
            },
            platform: {
                type: Sequelize.STRING,
            },
            source: {
                type: Sequelize.STRING,
            },
            createdOn:{
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            destroyedOn:{
                type: Sequelize.DATE
            }
        }
    );
    
    loginSession.associate = (models) => {
        loginSession.belongsTo(models.user, {foreignKey: "userId"});
    }
    return loginSession;
}