const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userStudentMapping = sequelize.define('userStudentMapping', {
        userId: {
            type: Sequelize.INTEGER,
        },
        studentId: {
            type: Sequelize.INTEGER,
        },
        instituteId: {
            type: Sequelize.INTEGER,
        },
        classId: {
            type: Sequelize.INTEGER,
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        groupId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    userStudentMapping.associate = (models) => {
        userStudentMapping.belongsTo(models.user, { foreignKey: "userId" });
        userStudentMapping.belongsTo(models.student, { foreignKey: "studentId" });
        userStudentMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        userStudentMapping.belongsTo(models.class, { foreignKey: "classId" });
        userStudentMapping.belongsTo(models.batch, { foreignKey: "batchId" });
        userStudentMapping.belongsTo(models.group, { foreignKey: "groupId" });
        userStudentMapping.belongsTo(models.classSubjectMapping, { foreignKey: "classId", targetKey:"classId"});
    }
    return userStudentMapping;
}