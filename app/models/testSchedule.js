const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const testSchedule = sequelize.define('testSchedule', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        testScheduleType: {
            type: Sequelize.ENUM('Class', 'Batch', 'Group'),
            comment: "Class, Batch, Group",
            allowNull: false
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        documentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    testSchedule.associate = (models) => {
        testSchedule.hasMany(models.testScheduleClassBatchGroup, { foreignKey: "testScheduleId" });
        testSchedule.belongsTo(models.document, { foreignKey: "documentId" });
    }

    return testSchedule;
}