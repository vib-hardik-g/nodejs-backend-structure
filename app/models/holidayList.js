const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const holidayList = sequelize.define(
        'holidayList', {
        holidayType: {
            type: Sequelize.ENUM('Weekend', 'Holiday'),
            allowNull: false
        },
        holidayName: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        year: {
            type: Sequelize.STRING(4),
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Year cannot be empty"
                }
            }
        },
        month: {
            type: Sequelize.ENUM('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
            allowNull: false
        },
        date: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    holidayList.associate = (models) => {
        holidayList.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return holidayList;
}