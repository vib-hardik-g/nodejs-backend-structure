const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const batchAssessmentMapping = sequelize.define(
        'batchAssessmentMapping', {
            assessmentId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            batchId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    batchAssessmentMapping.associate = (models) => { 
        batchAssessmentMapping.belongsTo(models.assessment, { foreignKey: 'assessmentId' });
        batchAssessmentMapping.belongsTo(models.batch, { foreignKey: 'batchId' });
    }
    return batchAssessmentMapping;
}