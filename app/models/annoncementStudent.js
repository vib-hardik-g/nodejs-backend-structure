const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const announcementStudent = sequelize.define('announcementStudent', {
        announcementType: {
            type: Sequelize.ENUM('Class', 'Batch', 'Group'),
            defaultValue: 'Class',
            allowNull: false
        },
        title: {
            type: Sequelize.STRING(255),
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Title cannot be empty"
                }
            }
        },
        description:{
            type:Sequelize.STRING(300),
            allowNull: false
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        announcementDocument: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    announcementStudent.associate = (models) => {
        announcementStudent.hasMany(models.announcementStudentType, { foreignKey: "announcementStudentId" });
        announcementStudent.belongsTo(models.document, { foreignKey: "announcementDocument" });
        announcementStudent.belongsTo(models.institution, { foreignKey: "instituteId" });
    }

    return announcementStudent;
};