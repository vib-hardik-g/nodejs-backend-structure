const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackTemplate = sequelize.define('feedbackTemplate', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        templateName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        noOfCategories: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackTemplate.associate = (models) => {
        feedbackTemplate.belongsTo(models.institution, { foreignKey: "instituteId" });
        feedbackTemplate.hasMany(models.feedbackTemplateCategory, { foreignKey: "feedbackTemplateId" });
    }
    return feedbackTemplate;
}