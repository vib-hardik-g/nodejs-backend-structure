const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const questionBankQuestionOption = sequelize.define(
        'questionBankQuestionOption', {
            questionBankQuestionId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            option:{
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Option cannot be empty"
                    }
                }
            },
            isCorrect: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    questionBankQuestionOption.associate = (models) => {
        questionBankQuestionOption.belongsTo(models.questionBankQuestion, { foreignKey: "questionBankQuestionId" });
    }
    return questionBankQuestionOption;
}