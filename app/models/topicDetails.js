const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const topicDetails = sequelize.define('topicDetails', {
        chapter: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Chapter cannot be empty"
                }
            }
        },
        topicId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    topicDetails.associate = (models) => {
        topicDetails.belongsTo(models.topic, { foreignKey: "topicId" });
        topicDetails.hasMany(models.subTopic, { foreignKey: "topicDetailsId" });
    }
    
    return topicDetails;
}