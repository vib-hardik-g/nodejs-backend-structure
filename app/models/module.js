const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const module = sequelize.define(
        'module', {
            moduleName: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Module Name already exists',
                    fields: [sequelize.col('moduleName')]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Module Name cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    module.associate = (models) => { 
        module.hasMany(models.subModule, { foreignKey: "moduleId", as: "submodules" });
    }
    return module;
}