const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assignment = sequelize.define(
        'assignment', {
            classId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            subjectId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            staffId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            assignmentName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Assignment Name cannot be empty"
                    }
                }
            },
            instituteId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            assignmentDueDate: {
                type: Sequelize.DATEONLY,
                allowNull: true,
            },
            assignmentDueTime: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            publishStatus: {
                type: Sequelize.ENUM('Pending', 'Published', 'Republished'),
                defaultValue: 'Pending'
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assignment.associate = (models) => {
        assignment.belongsTo(models.staff, {foreignKey: "staffId"});
        assignment.hasMany(models.batchAssignmentMapping, { foreignKey: "assignmentId" });
        assignment.belongsTo(models.class, { foreignKey: "classId" });
        assignment.belongsTo(models.subject, { foreignKey: "subjectId" });
        assignment.hasMany(models.assignmentAttachment, { foreignKey: "assignmentId" });
        assignment.hasMany(models.assignmentSubmission, { foreignKey: "assignmentId" });
    }
    return assignment;
}