const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const testResultReportCard = sequelize.define('testResultReportCard', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        term: {
            type: Sequelize.STRING,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        documentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        zipFileName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    testResultReportCard.associate = (models) => {
        testResultReportCard.hasMany(models.testResultReportCardStudent, { foreignKey: "testResultReportCardId" });
        testResultReportCard.belongsTo(models.document, { foreignKey: "documentId" });
        testResultReportCard.belongsTo(models.batch, { foreignKey: "batchId" });
    }

    return testResultReportCard;
}