const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentCapacity = sequelize.define(
        'studentCapacity', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        studentLimit: {
            type: Sequelize.INTEGER,
            allowNull: false,
            validate: {
                isNumeric: {
                    args: true,
                    msg: "Student capacity accept only numeric value"
                }
            }
        },
        requestStatus: {
            type: Sequelize.ENUM('Pending', 'Approved', 'Rejected'),
            defaultValue: 'Pending'
        }
    }
    );
    studentCapacity.associate = (models) => {
        studentCapacity.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return studentCapacity;
}