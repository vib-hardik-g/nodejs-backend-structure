const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffClassMapping = sequelize.define('staffClassMapping', {
        staffId: {
            type: Sequelize.INTEGER,
        },
        classId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    staffClassMapping.associate = (models) => {
        staffClassMapping.belongsTo(models.staff, { foreignKey: "staffId" });
        staffClassMapping.belongsTo(models.class, { foreignKey: "classId" });

        staffClassMapping.hasMany(models.staffClassBatchMapping, { foreignKey: "staffClassMappingId" });
    }
    return staffClassMapping;
}