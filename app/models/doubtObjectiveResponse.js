const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubtObjectiveResponse = sequelize.define(
        'doubtObjectiveResponse', {
            doubtId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            staffId:{
                type: Sequelize.INTEGER,
            },
            optionId:{
                type: Sequelize.INTEGER,
            },
            correct:{
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubtObjectiveResponse.associate = (models) => {
        doubtObjectiveResponse.belongsTo(models.student, { foreignKey: "studentId" });
        doubtObjectiveResponse.belongsTo(models.staff, { foreignKey: "staffId" });
        doubtObjectiveResponse.belongsTo(models.doubt, { foreignKey: "doubtId" });
        doubtObjectiveResponse.belongsTo(models.doubtObjectiveOption, { foreignKey: "optionId" });
    }
    return doubtObjectiveResponse;
}