const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackAnswer = sequelize.define(
        'feedbackAnswer', {
            feedbackRequestId:{
                type: Sequelize.INTEGER,
            },
            feedbackQuestionId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            feedbackQuestionAnswer:{
                type: Sequelize.TEXT,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    feedbackAnswer.associate = (models) => {
        feedbackAnswer.belongsTo(models.feedbackQuestion, { foreignKey: "feedbackQuestionId" });
        feedbackAnswer.belongsTo(models.feedbackRequest, { foreignKey: "feedbackRequestId" });
        feedbackAnswer.belongsTo(models.student, { foreignKey: "studentId" });
    }
    return feedbackAnswer;
}