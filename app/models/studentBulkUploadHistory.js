const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentBulkUploadHistory = sequelize.define('studentBulkUploadHistory', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        documentId: {
            type: Sequelize.INTEGER,
        },
        classId: {
            type: Sequelize.INTEGER,
        },
        noOfStudent: {
            type: Sequelize.INTEGER,
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
    });

    studentBulkUploadHistory.associate = (models) => {
        studentBulkUploadHistory.belongsTo(models.institution, { foreignKey: "instituteId" });
        studentBulkUploadHistory.belongsTo(models.document, { foreignKey: "documentId" });
        studentBulkUploadHistory.belongsTo(models.batch, { foreignKey: "batchId" });
        studentBulkUploadHistory.belongsTo(models.class, { foreignKey: "classId" });
    };

    return studentBulkUploadHistory;
}