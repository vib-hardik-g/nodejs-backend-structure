const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subjectAssessmentMapping = sequelize.define(
        'subjectAssessmentMapping', {
            assessmentId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            subjectId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            noOfQuestions: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    subjectAssessmentMapping.associate = (models) => { 
        subjectAssessmentMapping.belongsTo(models.assessment, { foreignKey: 'assessmentId' });
        subjectAssessmentMapping.belongsTo(models.subject, { foreignKey: 'subjectId' });
        subjectAssessmentMapping.hasMany(models.subjectChapterAssessmentMapping, { foreignKey: 'subjectAssessmentMappingId' });
    }
    return subjectAssessmentMapping;
}