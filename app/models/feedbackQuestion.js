const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackQuestion = sequelize.define(
        'feedbackQuestion', {
            feedbackTypeId:{
                type: Sequelize.INTEGER,
            },
            feedbackQuestion: {
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Feedback Question cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    feedbackQuestion.associate = (models) => {
        feedbackQuestion.belongsTo(models.feedbackQuestionType, { foreignKey: "feedbackTypeId" });
    }
    return feedbackQuestion;
}