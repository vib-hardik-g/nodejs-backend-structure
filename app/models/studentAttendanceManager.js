const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentAttendanceManager = sequelize.define(
        'studentAttendanceManager', {
            instituteId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            classId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            batchId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            date: {
                type: Sequelize.DATEONLY,
                allowNull: false,
            },
            day: {
                type: Sequelize.ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Day cannot be empty"
                    }
                }
            },
            publishStatus: {
                type: Sequelize.ENUM('Pending', 'Published'),
                defaultValue: 'Pending'
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    studentAttendanceManager.associate = (models) => {
        studentAttendanceManager.belongsTo(models.institution, {foreignKey: "instituteId" });
        studentAttendanceManager.belongsTo(models.class, {foreignKey: "classId" });
        studentAttendanceManager.belongsTo(models.batch, {foreignKey: "batchId" });
        studentAttendanceManager.hasMany(models.studentAbsentee, {foreignKey: "studentAttendanceManagerId" });
    }
    return studentAttendanceManager;
}