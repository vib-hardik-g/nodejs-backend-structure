const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assignmentAttachment = sequelize.define(
        'assignmentAttachment', {
            assignmentId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            assignmentFile: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assignmentAttachment.associate = (models) => {
        assignmentAttachment.belongsTo(models.assignment, { foreignKey: "assignmentId" });
        assignmentAttachment.belongsTo(models.document, { foreignKey: "assignmentFile" });
    }
    return assignmentAttachment;
}