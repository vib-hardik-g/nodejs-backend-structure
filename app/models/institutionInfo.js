const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institutionInfo = sequelize.define(
        'institutionInfo', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            boardType: {
                type: Sequelize.TEXT,
                defaultValue: '',
            },
            content: {
                type: Sequelize.TEXT,
                defaultValue: '',
            },
            address: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            googleMap: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            googleImage: {
                type: Sequelize.STRING,
                defaultValue: 'https://www.ydesignservices.com/wp-content/uploads/2016/07/Googlemap-600x551.jpg',
            },
            website: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            contactPersonName: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            contactPersonEmail: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            contactPersonPhone: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            cityId: {
                type: Sequelize.INTEGER,
            },
            stateId: {
                type: Sequelize.INTEGER,
            },
            countryId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    institutionInfo.associate = (models) => {
        institutionInfo.belongsTo(models.institution, { foreignKey: "instituteId", as: "info" });
        institutionInfo.belongsTo(models.city, { foreignKey: "cityId", as: "city" });
        institutionInfo.belongsTo(models.country, { foreignKey: "countryId", as: "country" });
        institutionInfo.belongsTo(models.state, { foreignKey: "stateId", as: "state" });
    }
    return institutionInfo;
}