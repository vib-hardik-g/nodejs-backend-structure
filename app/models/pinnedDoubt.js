const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const pinnedDoubt = sequelize.define(
        'pinnedDoubt', {
            doubtId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    pinnedDoubt.associate = (models) => {
        pinnedDoubt.belongsTo(models.doubt, { foreignKey: "doubtId" });
        pinnedDoubt.belongsTo(models.student, { foreignKey: "studentId" });
    }
    return pinnedDoubt;
}