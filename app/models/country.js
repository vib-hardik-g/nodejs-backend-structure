const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const country = sequelize.define(
        'country', {
            countryName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Country Name cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    country.associate = (models) => {
        country.hasMany(models.state, {foreignKey: "countryId" });
    }
    return country;
}