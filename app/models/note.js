const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const note = sequelize.define(
        'note', {
            noteName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Note Name cannot be empty"
                    }
                }
            },
            classId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            subjectId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            instituteId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            publishStatus: {
                type: Sequelize.ENUM('Pending', 'Published', 'Republished'),
                defaultValue: 'Pending',
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    note.associate = (models) => {
        note.belongsTo(models.class, { foreignKey: "classId" });
        note.hasMany(models.batchNoteMapping, { foreignKey: "noteId" });
        note.hasMany(models.noteAttachment, { foreignKey: "noteId" });
        note.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return note;
}