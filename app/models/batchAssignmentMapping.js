const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const batchAssignmentMapping = sequelize.define(
        'batchAssignmentMapping', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        assignmentId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    batchAssignmentMapping.associate = (models) => {
        batchAssignmentMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        batchAssignmentMapping.belongsTo(models.assignment, { foreignKey: "assignmentId" });
        batchAssignmentMapping.belongsTo(models.batch, { foreignKey: "batchId" });
    }
    return batchAssignmentMapping;
}