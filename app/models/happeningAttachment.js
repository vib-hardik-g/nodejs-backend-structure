const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const happeningAttachment = sequelize.define(
        'happeningAttachment', {
            happeningId:{
                type: Sequelize.INTEGER,
            },
            happeningImage: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    happeningAttachment.associate = (models) => {
        happeningAttachment.belongsTo(models.happening, { foreignKey: "happeningId" });
        happeningAttachment.belongsTo(models.document, { foreignKey: "happeningImage" });
    }
    return happeningAttachment;
}