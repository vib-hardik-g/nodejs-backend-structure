const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assignmentSubmission = sequelize.define(
        'assignmentSubmission', {
            assignmentId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            studentId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            assignmentFile: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            assignmentResubmit: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assignmentSubmission.associate = (models) => {
        assignmentSubmission.belongsTo(models.student, { foreignKey: "studentId" });
        assignmentSubmission.belongsTo(models.assignment, { foreignKey: "assignmentId" });
        assignmentSubmission.belongsTo(models.document, { foreignKey: "assignmentFile" });
    }
    return assignmentSubmission;
}