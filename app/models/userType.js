const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userType = sequelize.define(
        'userType', {
        userType: {
            type: Sequelize.STRING,
            unique: {
                args: true,
                msg: 'User Type aleady exists',
                fields: [sequelize.fn('lower', sequelize.col('userType'))]
            },
            validate: {
                notEmpty: {
                    args: true,
                    msg: "User Type cannot be empty"
                }
            }
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
    }
    );
    return userType;
}