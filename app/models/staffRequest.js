const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffRequest = sequelize.define(
        'staffRequest', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            staffId: {
                type: Sequelize.INTEGER,
            },
            request: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            approvedStatus: {
                type: Sequelize.STRING(50),
                defaultValue: "Pending"
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    staffRequest.associate = (models) => {
        staffRequest.belongsTo(models.institution, {foreignKey: "instituteId" });
        staffRequest.belongsTo(models.staff, {foreignKey: "staffId" });
        staffRequest.hasMany(models.staffRequestAttachment, {foreignKey: "staffRequestId" });
    }
    return staffRequest;
}