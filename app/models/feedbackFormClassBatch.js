const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackFormClassBatch = sequelize.define('feedbackFormClassBatch', {
        feedbackFormId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackFormClassBatch.associate = (models) => {
        feedbackFormClassBatch.belongsTo(models.feedbackForm, { foreignKey: "feedbackFormId" })
        feedbackFormClassBatch.belongsTo(models.class, { foreignKey: "classId" })
        feedbackFormClassBatch.belongsTo(models.batch, { foreignKey: "batchId" })
    }
    return feedbackFormClassBatch;
}