const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const testScheduleClassBatchGroup = sequelize.define('testScheduleClassBatchGroup', {
        testScheduleId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        groupId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    testScheduleClassBatchGroup.associate = (models) => {
        testScheduleClassBatchGroup.belongsTo(models.testSchedule, { foreignKey: "testScheduleId" })
        testScheduleClassBatchGroup.belongsTo(models.class, { foreignKey: "classId" })
        testScheduleClassBatchGroup.belongsTo(models.batch, { foreignKey: "batchId" })
        testScheduleClassBatchGroup.belongsTo(models.group, { foreignKey: "groupId" })
    }
    return testScheduleClassBatchGroup;
}