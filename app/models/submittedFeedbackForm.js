const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const submittedFeedbackForm = sequelize.define('submittedFeedbackForm', {
        feedbackFormId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        staffId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        studentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedAt: {
            type: Sequelize.DATE,
            allowNull: true
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    submittedFeedbackForm.associate = (models) => {
        submittedFeedbackForm.belongsTo(models.feedbackForm, { foreignKey: "feedbackFormId" });
        submittedFeedbackForm.belongsTo(models.staff, { foreignKey: "staffId" });
        submittedFeedbackForm.belongsTo(models.student, { foreignKey: "studentId" });
        submittedFeedbackForm.belongsTo(models.subject, { foreignKey: "subjectId" });
        submittedFeedbackForm.belongsTo(models.batch, { foreignKey: "batchId" });
        submittedFeedbackForm.hasMany(models.submittedFeedbackResponse, { foreignKey: "submittedFeedbackResponseId" });
    }
    return submittedFeedbackForm;
}