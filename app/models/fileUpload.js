const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const fileUpload = sequelize.define(
        'fileUpload', {
            fileName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "File Name cannot be empty"
                    }
                }
            },
            fileSize: {
                type: Sequelize.STRING(100),
            },
            uploadStatus: {
                type: Sequelize.STRING,
            },
            fileUploadDate: {
                type: Sequelize.DATE,
            },
            staffId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    
    fileUpload.associate = (models) => {
        fileUpload.belongsTo(models.staff, {foreignKey: "staffId"});
    }
    return fileUpload;
}