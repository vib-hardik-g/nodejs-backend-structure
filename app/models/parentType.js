const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const parentType = sequelize.define(
        'parentType', {
            parentType: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Parent Type aleady exists',
                    fields: [sequelize.fn('lower', sequelize.col('parentType'))]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Parent Type cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    return parentType;
}