const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const examQuestion = sequelize.define(
        'examQuestion', {
            classId:{
                type: Sequelize.INTEGER,
            },
            subjectId:{
                type: Sequelize.INTEGER,
            },
            batchId:{
                type: Sequelize.INTEGER,
            },
            question:{
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Question cannot be empty"
                    }
                }
            },
            marks: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Marks cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    examQuestion.associate = (models) => {
        examQuestion.belongsTo(models.batch, { foreignKey: "batchId" });
        examQuestion.belongsTo(models.class, { foreignKey: "classId" });
        examQuestion.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return examQuestion;
}