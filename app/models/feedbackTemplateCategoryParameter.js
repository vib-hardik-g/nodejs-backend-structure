const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackTemplateCategoryParameter = sequelize.define('feedbackTemplateCategoryParameter', {
        feedbackTemplateCategoryId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        parameter: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackTemplateCategoryParameter.associate = (models) => {
        feedbackTemplateCategoryParameter.belongsTo(models.feedbackTemplateCategory, { foreignKey: "feedbackTemplateCategoryId" });
    }
    return feedbackTemplateCategoryParameter;
}