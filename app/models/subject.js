const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subject = sequelize.define('subject', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        subjectName: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Subject Name cannot be empty"
                }
            }
        },
        icon: {
            type: Sequelize.STRING,
            defaultValue: 'https://image.flaticon.com/icons/png/512/130/130304.png'
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    subject.associate = (models) => {
        subject.hasMany(models.classSubjectMapping, { foreignKey: "subjectId" });
        subject.belongsTo(models.institution, { foreignKey: "instituteId" });
        subject.hasMany(models.topic, { foreignKey: "subjectId" });
    }
    return subject;
}