const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const groupAssessmentMapping = sequelize.define(
        'groupAssessmentMapping', {
            assessmentId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            groupId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    groupAssessmentMapping.associate = (models) => { 
        groupAssessmentMapping.belongsTo(models.assessment, { foreignKey: 'assessmentId' });
        groupAssessmentMapping.belongsTo(models.group, { foreignKey: 'groupId' });
    }
    return groupAssessmentMapping;
}