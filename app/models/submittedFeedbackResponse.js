const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const submittedFeedbackResponse = sequelize.define('submittedFeedbackResponse', {
        submittedFeedbackFormId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        categoryId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        benchmarkId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        parameterId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    submittedFeedbackResponse.associate = (models) => {
        submittedFeedbackResponse.belongsTo(models.submittedFeedbackForm, { foreignKey: "submittedFeedbackFormId" });
        submittedFeedbackResponse.belongsTo(models.feedbackTemplateCategory, { foreignKey: "categoryId" });
        submittedFeedbackResponse.belongsTo(models.feedbackTemplateBenchmark, { foreignKey: "benchmarkId" });
        submittedFeedbackResponse.belongsTo(models.feedbackTemplateCategoryParameter, { foreignKey: "parameterId" });
    }
    return submittedFeedbackResponse;
}