const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const happeningLike = sequelize.define(
        'happeningLike', {
        happeningId: {
            type: Sequelize.INTEGER,
        },
        studentId: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        staffId: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    happeningLike.associate = (models) => {
        happeningLike.belongsTo(models.student, { foreignKey: "studentId" });
        happeningLike.belongsTo(models.staff, { foreignKey: "staffId", as: "staff" });
        happeningLike.belongsTo(models.happening, { foreignKey: "happeningId" });
    }
    return happeningLike;
}