const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assessmentQuestion = sequelize.define(
        'assessmentQuestion', {
            assessmentId:{
                type: Sequelize.INTEGER,
                allowNull: false
            },
            question:{
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Question cannot be empty"
                    }
                }
            },
            subjectId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            topicDetailsId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assessmentQuestion.associate = (models) => { 
        assessmentQuestion.belongsTo(models.assessment, { foreignKey: 'assessmentId' });
        assessmentQuestion.hasMany(models.assessmentQuestionOption, { foreignKey: 'assessmentQuestionId' });
        assessmentQuestion.belongsTo(models.subject, { foreignKey: 'subjectId' });
        assessmentQuestion.belongsTo(models.topicDetails, { foreignKey: 'topicDetailsId' });
        assessmentQuestion.hasMany(models.assessmentQuestionAnswer, { foreignKey: 'assessmentQuestionId' });
    }
    return assessmentQuestion;
}