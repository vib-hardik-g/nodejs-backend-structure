const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const classSubjectMapping = sequelize.define(
        'classSubjectMapping', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            classId: {
                type: Sequelize.INTEGER
            },
            subjectId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    classSubjectMapping.associate = (models) => {
        classSubjectMapping.belongsTo(models.class, { foreignKey: "classId" });
        classSubjectMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        classSubjectMapping.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return classSubjectMapping;
}