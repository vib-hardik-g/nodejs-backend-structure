const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const testResultReportCardStudent = sequelize.define('testResultReportCardStudent', {
        testResultReportCardId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        studentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        documentId: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        isPublished: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        publishedAt: {
            type: Sequelize.DATE,
            defaultValue: null
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    testResultReportCardStudent.associate = (models) => {
        testResultReportCardStudent.belongsTo(models.testResultReportCard, { foreignKey: "testResultReportCardId" });
        testResultReportCardStudent.belongsTo(models.document, { foreignKey: "documentId" });
        testResultReportCardStudent.belongsTo(models.student, { foreignKey: "studentId" });
    }

    return testResultReportCardStudent;
}