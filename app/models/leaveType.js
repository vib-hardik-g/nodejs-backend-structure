const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const leaveType = sequelize.define(
        'leaveType', {
            leaveType: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Leave Type aleady exists',
                    fields: [sequelize.fn('lower', sequelize.col('leaveType'))]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Leave Type cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    return leaveType;
}