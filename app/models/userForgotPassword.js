const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userForgotPassword = sequelize.define(
        'userForgotPassword', {
            userId: {
                type: Sequelize.INTEGER,
            },
            otp: {
                type: Sequelize.INTEGER,
            },
            type: {
                type: Sequelize.ENUM('0','1'),
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                type: Sequelize.DATE,
            }
        }
    );

    userForgotPassword.associate = (models) => {
        userForgotPassword.belongsTo(models.user, { foreignKey: "userId" });
    }
    return userForgotPassword;
}