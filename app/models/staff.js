const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staff = sequelize.define(
        'staff', {
        salutation: {
            type: Sequelize.STRING
        },
        firstName: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "First Name cannot be empty"
                }
            }
        },
        lastName: {
            type: Sequelize.STRING,
        },
        employeeCode: {
            type: Sequelize.STRING,
        },
        designation: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        dateOfBirth: {
            type: Sequelize.DATEONLY,
        },
        gender: {
            type: Sequelize.STRING(15),
            defaultValue: ''
        },
        bloodGroup: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        contactNo: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        address: {
            type: Sequelize.TEXT,
            defaultValue: ''
        },
        pincode: {
            type: Sequelize.STRING(6),
            defaultValue: ''
        },
        cityId: {
            type: Sequelize.INTEGER,
        },
        stateId: {
            type: Sequelize.INTEGER,
        },
        profilePicture: {
            type: Sequelize.INTEGER
        },
        dateOfJoining: {
            type: Sequelize.DATEONLY,
        },
        experience: {
            type: Sequelize.TEXT,
            defaultValue: ''
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        loginStatus: {
            type: Sequelize.ENUM('Not Yet Invited', 'Signed Up', 'Not Signed Up'),
            defaultValue: 'Not Yet Invited'
        }
    }
    );

    staff.associate = (models) => {
        staff.hasMany(models.userStaffMapping, { foreignKey: "staffId" });
        staff.hasMany(models.timeTableDetail, { foreignKey: 'staffId' });
        staff.hasMany(models.staffClassMapping, { foreignKey: 'staffId' });
        staff.hasMany(models.submittedFeedbackResponse, { foreignKey: 'staffId' });
        staff.belongsTo(models.city, { foreignKey: "cityId" });
        staff.belongsTo(models.state, { foreignKey: "stateId" });
        staff.belongsTo(models.country, { foreignKey: "countryId" });
        staff.belongsTo(models.document, { foreignKey: "profilePicture" });
    }
    return staff;
}