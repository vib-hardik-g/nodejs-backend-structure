const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const grade = sequelize.define(
        'grade', {
            gradeName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Grade Name cannot be empty"
                    }
                }
            },
            gradeRangeStart:{
                type: Sequelize.INTEGER,
            },
            gradeRangeEnd:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    return grade;
}