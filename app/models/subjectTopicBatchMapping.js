const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subjectTopicBatchMapping = sequelize.define(
        'subjectTopicBatchMapping', {
            instituteId: {
                type: Sequelize.INTEGER
            },
            batchId: {
                type: Sequelize.INTEGER
            },
            groupId: {
                type: Sequelize.INTEGER
            },
            classId: {
                type: Sequelize.INTEGER
            },
            sectionId: {
                type: Sequelize.INTEGER
            },
            subjectId: {
                type: Sequelize.INTEGER
            },
            topicId: {
                type: Sequelize.INTEGER
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    subjectTopicBatchMapping.associate = (models) => {
        subjectTopicBatchMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        subjectTopicBatchMapping.belongsTo(models.batch, { foreignKey: "batchId" });
        subjectTopicBatchMapping.belongsTo(models.group, { foreignKey: "groupId" });
        subjectTopicBatchMapping.belongsTo(models.class, { foreignKey: "classId" });
        subjectTopicBatchMapping.belongsTo(models.section, { foreignKey: "sectionId" });
        subjectTopicBatchMapping.belongsTo(models.subject, { foreignKey: "subjectId" });
        subjectTopicBatchMapping.belongsTo(models.topic, { foreignKey: "topicId" });
    }
    return subjectTopicBatchMapping;
}