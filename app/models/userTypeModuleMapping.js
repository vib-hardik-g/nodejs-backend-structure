const Sequelize = require('sequelize');
module.exports = (sequilize, DataTypes) => {
    const userTypeModuleMapping = sequilize.define(
        'userTypeModuleMapping', {
            userTypeId: {
                type: Sequelize.INTEGER,
            },
            moduleId: {
                type: Sequelize.INTEGER,
            },
            subModuleId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
            },
    });

    userTypeModuleMapping.associate = (models) => {
        userTypeModuleMapping.belongsTo(models.userType, { foreignKey: "userTypeId" });
        userTypeModuleMapping.belongsTo(models.module, { foreignKey: "moduleId" });
        userTypeModuleMapping.belongsTo(models.subModule, { foreignKey: "subModuleId" });
    };
    return userTypeModuleMapping;
}