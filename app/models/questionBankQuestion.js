const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const questionBankQuestion = sequelize.define(
        'questionBankQuestion', {
            questionBankId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            question:{
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Question cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    questionBankQuestion.associate = (models) => {
        questionBankQuestion.belongsTo(models.questionBank, { foreignKey: "questionBankId" });
        questionBankQuestion.hasMany(models.questionBankQuestionOption, { foreignKey: "questionBankQuestionId" });
    }
    return questionBankQuestion;
}