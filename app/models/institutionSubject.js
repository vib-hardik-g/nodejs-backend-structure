const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institutionSubject = sequelize.define(
        'institutionSubject', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            subjectId: {
                type: Sequelize.INTEGER,
            },
            acadYear: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    institutionSubject.associate = (models) => {
        institutionSubject.belongsTo(models.institution, { foreignKey: "instituteId", as: "institition" });
        institutionSubject.belongsTo(models.subject, { foreignKey: "subjectId", as: "subject" });
    }
    return institutionSubject;
}