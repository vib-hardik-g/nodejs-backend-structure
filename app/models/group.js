const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const group = sequelize.define(
        'group', {
        groupName: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Group Name cannot be empty"
                }
            }
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    group.associate = (models) => {
        group.hasMany(models.groupBatch, { foreignKey: "groupId" });
        group.hasMany(models.groupSubject, { foreignKey: "groupId" });
        group.belongsTo(models.class, { foreignKey: "classId" });
        group.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return group;
}