const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assessmentQuestionOption = sequelize.define(
        'assessmentQuestionOption', {
            assessmentQuestionId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            option:{
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Option cannot be empty"
                    }
                }
            },
            isCorrect: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assessmentQuestionOption.associate = (models) => {
        assessmentQuestionOption.belongsTo(models.assessmentQuestion, { foreignKey: "assessmentQuestionId" });
    }
    return assessmentQuestionOption;
}