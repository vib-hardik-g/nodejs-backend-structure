const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const groupBatch = sequelize.define(
        'groupBatch', {
       
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        groupId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    groupBatch.associate = (models) => {
        groupBatch.belongsTo(models.batch, { foreignKey: "batchId" });
        groupBatch.belongsTo(models.group, { foreignKey: "groupId" });
    }
    return groupBatch;
}