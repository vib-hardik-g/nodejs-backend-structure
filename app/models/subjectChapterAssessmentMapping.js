const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const subjectChapterAssessmentMapping = sequelize.define(
        'subjectChapterAssessmentMapping', {
            subjectAssessmentMappingId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            topicDetailsId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            noOfQuestions: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    subjectChapterAssessmentMapping.associate = (models) => { 
        subjectChapterAssessmentMapping.belongsTo(models.subjectAssessmentMapping, { foreignKey: 'subjectAssessmentMappingId' });
        subjectChapterAssessmentMapping.belongsTo(models.topicDetails, { foreignKey: 'topicDetailsId' });
    }
    return subjectChapterAssessmentMapping;
}