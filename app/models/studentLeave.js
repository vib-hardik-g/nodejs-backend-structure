const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentLeave = sequelize.define(
        'studentLeave', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            studentId: {
                type: Sequelize.INTEGER,
            },
            leaveTypeId: {
                type: Sequelize.INTEGER,
            },
            reason: {
                type: Sequelize.STRING,
                defaultValue: ''
            },
            startDate: {
                type: Sequelize.DATEONLY,
            },
            endDate: {
                type: Sequelize.DATEONLY,
            },
            leaveStatus: {
                type: Sequelize.ENUM('Pending', 'Approved', 'Rejected'),
                defaultValue: 'Pending'
            },
            approvedBy: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    studentLeave.associate = (models) => {
        studentLeave.belongsTo(models.institution, {foreignKey: "instituteId" });
        studentLeave.belongsTo(models.staff, {foreignKey: "approvedBy" });
        studentLeave.belongsTo(models.leaveType, {foreignKey: "leaveTypeId" });
        studentLeave.belongsTo(models.student, {foreignKey: "studentId" });
        studentLeave.hasMany(models.studentLeaveAttachment, { foreignKey: "leaveId", as: "attachments" });
    }
    return studentLeave;
}