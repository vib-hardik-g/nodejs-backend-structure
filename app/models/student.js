const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const student = sequelize.define('student', {
        firstName: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "First Name cannot be empty"
                }
            }
        },
        lastName: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        dateOfBirth: {
            type: Sequelize.DATEONLY,
        },
        gender: {
            type: Sequelize.STRING(15),
            defaultValue: ''
        },
        bloodGroup: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        religion: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        primaryPhoneNumber: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        address: {
            type: Sequelize.TEXT,
            defaultValue: ''
        },
        pincode: {
            type: Sequelize.STRING(6),
            defaultValue: ''
        },
        cityId: {
            type: Sequelize.INTEGER,
        },
        stateId: {
            type: Sequelize.INTEGER,
        },
        profilePicture: {
            type: Sequelize.INTEGER
        },
        fatherName: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        fatherEmailId: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        fatherPhoneNumber: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        motherName: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        motherPhoneNumber: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        motherEmailId: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        legalGuardianName: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        legalGuardianPhoneNumber: {
            type: Sequelize.STRING(10),
            defaultValue: ''
        },
        legalGuardianEmailId: {
            type: Sequelize.STRING,
            defaultValue: ''
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        },
        loginStatus: {
            type: Sequelize.ENUM('Not Yet Invited', 'Signed Up', 'Not Signed Up'),
            defaultValue: 'Not Yet Invited'
        }
    });

    student.associate = (models) => {
        student.hasMany(models.parent, { foreignKey: "studentId" });
        student.hasMany(models.userStudentMapping, { foreignKey: "studentId" });
        student.belongsTo(models.city, { foreignKey: "cityId" });
        student.belongsTo(models.state, { foreignKey: "stateId" });
        student.belongsTo(models.document, { foreignKey: "profilePicture" });
        student.hasMany(models.studentAbsentee, { foreignKey: "studentId" });
        student.hasMany(models.studentLeave, { foreignKey: "studentId" });
        student.hasOne(models.studentBatchUniqueId, { foreignKey: "studentId" });
        student.hasMany(models.assignmentSubmission, { foreignKey: "studentId" });
        student.hasMany(models.assessmentQuestionAnswer, { foreignKey: "studentId" });
    }
    return student;
}