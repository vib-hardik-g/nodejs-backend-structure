const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffLeave = sequelize.define(
        'staffLeave', {
            staffId: {
                type: Sequelize.INTEGER,
            },
            leaveTypeId: {
                type: Sequelize.INTEGER,
            },
            startDate: {
                type: Sequelize.DATEONLY,
            },
            endDate: {
                type: Sequelize.DATEONLY,
            },
            leaveStatus: {
                type: Sequelize.STRING(50),
            },
            approvedBy: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    staffLeave.associate = (models) => {
        staffLeave.belongsTo(models.staff, {foreignKey: "staffId" });
        staffLeave.belongsTo(models.leaveType, {foreignKey: "leaveTypeId" });
        staffLeave.belongsTo(models.user, {foreignKey: "approvedBy" });
    }
    return staffLeave;
}