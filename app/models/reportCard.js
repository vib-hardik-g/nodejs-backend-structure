const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const reportCard = sequelize.define(
        'reportCard', {
            batchId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            subjectId:{
                type: Sequelize.INTEGER,
            },
            month:{
                type: Sequelize.FLOAT,
                defaultValue: null

            },
            reportCardName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Report Card Name cannot be empty"
                    }
                }
            },
            reportCardPath: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Report Card Path cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    reportCard.associate = (models) => {
        reportCard.belongsTo(models.batch, { foreignKey: "batchId" });
        reportCard.belongsTo(models.student, { foreignKey: "studentId" });
        reportCard.belongsTo(models.subjectTopicBatchMapping, { foreignKey: "subjectId" });
    }
    return reportCard;
}