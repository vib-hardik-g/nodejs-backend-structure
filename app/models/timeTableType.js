const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const timeTableType = sequelize.define(
        'timeTableType', {
            timeTableType: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Time Table Type already exists',
                    fields: [sequelize.col('timeTableType')]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Time Table Type cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    return timeTableType;
}