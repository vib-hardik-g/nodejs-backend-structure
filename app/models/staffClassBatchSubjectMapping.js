const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffClassBatchSubjectMapping = sequelize.define('staffClassBatchSubjectMapping', {
        staffClassBatchMappingId: {
            type: Sequelize.INTEGER,
        },
        subjectId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    staffClassBatchSubjectMapping.associate = (models) => {
        staffClassBatchSubjectMapping.belongsTo(models.staffClassBatchMapping, { foreignKey: "staffClassBatchMappingId" });
        staffClassBatchSubjectMapping.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return staffClassBatchSubjectMapping;
}