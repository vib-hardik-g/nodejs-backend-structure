const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffProgressReport = sequelize.define(
        'staffProgressReport', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        staffId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        classId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        batchId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        topicDetailsId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subTopicId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        date: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        actualHours: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );

    staffProgressReport.associate = (models) => {
        staffProgressReport.belongsTo(models.staff, { foreignKey: "staffId" });
        staffProgressReport.belongsTo(models.class, { foreignKey: "classId" });
        staffProgressReport.belongsTo(models.batch, { foreignKey: "batchId" });
        staffProgressReport.belongsTo(models.subject, { foreignKey: "SubjectId" });
        staffProgressReport.belongsTo(models.topicDetails, { foreignKey: "topicDetailsId" });
        staffProgressReport.belongsTo(models.subTopic, { foreignKey: "subTopicId" });
    }
    return staffProgressReport;
}