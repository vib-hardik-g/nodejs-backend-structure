const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const examType = sequelize.define(
        'examType', {
            examType: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Exam Type cannot be empty"
                    }
                }
            },
            instituteId:{
                type: Sequelize.INTEGER,
            },
            weightage: {
                type: Sequelize.FLOAT
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    examType.associate = (models) => {
        examType.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return examType;
}