const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentRequest = sequelize.define(
        'studentRequest', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            studentId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            batchId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            request: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            approvedStatus: {
                type: Sequelize.STRING(50),
                defaultValue: "Pending"
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    studentRequest.associate = (models) => {
        studentRequest.belongsTo(models.institution, {foreignKey: "instituteId" });
        studentRequest.belongsTo(models.batch, {foreignKey: "batchId" });
        studentRequest.belongsTo(models.student, {foreignKey: "studentId" });
        studentRequest.hasMany(models.studentRequestAttachment, {foreignKey: "studentRequestId" });
    }
    return studentRequest;
}