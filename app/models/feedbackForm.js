const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackForm = sequelize.define('feedbackForm', {
        instituteId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        formType: {
            type: Sequelize.STRING,
            comment: "Class, Batch",
            allowNull: false
        },
        feedbackCycle: {
            type: Sequelize.STRING,
            allowNull: false
        },
        feedbackTemplateId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        deadlineDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        deadlineTime: {
            type: Sequelize.TIME,
            allowNull: false
        },
        formStatus: {
            type: Sequelize.STRING,
            comment: "Completed, Ongoing",
            defaultValue: 'Ongoing'
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackForm.associate = (models) => {
        feedbackForm.hasMany(models.feedbackFormClassBatch, { foreignKey: "feedbackFormId" });
        feedbackForm.belongsTo(models.feedbackTemplate, { foreignKey: "feedbackTemplateId" });
    }

    return feedbackForm;
}