const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackQuestionOption = sequelize.define(
        'feedbackQuestionOption', {
            feedbackQuestionId:{
                type: Sequelize.INTEGER,
            },
            feedbackOptions: {
                type: Sequelize.TEXT,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Feedback Options cannot be empty"
                    }
                }
            },
            
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    feedbackQuestionOption.associate = (models) => {
        feedbackQuestionOption.belongsTo(models.feedbackQuestion, { foreignKey: "feedbackQuestionId" });
    }
    return feedbackQuestionOption;
}