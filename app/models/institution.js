const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institution = sequelize.define(
        'institution', {
            orgId: {
                type: Sequelize.INTEGER
            },
            institutionName: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Institution Name already exists',
                    fields: [sequelize.col('institutionName')]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Institution Name cannot be empty"
                    }
                }
            },
            legalName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Legal Name cannot be empty"
                    }
                }
            },
            institutionType: {
                type: Sequelize.STRING(100),
                defaultValue: '',
            },
            studentLimit: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },
            instituteIcon: {
                type: Sequelize.INTEGER,
            },
            licenseNo: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            panNo: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            gstNo: {
                type: Sequelize.STRING,
                defaultValue: '',
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    institution.associate = (models) => {
        institution.belongsTo(models.organisation, { foreignKey: "orgId", as: "organisation" });
        institution.hasMany(models.institutionSubject, { foreignKey: "instituteId", as: "subjects" });
        institution.hasOne(models.institutionInfo, { foreignKey: "instituteId", as: "info" });
        institution.hasMany(models.institutionEmail, { foreignKey: "instituteId", as: "emails" });
        institution.hasMany(models.institutionPhone, { foreignKey: "instituteId", as: "phones" });
        institution.hasMany(models.institutionWebsite, { foreignKey: "instituteId", as: "websites" });
        institution.belongsTo(models.document, { foreignKey: "instituteIcon" });
        institution.hasMany(models.userStaffMapping, { foreignKey: "instituteId" });
        institution.hasMany(models.userStudentMapping, { foreignKey: "instituteId" });
    }
    return institution;
}