const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackRequest = sequelize.define(
        'feedbackRequest', {
            userFeedbackId:{
                type: Sequelize.INTEGER,
            },
            studentId:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    feedbackRequest.associate = (models) => {
        feedbackRequest.belongsTo(models.userFeedbackMapping, { foreignKey: "userFeedbackId" });
        feedbackRequest.belongsTo(models.student, { foreignKey: "studentId" });
    }
    return feedbackRequest;
}