const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const feedbackTemplateBenchmark = sequelize.define('feedbackTemplateBenchmark', {
        feedbackTemplateCategoryId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        benchmark: {
            type: Sequelize.STRING,
            allowNull: false
        },
        points: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    feedbackTemplateBenchmark.associate = (models) => {
        feedbackTemplateBenchmark.belongsTo(models.feedbackTemplateCategory, { foreignKey: "feedbackTemplateCategoryId" });
    }
    return feedbackTemplateBenchmark;
}