const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const section = sequelize.define(
        'section', {
        sectionName: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Section Name cannot be empty"
                }
            }
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    return section;
}