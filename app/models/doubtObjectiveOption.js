const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubtObjectiveOption = sequelize.define(
        'doubtObjectiveOption', {
            option: {
                type: Sequelize.STRING,
            },
            doubtId:{
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubtObjectiveOption.associate = (models) => {
        doubtObjectiveOption.belongsTo(models.doubt, { foreignKey: "doubtId" });
    }
    return doubtObjectiveOption;
}