const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentLeaveAttachment = sequelize.define(
        'studentLeaveAttachment', {
            leaveId:{
                type: Sequelize.INTEGER,
            },
            supportingDocument: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    studentLeaveAttachment.associate = (models) => {
        studentLeaveAttachment.belongsTo(models.studentLeave, { foreignKey: "leaveId" });
        studentLeaveAttachment.belongsTo(models.document, { foreignKey: "supportingDocument" });
    }
    return studentLeaveAttachment;
}