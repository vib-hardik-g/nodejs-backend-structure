const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const cls = sequelize.define('class', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        className: {
            type: Sequelize.STRING,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "Class Name cannot be empty"
                }
            }
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    cls.associate = (models) => {
        cls.belongsTo(models.institution, { foreignKey: "instituteId" });
    }

    return cls;
}