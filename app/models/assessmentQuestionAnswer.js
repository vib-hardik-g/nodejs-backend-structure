const Sequelize = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  const assessmentQuestionAnswer = sequelize.define("assessmentQuestionAnswer", {
    instituteId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    assessmentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    studentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    assessmentQuestionId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    assessmentSelectedOptionId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    isCorrect: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    status: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  assessmentQuestionAnswer.associate = (models) => {
    assessmentQuestionAnswer.belongsTo(models.institution, {
      foreignKey: "instituteId",
    });
    assessmentQuestionAnswer.belongsTo(models.assessment, {
      foreignKey: "assessmentId",
    });
    assessmentQuestionAnswer.belongsTo(models.student, {
      foreignKey: "studentId",
    });
    assessmentQuestionAnswer.belongsTo(models.assessmentQuestion, {
      foreignKey: "assessmentQuestionId",
    });
    assessmentQuestionAnswer.belongsTo(models.assessmentQuestionOption, {
      foreignKey: "assessmentSelectedOptionId", as: "selectedOption"
    });
  };
  return assessmentQuestionAnswer;
};
