const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const assessment = sequelize.define(
        'assessment', {
            instituteId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            assessmentName: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Assessment Name cannot be empty"
                    }
                }
            },
            classId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            duration: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            startDate: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            startTime: {
                type: Sequelize.STRING(5),
                allowNull: false
            },
            endDate: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            endTime: {
                type: Sequelize.STRING(5),
                allowNull: false
            },
            correctMark: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            incorrectMark: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    assessment.associate = (models) => { 
        assessment.belongsTo(models.institution, { foreignKey: 'instituteId' });
        assessment.belongsTo(models.class, { foreignKey: 'classId' });
        assessment.hasMany(models.batchAssessmentMapping, { foreignKey: 'assessmentId' });
        assessment.hasMany(models.groupAssessmentMapping, { foreignKey: 'assessmentId' });
        assessment.hasMany(models.subjectAssessmentMapping, { foreignKey: 'assessmentId' });
        assessment.hasMany(models.assessmentQuestion, { foreignKey: 'assessmentId' });
        assessment.hasMany(models.assessmentQuestionAnswer, { foreignKey: 'assessmentId' });
    }
    return assessment;
}