const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const noteAttachment = sequelize.define(
        'noteAttachment', {
            noteId:{
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            noteFile: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    noteAttachment.associate = (models) => {
        noteAttachment.belongsTo(models.note, { foreignKey: "noteId" });
        noteAttachment.belongsTo(models.document, { foreignKey: "noteFile" });
    }
    return noteAttachment;
}