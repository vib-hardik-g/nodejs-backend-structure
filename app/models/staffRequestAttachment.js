const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffRequestAttachment = sequelize.define(
        'staffRequestAttachment', {
            staffRequestId:{
                type: Sequelize.INTEGER,
            },
            requestAttachment: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    staffRequestAttachment.associate = (models) => {
        staffRequestAttachment.belongsTo(models.staffRequest, { foreignKey: "staffRequestId" });
        staffRequestAttachment.belongsTo(models.document, { foreignKey: "requestAttachment" });
    }
    return staffRequestAttachment;
}