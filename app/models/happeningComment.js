const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const happeningComment = sequelize.define(
        'happeningComment', {
        happeningId: {
            type: Sequelize.INTEGER,
        },
        studentId: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        staffId: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        approverId: {
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        approvalStatus: {
            type: Sequelize.STRING,
            defaultValue: false
        },
        comment: {
            type: Sequelize.TEXT,
            validate: {
                notEmpty: {
                    args: true,
                    msg: "comment cannot be empty"
                }
            }
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    happeningComment.associate = (models) => {
        happeningComment.belongsTo(models.student, { foreignKey: "studentId" });
        happeningComment.belongsTo(models.staff, { foreignKey: "staffId", as: "staff" });
        happeningComment.belongsTo(models.staff, { foreignKey: "approverId" ,as: "approver"});
        happeningComment.belongsTo(models.happening, { foreignKey: "happeningId" });
    }
    return happeningComment;
}