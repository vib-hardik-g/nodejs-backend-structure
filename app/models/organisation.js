const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const organisation = sequelize.define(
        'organisation', {
            orgName: {
                type: Sequelize.STRING,
                unique: {
                    args: true,
                    msg: 'Organisation Name already exists',
                    fields: [sequelize.col('orgName')]
                },
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Organisation Name cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    organisation.associate = (models) => {
        organisation.hasMany(models.institution, { foreignKey: "orgId", as: "institutions" });
    }
    return organisation;
}