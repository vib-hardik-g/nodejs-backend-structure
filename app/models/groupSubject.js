const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const groupSubject = sequelize.define(
        'groupSubject', {
       
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        groupId: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    groupSubject.associate = (models) => {
        groupSubject.belongsTo(models.subject, { foreignKey: "subjectId" });
        groupSubject.belongsTo(models.group, { foreignKey: "groupId" });
    }
    return groupSubject;
}