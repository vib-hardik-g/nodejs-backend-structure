const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const doubtSubjectiveResponseAttachment = sequelize.define(
        'doubtSubjectiveResponseAttachment', {
            doubtSubjectiveResponseId:{
                type: Sequelize.INTEGER,
            },
            doubtFile: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    doubtSubjectiveResponseAttachment.associate = (models) => {
        doubtSubjectiveResponseAttachment.belongsTo(models.doubtSubjectiveResponse, { foreignKey: "doubtSubjectiveResponseId" });
        doubtSubjectiveResponseAttachment.belongsTo(models.document, { foreignKey: "doubtFile" });
    }
    return doubtSubjectiveResponseAttachment;
}