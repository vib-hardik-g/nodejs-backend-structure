const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const studentAbsentee = sequelize.define(
        'studentAbsentee', {
            studentId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            studentAttendanceManagerId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            date: {
                type: Sequelize.DATEONLY,
                allowNull: false,
            },
            absenceType: {
                type: Sequelize.ENUM('Absent', 'Leave'),
                defaultValue: 'Absent',
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );

    studentAbsentee.associate = (models) => {
        studentAbsentee.belongsTo(models.student, { foreignKey: "studentId"  });
        studentAbsentee.belongsTo(models.studentAttendanceManager, { foreignKey: "studentAttendanceManagerId"  });
    }
    return studentAbsentee;
}