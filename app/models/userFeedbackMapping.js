const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userFeedbackMapping = sequelize.define(
        'userFeedbackMapping', {
            staffId:{
                type: Sequelize.INTEGER,
            },
            questionId:{
                type: Sequelize.STRING,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    userFeedbackMapping.associate = (models) => {
        userFeedbackMapping.belongsTo(models.staff, { foreignKey: "staffId" });
    }
    return userFeedbackMapping;
}