const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const batchSubjectMapping = sequelize.define(
        'batchSubjectMapping', {
        instituteId: {
            type: Sequelize.INTEGER,
        },
        subjectId: {
            type: Sequelize.INTEGER,
        },
        batchId: {
            type: Sequelize.INTEGER,
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    }
    );
    batchSubjectMapping.associate = (models) => {
        batchSubjectMapping.belongsTo(models.institution, { foreignKey: "instituteId" });
        batchSubjectMapping.belongsTo(models.subject, { foreignKey: "subjectId" });
        batchSubjectMapping.belongsTo(models.batch, { foreignKey: "batchId" });
    }
    return batchSubjectMapping;
}