const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    const testResultTemplateSubject = sequelize.define('testResultTemplateSubject', {
        testResultTemplateId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        subjectId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        maximumMarks: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        status: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    });

    testResultTemplateSubject.associate = (models) => {
        testResultTemplateSubject.belongsTo(models.testResultTemplate, { foreignKey: "testResultTemplateId" });
        testResultTemplateSubject.belongsTo(models.subject, { foreignKey: "subjectId" });
    }
    return testResultTemplateSubject;
}