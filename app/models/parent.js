const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const parent= sequelize.define(
        'parent', {
            parentTypeId: {
                type: Sequelize.INTEGER,
            },
            studentId: {
                type: Sequelize.INTEGER,
            },
            parentName: {
                type: Sequelize.STRING,
                defaultValue: ''
            },
            parentPhoneNumber: {
                type: Sequelize.STRING(10),
                defaultValue: ''
            },
            parentEmail: {
                type: Sequelize.STRING,
                defaultValue: ''
            },
            parentEducation: {
                type: Sequelize.STRING,
                defaultValue: ''
            },
            previousEducation: {
                type: Sequelize.STRING,
                defaultValue: ''
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    parent.associate = (models) => {
        parent.belongsTo(models.parentType, { foreignKey: "parentTypeId" });
        parent.belongsTo(models.student, { foreignKey: "studentId"  });
    }
    return parent;
}