const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const document = sequelize.define(
        'document', {
            type: {
                type: Sequelize.STRING,
            },
            etag: {
                type: Sequelize.STRING,
            },
            fileName: {
                type: Sequelize.STRING,
            },
            filePath: {
                type: Sequelize.STRING,
            },
            fullPath: {
                type: Sequelize.STRING,
            },
            originalFileName: {
                type: Sequelize.STRING,
            },
            fileExtension: {
                type: Sequelize.STRING(10),
            },
            size: {
                type: Sequelize.INTEGER,
            },
            mimeType: {
                type: Sequelize.STRING,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        },
        {
            defaultScope: {
                attributes: {
                    exclude: ['etag', 'fileName', 'filePath', 'status', 'createdAt', 'updatedAt']
                }
            },
            scopes: {
                all: {
                    attributes: {}
                }
            }

        }
    );
    return document;
}