const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const userInstituteMapping = sequelize.define(
        'userInstituteMapping', {
            userTypeId: {
                type: Sequelize.INTEGER
            },
            userId: {
                allowNull: true,
                type: Sequelize.INTEGER,
            },
            studentId: {
                allowNull: true,
                type: Sequelize.INTEGER,
            },
            staffId: {
                allowNull: true,
                type: Sequelize.INTEGER,
            },
            instituteId: {
                type: Sequelize.INTEGER,
            },
            subjectId: {
                type: Sequelize.INTEGER,
            },
            batchId: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    );
    
    userInstituteMapping.associate = (models) => {
        userInstituteMapping.belongsTo(models.userType, {foreignKey: "userTypeId"});
        userInstituteMapping.belongsTo(models.user, {foreignKey: "userId"});
        userInstituteMapping.belongsTo(models.student, {foreignKey: "studentId"});
        userInstituteMapping.belongsTo(models.staff, {foreignKey: "staffId"});
        userInstituteMapping.belongsTo(models.institution, {foreignKey: "instituteId"});
        userInstituteMapping.belongsTo(models.subject, {foreignKey: "subjectId"});
        userInstituteMapping.belongsTo(models.batch, {foreignKey: "batchId"});
    }
    return userInstituteMapping;
}