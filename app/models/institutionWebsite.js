const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const institutionWebsite = sequelize.define(
        'institutionWebsite', {
            instituteId: {
                type: Sequelize.INTEGER,
            },
            website: {
                type: Sequelize.STRING,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: "Website cannot be empty"
                    }
                }
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    institutionWebsite.associate = (models) => {
        institutionWebsite.belongsTo(models.institution, { foreignKey: "instituteId" });
    }
    return institutionWebsite;
}