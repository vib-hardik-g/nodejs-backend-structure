const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const staffHour = sequelize.define(
        'staffHour', {
            subTopicId: {
                type: Sequelize.INTEGER,
            },
            date:{
                type: Sequelize.DATEONLY,
            },
            hours:{
                type: Sequelize.INTEGER,
            },
            chapterCompleted:{
                type: Sequelize.BOOLEAN,
            },
            status: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
        }
    );
    staffHour.associate = (models) => {
        staffHour.belongsTo(models.subTopic, { foreignKey: "subTopicId" });
    }
    return staffHour;
}