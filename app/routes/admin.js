const routes = require('express').Router();
var multer = require('multer')

var storage = multer.diskStorage({
    destination: function (request, file, callback) {
        callback(null, 'app/public/student/');
    },
    filename: function (request, file, callback) {
        callback(null, file.originalname)
    }
});

var upload = multer({ storage: storage });

// admin actions for institution 
const { createUpdateInstitutionPhoneNumber, createUpdateInstitutionEmail, createUpdateInstitutionWebsite } = require('../controllers/institutions/institutionContactDetails');
const { updateInstitutionDetailsByAdmin } = require('../controllers/institutions/institutionDetailsUpdateByAdmin');

const { institutionEmailCreateUpdateSchema, institutionPhoneNumberCreateUpdateSchema, institutionWebsiteCreateUpdateSchema, institutionUpdateByAdminSchema } = require('../validators/institution.validator');

routes.put('/update-institution', [institutionUpdateByAdminSchema], updateInstitutionDetailsByAdmin);

routes.post('/institution/phone', [institutionPhoneNumberCreateUpdateSchema], createUpdateInstitutionPhoneNumber);
routes.post('/institution/email', [institutionEmailCreateUpdateSchema], createUpdateInstitutionEmail);
routes.post('/institution/website', [institutionWebsiteCreateUpdateSchema], createUpdateInstitutionWebsite);


// Staff onboarding
const { createStaff, allStaffsByInstituteId, staffBulkUpload, updateStaff, deleteStaff, getStaffById, inviteStaff, sendRemindersToStaff } = require('../controllers/admins/staffs');
const { staffCreateSchema, staffUpdateSchema, staffAssignSubjectSchema } = require('../validators/staff.validator');

routes.post('/staff', staffCreateSchema, createStaff);
routes.get('/staffs', allStaffsByInstituteId);
routes.get('/staff/:id', getStaffById);
routes.put('/staff/:id', staffUpdateSchema, updateStaff);
routes.post('/staffs/bulk/create', staffBulkUpload);
routes.delete('/staff/:id', deleteStaff);
routes.post('/staff/invite', inviteStaff);
routes.post('/staff/reminders', sendRemindersToStaff);

// Student onboarding
const {
    createStudent, updateStudent, studentBulkUpload, deleteStudent, allStudentsWithPagination,
    createStudentBulkUploadTemplate, downloadStudentBulkUploadTemplate, studentById, inviteStudents, getAllUploadedExcelByInstitute,
    deleteTemplates, requestStudentCapacity
} = require('../controllers/admins/students');
const { studentCreateSchema, studentUpdateSchema, studentBulkUploadSchema, studentCapacitySchema } = require('../validators/student.validator');

routes.post('/students/invite', inviteStudents);
routes.get('/student/:id', studentById);
routes.post('/students', studentCreateSchema, createStudent);
routes.put('/student/:id', studentUpdateSchema, updateStudent);
routes.get('/students/list', allStudentsWithPagination);
routes.delete('/student/:id', deleteStudent);
routes.post('/students/template/create', studentBulkUploadSchema, createStudentBulkUploadTemplate);
routes.get('/students/template/download', downloadStudentBulkUploadTemplate);
routes.get('/students/template/list', getAllUploadedExcelByInstitute);
routes.post('/students/templates/delete', deleteTemplates);
routes.post('/students/capacity', [studentCapacitySchema], requestStudentCapacity);

routes.post('/students/bulk/create', upload.fields([
    {
        name: 'studentExcel',
        maxCount: 1
    },
    {
        name: 'studentProfilePictures'
    },
]), studentBulkUploadSchema, studentBulkUpload);

// Happenings
const { createHappening, allHappenings, happeningById, updateHappening, deleteHappening, approveOrRejectHappeningComment, deleteHappeningComment } = require('../controllers/admins/happenings');
const { happeningCreateSchema, happeningUpdateSchema, happeningCommentCreateSchema, approveOrRejectHappeningCommentSchema } = require('../validators/happening.validator');

routes.post('/happening', [happeningCreateSchema], createHappening);
routes.get('/happenings', allHappenings);
routes.get('/happening/:id', happeningById);
routes.put('/happening/:id', [happeningUpdateSchema], updateHappening);
routes.delete('/happening/:id', deleteHappening);

routes.put('/happening-comment-approve-or-reject/:id', [approveOrRejectHappeningCommentSchema], approveOrRejectHappeningComment);
routes.delete('/happening-comment/:id', deleteHappeningComment);


// Time Tables
const { allTimeTables, createTimeTable, deleteTimeTable, checkTimeTable, updateTimeTable, publishTimeTable, getTimeTableDetails } = require('../controllers/admins/timeTables');
const { createTimeTableSchema, checkTimeTableSchema, updateTimeTableSchema, publishTimeTableSchema } = require('../validators/timeTable.validator');

routes.get('/timetables', allTimeTables);
routes.post('/timetable', [createTimeTableSchema], createTimeTable);
routes.post('/checktimetable', [checkTimeTableSchema], checkTimeTable);
routes.put('/timetable/:id', [updateTimeTableSchema], updateTimeTable);
routes.put('/publishTimeTable', [publishTimeTableSchema], publishTimeTable);
routes.delete('/timetable/:id', deleteTimeTable);
routes.get('/timetableDetails/:id', getTimeTableDetails);


// Holiday List
const { createHoliday, allHolidays, updateHoliday, deleteHoliday } = require('../controllers/admins/holidays');
const { holidayCreateSchema, holidayUpdateSchema } = require('../validators/holidayList.validator');

routes.post('/holiday', [holidayCreateSchema], createHoliday);
routes.get('/holidaylist', allHolidays);
routes.put('/holiday/:id', [holidayUpdateSchema], updateHoliday);
routes.delete('/holiday/:id', deleteHoliday);


// Attendance
const { listStudentsByBatchDate, submitAttendance, approveOrRejectLeave, listAttendanceByClassBatch, publishAttendance, listAttendanceByStudentAttendanceManagerId, listAttendanceByStudentId } = require('../controllers/admins/attendance');
const { submitAttendanceSchema, approveOrRejectLeaveSchema, publishAttendanceSchema } = require('../validators/attendance.validator');

routes.get('/attendance/list/students/:batchId', listStudentsByBatchDate);
routes.post('/attendance/submit', [submitAttendanceSchema], submitAttendance);
routes.put('/attendance/publish', [publishAttendanceSchema], publishAttendance);
routes.put('/leave/approve-or-reject', [approveOrRejectLeaveSchema], approveOrRejectLeave);

routes.get('/attendance/list/class-batch/:classId/:batchId', listAttendanceByClassBatch);
routes.get('/attendance/list/manager/:id', listAttendanceByStudentAttendanceManagerId);
routes.get('/attendance/list/attendance/:classId/:batchId/:studentId', listAttendanceByStudentId);

// Notes
const { createNote, deleteNote, publishNote, listAllNotes, updateNote } = require('../controllers/admins/notes');
const { noteCreateSchema, notePublishSchema } = require('../validators/note.validator');

routes.get('/notes', listAllNotes);
routes.post('/note', [noteCreateSchema], createNote);
routes.put('/note/:id', [noteCreateSchema], updateNote);
routes.delete('/note/:id', deleteNote);
routes.put('/publishNote', [notePublishSchema], publishNote);

// Assignments
const { createAssignment, deleteAssignment, publishAssignment, listAllAssignments, updateAssignment, listStudentsByAssignmentId } = require('../controllers/admins/assignments');
const { assignmentCreateSchema, assignmentPublishSchema } = require('../validators/assignment.validator');

routes.get('/assignments', listAllAssignments);
routes.post('/assignment', [assignmentCreateSchema], createAssignment);
routes.put('/assignment/:id', [assignmentCreateSchema], updateAssignment);
routes.delete('/assignment/:id', deleteAssignment);
routes.put('/publishAssignment', [assignmentPublishSchema], publishAssignment);
routes.get('/assignment/list/students/:assignmentId', listStudentsByAssignmentId);

// Relational Apis
const { batchesByClasses, subjectsByBatches, groupsByBatchClass, getEmployeecodeBySubject, getStaffAndSubjectByBatch } = require('../controllers/admins/reationalApis');
const { batchesByClassesSchema, subjectsByBatchesSchema, groupsByBatchClassSchema, getEmployeecodeBySubjectSchema } = require('../validators/relational.validator');

routes.post('/relations/batchesByClasses', [batchesByClassesSchema], batchesByClasses);
routes.post('/relations/subjectsByBatches', [subjectsByBatchesSchema], subjectsByBatches);
routes.post('/relations/groupsByBatchClass', [groupsByBatchClassSchema], groupsByBatchClass);
routes.post('/relations/getEmployeecodeBySubject', [getEmployeecodeBySubjectSchema], getEmployeecodeBySubject);
routes.get('/relations/getEmployeecodeByBatch/:batchId', getStaffAndSubjectByBatch);

// Feedback Manager
const {
    createFeedbackTemplate, allFeedbackTemplates, getFeedbackTemplateById, updateFeedbackTemplate,
    deleteFeedbackTemplate, downloadFeedbackTemplate, getFeedbackTemplates,
    createFeedbackForm, allFeedbackForms, allFeedbackReports, getAllFeedbackCycles,
    publishFeedbackResult, getAllFeedbackResultsByStaff, downloadFeedbackResultOfStaff,
    viewFacultyFeedback
} = require('../controllers/admins/feedbackManager');
const {
    createFeedbackTemplateSchema, downloadFeedbackTemplateSchema,
    createFeedbackFormSchema, getFeedbackResultByStaffSchema,
    downloadFeedbackResultByStaffSchema, viewFeedbackResultByStaffSchema
} = require('../validators/feedbackManager.validator');

// Feedback Template
routes.post('/feedback/template', [createFeedbackTemplateSchema], createFeedbackTemplate);
routes.get('/feedback/template', allFeedbackTemplates);
routes.get('/feedback/template/all', getFeedbackTemplates);
routes.get('/feedback/template/:id', getFeedbackTemplateById);
routes.put('/feedback/template/:id', [createFeedbackTemplateSchema], updateFeedbackTemplate);
routes.delete('/feedback/template/:id', deleteFeedbackTemplate);
routes.post('/feedback/template/download', downloadFeedbackTemplateSchema, downloadFeedbackTemplate);

// Feedback Form
routes.post('/feedback/form', [createFeedbackFormSchema], createFeedbackForm);
routes.get('/feedback/forms', allFeedbackForms);
routes.get('/feedback/forms/all', getAllFeedbackCycles);

routes.get('/feedback/results', allFeedbackReports);
routes.put('/feedback/result/publish/:id', publishFeedbackResult);
routes.post('/feedback/results/staff', getFeedbackResultByStaffSchema, getAllFeedbackResultsByStaff);
routes.post('/feedback/results/download', downloadFeedbackResultByStaffSchema, downloadFeedbackResultOfStaff);
routes.post('/feedback/results/chart', viewFeedbackResultByStaffSchema, viewFacultyFeedback);


// Batch Progress Report
const { listStaffs, batchProgressReportByStaffClassBatchSubject, } = require('../controllers/admins/batchProgressReports');
routes.get('/batchProgressReports', listStaffs);
routes.get('/batchProgressReportByStaffClassBatchSubject/:staffId/:classId/:batchId/:subjectId', batchProgressReportByStaffClassBatchSubject);


// Online Assessment
// Question Bank
const { allQuestionBanks, createQuestionBank, deleteQuestionBank, getQuestionBankById, createQuestions, deleteQuestion, updateQuestion, uploadQuestionBankTemplate } = require('../controllers/admins/questionBank');
const { questionBankCreateSchema, questionsCreateSchema, questionUpdateSchema, questionBankUploadSchema } = require('../validators/questionBank.validator');
routes.get('/online-assessment/question-bank', allQuestionBanks);
routes.get('/online-assessment/question-bank/:id', getQuestionBankById);
routes.post('/online-assessment/question-bank', [questionBankCreateSchema], createQuestionBank);
routes.delete('/online-assessment/question-bank/:id', deleteQuestionBank);
routes.post('/online-assessment/questions', [questionsCreateSchema], createQuestions);
routes.delete('/online-assessment/question/:id', deleteQuestion);
routes.put('/online-assessment/question/:id', [questionUpdateSchema], updateQuestion);

var questionBankUpload = multer({
    storage: multer.diskStorage({
        destination: function (request, file, callback) {
            callback(null, 'tempFiles/');
        },
        filename: function (request, file, callback) {
            callback(null, file.originalname)
        }
    })
});
routes.post('/online-assessment/question-bank/upload', questionBankUpload.fields([{
    name: 'questionBankTemplate',
    maxCount: 1
}
]), questionBankUploadSchema, uploadQuestionBankTemplate);

//request
const { getStudentRequests, getStudentRequestsById, approveOrRejectRequest, getStaffRequests, getStaffRequestsById } = require('../controllers/admins/request');
const { requestUpdateSchema } = require('../validators/request.validator')
routes.get('/request/get/student-request', getStudentRequests);
routes.get('/request/get/student-request/:id', getStudentRequestsById);
routes.put('/request/update/request', requestUpdateSchema, approveOrRejectRequest);
routes.get('/request/get/staff-request', getStaffRequests);
routes.get('/request/get/staff-request/:id', getStaffRequestsById);

// Assessment
const { createAssessment, allAssessments, downloadAssessmentPDF, getAssessmentById, allAssessmentResults, downloadAssessmentResultPDF } = require('../controllers/admins/assessments');
const { assessmentCreateSchema } = require('../validators/assessment.validator');
routes.post('/online-assessment/assessment', [assessmentCreateSchema], createAssessment);
routes.get('/online-assessment/assessments', allAssessments);
routes.get('/online-assessment/downloadPDF/:assessmentId', downloadAssessmentPDF);
routes.get('/online-assessment/assessment/:assessmentId', getAssessmentById);

// Assessment Result
routes.get('/online-assessment/assessment-results', allAssessmentResults);
routes.get('/online-assessment/assessment-result/:assessmentId', downloadAssessmentResultPDF);

// Dashboard
const { dashboardCounts, studentLeaveRequestList, studentRequest, staffRequest, happeningList } = require('../controllers/admins/dashboard');

routes.get('/dashboard/counts', dashboardCounts);
routes.get('/dashboard/studentLeaveRequestList', studentLeaveRequestList);
routes.get('/dashboard/studentRequest', studentRequest);
routes.get('/dashboard/staffRequest', staffRequest);
routes.get('/dashboard/happeningList', happeningList);

// Assessment Manager
const {
    // Test Result Template
    createTestResultTemplate, getTestResultTemplateById,
    allTestResultTemplateWithPagination, updateTestResultTemplate,
    deleteTestResultTemplate, publishTestResult, downloadTestResultTemplate,
    uploadTestResult,

    // Test Schedule
    createTestSchedule, getTestScheduleById, updateTestSchedule, publishTestSchedule,
    deleteTestSchedule, getAllTestSchedulesWithPagination,

    // Test Result Report Card
    createTestResultReportCard, getAllTestResultReportCardWithPagination, getTestResultReportCardById,
    updateTestResultReportCard, deleteTestResultReportCard, getAllStudentsReportCardById,
    getStudentsReportCardById, updateStudentsReportCard, publishStudentsReportCard, deleteStudentsReportCard,

    // Perivous Year Test Paper
    createPreviousYearTestPaper, allPreviousYearTestPaperWithPagination, getPreviousYearTestPaperById,
    updatePreviousYearTestPaper, publishPreviousYearTestPaper, deletePreviousYearTestPaper
} = require('../controllers/admins/assessmentManager');
const {
    createTestResultTemplateSchema, uploadTestResultSchema,
    createTestScheduleSchema,
    createTestResultReportCardSchema, updateStudentReportCardSchema,
    createPreviousYearTestPaperSchema, updatePreviousYearTestPaperSchema, publishPreviousYearTestPaperSchema
} = require('../validators/assessmentManager.validator');

// Test Result Template Module
routes.post('/test/result/template', createTestResultTemplateSchema, createTestResultTemplate);
routes.get('/test/result/template/:id', getTestResultTemplateById);
routes.get('/test/result/template', allTestResultTemplateWithPagination);
routes.put('/test/result/template/:id', createTestResultTemplateSchema, updateTestResultTemplate);
routes.delete('/test/result/template/:id', deleteTestResultTemplate);
routes.put('/test/result/template/publish/:id', publishTestResult);
routes.get('/test/result/template/download/:id', downloadTestResultTemplate);
routes.post('/test/result/student/upload', upload.fields([
    {
        name: 'studentResultAttachment',
        maxCount: 1
    }
]), uploadTestResultSchema, uploadTestResult);

// Test Schedule Module
routes.post('/test/schedule', createTestScheduleSchema, createTestSchedule);
routes.get('/test/schedule', getAllTestSchedulesWithPagination);
routes.get('/test/schedule/:id', getTestScheduleById);
routes.put('/test/schedule/:id', createTestScheduleSchema, updateTestSchedule);
routes.put('/test/schedule/publish/:id', publishTestSchedule);
routes.delete('/test/schedule/:id', deleteTestSchedule);

// Test Schedule Module
routes.post('/test/result/report/card', upload.fields([
    {
        name: 'zipFile',
        maxCount: 1
    }
]), createTestResultReportCardSchema, createTestResultReportCard);
routes.get('/test/result/report/card', getAllTestResultReportCardWithPagination);
routes.get('/test/result/report/card/:id', getTestResultReportCardById);
routes.put('/test/result/report/card/:id', upload.fields([
    {
        name: 'zipFile',
        maxCount: 1
    }
]), updateTestResultReportCard);
routes.delete('/test/result/report/card/:id', deleteTestResultReportCard);
routes.get('/test/result/student/report/cards/:id', getAllStudentsReportCardById);
routes.get('/test/result/student/report/card/:id', getStudentsReportCardById);
routes.put('/test/result/student/report/card/:id', updateStudentReportCardSchema, updateStudentsReportCard);
routes.put('/test/result/student/report/card/publish/:id', publishStudentsReportCard);
routes.delete('/test/result/student/report/card/publish/:id', deleteStudentsReportCard);

// Previous Year Test Paper
routes.post('/previous/year/test/paper', createPreviousYearTestPaperSchema, createPreviousYearTestPaper);
routes.get('/previous/year/test/paper', allPreviousYearTestPaperWithPagination);
routes.get('/previous/year/test/paper/:id', getPreviousYearTestPaperById);
routes.put('/previous/year/test/paper/:id', updatePreviousYearTestPaperSchema, updatePreviousYearTestPaper);
routes.put('/previous/year/test/paper/publish/:id', publishPreviousYearTestPaperSchema, publishPreviousYearTestPaper);
routes.delete('/previous/year/test/paper/:id', deletePreviousYearTestPaper);

// Notice
const { createNoticeStudent, allNoticeStudentList, createNoticeStaff, allNoticeStaffList, getNoticeStaffById, getNoticeStudentById, deleteNotice } = require('../controllers/admins/notice');
const { noticeCreateSchema, noticeCreateSchemaStaff, noticeDeleteSchema } = require('../validators/notice.validator');

routes.post('/notice/student/create', noticeCreateSchema, createNoticeStudent);
routes.get('/notice/student/list', allNoticeStudentList);
routes.post('/notice/staff/create', noticeCreateSchemaStaff, createNoticeStaff);
routes.get('/notice/staff/list', allNoticeStaffList);
routes.get('/notice/staff/:noticeId', getNoticeStaffById);
routes.get('/notice/student/:noticeId', getNoticeStudentById);
routes.delete('/notice/:noticeId', noticeDeleteSchema, deleteNotice);

module.exports = routes;

// Announcement
const { createAnnouncementStudent, allAnnouncementStudentList, createAnnouncementStaff, allAnnouncementStaffList, getAnnouncementStaffById, getAnnouncementStudentById, deleteAnnouncement } = require('../controllers/admins/announcement');
const { announcementCreateSchemaStudent, announcementCreateSchemaStaff, announcementDeleteSchema } = require('../validators/announcement.validator');

routes.post('/announcement/student/create', announcementCreateSchemaStudent, createAnnouncementStudent);
routes.get('/announcement/student/list', allAnnouncementStudentList);
routes.post('/announcement/staff/create', announcementCreateSchemaStaff, createAnnouncementStaff);
routes.get('/announcement/staff/list', allAnnouncementStaffList);
routes.get('/announcement/staff/:announcementId', getAnnouncementStaffById);
routes.get('/announcement/student/:announcementId', getAnnouncementStudentById);
routes.delete('/announcement/:announcementId', announcementDeleteSchema, deleteAnnouncement);

module.exports = routes;