const routes = require("express").Router();
const { validateToken } = require("../../utils/jwt");
const {
  isSuperAdmin,
  isAdmin,
  isAdminOrSuperAdmin,
} = require("../../utils/accessRestrictionMiddleware");

const auth = require("./auth");
const profile = require("./App/profile");
const MDM = require("./mdm");
const TimeTable = require("./App/timeTable");
const Happenings = require("./App/happenings");
const Institute = require("./App/institute");
const Attendance = require("./App/attendance");
const Note = require("./App/note");
const Student = require("./App/student");
const Staff = require("./App/staff");
const Assignment = require("./App/assignment");
const StaffProgressReport = require("./App/staffProgressReport");
const Request = require("./App/request");
const assessment = require("./App/assessment");
const doubt = require("./App/doubt");
const Notice = require("./App/notice");
const Announcement = require("./App/annoucement");

const organisation = require("./organisations");
const institution = require("./institutions");

const { getProfile } = require("../controllers/admins/profile");

const { awsS3Handler } = require("../../utils/awsS3Handler");
const {
  uploadFile,
  deleteFile,
} = require("../../app/controllers/common/fileUpload");

routes.use("/auth", auth);
routes.use("/profile", [validateToken], profile);
routes.use("/time-table", [validateToken], TimeTable);
routes.use("/happening", [validateToken], Happenings);
routes.use("/institute", [validateToken], Institute);
routes.use("/attendance", [validateToken], Attendance);
routes.use("/note", [validateToken], Note);
routes.use("/student", [validateToken], Student);
routes.use("/staff", [validateToken], Staff);
routes.use("/assignment", [validateToken], Assignment);
routes.use("/staffProgressReport", [validateToken], StaffProgressReport);
routes.use("/request", [validateToken], Request);
routes.use("/online-assessment", [validateToken], assessment);
routes.use("/doubt", [validateToken], doubt);
routes.use("/notice", [validateToken], Notice);
routes.use("/announcement", [validateToken], Announcement);

// mdm for superadmin
routes.use("/org", [validateToken, isSuperAdmin], organisation);
routes.use("/institution", [validateToken, isAdminOrSuperAdmin], institution);
routes.use("/mdm", [validateToken, isAdminOrSuperAdmin], MDM);

// profile for admin or superadmin
routes.get("/admin/profile", [validateToken, isAdminOrSuperAdmin], getProfile);

// admin routes
const admin = require("./admin");
routes.use("/admin", [validateToken, isAdmin], admin);

// superadmin routes
const superAdmin = require("./superAdmin");
routes.use("/superAdmin", [validateToken, isSuperAdmin], superAdmin);

// common api routes
const { getCountries } = require("../controllers/common/countryStateCity");
routes.get("/countries", [validateToken], getCountries);
routes.post(
  "/uploadFile",
  [
    awsS3Handler().fields([
      {
        name: "file",
        maxCount: 20,
      },
    ]),
  ],
  uploadFile
);

routes.delete("/deleteFile/:id", deleteFile);

module.exports = routes;
