const routes = require('express').Router();
var multer = require('multer');

// Student Capacity Request
const { allRequestStudentCapacity, updateRequestStatus, studentCapacityById } = require('../controllers/superAdmins/studentCapacity');

routes.get('/requestStudentCapacities', allRequestStudentCapacity);
routes.post('/updateRequestStatus', updateRequestStatus);
routes.get('/studentCapacityById/:id', studentCapacityById);

// Administrator
const { allAdministrators, addAdministrator,administratorById, updateAdministrator,deleteAdministrator,otpSendToExistingEmail,otpVerifyToExistingEmail,otpSendToNewEmail,updateAdministratorEmail } = require('../controllers/superAdmins/administrator');
const { administratorCreateSchema, administratorUpdateSchema, administratorOTPSendSchema, administratorOTPVarifySchema, administratorOTPVarifyUpdateSchema } = require('../validators/administrator.validator');

routes.get('/administrators', allAdministrators);
routes.post('/addAdministrator', administratorCreateSchema, addAdministrator);
routes.get('/administratorById/:id', administratorById);
routes.post('/updateAdministrator', administratorUpdateSchema, updateAdministrator);
routes.delete('/deleteAdministrator/:id', deleteAdministrator);

routes.post('/otpSendToExistingEmail', administratorOTPSendSchema, otpSendToExistingEmail);
routes.post('/otpVerifyToExistingEmail', administratorOTPVarifySchema, otpVerifyToExistingEmail);
routes.post('/otpSendToNewEmail', administratorOTPSendSchema, otpSendToNewEmail);
routes.post('/updateAdministratorEmail', administratorOTPVarifyUpdateSchema, updateAdministratorEmail);

// Users
const { allstudents, allstaffs } = require('../controllers/superAdmins/user');

routes.get('/students', allstudents);
routes.get('/staffs', allstaffs);

// Dashboard
const { dashboardCounts,studentRequestCapacityList } = require('../controllers/superAdmins/dashboard');

routes.get('/dashboardCounts', dashboardCounts);
routes.get('/studentRequestCapacityList', studentRequestCapacityList);

module.exports = routes;