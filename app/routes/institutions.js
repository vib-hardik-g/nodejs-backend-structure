const routes = require('express').Router();
const { isSuperAdmin, isAdmin } = require('../../utils/accessRestrictionMiddleware');

const { createInstitution,
    allInstitutions,
    institutionById,
    updateInstitution,
    deleteInstitution,
    getAllInstitutions
} = require('../controllers/institutions/institutions');

const { institutionCreateSchema, institutionUpdateSchema } = require('../validators/institution.validator');

// super admin/admin institution access
routes.get('/list',[isSuperAdmin], allInstitutions);
routes.post('/', [isSuperAdmin, institutionCreateSchema], createInstitution);
routes.get('/:id?', institutionById);
routes.put('/:id', [isSuperAdmin,institutionUpdateSchema], updateInstitution);
routes.delete('/:id',[isSuperAdmin], deleteInstitution);
routes.get('/get/all', getAllInstitutions);

// Class
const { createClass, allClasses, allClassesWithPagination, classById, updateClass, deleteClass, deleteClassMsg } = require('../controllers/MDM/classes');
const { classCreateSchema, classUpdateSchema } = require('../validators/class.validator');

routes.post('/class', [isAdmin, classCreateSchema], createClass);
routes.get('/classes/list', allClasses);
routes.get('/class/:id', classById);
routes.put('/class/:id', [isAdmin, classUpdateSchema], updateClass);
routes.delete('/class/:id', [isAdmin], deleteClass);
routes.delete('/classMsg/:id', [isAdmin], deleteClassMsg);

// Subject
const { createSubject, allSubjects, subjectById, updateSubject, deleteSubject, deleteSubjectMsg } = require('../controllers/MDM/subjects');
const { subjectCreateSchema, subjectUpdateSchema } = require('../validators/subject.validator');

routes.post('/subject', [isAdmin, subjectCreateSchema], createSubject);
routes.get('/subjects/list', allSubjects);
routes.get('/subject/:id', subjectById);
routes.put('/subject/:id', [isAdmin, subjectUpdateSchema], updateSubject);
routes.delete('/subject/:id', [isAdmin], deleteSubject);
routes.delete('/subjectMsg/:id', [isAdmin], deleteSubjectMsg);

// Topic
const { createTopic, allTopics, topicById, updateTopic, deleteTopic, deleteTopicMsg} = require('../controllers/MDM/topics');
const { topicCreateSchema, topicUpdateSchema } = require('../validators/topic.validator');

routes.post('/topic', [isAdmin, topicCreateSchema], createTopic);
routes.get('/topics/list', allTopics);
routes.get('/topic/:id', topicById);
routes.put('/topic/:id', [isAdmin, topicUpdateSchema], updateTopic);
routes.delete('/topic/:id', [isAdmin], deleteTopic);
routes.delete('/topicMsg/:id', [isAdmin], deleteTopicMsg);

// Topic Details
const { createTopicDetails, allTopicDetails, topicDetailsById, updateTopicDetails, deleteTopicDetails } = require('../controllers/MDM/topicDetails');
const { topicDetailsCreateSchema, topicDetailsUpdateSchema } = require('../validators/topicDetails.validator');

routes.post('/topic-details', [isAdmin, topicDetailsCreateSchema], createTopicDetails);
routes.get('/topic-details/list', allTopicDetails);
routes.get('/topic-details/:id', topicDetailsById);
routes.put('/topic-details/:id', [isAdmin, topicDetailsUpdateSchema], updateTopicDetails);
routes.delete('/topic-details/:id', [isAdmin], deleteTopicDetails);

// Sub Topic
const { createSubTopic, allSubTopic, subTopicById, updateSubTopic, deleteSubTopic } = require('../controllers/MDM/subTopics');
const { subTopicCreateSchema, subTopicUpdateSchema } = require('../validators/subTopic.validator');

routes.post('/sub-topic', [isAdmin, subTopicCreateSchema], createSubTopic);
routes.get('/sub-topic/list', allSubTopic);
routes.get('/sub-topic/:id', subTopicById);
routes.put('/sub-topic/:id', [isAdmin, subTopicUpdateSchema], updateSubTopic);
routes.delete('/sub-topic/:id', [isAdmin], deleteSubTopic);

module.exports = routes;