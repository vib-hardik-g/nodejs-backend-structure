const routes = require('express').Router();

const { createOrgnisation,
    allOrganisations,
    organisationById,
    updateOrganisation,
    deleteOrganisation
} = require('../controllers/organisations/orgnisations');

const { institutionListsByOrgId } = require('../controllers/organisations/institutionListsByOrgId');

const { orgCreateSchema, orgUpdateSchema } = require('../validators/organisation.validator');

routes.post('/', [ orgCreateSchema ], createOrgnisation);
routes.get('/:id', organisationById);
routes.get('/', allOrganisations);
routes.put('/:id', [ orgUpdateSchema ], updateOrganisation);
routes.delete('/:id', deleteOrganisation);

routes.get('/institutions/:orgId', institutionListsByOrgId);


module.exports = routes;