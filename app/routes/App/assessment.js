const routes = require('express').Router();

const assessment = require('../../controllers/user/assessment');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');
const { submitAssessmentSchema } = require('../../validators/assessment.validator');

routes.post('/submitAssessment', [checkStudentAndStaffDetail, submitAssessmentSchema], assessment.submitAssessment);
routes.get('/getAnswers/:assessmentId', [checkStudentAndStaffDetail], assessment.getAnswersByAssessmentId);
routes.get('/assessments', [checkStudentAndStaffDetail], assessment.allAssessments);
routes.get('/analysis/:assessmentId', [checkStudentAndStaffDetail], assessment.analysis);

module.exports = routes;