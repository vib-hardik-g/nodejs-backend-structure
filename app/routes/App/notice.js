
const routes = require('express').Router();

const notice = require('../../controllers/user/notice');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/getStudentNoticeList',checkStudentAndStaffDetail, notice.getStudentNoticeList);
routes.get('/getStaffNoticeList',checkStudentAndStaffDetail, notice.getStaffNoticeList);

module.exports = routes;