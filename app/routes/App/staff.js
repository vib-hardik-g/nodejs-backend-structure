const routes = require('express').Router();

// Feedback Form
const {
    getFeedbackCyclesByStaff, getAllSubjectsOfFeedbackCycles, viewFacultyFeedback
} = require('../../controllers/staffs/feedbackResult');
const { getSubjectFeedbackByStaffSchema, viewFeedbackResultByStaffSchema } = require('../../validators/feedbackManager.validator');

// Feedback Results For Staff
routes.get('/feedback/get/:staffId', getFeedbackCyclesByStaff);
routes.post('/feedback/subjects', getSubjectFeedbackByStaffSchema, getAllSubjectsOfFeedbackCycles);
routes.post('/feedback/result', viewFeedbackResultByStaffSchema, viewFacultyFeedback);

module.exports = routes;