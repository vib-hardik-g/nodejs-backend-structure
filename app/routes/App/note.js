const routes = require('express').Router();

const note = require('../../controllers/user/note');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/getNotes/:subjectId',checkStudentAndStaffDetail, note.getNotes);

module.exports = routes;