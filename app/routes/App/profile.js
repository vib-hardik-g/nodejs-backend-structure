const routes = require('express').Router();
const { validateToken } = require('../../../utils/jwt');

const Profile = require('../../controllers/user/profile');
const { getStaffDetails } = require('../../validators/profile.validator');

const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');

routes.get('/', Profile.getProfile);
routes.get('/staff/:staffId', Profile.getStaffDetails);
routes.get('/staff/by/subject/:subjectId', Profile.getStaffsBySubject);
routes.get('/subject/by/student',checkStudentAndStaffDetail, Profile.getSubjectByStudent);
routes.post('/switch-profile', Profile.switchProfile);

module.exports = routes;