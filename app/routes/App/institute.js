const routes = require('express').Router();

const institute = require('../../controllers/user/institute');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/getInstituteInfo',checkStudentAndStaffDetail, institute.getInstituteInfo);

module.exports = routes;