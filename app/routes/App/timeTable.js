const routes = require('express').Router();

const timeTable = require('../../controllers/user/timeTable');
const { timeTableSchema } = require('../../validators/timeTable.validator');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.post('/student', [checkStudentAndStaffDetail, timeTableSchema], timeTable.getStudentTimeTable);
routes.post('/staff', [checkStudentAndStaffDetail, timeTableSchema], timeTable.getStaffRoutine);
module.exports = routes;