const routes = require('express').Router();

// Feedback Form
const {
    getFeedbackForm, saveSubmittedFeedbackResponse
} = require('../../controllers/students/releaseFeedbackForm');
const { getReleasedFeedbackFormSchema, submitReleasedFeedbackFormSchema } = require('../../validators/feedbackManager.validator');

// Feedback Template
routes.post('/feedback/form', getReleasedFeedbackFormSchema, getFeedbackForm);
routes.post('/feedback/form/submit', [submitReleasedFeedbackFormSchema], saveSubmittedFeedbackResponse);

// Previous Year Test Paper
const { getAllTestPapers } = require('../../controllers/students/previousYearTestPapaer');
routes.get('/previous/year/test/papers/:classId', getAllTestPapers);

// Report Card
const { getReportCard } = require('../../controllers/students/reportCard');
routes.get('/report/card', getReportCard);



module.exports = routes;