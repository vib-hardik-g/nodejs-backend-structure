const routes = require('express').Router();

const Request = require('../../controllers/user/request');
const { requestCreateSchema } = require('../../validators/request.validator');

const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');

routes.post('/create',[checkStudentAndStaffDetail, requestCreateSchema], Request.createRequest);
routes.get('/get',[checkStudentAndStaffDetail], Request.getRequest);

module.exports = routes;