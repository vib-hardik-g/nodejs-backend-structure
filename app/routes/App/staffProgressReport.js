const routes = require('express').Router();

const staffProgressReport = require('../../controllers/staffs/staffProgressReport');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/listBatchSubjects', checkStudentAndStaffDetail, staffProgressReport.listBatchSubjects);
routes.get('/listTopicsBySubjectId/:subjectId', checkStudentAndStaffDetail, staffProgressReport.listTopicsBySubjectId);
routes.get('/listSubTopicsByTopicDetailsId/:topicDetailsId', checkStudentAndStaffDetail, staffProgressReport.listSubTopicsByTopicDetailsId);
routes.get('/summaryByTopicDetailsId/:topicDetailsId', checkStudentAndStaffDetail, staffProgressReport.summaryByTopicDetailsId);
routes.post('/submitStaffProgressReport', checkStudentAndStaffDetail, staffProgressReport.submitStaffProgressReport);

module.exports = routes;