
const routes = require('express').Router();

const Announcement = require('../../controllers/user/announcement');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/getStudentAnnouncementList',checkStudentAndStaffDetail, Announcement.getStudentAnnouncementList);
routes.get('/getStaffAnnouncementList',checkStudentAndStaffDetail, Announcement.getStaffAnnouncementList);

module.exports = routes;