const routes = require('express').Router();
const attendance = require('../../controllers/user/attendance');
const { applyLeaveSchema,attendanceHistory,editLeaveSchema } = require('../../validators/attendance.validator');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.post('/apply-leave', [applyLeaveSchema, checkStudentAndStaffDetail], attendance.applyLeave);
routes.get('/leave-list', checkStudentAndStaffDetail, attendance.leaveList);
routes.post('/attendance-history', [attendanceHistory, checkStudentAndStaffDetail], attendance.attendanceHistory);
routes.put('/edit-leave', [editLeaveSchema, checkStudentAndStaffDetail], attendance.editLeave);

module.exports = routes;