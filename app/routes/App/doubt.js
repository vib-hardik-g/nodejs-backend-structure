const routes = require('express').Router();

const doubt = require('../../controllers/user/doubt');
//const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');
const { doubtCreateSchema, respondCreateSchema, doubtLikeUnlikeSchema, respondLikeUnlikeSchema } = require('../../validators/doubt.validator');

routes.post('/postDoubt', doubtCreateSchema, doubt.postDoubt);
routes.post('/postDoubtRespond', respondCreateSchema, doubt.postDoubtRespond);
routes.post('/doubtLikeUnlike', doubtLikeUnlikeSchema, doubt.doubtLikeUnlike);
routes.post('/doubtRespondLikeUnlike', respondLikeUnlikeSchema, doubt.doubtRespondLikeUnlike);
routes.post('/approveCorrectResponce', doubt.approveCorrectResponce);

routes.get('/batchWiseUnresolvedCount', doubt.batchWiseUnresolvedCount);

module.exports = routes;