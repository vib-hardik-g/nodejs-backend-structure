const routes = require('express').Router();

const happening = require('../../controllers/user/happening');
const { happeningLikeSchema, happeningCommentSchema } = require('../../validators/happeningLikeAndComment');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');


routes.get('/pagination',checkStudentAndStaffDetail, happening.getHappeningWithPagination);
routes.get('/:happeningId', happening.getHappeningById);
routes.get('/comments/pagination', happening.getHappeningCommentsWithPagination);
routes.post('/comment', [happeningCommentSchema, checkStudentAndStaffDetail], happening.createHappeningComment);
routes.post('/like', [happeningLikeSchema, checkStudentAndStaffDetail], happening.createHappeningLike);
module.exports = routes;