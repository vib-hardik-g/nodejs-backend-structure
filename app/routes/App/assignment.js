const routes = require('express').Router();
const assignment = require('../../controllers/user/assignment');
const { checkStudentAndStaffDetail } = require('../../../utils/studentAndStaffDetail');
const { assignmentSubmitSchema } = require('../../validators/assignment.validator');

routes.get('/list/:subjectId',checkStudentAndStaffDetail, assignment.listAssignmentsByClassSubject);
routes.post('/submit',[checkStudentAndStaffDetail, assignmentSubmitSchema], assignment.submitAssignment);

module.exports = routes;