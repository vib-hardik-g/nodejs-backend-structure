const routes = require('express').Router();
const { validateToken } = require('../../utils/jwt');

const { LoginSchema, ForgotPasswordSchema, validaterOtpSchema, resetPasswordSchema, SignUpSchema, AdminLoginSchema } = require('../validators/auth.validator');

const login = require('../controllers/auth/login');
const forgotPassword = require('../controllers/auth/forgotPassword');
const validateOtp = require('../controllers/auth/validateOtp');
const resetPassword = require('../controllers/auth/resetPassword');
const verifyToken = require('../controllers/auth/verifyToken');
const signup = require('../controllers/auth/signup');
const changePassword = require('../controllers/auth/changePassword');

routes.post('/login',LoginSchema, login);
routes.post('/forgot-password', ForgotPasswordSchema,  forgotPassword);
routes.post('/validate-otp', validaterOtpSchema, validateOtp);
routes.post('/reset-password', resetPasswordSchema, resetPassword);
routes.get('/verify-token',validateToken, verifyToken);
routes.post('/sign-up',SignUpSchema, signup);
routes.post('/change-password',resetPasswordSchema, changePassword);


const adminLogin = require('../controllers/auth/admin/login');
const adminForgotPassword = require('../controllers/auth/admin/forgotPassword');
const adminResetPassword = require('../controllers/auth/admin/resetPassword');
const adminVerifyToken = require('../controllers/auth/verifyToken');

const adminSetPassword = require('../controllers/auth/admin/setPassword');

routes.post('/admin/login', AdminLoginSchema, adminLogin);
routes.post('/admin/forgot-password', ForgotPasswordSchema, adminForgotPassword);
routes.post('/admin/change-password', resetPasswordSchema, adminResetPassword);
routes.get('/admin/verify-token',validateToken, adminVerifyToken);

routes.post('/admin/set-password',resetPasswordSchema, adminSetPassword);

const logout = require('../controllers/auth/logout');
routes.delete('/logout', validateToken, logout);

module.exports = routes;