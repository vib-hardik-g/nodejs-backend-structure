const routes = require('express').Router();
const { isAdmin } = require('../../utils/accessRestrictionMiddleware');

// User Types
const { createUserType, allUserTypes, userTypeById, updateUserType, deleteUserType } = require('../controllers/MDM/userTypes');
const { userTypeCreateSchema, userTypeUpdateSchema } = require('../validators/userType.validator');

routes.post('/user-type', [userTypeCreateSchema], createUserType);
routes.get('/user-types', allUserTypes);
routes.get('/user-type/:id', userTypeById);
routes.put('/user-type/:id', [userTypeUpdateSchema], updateUserType);
routes.delete('/user-type/:id', deleteUserType);


// Modules
const { createModule, allModules, moduleById, updateModule, deleteModule } = require('../controllers/MDM/modules');
const { moduleCreateSchema, moduleUpdateSchema } = require('../validators/module.validator');

routes.post('/module', [moduleCreateSchema], createModule);
routes.get('/modules', allModules);
routes.get('/module/:id', moduleById);
routes.put('/module/:id', [moduleUpdateSchema], updateModule);
routes.delete('/module/:id', deleteModule);


// Sub Modules
const { createSubModule, allSubModules, subModuleById, updateSubModule, deleteSubModule } = require('../controllers/MDM/subModules');
const { subModuleCreateSchema, subModuleUpdateSchema } = require('../validators/subModule.validator');

routes.post('/sub-module', [subModuleCreateSchema], createSubModule);
routes.get('/sub-modules', allSubModules);
routes.get('/sub-module/:id', subModuleById);
routes.put('/sub-module/:id', [subModuleUpdateSchema], updateSubModule);
routes.delete('/sub-module/:id', deleteSubModule);

// Batch
const { createBatch, allBatches, batchById, updateBatch, deleteBatch,deleteBatchMsg } = require('../controllers/MDM/batches');
const { batchCreateSchema, batchUpdateSchema } = require('../validators/batch.validator');

routes.post('/batch', [batchCreateSchema], createBatch);
routes.get('/batches', allBatches);
routes.get('/batch/:id', batchById);
routes.put('/batch/:id', [batchUpdateSchema], updateBatch);
routes.delete('/batch/:id', deleteBatch);
routes.delete('/batchMsg/:id', deleteBatchMsg);


// Sections
const { createSection, allSections, sectionById, updateSection, deleteSection } = require('../controllers/MDM/sections');
const { sectionCreateSchema, sectionUpdateSchema } = require('../validators/section.validator');

routes.post('/section', [sectionCreateSchema], createSection);
routes.get('/section', allSections);
routes.get('/section/:id', sectionById);
routes.put('/section/:id', [sectionUpdateSchema], updateSection);
routes.delete('/section/:id', deleteSection);

// Exam Types
const { createExamType, allExamTypes, examTypeById, updateExamType, deleteExamType, deleteExamTypeMsg } = require('../controllers/MDM/examTypes');
const { examTypeCreateSchema, examTypeUpdateSchema } = require('../validators/examType.validator');

routes.post('/exam-type', [isAdmin, examTypeCreateSchema], createExamType);
routes.get('/exam-types', allExamTypes);
routes.get('/exam-type/:id', examTypeById);
routes.put('/exam-type/:id', [isAdmin, examTypeUpdateSchema], updateExamType);
routes.delete('/exam-type/:id', [isAdmin], deleteExamType);
routes.delete('/exam-typeMsg/:id', [isAdmin], deleteExamTypeMsg);

// Leave Types
const { createLeaveType, allLeaveTypes, leaveTypeById, updateLeaveType, deleteLeaveType } = require('../controllers/MDM/leaveTypes');
const { leaveTypeCreateSchema, leaveTypeUpdateSchema } = require('../validators/leaveType.validator');

routes.post('/leave-type', [leaveTypeCreateSchema], createLeaveType);
routes.get('/leave-types', allLeaveTypes);
routes.get('/leave-type/:id', leaveTypeById);
routes.put('/leave-type/:id', [leaveTypeUpdateSchema], updateLeaveType);
routes.delete('/leave-type/:id', deleteLeaveType);

// Parent Types
const { createParentType, allParentTypes, parentTypeById, updateParentType, deleteParentType } = require('../controllers/MDM/parentTypes');
const { parentTypeCreateSchema, parentTypeUpdateSchema } = require('../validators/parentType.validator');

routes.post('/parent-type', [parentTypeCreateSchema], createParentType);
routes.get('/parent-types', allParentTypes);
routes.get('/parent-type/:id', parentTypeById);
routes.put('/parent-type/:id', [parentTypeUpdateSchema], updateParentType);
routes.delete('/parent-type/:id', deleteParentType);

// Time Table Types
const { createTimeTableType, allTimeTableTypes, timeTableTypeById, updateTimeTableType, deleteTimeTableType } = require('../controllers/MDM/timeTableTypes');
const { timeTableTypeCreateSchema, timeTableTypeUpdateSchema } = require('../validators/timeTableType.validator');

routes.post('/time-table-type', [timeTableTypeCreateSchema], createTimeTableType);
routes.get('/time-table-types', allTimeTableTypes);
routes.get('/time-table-type/:id', timeTableTypeById);
routes.put('/time-table-type/:id', [timeTableTypeUpdateSchema], updateTimeTableType);
routes.delete('/time-table-type/:id', deleteTimeTableType);

// Grades
const { createGrade, allGrades, gradeById, updateGrade, deleteGrade } = require('../controllers/MDM/grades');
const { gradeCreateSchema, gradeUpdateSchema } = require('../validators/grade.validator');

routes.post('/grade', [gradeCreateSchema], createGrade);
routes.get('/grades', allGrades);
routes.get('/grade/:id', gradeById);
routes.put('/grade/:id', [gradeUpdateSchema], updateGrade);
routes.delete('/grade/:id', deleteGrade);

// Announcement Types
const { createAnnouncementType, allAnnouncementTypes, announcementTypeById, updateAnnouncementType, deleteAnnouncementType } = require('../controllers/MDM/announcementTypes');
const { announcementTypeCreateSchema, announcementTypeUpdateSchema } = require('../validators/anouncementType.validator');

routes.post('/announcement-type', [announcementTypeCreateSchema], createAnnouncementType);
routes.get('/announcement-types', allAnnouncementTypes);
routes.get('/announcement-type/:id', announcementTypeById);
routes.put('/announcement-type/:id', [announcementTypeUpdateSchema], updateAnnouncementType);
routes.delete('/announcement-type/:id', deleteAnnouncementType);

// Feedback Question Types
const { createFeedbackQuestionType, allFeedbackQuestionTypes, feedbackQuestionTypeById, updateFeedbackQuestionType, deleteFeedbackQuestionType } = require('../controllers/MDM/feedbackQuestionTypes');
const { feedbackQuestionTypeCreateSchema, feedbackQuestionTypeUpdateSchema } = require('../validators/feedbackQuestionType.validator');

routes.post('/feedback-question-type', [feedbackQuestionTypeCreateSchema], createFeedbackQuestionType);
routes.get('/feedback-question-types', allFeedbackQuestionTypes);
routes.get('/feedback-question-type/:id', feedbackQuestionTypeById);
routes.put('/feedback-question-type/:id', [feedbackQuestionTypeUpdateSchema], updateFeedbackQuestionType);
routes.delete('/feedback-question-type/:id', deleteFeedbackQuestionType);

// Groups
const { createGroup, allGroups, groupById, updateGroup, deleteGroup,deleteGroupMsg } = require('../controllers/MDM/groups');
const { groupCreateSchema, groupUpdateSchema } = require('../validators/group.validator');

routes.post('/group', [groupCreateSchema], createGroup);
routes.get('/groups', allGroups);
routes.get('/group/:id', groupById);
routes.put('/group/:id', [groupUpdateSchema], updateGroup);
routes.delete('/group/:id', deleteGroup);
routes.delete('/groupMsg/:id', deleteGroupMsg);


// Centers
const { createCenter, allCenters, centerById, updateCenter, deleteCenter, deleteCenterMsg } = require('../controllers/MDM/centers');
const { centerCreateSchema, centerUpdateSchema } = require('../validators/center.validator');

routes.post('/center', [isAdmin, centerCreateSchema], createCenter);
routes.get('/centers', allCenters);
routes.get('/center/:id', centerById);
routes.put('/center/:id', [isAdmin, centerUpdateSchema], updateCenter);
routes.delete('/center/:id', [isAdmin], deleteCenter);
routes.delete('/centerMsg/:id', [isAdmin], deleteCenterMsg);


module.exports = routes;