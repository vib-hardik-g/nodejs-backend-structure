const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.happeningCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        happening: Joi.string().label("Happening is required").required(),
        happeningDescription: Joi.string().label("Happening Description is required").required(),
        happeningThumnbnailImage:Joi.number().label("happeningThumnbnailImage Description is required"),
        attachments: Joi.array().items(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.happeningUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        happening: Joi.string(),
        happeningDescription: Joi.string(),
        status: Joi.boolean()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.happeningCommentCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        happeningId: Joi.number().label("Happening Id is required").required(),
        comment: Joi.string().label("Happening Comment is required").required(),
        studentId: Joi.number(),
        staffId: Joi.number(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.approveOrRejectHappeningCommentSchema = async (req, res, next) => {
    const schema = Joi.object({
        approvalStatus: Joi.string().label("Approval Status is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}