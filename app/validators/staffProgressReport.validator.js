const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.staffProgressReportCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        staffId: Joi.number().label("Staff Id is required").required(),
        classId: Joi.number().label("Class Id is required").required(),
        subjectId: Joi.number().label("Subject Id is required").required(),
        batchId: Joi.number().label("Batch Id is required").required(),
        topicDetailsId: Joi.number().label("Topic Details Id is required").required(),
        subTopicId: Joi.number().label("Sub Topic Id is required").required(),
        date: Joi.date().label('Date is required').required(),
        actualHours: Joi.number().label('Actual Hours required').required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}