const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.createFeedbackTemplateSchema = async (req, res, next) => {
    const schema = Joi.object({
        templateName: Joi.string().label("Template name is required").required(),
        noOfCategories: Joi.number().label("No. of categories is required").required(),
        categories: Joi.array().label("Category(s) details is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.downloadFeedbackTemplateSchema = async (req, res, next) => {
    const schema = Joi.object({
        id: Joi.number().label("Id is required").required(),
        fileType: Joi.string().label("File type is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.createFeedbackFormSchema = async (req, res, next) => {
    const schema = Joi.object({
        formType: Joi.string().label("Form type is required").required(),
        feedbackCycle: Joi.string().label("Feedback cycle is required").required(),
        feedbackTemplateId: Joi.number().label("Feedback template id is required").required(),
        deadlineDate: Joi.date().label("Deadline date is required").required(),
        deadlineTime: Joi.string().regex(/^([0-9]{2})\:([0-9]{2})\:([0-9]{2})$/).label("Deadline time is required. Time format should be like: HH:MM:SS").required(),
        classIds: Joi.array().items(Joi.number().required()).strict().required().label("Class id is required"),
        batchIds: Joi.array()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.getReleasedFeedbackFormSchema = async (req, res, next) => {
    const schema = Joi.object({
        instituteId: Joi.number().label("Institute id is required").required(),
        classId: Joi.number().label("Class id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.submitReleasedFeedbackFormSchema = async (req, res, next) => {
    const schema = Joi.object({
        feedbackFormId: Joi.number().label("Feedback form id is required").required(),
        subjectId: Joi.number().label("Subject id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
        staffId: Joi.number().label("Staff id is required").required(),
        studentId: Joi.number().label("Student id is required").required(),
        submittedFeedbacks: Joi.array().items(Joi.any().required()).strict().required().label("Submitted feedback response can not be empty"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.getFeedbackResultByStaffSchema = async (req, res, next) => {
    const schema = Joi.object({
        staffId: Joi.number().label("Staff id is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.downloadFeedbackResultByStaffSchema = async (req, res, next) => {
    const schema = Joi.object({
        feedbackFormId: Joi.number().label("Feedback form id is required").required(),
        subjectId: Joi.number().label("Subject id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
        staffId: Joi.number().label("Staff id is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.viewFeedbackResultByStaffSchema = async (req, res, next) => {
    const schema = Joi.object({
        feedbackFormId: Joi.number().label("Feedback form id is required").required(),
        staffId: Joi.number().label("Staff id is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.getSubjectFeedbackByStaffSchema = async (req, res, next) => {
    const schema = Joi.object({
        feedbackFormId: Joi.number().label("Feedback form id is required").required(),
        staffId: Joi.number().label("Staff id is required").required(),
        searchKey: Joi.string().allow('').optional()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}