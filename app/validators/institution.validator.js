const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.institutionCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        orgId: Joi.number().label("Organisation Id is required").required(),
        institutionName: Joi.string().label("Institution Name is required").required(),
        legalName: Joi.string().label("Legal Name is required").required(),
        institutionType: Joi.string().label("Institution Type is required").required(),
        studentLimit: Joi.number().label("Student Limit is required").required(),
        instituteIcon: Joi.number(),
        //licenseNo: Joi.string().label("License No is required").required(),
        panNo: Joi.string().label("Pan No is required").required(),
        gstNo: Joi.string().label("GST No is required").required(),
        //boardType: Joi.string(),
        //content: Joi.string().label("Content is required").required(),
        //address: Joi.string().allow(''),
        //googleMap: Joi.string().allow(''),
        //website: Joi.string().allow(''),
        contactPersonName: Joi.string().label("Contact Person Name is required").required(),
        contactPersonEmail: Joi.string().label("Contact Email Address is required").required(),
        contactPersonPhone: Joi.string().label("Contact Number is required").required(),
        //countryId: Joi.number(),
        //stateId: Joi.number(),
        //cityId: Joi.number(),
        adminData: Joi.array().items(
            Joi.object({
                firstName: Joi.string().label("First Name is required").required(),
                lastName: Joi.string(),
                email: Joi.string().label("Email is required").required(),
            })
        ).label("Admin Data as array required").required(),

    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.institutionUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        orgId: Joi.number().label("Organisation Id is required").required(),
        institutionName: Joi.string().label("Institution Name is required").required(),
        legalName: Joi.string().label("Legal Name is required").required(),
        institutionType: Joi.string().label("Institution Type is required").required(),
        instituteIcon: Joi.number(),
        studentLimit: Joi.number().label("Student Limit is required").required(),
        //licenseNo: Joi.string(),
        panNo: Joi.string().label("Pan No is required").required(),
        gstNo: Joi.string().label("GST No is required").required(),

        //boardType: Joi.string(),
        //content: Joi.string(),
        //address: Joi.string().allow(''),
        //googleMap: Joi.string().allow(''),
        //website: Joi.string().allow(''),
        contactPersonName: Joi.string().label("Contact Person Name is required").required(),
        contactPersonEmail: Joi.string().label("Contact Email Address is required").required(),
        contactPersonPhone: Joi.string().label("Contact Number is required").required(),
        //countryId: Joi.number(),
        //stateId: Joi.number(),
        //cityId: Joi.number(),
        status: Joi.boolean(),
        adminData: Joi.array().items(
            Joi.object({
                id:Joi.number(),
                firstName: Joi.string().label("First Name is required").required(),
                lastName: Joi.string(),
                email: Joi.string().label("Email is required").required(),
            })
        ).label("Admin Data as array required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.institutionPhoneNumberCreateUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        phoneNumbers: Joi.array().items().label("Phone Numbers are required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.institutionEmailCreateUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        emails: Joi.array().items(
            Joi.object({
                emailType: Joi.string().label("Email Type is required").required(),
                emailId: Joi.string().label("Email Id is required").required()
            })
        ).label("emails are required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.institutionWebsiteCreateUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        websites: Joi.array().items().label("Websites are required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.institutionUpdateByAdminSchema = async (req, res, next) => {
    const schema = Joi.object({
        institutionName: Joi.string(),
        legalName: Joi.string(),
        institutionType: Joi.string(),
        instituteIcon: Joi.number(),

        boardType: Joi.string(),
        content: Joi.string(),
        address: Joi.string().allow(''),
        googleMap: Joi.string().allow(''),
        website: Joi.string().allow(''),
        contactPersonName: Joi.string(),
        contactPersonEmail: Joi.string(),
        contactPersonPhone: Joi.string(),
        countryId: Joi.number(),
        stateId: Joi.number(),
        cityId: Joi.number(),
        status: Joi.boolean()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}