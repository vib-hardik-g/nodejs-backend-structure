const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.questionBankCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        classId: Joi.number().label("Class Id is required").required(),
        subjectId: Joi.number().label("Subject Id is required").required(),
        topicDetailsId: Joi.number().label("Topic Details Id is required").required(),
        noOfQuestions: Joi.number().label("Number of Questions is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.questionsCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        questionBankId: Joi.number().label("Question Bank Id is required").required(),
        questions: Joi.array().items(Joi.object({
            question: Joi.string().label('Question is requiured').required(),
            options: Joi.array().items(Joi.object({
                option: Joi.string().label('Option is required').required(),
                isCorrect: Joi.boolean().label('Is Correct is required').required(),
            })).min(4).label('Options are required').required(),
        })).label("questions are required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.questionUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        question: Joi.string().label("Question is required").required(),
        options: Joi.array().items(Joi.object({
            option: Joi.string().label('Option is required').required(),
            isCorrect: Joi.boolean().label('Is Correct is required').required(),
        })).min(4).label("Options are required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.questionBankUploadSchema = async (req, res, next) => {
    const schema = Joi.object({
        questionBankId: Joi.number().label("Question Bank Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}