const Joi = require("joi");
const commonResponse = require("../../utils/commonResponse");
const { Message } = require("../../utils/commonMessages");

module.exports.assessmentCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        assessmentName: Joi.string().label("Assessment Name is required").required(),
        startDate: Joi.date().label("Start Date is required").required(),
        startTime: Joi.string().max(5).label("Start Date is required").required(),
        duration: Joi.number().label("Duration is required").required(),
        classId: Joi.number().label("Class Id is required").required(),
        correctMark: Joi.number().greater(0).label("Correct Mark is required").required(),
        incorrectMark: Joi.number().less(1).label("Incorrect Mark is required").required(),
        
        type: Joi.string().valid("class", "batch", "group").label("Type must be either class, batch or group").required(),

        batchIds: Joi.alternatives().conditional("type", { is: "batch", then: Joi.array().items().min(1).required(), otherwise: Joi.optional() }).label("Batch ids are empty"),
        groupIds: Joi.alternatives().conditional("type", { is: "group", then: Joi.array().items().min(1).required(), otherwise: Joi.optional() }).label("Group ids are empty"),
        subjects: Joi.array().items(Joi.object({
            subjectId: Joi.number().label("Subject id is required").required(),
            noOfQuestions: Joi.number().label("Number of Questions required"),
            chapters: Joi.array().items(Joi.object({
                topicDetailsId: Joi.number().label("Topic Details Id is required").required(),
                noOfQuestions: Joi.number().label("Number of questions required"),
            })).min(1).label("Chapters are required").required(),
        })).min(1).label("Subjects are required").required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.submitAssessmentSchema = async (req, res, next) => {
    const schema = Joi.object({
        assessmentId: Joi.number().label("Assessment Id is required").required(),
        answers: Joi.array().items(Joi.object({
            assessmentQuestionId: Joi.number().label("Assessment Question Id is required").required(),
            assessmentSelectedOptionId: Joi.number().label("Assessment Selected Option Id is required")
        })).min(1).label("answers are required").required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}