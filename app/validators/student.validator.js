const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

module.exports.studentCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        // Personal details
        uniqueId: Joi.number().label("Unique id is required").required(),
        firstName: Joi.string().label("First Name is required").required(),
        lastName: Joi.string().label("Last Name is required").required(),
        email: Joi.string().label("Email is required").required().label("Please enter valid email address").email(),
        gender: Joi.string().label("Gender is required").required(),
        bloodGroup: Joi.string().label("Blood group is required").required(),
        religion: Joi.string().label("Religion is required").required(),
        dateOfBirth: Joi.date().label("Date of birth is required").required(),
        primaryPhoneNumber: Joi.string().allow(''),
        profilePicture: Joi.number().label("Profile picture is required").required(),

        // Institute
        classId: Joi.number().label("Class id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
        groupId: Joi.number().label("Group id is required").required(),

        // Address
        stateId: Joi.number().label("City id is required").required(),
        cityId: Joi.number().label("City id is required").required(),
        pincode: Joi.string().max(6).label("Pincode is required").required(),
        address: Joi.string().label("Address is required").required(),

        // Parents
        fatherName: Joi.string().allow('').optional(),
        fatherPhoneNumber: Joi.string().allow('').optional(),
        fatherEmailId: Joi.string().email().allow('').optional(),
        motherName: Joi.string().allow('').optional(),
        motherPhoneNumber: Joi.string().allow('').optional(),
        motherEmailId: Joi.string().email().allow('').optional(),
        legalGuardianName: Joi.string().allow('').optional(),
        legalGuardianPhoneNumber: Joi.string().allow('').optional(),
        legalGuardianEmailId: Joi.string().email().allow('').optional(),
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.studentUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        // Personal details
        uniqueId: Joi.number().label("Unique id is required").required(),
        firstName: Joi.string().label("First Name is required").required(),
        lastName: Joi.string().label("Last Name is required").required(),
        email: Joi.string().label("Email is required").required().label("Please enter valid email address").email(),
        gender: Joi.string().label("Gender is required").required(),
        bloodGroup: Joi.string().label("Blood group is required").required(),
        religion: Joi.string().label("Religion is required").required(),
        dateOfBirth: Joi.date().label("Date of birth is required").required(),
        primaryPhoneNumber: Joi.string().allow(''),
        profilePicture: Joi.number().label("Profile picture is required").required(),

        // Institute
        classId: Joi.number().label("Class id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
        groupId: Joi.number().label("Group id is required").required(),

        // Address
        stateId: Joi.number().label("City id is required").required(),
        cityId: Joi.number().label("City id is required").required(),
        pincode: Joi.string().max(6).label("Pincode is required").required(),
        address: Joi.string().label("Address is required").required(),

        // Parents
        fatherName: Joi.string().allow('').optional(),
        fatherPhoneNumber: Joi.string().allow('').optional(),
        fatherEmailId: Joi.string().email().allow('').optional(),
        motherName: Joi.string().allow('').optional(),
        motherPhoneNumber: Joi.string().allow('').optional(),
        motherEmailId: Joi.string().email().allow('').optional(),
        legalGuardianName: Joi.string().allow('').optional(),
        legalGuardianPhoneNumber: Joi.string().allow('').optional(),
        legalGuardianEmailId: Joi.string().email().allow('').optional(),
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.studentBulkUploadSchema = async (req, res, next) => {
    const schema = Joi.object({
        // Institute
        id: Joi.number().label("Id is required").required(),
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.studentCapacitySchema = async (req, res, next) => {
    const schema = Joi.object({
        studentLimit: Joi.number().label("Student Limit is required").required()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}