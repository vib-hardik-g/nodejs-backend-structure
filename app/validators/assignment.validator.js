const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.assignmentCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        assignmentName: Joi.string().label("Assignment name is required").required(),
        classId: Joi.number().label("Class Id is required").required(),
        subjectId: Joi.number().label("Subject Id is required").required(),
        batchIds: Joi.array().items(Joi.number()).min(1).label("Batch Ids are required").required(),
        assignmentFiles: Joi.array().items(Joi.number()).min(1).label("Assignment Files are required").required(),
        assignmentDueDate: Joi.date().label('Assignment Due Date is required in YYYY-MM-DD format'),
        assignmentDueTime: Joi.string().regex(/^([0-9]{2})\:([0-9]{2})$/).label('Assignment Due Time is missing'),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.assignmentPublishSchema = async (req, res, next) => {
    const schema = Joi.object({
        assignmentId: Joi.number().label("Assignment Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.assignmentSubmitSchema = async (req, res, next) => {
    const schema = Joi.object({
        assignmentId: Joi.number().label("Assignment Id is required").required(),
        assignmentFile: Joi.number().label("Assignment File is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}