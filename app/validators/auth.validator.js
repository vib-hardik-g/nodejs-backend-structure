
const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message, Validation } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.LoginSchema = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().label("Email id is required").required(),
        password: Joi.string().label("Password is required").required(),
        loginAs: Joi.string().required(),
        deviceName: Joi.string(),
        deviceMake: Joi.string(),
        deviceModel: Joi.string(),
        deviceToken: Joi.string(),
        platform: Joi.string(),
        systemVersion: Joi.string(),
        deviceId: Joi.string(),

    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.password) {
            return res.status(400).json(await commonResponse.response(false, errors.password));
        } else if(errors.email){
            return res.status(400).json(await commonResponse.response(false, errors.email));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}

module.exports.AdminLoginSchema = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().label("Email id is required").required(),
        password: Joi.string().label("Password is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.password) {
            return res.status(400).json(await commonResponse.response(false, errors.password));
        } else if(errors.email){
            return res.status(400).json(await commonResponse.response(false, errors.email));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}

module.exports.ForgotPasswordSchema = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().label("Email id is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors.email));
    } else {
        req.body = value;
        next();
    }
}

module.exports.validaterOtpSchema = async (req, res, next) => {
    const schema = Joi.object({
        otp: Joi.number().label("Otp is required").required(),
        token: Joi.string().label("Token is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        console.log(error);
        if (errors.otp) {
            return res.status(400).json(await commonResponse.response(false, errors.otp));
        } else if(errors.token){
            return res.status(400).json(await commonResponse.response(false, errors.token));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }

    } else {
        req.body = value;
        next();
    }
}

module.exports.resetPasswordSchema = async (req, res, next) => {
    const schema = Joi.object({
        password: Joi.string().label("Password is required").required(),
        token: Joi.string().label("Token is required").required()
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.password) {
            return res.status(400).json(await commonResponse.response(false, errors.password));
        } else if(errors.token){
            return res.status(400).json(await commonResponse.response(false, errors.token));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}
module.exports.SignUpSchema = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().label("Email id is required").required(),
        password: Joi.string().label("Password is required").required(),
        loginAs: Joi.string().required(),

    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.password) {
            return res.status(400).json(await commonResponse.response(false, errors.password));
        } else if(errors.email){
            return res.status(400).json(await commonResponse.response(false, errors.email));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}