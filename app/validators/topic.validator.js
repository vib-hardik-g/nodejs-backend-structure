const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.topicCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        classId: Joi.number().label("Class id is required").required(),
        subjectId: Joi.number().label("Subject id is required").required(),
        topics: Joi.array().items(Joi.object({
            chapterName: Joi.string().label('Chapter Name is required').required(),
            subTopics: Joi.array().items(Joi.object({
                subTopicName: Joi.string().label('Sub Topic Name is required').required(),
                allotedHours: Joi.number().label("Alloted Hour is required").required(),
            })).label('Sub Topics are required').required(),
        })).label('Topics are required')
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.topicUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        topics: Joi.array().items(Joi.object({
            topicDetailsId: Joi.number().optional(),
            chapterName: Joi.string().label('Chapter Name is required').required(),
            subTopics: Joi.array().items(Joi.object({
                subTopicId: Joi.number().optional(),
                subTopicName: Joi.string().label('Sub Topic Name is required').required(),
                allotedHours: Joi.number().label("Alloted Hour is required").required(),
            })).label('Sub Topics are required').required(),
        })).label('Topics are required')
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}