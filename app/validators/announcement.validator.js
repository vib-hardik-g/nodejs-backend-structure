const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.announcementCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        announcement: Joi.string().label("Announcement is required").required(),
        announcementStatus: Joi.string().label("Announcement Status is required").required(),
        announcementTypeId: Joi.number().label("Announcement Type Id is required").required(),
        announcementStaffId: Joi.number().label("Announcement Staff Id is required").required(),
        announcementApproverId: Joi.number().label("Announcement Approver Id is required").required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.announcementUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        announcement: Joi.string().label("Announcement is missing"),
        announcementStatus: Joi.string().label("Announcement Status is missing"),
        announcementTypeId: Joi.number().label("Announcement Type Id is missing"),
        announcementStaffId: Joi.number().label("Announcement Staff Id is missing"),
        announcementApproverId: Joi.number().label("Announcement Approver Id is missing"),
        announcementApprovedDate: Joi.date().label("Announcement Approved Date is missing"),
        status: Joi.boolean()
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.announcementCreateSchemaStudent = async (req, res, next) => {
    const schema = Joi.object({
        announcementType: Joi.string().label("Announcement Type is required").required(),
        title: Joi.string().label("Title Type is required").required(),
        description: Joi.string().label("Description Type is required").required(),
        announcementDocument: Joi.number().label("Announcement Document Id is required").required(),
        classId: Joi.number().label("Class Id is required"),
        classIds: Joi.array().items(Joi.number()).label("Class Ids are required"),
        batchIds: Joi.array().items(Joi.number()).label("Batch Ids are required"),
        groupIds: Joi.array().items(Joi.number()).label("Group Ids are required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}
module.exports.announcementCreateSchemaStaff = async (req, res, next) => {
    const schema = Joi.object({
        announcementType: Joi.string().label("Announcement Type is required").required(),
        title: Joi.string().label("Title Type is required").required(),
        description: Joi.string().label("description is required").required(),
        announcementDocument: Joi.number().label("Announcement Document Id is required").required(),
        classIds: Joi.array().items(Joi.object({
            classId: Joi.number().label("class Id is required").required(),
            subjectIds: Joi.array().items(Joi.number()).label("Subject Ids are required"),
        })).label("Class Ids are required").required(),
        
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}
module.exports.announcementDeleteSchema = async (req, res, next) => {
    const schema = Joi.object({
        type: Joi.string().label("Type is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}