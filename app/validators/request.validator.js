const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.requestCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        request: Joi.string().label("request is required").required(),
        requestAttachment: Joi.number().label("requestAttachment is required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.request) {
            return res.status(400).json(await commonResponse.response(false, errors.request));
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}
module.exports.requestUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        requestType: Joi.string().label("request is required").required(),
        requestId: Joi.number().label("request Id is required").required(),
        approvedStatus: Joi.string().label("approved Status is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.requestId) {
            return res.status(400).json(await commonResponse.response(false, errors.requestId));
        } else if (errors.requestType) {
            return res.status(400).json(await commonResponse.response(false, errors.requestType));
        } else if (errors.approvedStatus) {
            return res.status(400).json(await commonResponse.response(false, errors.approvedStatus));
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}