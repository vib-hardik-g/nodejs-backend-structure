const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.happeningCommentSchema = async (req, res, next) => {
    const schema = Joi.object({
        happeningId: Joi.number().label("Happening id is required").required(),
        comment: Joi.string().label("Comment is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.happeningId) {
            return res.status(400).json(await commonResponse.response(false, errors.happeningId));
        } else if(errors.comment){
            return res.status(400).json(await commonResponse.response(false, errors.comment));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}
module.exports.happeningLikeSchema = async (req, res, next) => {
    const schema = Joi.object({
        happeningId: Joi.number().label("Happening id is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.happeningId) {
            return res.status(400).json(await commonResponse.response(false, errors.happeningId));
        }else{
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}