const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.holidayCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        holidayType: Joi.string().valid('Weekend', 'Holiday').label("Holiday Type must be either Weekend or Holiday").required(),
        day: Joi.alternatives().conditional('holidayType', { is: 'Weekend', then: Joi.string().valid('Saturday', 'Sunday').required(), otherwise: Joi.optional() }).label("Day must be either Saturday or Sunday"),
        date: Joi.alternatives().conditional('holidayType', { is: 'Holiday', then: Joi.string().max(10).required(), otherwise: Joi.optional() }).label("Date is required for normal Holiday"),
        holidayName: Joi.alternatives().conditional('holidayType', { is: 'Holiday', then: Joi.string().required(), otherwise: Joi.optional() }).label("Holiday Name is required"),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.holidayUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        holidayName: Joi.string().label("Holiday Name is required").required(),
        date: Joi.string().max(10).label("Date is required").required(),
    });
    
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}