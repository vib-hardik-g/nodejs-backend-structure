const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.submitAttendanceSchema = async (req, res, next) => {
    const schema = Joi.object({
        batchId: Joi.number().label("Batch Id is required").required(),
        date: Joi.date().label("Date is required").required(),
        data: Joi.array().items(Joi.object({
            studentId: Joi.number().label("Student Id is required").required(),
            absent: Joi.bool().label("Absent is required").required(),
        })).label("Data is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.publishAttendanceSchema = async (req, res, next) => {
    const schema = Joi.object({
        batchId: Joi.number().label("Batch Id is required").required(),
        date: Joi.date().label("Date is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.approveOrRejectLeaveSchema = async (req, res, next) => {
    const schema = Joi.object({
        leaveId: Joi.number().label("Leave Id is required").required(),
        leaveStatus: Joi.string().valid('Approved', 'Rejected').label("Leave Status is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.listAttendanceByClassBatchSchema = async (req, res, next) => {
    const schema = Joi.object({
        classId: Joi.number().label("Class Id is required").required(),
        batchId: Joi.number().label("Batch Id is required").required(),
        startDate: Joi.date().label("Start Date is required").required(),
        endDate: Joi.date().label("End Date is required").required(),
    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.applyLeaveSchema = async (req, res, next) => {
    const schema = Joi.object({
        reason: Joi.string().label("reason is required").required(),
        supportingDocument: Joi.number().label("Supporting Document is required"),
        startDate: Joi.date().label("Start Date is required").required(),
        endDate: Joi.date().label("End Date is required").required(),

    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.reason) {
            return res.status(400).json(await commonResponse.response(false, errors.reason));
        } else if (errors.supportingDocument) {
            return res.status(400).json(await commonResponse.response(false, errors.supportingDocument));
        } else if (errors.startDate) {
            return res.status(400).json(await commonResponse.response(false, errors.startDate));
        } else if (errors.endDate) {
            return res.status(400).json(await commonResponse.response(false, errors.endDate));
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}
module.exports.editLeaveSchema = async (req, res, next) => {
    const schema = Joi.object({
        leaveId: Joi.number().label("Leave Id is required").required(),
        reason: Joi.string().label("reason is required").required(),
        startDate: Joi.date().label("Start Date is required").required(),
        endDate: Joi.date().label("End Date is required").required(),
        supportingDocument: Joi.number().label("End Date is required"),

    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.reason) {
            return res.status(400).json(await commonResponse.response(false, errors.reason));
        } else if (errors.startDate) {
            return res.status(400).json(await commonResponse.response(false, errors.startDate));
        } else if (errors.endDate) {
            return res.status(400).json(await commonResponse.response(false, errors.endDate));
        } else if (errors.leaveId) {
            return res.status(400).json(await commonResponse.response(false, errors.leaveId));
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}
module.exports.attendanceHistory = async (req, res, next) => {
    const schema = Joi.object({
        startDate: Joi.string().label("startDate is required").required(),
        endDate: Joi.string().label("endDate is required").required(),

    });
    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        if (errors.endDate) {
            return res.status(400).json(await commonResponse.response(false, errors.endDate));
        } else if (errors.startDate) {
            return res.status(400).json(await commonResponse.response(false, errors.startDate));
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR));
        }
    } else {
        req.body = value;
        next();
    }
}