const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.noticeCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        noticeType: Joi.string().label("Notice Type is required").required(),
        title: Joi.string().label("Title Type is required").required(),
        noticeDocument: Joi.number().label("Notice Document Id is required").required(),
        classId: Joi.number().label("Class Id is required"),
        classIds: Joi.array().items(Joi.number()).label("Class Ids are required"),
        batchIds: Joi.array().items(Joi.number()).label("Batch Ids are required"),
        groupIds: Joi.array().items(Joi.number()).label("Group Ids are required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}
module.exports.noticeCreateSchemaStaff = async (req, res, next) => {
    const schema = Joi.object({
        noticeType: Joi.string().label("Notice Type is required").required(),
        title: Joi.string().label("Title Type is required").required(),
        noticeDocument: Joi.number().label("Notice Document Id is required").required(),
        classIds: Joi.array().items(Joi.object({
            classId: Joi.number().label("class Id is required").required(),
            subjectIds: Joi.array().items(Joi.number()).label("Subject Ids are required"),
        })).label("Class Ids are required").required(),
        
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}
module.exports.noticeDeleteSchema = async (req, res, next) => {
    const schema = Joi.object({
        type: Joi.string().label("Type is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}