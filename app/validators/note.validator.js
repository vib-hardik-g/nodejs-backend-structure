const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};
module.exports.noteCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        noteName: Joi.string().label("Note name is required").required(),
        classId: Joi.number().label("Class Id is required").required(),
        subjectId: Joi.number().label("Subject Id is required").required(),
        batchIds: Joi.array().items(Joi.number()).min(1).label("Batch Ids are required").required(),
        noteFiles: Joi.array().items(Joi.number()).min(1).label("Note Files are required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.notePublishSchema = async (req, res, next) => {
    const schema = Joi.object({
        noteId: Joi.number().label("Note Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}