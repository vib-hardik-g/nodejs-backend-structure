const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.createTestResultTemplateSchema = async (req, res, next) => {
    const schema = Joi.object({
        examTypeId: Joi.number().label("Exam type id is required").required(),
        batchId: Joi.number().label("Batch id is required").required(),
        subjects: Joi.array().items(Joi.any().required()).strict().required().label("Please add atleast a subject and maximamum marks"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.publishTestResultTemplateSchema = async (req, res, next) => {
    const schema = Joi.object({
        isPublishedForStudent: Joi.boolean().required().label("isPublishedForStudent is required"),
        publishedForStudentAt: Joi.string().required().label("publishedForStudentAt is required"),
        isPublishedForStaff: Joi.boolean().required().label("isPublishedForStaff is required"),
        publishedForStaffAt: Joi.string().required().label("publishedForStaffAt is required"),
        isRepublishedForStaff: Joi.boolean().required().label("isRepublishedForStaff is required"),
        republishedForStaffAt: Joi.string().required().label("republishedForStaffAt is required"),
        isRepublishedForStudent: Joi.boolean().required().label("isRepublishedForStudent is required"),
        republishedForStudentAt: Joi.string().required().label("republishedForStudentAt is required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.uploadTestResultSchema = async (req, res, next) => {
    const schema = Joi.object({
        id: Joi.number().required().label("Id is required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.publishTestResultTemplateSchema = async (req, res, next) => {
    const schema = Joi.object({
        isPublishedForStudent: Joi.boolean().required().label("isPublishedForStudent is required"),
        publishedForStudentAt: Joi.string().required().label("publishedForStudentAt is required"),
        isPublishedForStaff: Joi.boolean().required().label("isPublishedForStaff is required"),
        publishedForStaffAt: Joi.string().required().label("publishedForStaffAt is required"),
        isRepublishedForStaff: Joi.boolean().required().label("isRepublishedForStaff is required"),
        republishedForStaffAt: Joi.string().required().label("republishedForStaffAt is required"),
        isRepublishedForStudent: Joi.boolean().required().label("isRepublishedForStudent is required"),
        republishedForStudentAt: Joi.string().required().label("republishedForStudentAt is required"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.createTestScheduleSchema = async (req, res, next) => {
    const schema = Joi.object({
        testScheduleType: Joi.string().required().label("Test schedule type"),
        title: Joi.string().max(255).required().label("Title").error(errors => {
            errors.forEach(err => {
                switch (err.code) {
                    case "string.required":
                        err.message = "Title is required";
                        break;
                    case "string.max":
                        err.message = `Title should have at most ${err.local.limit} characters!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        documentId: Joi.number().required().label("Document id is required"),
        classId: Joi.number().allow(''),
        classIds: Joi.array().items(Joi.any().optional()),
        batchIds: Joi.array().items(Joi.any().optional()),
        groupIds: Joi.array().items(Joi.any().optional())
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.createTestResultReportCardSchema = async (req, res, next) => {
    const schema = Joi.object({
        term: Joi.string().max(45).required().label("Term").error(errors => {
            errors.forEach(err => {
                switch (err.code) {
                    case "string.required":
                        err.message = "Term is required";
                        break;
                    case "string.max":
                        err.message = `Term should have at most ${err.local.limit} characters!`;
                        break;
                    default:
                        break;
                }
            });
            return errors;
        }),
        zipFileName: Joi.string().required().label("Zip file name"),
        batchId: Joi.number().required().label("Batch id"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.updateStudentReportCardSchema = async (req, res, next) => {
    const schema = Joi.object({
        documentId: Joi.number().required().label("Document id"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.createPreviousYearTestPaperSchema = async (req, res, next) => {
    const schema = Joi.object({
        documentId: Joi.number().required().label("Document id"),
        classId: Joi.number().required().label("Class id"),
        subjectId: Joi.number().required().label("Subject id"),
        examTypeId: Joi.number().required().label("Exam type id"),
        year: Joi.number().required().label("Year"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.updatePreviousYearTestPaperSchema = async (req, res, next) => {
    const schema = Joi.object({
        documentId: Joi.number().required().label("Document id"),
        year: Joi.number().required().label("Year"),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.publishPreviousYearTestPaperSchema = async (req, res, next) => {
    const schema = Joi.object({
        isPublished: Joi.boolean().allow(''),
        publishedAt: Joi.any().allow(''),
        isRePublished: Joi.boolean().allow(''),
        rePublishedAt: Joi.any().allow(''),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};

        error.details.forEach(err => {
            errors[err.context.key] = err.message
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}