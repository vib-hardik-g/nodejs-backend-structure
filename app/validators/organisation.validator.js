const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
module.exports.orgCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        orgName: Joi.string().label("Org name is required").required()
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.orgUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        orgName: Joi.string().label("Org name is missing"),
        status: Joi.boolean()
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}