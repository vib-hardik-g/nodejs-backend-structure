const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.timeTableSchema = async (req, res, next) => {
    const schema = Joi.object({
        day: Joi.string().label("Day is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.createTimeTableSchema = async (req, res, next) => {
    const schema = Joi.object({
        timeTableTypeId: Joi.number().label('Time Table Type Id is required').required(),
        batchId: Joi.number().label('Batch Id is required').required(),
        days: Joi.array().items(
            Joi.object({
                day: Joi.string().valid('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday').label('Day is required').required(),
                data: Joi.array().items(
                    Joi.object({
                        subjectId: Joi.number().label('Subject Id is required').required(),
                        staffId: Joi.number().allow(null).label('Staff Id is missing'),
                        centerId: Joi.number().allow(null).label('Center Id is missing'),
                        room: Joi.any().allow(null).label('Room is missing'),
                        startTime: Joi.string().min(5).max(5).label('Start Time is required').required(),
                        endTime: Joi.string().min(5).max(5).label('End Time is required').required(),
                    })
                ).label("Data is required").required(),
            })
        ).label('days are requuired'),
        
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        console.error(error)
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.checkTimeTableSchema = async (req, res, next) => {
    const schema = Joi.object({
        batchId: Joi.number().label("Batch Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.updateTimeTableSchema = async (req, res, next) => {
    const schema = Joi.object({
        days: Joi.array().items(
            Joi.object({
                day: Joi.string().valid('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday').label('Day is required').required(),
                data: Joi.array().items(
                    Joi.object({
                        subjectId: Joi.number().label('Subject Id is required').required(),
                        staffId: Joi.number().allow(null).label('Staff Id is missing'),
                        centerId: Joi.number().allow(null).label('Center Id is missing'),
                        room: Joi.any().allow(null).label('Room is missing'),
                        startTime: Joi.string().min(5).max(5).label('Start Time is required').required(),
                        endTime: Joi.string().min(5).max(5).label('End Time is required').required(),
                    })
                ).label("Data is required").required(),
            })
        ).label('days are requuired'),
        
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        console.error(error)
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.publishTimeTableSchema = async (req, res, next) => {
    const schema = Joi.object({
        timeTableId: Joi.number().label("Time Table Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}