const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message, StaffAge } = require('../../utils/commonMessages');
const { getAge } = require('../../utils/commonHelper');

module.exports.staffCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        roleName: Joi.string().label("Role name is required").required(),
        salutation: Joi.string().label("Salutation is required").required(),
        employeeCode: Joi.string().label("Employee code is required").required(),
        email: Joi.string().label("Email is required").required(),
        firstName: Joi.string().label("First Name is required").required(),
        lastName: Joi.string().label("Last Name is required").required(),
        gender: Joi.string().label("Gender is required").required(),
        contactNo: Joi.string().label("Contact number is required").required(),
        designation: Joi.string().label("Designation is required").required(),
        dateOfBirth: Joi.date().label("Date of birth is required").required(),
        address: Joi.string().label("Address is required").required(),
        pincode: Joi.string().label("Pincode is required").required().max(6),
        cityId: Joi.number().label("City is required").required(),
        stateId: Joi.number().label("State is required").required(),
        profilePicture: Joi.number().label("Profile picture is required").required(),
        experience: Joi.any(),
        dateOfJoining: Joi.date(),
        staffClassBatchSubject: Joi.array(),
        accessRights: Joi.array(),
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        // Birth date validation
        let getStaffAge = await getAge(req.body.dateOfBirth);

        if (StaffAge.MIN <= getStaffAge) {
            req.body = value;
            next();
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.STAFF_DOB_VALIDATION));
        }
    }
}

module.exports.staffUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        salutation: Joi.string().label("Salutation is required").required(),
        profilePicture: Joi.number().label("Profile picture is required").required(),
        employeeCode: Joi.string().label("Employee code is required").required(),
        email: Joi.string().label("Email is required").required(),
        firstName: Joi.string().label("First Name is required").required(),
        lastName: Joi.string().label("Last Name is required").required(),
        gender: Joi.string().label("Gender is required").required(),
        contactNo: Joi.string().label("Contact number is required").required(),
        designation: Joi.string().label("Designation is required").required(),
        dateOfBirth: Joi.date().label("Date of birth is required").required(),
        address: Joi.string().label("Address is required").required(),
        pincode: Joi.string().label("Pincode is required").required().max(6),
        cityId: Joi.number().label("City is required").required(),
        stateId: Joi.number().label("State is required").required(),
        experience: Joi.any(),
        dateOfJoining: Joi.date(),
        staffClassBatchSubject: Joi.array(),
        accessRights: Joi.array(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        // Birth date validation
        let getStaffAge = await getAge(req.body.dateOfBirth);

        if (StaffAge.MIN <= getStaffAge) {
            req.body = value;
            next();
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.STAFF_DOB_VALIDATION));
        }
    }
}

module.exports.staffAssignSubjectSchema = async (req, res, next) => {
    const schema = Joi.object({
        staffId: Joi.number().label("Staff Id required").required(),
        subjectId: Joi.number().label("Subject Id required").required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}