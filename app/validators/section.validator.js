const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
module.exports.sectionCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        sectionName: Joi.string().label("Section Name is required").required()
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.sectionUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        sectionName: Joi.string().label("Section Name is missing"),
        status: Joi.boolean()
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}