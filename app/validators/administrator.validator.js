const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

module.exports.administratorCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().label("Email is required").required(),
        administratorName: Joi.string().label("Administrator Name is required").required(),
        designation:  Joi.string().allow("")
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.administratorUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        id: Joi.number().label("Id is required").required(),
        administratorName: Joi.string().label("Administrator Name is required").required(),
        designation:  Joi.string().allow("")
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.administratorOTPSendSchema = async (req, res, next) => {
    const schema = Joi.object({
        id: Joi.number().label("Id is required").required(),
        email: Joi.string().label("Email is required").required(),
        administratorName: Joi.string().label("Administrator Name is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.administratorOTPVarifySchema = async (req, res, next) => {
    const schema = Joi.object({
        otp: Joi.number().label("OTP is required").required(),
        id:  Joi.number().label("Id is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.administratorOTPVarifyUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        otp: Joi.number().label("OTP is required").required(),
        id:  Joi.number().label("Id is required").required(),
        email: Joi.string().label("Email is required").required(),
    });

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

