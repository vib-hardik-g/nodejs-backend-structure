const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message,typeOfQuestions } = require('../../utils/commonMessages');

module.exports.doubtCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        subjectId: Joi.number().label("Subject is required").required(),
        topicId: Joi.number().label("Chapter is required").required(),
        typeOfQuestion: Joi.string().label("Type of Question is required").required(),
        title: Joi.string().label("Title is required").required(),
        question: Joi.string().label("Question is required").required(),
        options : Joi.alternatives().conditional("typeOfQuestion", { is: typeOfQuestions.Objective, then: Joi.array().items().min(2).required(), otherwise: Joi.optional() }).label("Please add two or more options"),
        documents : Joi.array()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.respondCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        doubtId: Joi.number().label("Doubt Id is required").required(),
        typeOfQuestion: Joi.string().label("Type of Question is required").required(),
        optionId: Joi.alternatives().conditional("typeOfQuestion", { is: typeOfQuestions.Objective, then: Joi.number().required(), otherwise: Joi.optional() }).label("Please select any one option"),
        respond: Joi.alternatives().conditional("typeOfQuestion", { is: typeOfQuestions.Subjective, then: Joi.string().required(), otherwise: Joi.optional() }).label("Respond is required"),
        documents : Joi.array()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.doubtLikeUnlikeSchema = async (req, res, next) => {
    const schema = Joi.object({
        doubtId: Joi.number().label("Doubt Id is required").required(),
        isLike: Joi.boolean()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.respondLikeUnlikeSchema = async (req, res, next) => {
    const schema = Joi.object({
        doubtId: Joi.number().label("Doubt Id is required").required(),
        respondId : Joi.number().label("Respond Id is required").required(),
        typeOfQuestion: Joi.string().label("Type of Question is required").required()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.approveCorrectSchema = async (req, res, next) => {
    const schema = Joi.object({
        doubtId: Joi.number().label("Doubt Id is required").required(),
        respondId : Joi.number().label("Respond Id is required").required(),
        isLike: Joi.boolean()
    });

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}