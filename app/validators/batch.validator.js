const Joi = require('joi');
const commonResponse = require('../../utils/commonResponse');
const { Message } = require('../../utils/commonMessages');
module.exports.batchCreateSchema = async (req, res, next) => {
    const schema = Joi.object({
        data: Joi.array().items(Joi.object({
            batchName: Joi.string().label("Batch Name is required").required(),
            classId: Joi.number().label("Class id is required").required(),
            subjectIds: Joi.array().items().label("Subject ids are required").required(),
        })).label('Data is required').required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}

module.exports.batchUpdateSchema = async (req, res, next) => {
    const schema = Joi.object({
        batchName: Joi.string().label("Batch Name is missing"),
        status: Joi.boolean(),
        subjectIds: Joi.array().items().required(),
    });
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    // validate request body against schema
    const { error, value } = schema.validate(req.body, options);

    if (error) {
        let errors = {};
        error.details.forEach(err => {
            errors[err.context.key] = err.context.label
        });
        return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errors));
    } else {
        req.body = value;
        next();
    }
}