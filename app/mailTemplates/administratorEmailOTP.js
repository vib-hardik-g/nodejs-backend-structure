const sendmail = require("../../utils/mailHandler");
module.exports = async function (email, userName, otp) {

    var mailOptions = await sendmail({
        to: email,
        subject: 'Agorae - OTP',
        html: `Hello, ${userName}
            <br/><br/>
            OTP for updation administrator email is <b>${otp}</b>.`,
    });
    if (mailOptions) {
        return mailOptions
    }
}