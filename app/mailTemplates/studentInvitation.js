const sendmail = require("../../utils/mailHandler");
module.exports = async function (email, userName) {

    var mailOptions = await sendmail({
        to: email,
        subject: 'Agorae - Student Invitation',
        html: `Hello, ${userName}
            <br/><br/>
            You are invited as student. Please download Agorae APP. 
            <br/>Using your email: <b>${email}</b> reset your password.
            
            <br/><br/> Thank You, <br/> Agorae Support Team`,
    });
    if (mailOptions) {
        return mailOptions
    }
}