const sendmail = require("../../utils/mailHandler");
const ADMIN_SET_PASSWORD_URL = process.env.ADMIN_SET_PASSWORD_URL;
module.exports = async function(email,token)  {
    
    var mailOptions = await sendmail ({
        to: email,  
        subject: 'Agorae - Reset Password',
        html: `Hi <br> Link for setting password is <br/> <a href="${ADMIN_SET_PASSWORD_URL}/${token}">${ADMIN_SET_PASSWORD_URL}/${token}</a>`,
    });
    if(mailOptions){
        return mailOptions
    }
}  