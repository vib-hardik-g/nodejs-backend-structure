const sendmail = require("../../utils/mailHandler");
module.exports = async function(email,otp)  {
    
    var mailOptions = await sendmail ({
        to: email,  
        subject: 'Agorae - OTP',
        html: `Hi <br> OTP for resetting password is  ${otp}`,
    });
    if(mailOptions){
        return mailOptions
    }
}  