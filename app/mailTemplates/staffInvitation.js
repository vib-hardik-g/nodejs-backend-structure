const sendmail = require("../../utils/mailHandler");
module.exports = async function (email, userName) {

    var mailOptions = await sendmail({
        to: email,
        subject: 'Agorae - Staff Invitation',
        html: `Hello, ${userName}
            <br/><br/>
            You are invited as staff. 
            <br/>Using your email: <b>${email}</b> reset your password.
            
            <br/><br/> Thank You, <br/> Agorae Support Team`,
    });
    if (mailOptions) {
        return mailOptions
    }
}