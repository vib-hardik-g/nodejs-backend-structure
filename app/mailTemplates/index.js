module.exports = {
    forgotPassword: require("./forgotPassword"),
    setPassword: require("./setPassword"),
    studentInvitation: require("./studentInvitation"),
    staffInvitation: require("./staffInvitation"),
    administratorEmailOTP: require("./administratorEmailOTP"),
}