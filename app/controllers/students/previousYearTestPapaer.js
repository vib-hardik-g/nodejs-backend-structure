const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Get feedback form
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllTestPapers = async (req, res) => {
    try {
        await models.userStudentMapping.sync({ force: false });
        await models.previousYearTestPaper.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.document.sync({ force: false });
        await models.examType.sync({ force: false });

        let { studentId } = req.authData.data;
        let { classId } = req.params;

        let student = await models.userStudentMapping.findOne({
            where: {
                studentId: studentId,
                status: true
            },
            attributes: ['instituteId']
        });

        if (student) {
            let instituteId = student.instituteId;

            let whereClause = {
                instituteId,
                classId,
                status: true,
                isPublished: true
            };

            if (req.query.year) {
                whereClause.year = req.query.year;
            }
            if (req.query.examTypeId) {
                whereClause.examTypeId = req.query.examTypeId;
            }
            if (req.query.subjectId) {
                whereClause.subjectId = req.query.subjectId;
            }

            let testPapers = await models.previousYearTestPaper.findAll({
                where: whereClause,
                attributes: ["year", "updatedAt"],
                include: [
                    {
                        model: models.document,
                        attributes: [['fullPath', 'testPaper'], 'fileName']
                    },
                    {
                        model: models.subject,
                        attributes: ['subjectName']
                    },
                    {
                        model: models.examType,
                        attributes: ['examType']
                    }
                ]
            });

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, testPapers));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};