const { Op } = require('sequelize');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Get feedback form
 * @param  {} req
 * @param  {} res
 */
module.exports.getReportCard = async (req, res) => {
    try {
        await models.userStudentMapping.sync({ force: false });
        await models.testResultReportCardStudent.sync({ force: false });
        await models.testResultReportCard.sync({ force: false });

        let { studentId } = req.authData.data;

        let student = await models.userStudentMapping.findOne({
            where: {
                studentId: studentId,
                status: true
            },
            attributes: ['instituteId']
        });

        if (student) {
            let instituteId = student.instituteId;

            let mainWhereClause = {
                instituteId,
                status: true,
                isPublished: true
            };

            if (req.query.searchKey) {
                mainWhereClause.term =  { [Op.iLike]: '%' + req.query.searchKey + '%' };
            }

            let reportCards = await models.testResultReportCardStudent.findAll({
                where: {
                    studentId,
                    status: true,
                    isPublished: true
                },
                attributes: ["updatedAt"],
                include: [
                    {
                        model: models.document,
                        attributes: [['fullPath', 'reportCard'], 'fileName']
                    },
                    {
                        model: models.testResultReportCard,
                        where: mainWhereClause,
                        attributes: ['term'],
                        required: true,
                    }
                ]
            });

            if (reportCards && reportCards.length > 0) {
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, reportCards));
            } else {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND, []));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};