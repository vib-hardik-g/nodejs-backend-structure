const asyncL = require('async');
const { Op } = require('sequelize');
const Excel = require('exceljs');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
var htmlPdf = require('html-pdf');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormStatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Get feedback form
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllTestResults = async (req, res) => {
    try {
        await models.testResultTemplate.sync({ force: false });
        await models.studentTestResult.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });

        let { studentId } = req.authData.data;

        let getStudentResults = await models.studentTestResult.findAll({
            where: {
                studentId: studentId
            },
            attributes: ['testResultTemplateId', 'studentId'],
            group: ['testResultTemplateId', 'studentId']
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, getStudentResults));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};