const asyncL = require('async');
const { Op } = require('sequelize');
const Excel = require('exceljs');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
var htmlPdf = require('html-pdf');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormStatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Get feedback form
 * @param  {} req
 * @param  {} res
 */
module.exports.getFeedbackForm = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });
        await models.feedbackForm.sync({ force: false });
        await models.feedbackFormClassBatch.sync({ force: false });

        let { instituteId, classId, batchId } = req.body;

        let response = await models.feedbackForm.findOne({
            where: { instituteId, formStatus: FeedbackFormStatus.Ongoing, status: true },
            attributes: ['feedbackCycle'],
            include: [
                {
                    model: models.feedbackFormClassBatch,
                    attributes: [],
                    where: {
                        classId,
                        batchId
                    },
                    required: true
                },
                {
                    model: models.feedbackTemplate,
                    attributes: ['templateName'],
                    include: [
                        {
                            model: models.feedbackTemplateCategory,
                            attributes: ['id', 'categoryName']
                        }
                    ]
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.feedbackTemplate.feedbackTemplateCategories && response.feedbackTemplate.feedbackTemplateCategories.length > 0) {
                for (var i = 0; i < response.feedbackTemplate.feedbackTemplateCategories.length; i++) {
                    let element = response.feedbackTemplate.feedbackTemplateCategories[i];
                    let categoryId = element.id;

                    response.feedbackTemplate.feedbackTemplateCategories[i].benchmark = await models.feedbackTemplateBenchmark.findAll({
                        where: {
                            feedbackTemplateCategoryId: categoryId
                        },
                        attributes: ['id', 'benchmark', 'points']
                    });

                    response.feedbackTemplate.feedbackTemplateCategories[i].parameters = await models.feedbackTemplateCategoryParameter.findAll({
                        where: {
                            feedbackTemplateCategoryId: categoryId
                        },
                        attributes: ['id', 'parameter']
                    });
                }
            }

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Save Submitted Feedback Form Response
 * @param  {} req
 * @param  {} res
 */
module.exports.saveSubmittedFeedbackResponse = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });
        await models.submittedFeedbackResponse.sync({ force: false });

        let { feedbackFormId, subjectId, batchId, staffId, studentId, submittedFeedbacks } = req.body;

        if (submittedFeedbacks && submittedFeedbacks.length > 0) {
            submittedFeedbacks = JSON.parse(JSON.stringify(submittedFeedbacks));
        }

        // Check submitted feedback
        let checkResponse = await models.submittedFeedbackForm.count({
            where: {
                feedbackFormId,
                subjectId,
                batchId,
                staffId,
                studentId
            }
        });

        if (checkResponse > 0) {
            return res.send(await commonResponse.response(false, Message.ALREADY_SUBMITTED_RESPONSE));
        }

        // Create submitted feedback
        let submittedFeedbackForm = await models.submittedFeedbackForm.create({
            feedbackFormId,
            subjectId,
            batchId,
            staffId,
            studentId
        });

        if (submittedFeedbackForm) {
            let submittedFeedbackFormId = submittedFeedbackForm.id;
            let feedbackResponseData = [];

            submittedFeedbacks.map(element => {
                let categoryId = element.categoryId;
                element.answers.map(async (answer) => {

                    feedbackResponseData.push({
                        submittedFeedbackFormId,
                        categoryId,
                        benchmarkId: answer.benchmarkId,
                        parameterId: answer.parameterId
                    });
                });
            });

            // Check submitted feedback responses
            await models.submittedFeedbackResponse.bulkCreate(feedbackResponseData);

            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};