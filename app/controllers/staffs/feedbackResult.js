const { Op } = require('sequelize');
const sequelize = require('sequelize');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormStatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Get all feedback cycles by staff
 * @param  {} req
 * @param  {} res
 */
module.exports.getFeedbackCyclesByStaff = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });
        await models.feedbackForm.sync({ force: false });

        let { staffId } = req.params;

        let feedbackCycles = await models.submittedFeedbackForm.findAll({
            where: {
                staffId
            },
            attributes: ['staffId', 'batchId', 'subjectId'],
            include: [{
                model: models.feedbackForm,
                attributes: ['id', 'feedbackCycle']
            }],
            group: ['staffId', 'batchId', 'subjectId', 'feedbackForm.id']
        });

        return res.json(await commonResponse.response(true, Message.DATA_FOUND, feedbackCycles));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get all subjects of feedback cycles
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllSubjectsOfFeedbackCycles = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });
        await models.feedbackForm.sync({ force: false });

        let { feedbackFormId, staffId, searchKey } = req.body;

        let subjectWhereClause = {};

        if (searchKey) {
            subjectWhereClause = {
                subjectName: { [Op.iLike]: '%' + searchKey + '%' }
            }
        }

        let feedbackSubjects = await models.submittedFeedbackForm.findAll({
            where: {
                feedbackFormId,
                staffId
            },
            attributes: ['staffId', 'batchId', 'subjectId', 'feedbackFormId'],
            include: [{
                model: models.subject,
                attributes: ['subjectName'],
                where: subjectWhereClause
            }],
            group: ['staffId', 'batchId', 'subjectId', 'feedbackFormId', 'subject.id']
        });

        return res.json(await commonResponse.response(true, Message.DATA_FOUND, feedbackSubjects));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description View feedback result
 * @param  {} req
 * @param  {} res
 */
 module.exports.viewFacultyFeedback = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });

        let { staffId, feedbackFormId } = req.body;

        let submittedfeedbackForm = await models.submittedFeedbackForm.findOne({
            attributes: ['feedbackFormId', 'staffId', 'subjectId', 'batchId'],
            where: {
                staffId, feedbackFormId
            },
            include: [
                {
                    model: models.feedbackForm,
                    attributes: ['feedbackCycle']
                },
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ],
            group: ['feedbackForm.id','submittedFeedbackForm.feedbackFormId', 'submittedFeedbackForm.staffId', 'subjectId', 'batchId', 'subject.id', 'batch.id'],
        });

        if (submittedfeedbackForm) {
            submittedfeedbackForm = JSON.parse(JSON.stringify(submittedfeedbackForm));

            // No. of Student who gave feedback
            let submittedFeedbacks = await models.submittedFeedbackForm.findAll({
                attributes: ['id'],
                where: {
                    feedbackFormId,
                    subjectId: submittedfeedbackForm.subjectId,
                    batchId: submittedfeedbackForm.batchId,
                    staffId,
                    status: true
                }
            });

            let categoryPercentages = [];
            let submittedFeedbackFormIds = submittedFeedbacks.map(data => data.id);
            let totalStudents = submittedFeedbacks.length;

            let totalOfBenchmarks = await models.submittedFeedbackResponse.findAll({
                attributes: [[sequelize.fn('SUM', sequelize.col('feedbackTemplateBenchmark.points')), 'totalFeedbacks']],
                where: {
                    submittedFeedbackFormId: submittedFeedbackFormIds
                },
                include: [
                    {
                        model: models.feedbackTemplateBenchmark,
                        attributes: ["id", "points"]
                    },
                    {
                        model: models.feedbackTemplateCategory,
                        attributes: ["id", "categoryName"]
                    },
                    {
                        model: models.submittedFeedbackForm,
                        attributes: ["id"],
                        include: [
                            {
                                model: models.feedbackForm,
                                attributes: ["feedbackTemplateId"]
                            }
                        ]
                    }
                ],
                group: ['categoryId', 'benchmarkId', 'feedbackTemplateBenchmark.id', 'feedbackTemplateCategory.id', 'submittedFeedbackForm.id', 'submittedFeedbackForm->feedbackForm.id']
            });

            totalOfBenchmarks = JSON.parse(JSON.stringify(totalOfBenchmarks));

            for (var j = 0; j < totalOfBenchmarks.length; j++) {
                let catElement = totalOfBenchmarks[j];

                let categoryName = catElement.feedbackTemplateCategory.categoryName;
                let categoryId = catElement.feedbackTemplateCategory.id;
                let secureMarks = parseInt(catElement.totalFeedbacks) * parseInt(catElement.feedbackTemplateBenchmark.points);

                let findCategory = categoryPercentages.find((element) => element.categoryName == categoryName);

                if (findCategory && findCategory != undefined) {
                    let key = categoryPercentages.findIndex((element) => element.categoryName == categoryName);
                    key = Math.abs(key);

                    categoryPercentages[key].secureMarks += secureMarks;
                } else {
                    categoryPercentages.push({
                        categoryId,
                        categoryName,
                        secureMarks,
                        feedbackTemplateId: catElement.submittedFeedbackForm.feedbackForm.feedbackTemplateId
                    });
                }
            }

            let chartData = [];

            for (var i = 0; i < categoryPercentages.length; i++) {
                let element = categoryPercentages[i];

                let templateDetail = await models.feedbackTemplate.findAll({
                    attributes: ['id'],
                    where: {
                        id: element.feedbackTemplateId,
                    },
                    include: [{
                        model: models.feedbackTemplateCategory,
                        where: {
                            id: element.categoryId
                        },
                        attributes: ['id'],
                        include: [
                            {
                                model: models.feedbackTemplateBenchmark,
                                attributes: ['points']
                            },
                            {
                                model: models.feedbackTemplateCategoryParameter,
                                attributes: ['id']
                            }
                        ]
                    }]
                });

                templateDetail = JSON.parse(JSON.stringify(templateDetail));

                let benchmarkPoints = templateDetail.map(data => {
                    let element = data.feedbackTemplateCategories[0];
                    let parameters = element.feedbackTemplateCategoryParameters.length;
                    let points = element.feedbackTemplateBenchmarks.map(data => data.points);
                    return {
                        maxPoints: Math.max(...points),
                        totalParams: parameters
                    };
                });

                let maxPoints = parseInt(benchmarkPoints[0].maxPoints);
                let totalParameters = parseInt(benchmarkPoints[0].totalParams);

                let maximumMarks = (totalStudents * maxPoints * totalParameters);
                let securedMarks = parseFloat(element.secureMarks);

                let categoryPercentage = maximumMarks * 100 / securedMarks;

                chartData.push({
                    categoryName: element.categoryName,
                    percentage: parseFloat(parseFloat(categoryPercentage).toFixed(2))
                })
            }

            submittedfeedbackForm.chartResult = chartData;

            return res.json(await commonResponse.response(true, Message.DATA_FOUND, submittedfeedbackForm));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};