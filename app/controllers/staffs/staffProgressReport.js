const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.submitStaffProgressReport = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });

        let { staffId, instituteId } = req.authData.data;
        let { classId, batchId, subjectId, topicDetailsId, subTopicId, date, actualHours } = req.body;
        let count = await models.staffProgressReport.count({
            where: {
                instituteId,
                staffId,
                classId,
                batchId,
                subjectId,
                topicDetailsId,
                subTopicId
            }
        });
        if (count) {
            // update only the date and actual hours
            await models.staffProgressReport.update({
                date,
                actualHours
            }, {
                where: {
                    instituteId,
                    staffId,
                    classId,
                    batchId,
                    subjectId,
                    topicDetailsId,
                    subTopicId
                }
            });
        } else {
            // create new record
            await models.staffProgressReport.create({
                instituteId,
                staffId,
                classId,
                batchId,
                subjectId,
                topicDetailsId,
                subTopicId,
                date,
                actualHours
            });
        }
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listBatchSubjects = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });

        let { staffId, instituteId } = req.authData.data;

        let response = await models.staffClassMapping.findAll({
            attributes: [],
            where: {
                status: true,
                staffId
            },
            include: [{
                model: models.staffClassBatchMapping,
                attributes: ['id'],
                required: true,
                include: [{
                    model: models.staffClassBatchSubjectMapping,
                    attributes: ['id'],
                    include: [{
                        model: models.subject,
                        attributes: ['id', 'subjectName']
                    }]
                }, {
                    model: models.batch,
                    attributes: ['id', 'batchName'],
                }]
            }, {
                model: models.class,
                attributes: ['id', 'className'],
            }]
        });

        // format data
        let result = [];
        if(response.length > 0){
            response.forEach((row)=>{
                result.push({
                    classId: row.class.id,
                    className: row.class.className,
                    batchId: row.staffClassBatchMappings[0].batch.id,
                    batchName: row.staffClassBatchMappings[0].batch.batchName,
                    subjectId: row.staffClassBatchMappings[0].staffClassBatchSubjectMappings[0].subject.id,
                    subjectName: row.staffClassBatchMappings[0].staffClassBatchSubjectMappings[0].subject.subjectName
                });
            });
        }
        let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listTopicsBySubjectId = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });

        let { subjectId } = req.params;
        let { instituteId } = req.authData.data;

        let where = {
            status: true,
        };
        if (req.query.searchKey) {
            where = {
                status: true,
                chapter: { [Op.iLike]: '%' + req.query.searchKey + '%' }
            }
        }
        let response = await models.topic.findAll({
            where: {
                instituteId,
                subjectId,
                status: true,
            },
            attributes: [],
            include: [{
                model: models.topicDetails,
                where: where,
                attributes: ['id', 'chapter']
            }]
        });
        let result = [];
        if(response.length > 0){
            result = response[0].topicDetails;
        }
        let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listSubTopicsByTopicDetailsId = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });

        let { staffId, instituteId } = req.authData.data;
        let { topicDetailsId } = req.params;

        let response = await models.topicDetails.findAll({
            where: {
                status: true,
                id: topicDetailsId
            },
            attributes: ['id', 'chapter'],
            include: [{
                model: models.subTopic,
                where: {
                    status: true,
                },
                attributes: ['id', 'subTopicName', 'allotedHours'],
                include: [{
                    model: models.staffProgressReport,
                    required: false,
                    where: {
                        staffId,
                        instituteId,
                    },
                    attributes: ['actualHours', 'date']
                }]
            }]
        });
        let result = [];
        if(response.length > 0){
            result = response[0];
        }
        let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.summaryByTopicDetailsId = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });

        let { staffId, instituteId } = req.authData.data;
        let { topicDetailsId } = req.params;

        let data = await models.subTopic.findAll({
            where: {
                status: true,
                topicDetailsId,
            },
            attributes: ['allotedHours'],
            include: [{
                model: models.staffProgressReport,
                required: false,
                where: {
                    instituteId,
                    staffId,
                    status: true,
                },
                attributes: ['actualHours', 'date'],
            }],
        });

        let totalAllotedHours = 0;
        let totalActualHours = 0;
        let dates = [];
        let checkAllCompleted = 0;
        data.forEach(item => {
            totalAllotedHours += item.allotedHours;
            if (item.staffProgressReports.length > 0) {
                totalActualHours += item.staffProgressReports[0].actualHours;
                dates.push(item.staffProgressReports[0].date);
                checkAllCompleted++;
            }
        });

        let maxDate = dates.length > 0 ? dates.reduce(function (a, b) { return a > b ? a : b; }) : "";
        let result = {
            totalAllotedHours,
            totalActualHours,
            slippageTime: (totalActualHours - totalAllotedHours) > 0 ? totalActualHours - totalAllotedHours : 0,
            completdOn: (data.length == checkAllCompleted) ? maxDate : "",
        }
        let msg = data.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}