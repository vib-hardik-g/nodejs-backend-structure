const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const {
  Message,
  Constant,
  Generalstatus,
  Attendance,
  Publishstatus,
} = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const { Op } = require("sequelize");
const moment = require("moment");
const {
  generateAssessmentPdfHtmlTemplate,
  generateAssessmentResultPdfHtmlTemplate,
} = require("../../../utils/pdfGenerator");
const sequelize = models.sequelize;
const htmlPdf = require("html-pdf");

module.exports.createAssessment = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;
    let {
      assessmentName,
      classId,
      batchIds,
      groupIds,
      subjects,
      duration,
      startDate,
      startTime,
      correctMark,
      incorrectMark,
    } = req.body;

    let tempDate = moment(startDate, "YYYY-MM-DD", true).format("YYYY-MM-DD");
    let momentTimestamp = parseInt(
      moment(tempDate + " " + startTime)
        .tz("Asia/Kolkata")
        .format("x")
    );
    momentTimestamp += parseInt(duration * 60 * 1000);
    let endDate = moment(momentTimestamp).format("YYYY-MM-DD");
    let endTime = moment(momentTimestamp).format("HH:mm");

    // number of questions validation
    for (let subject of subjects) {
      let noOfQuestions = subject.noOfQuestions;
      let chapterNoOfQuestions = 0;
      for (let chapter of subject.chapters) {
        chapterNoOfQuestions += chapter.noOfQuestions;
      }
      if (noOfQuestions !== chapterNoOfQuestions) {
        return res
          .status(400)
          .json(
            await commonResponse.response(
              false,
              `Subject's total number of questions are not equal to the summation of chapter's total number of questions`
            )
          );
      }
    }
    let assessment = await models.assessment.create({
      instituteId,
      assessmentName,
      classId,
      duration,
      startDate,
      startTime,
      endDate,
      endTime,
      correctMark,
      incorrectMark,
    });
    if (batchIds) {
      batchIds.forEach(async (batchId) => {
        await models.batchAssessmentMapping.create({
          batchId,
          assessmentId: assessment.id,
        });
      });
    }
    if (groupIds) {
      groupIds.forEach(async (groupId) => {
        await models.groupAssessmentMapping.create({
          groupId,
          assessmentId: assessment.id,
        });
      });
    }

    // subject chapter
    for (let subject of subjects) {
      let subjectAssessmentMapping =
        await models.subjectAssessmentMapping.create({
          assessmentId: assessment.id,
          subjectId: subject.subjectId,
          noOfQuestions: subject.noOfQuestions,
        });
      for (let chapter of subject.chapters) {
        await models.subjectChapterAssessmentMapping.create({
          topicDetailsId: chapter.topicDetailsId,
          noOfQuestions: chapter.noOfQuestions,
          subjectAssessmentMappingId: subjectAssessmentMapping.id,
        });

        // find questions from question bank & insert into assessment questions
        let questionBank = await models.questionBank.findOne({
          where: {
            instituteId,
            classId,
            subjectId: subject.subjectId,
            topicDetailsId: chapter.topicDetailsId,
            status: true,
          },
          include: [
            {
              model: models.questionBankQuestion,
              where: {
                status: true,
              },
              limit: chapter.noOfQuestions,
              order: sequelize.random(),
              attributes: ["question"],
              include: [
                {
                  model: models.questionBankQuestionOption,
                  attributes: ["option", "isCorrect"],
                },
              ],
            },
          ],
        });

        if (questionBank) {
          for (let question of questionBank.questionBankQuestions) {
            let questionCreated = await models.assessmentQuestion.create({
              assessmentId: assessment.id,
              question: question.question,
              subjectId: subject.subjectId,
              topicDetailsId: chapter.topicDetailsId,
            });
            for (let option of question.questionBankQuestionOptions) {
              await models.assessmentQuestionOption.create({
                assessmentQuestionId: questionCreated.id,
                option: option.option,
                isCorrect: option.isCorrect,
              });
            }
          }
        }
      }
    }
    return res.json(await commonResponse.response(true, Message.DATA_CREATED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.allAssessments = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;

    if (req.query.pageSize) {
      Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
    }
    if (req.query.pageNo) {
      Constant.DEFAULT_PAGE =
        parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
    }
    let orderBy = [];
    if (req.query.sortBy && req.query.orderBy) {
      orderBy.push([req.query.sortBy, req.query.orderBy]);
    } else {
      orderBy.push(["id", "DESC"]);
    }

    let where = {
      status: true,
      instituteId,
    };
    if (req.query.searchKey) {
      where.assessmentName = { [Op.iLike]: "%" + req.query.searchKey + "%" };
    }
    if (req.query.classId) {
      where.classId = req.query.classId;
    }
    subjectWhere = {};
    if (req.query.subjectIds) {
      let subjectIds = req.query.subjectIds.split(",");
      subjectWhere.subjectId = { [Op.in]: subjectIds };
    }
    let response = await models.assessment.findAndCountAll({
      distinct: true,
      where: where,
      order: orderBy,
      offset: parseInt(Constant.DEFAULT_PAGE),
      limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
      include: [
        {
          model: models.class,
          attributes: ["className"],
        },
        {
          model: models.batchAssessmentMapping,
          attributes: ["batchId"],
          include: [
            {
              model: models.batch,
              attributes: ["batchName"],
            },
          ],
        },
        {
          model: models.groupAssessmentMapping,
          attributes: ["groupId"],
          include: [
            {
              model: models.group,
              attributes: ["groupName"],
            },
          ],
        },
        {
          model: models.subjectAssessmentMapping,
          required: true,
          where: subjectWhere,
          attributes: ["subjectId", "noOfQuestions"],
          include: [
            {
              model: models.subject,
              attributes: ["subjectName"],
            },
          ],
        },
      ],
    });
    if (response.rows.length > 0) {
      response.rows = JSON.parse(JSON.stringify(response.rows));
      response.rows.forEach((row, index) => {
        let totalCountOfQuestions = 0;
        row.subjectAssessmentMappings.forEach((subject) => {
          totalCountOfQuestions += subject.noOfQuestions;
        });
        response.rows[index].totalCountOfQuestions = totalCountOfQuestions;
        response.rows[index].maxMarks = totalCountOfQuestions * row.correctMark;

        // status
        let nowTime = moment(new Date()).tz("Asia/kolkata").format("x");
        let startTime = parseInt(
          moment(row.startDate + " " + row.startTime)
            .tz("Asia/Kolkata")
            .format("x")
        );
        let endTime = parseInt(
          moment(row.endDate + " " + row.endTime)
            .tz("Asia/Kolkata")
            .format("x")
        );
        if (nowTime > startTime) {
          if (nowTime < endTime) {
            response.rows[index].currentStatus = "Ongoing";
          } else {
            response.rows[index].currentStatus = "Completed";
          }
        } else {
          response.rows[index].currentStatus = "Upcoming";
        }
      });
    }
    let result = {
      rows: response.rows,
      total: response.count,
    };
    let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
    return res.send(await commonResponse.response(true, msg, result));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.downloadAssessmentPDF = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { assessmentId } = req.params;
    let assessment = await models.assessment.findOne({
      where: { id: assessmentId, instituteId, status: true },
    });
    if (!assessment) {
      return res
        .status(404)
        .json(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }
    let html = await generateAssessmentPdfHtmlTemplate(assessmentId);
    let options = {
      format: "A4",
      orientation: "portrait",
      header: {
        height: "20px",
      },
      footer: {
        height: "20px",
      },
    };
    htmlPdf.create(html, options).toStream(async (err, stream) => {
      if (err) {
        return res
          .status(400)
          .send(
            await commonResponse.response(
              false,
              "Something went wrong with PDF creation."
            )
          );
      } else {
        res.set("Content-type", "application/pdf");
        stream.pipe(res);
      }
    });
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.getAssessmentById = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { assessmentId } = req.params;

    let response = await models.assessment.findOne({
      where: { id: assessmentId, instituteId, status: true },
      include: [
        {
          model: models.class,
          attributes: ["className"],
        },
        {
          model: models.batchAssessmentMapping,
          attributes: ["batchId"],
          include: [
            {
              model: models.batch,
              attributes: ["batchName"],
            },
          ],
        },
        {
          model: models.groupAssessmentMapping,
          attributes: ["groupId"],
          include: [
            {
              model: models.group,
              attributes: ["groupName"],
            },
          ],
        },
        {
          model: models.subjectAssessmentMapping,
          attributes: ["subjectId", "noOfQuestions"],
          include: [
            {
              model: models.subject,
              attributes: ["subjectName"],
            },
          ],
        },
        {
          model: models.assessmentQuestion,
          attributes: ["question"],
          include: [
            {
              model: models.assessmentQuestionOption,
              attributes: ["option", "isCorrect"],
            },
          ],
        },
      ],
    });
    if (response) {
      return res.send(
        await commonResponse.response(true, Message.DATA_FOUND, response)
      );
    } else {
      return res
        .status(404)
        .send(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.downloadAssessmentResultPDF = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { assessmentId } = req.params;

    let assessment = await models.assessment.findOne({
      where: { id: assessmentId, instituteId, status: true },
    });
    if (!assessment) {
      return res
        .status(404)
        .json(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }

    let html = await generateAssessmentResultPdfHtmlTemplate(assessmentId);
    let options = {
      format: "A4",
      orientation: "portrait",
      header: {
        height: "20px",
      },
      footer: {
        height: "20px",
      },
    };
    htmlPdf.create(html, options).toStream(async (err, stream) => {
      if (err) {
        return res
          .status(400)
          .send(
            await commonResponse.response(
              false,
              "Something went wrong with PDF creation."
            )
          );
      } else {
        res.set("Content-type", "application/pdf");
        stream.pipe(res);
      }
    });
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.allAssessmentResults = async (req, res) => {
  try {
    await models.questionBank.sync({ force: false });
    await models.questionBankQuestion.sync({ force: false });
    await models.questionBankQuestionOption.sync({ force: false });
    await models.assessment.sync({ force: false });
    await models.batchAssessmentMapping.sync({ force: false });
    await models.groupAssessmentMapping.sync({ force: false });
    await models.subjectAssessmentMapping.sync({ force: false });
    await models.subjectChapterAssessmentMapping.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });

    let { instituteId } = req.authData.data;

    if (req.query.pageSize) {
      Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
    }
    if (req.query.pageNo) {
      Constant.DEFAULT_PAGE =
        parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
    }
    let orderBy = [];
    if (req.query.sortBy && req.query.orderBy) {
      orderBy.push([req.query.sortBy, req.query.orderBy]);
    } else {
      orderBy.push(["id", "DESC"]);
    }

    // filter only the assessments which has passed time
    let currentDate = moment().tz("Asia/kolkata").format("YYYY-MM-DD");
    let currentTime = moment().tz("Asia/kolkata").format("HH:mm");

    let where = {
      status: true,
      instituteId,
      [Op.or]: {
        endDate: {
          [Op.lt]: currentDate,
        },
        [Op.and]: {
          endDate: {
            [Op.lte]: currentDate,
          },
          endTime: {
            [Op.lte]: currentTime,
          },
        },
      },
    };

    if (req.query.searchKey) {
      where.assessmentName = { [Op.iLike]: "%" + req.query.searchKey + "%" };
    }
    if (req.query.classId) {
      where.classId = req.query.classId;
    }
    subjectWhere = {};
    if (req.query.subjectIds) {
      let subjectIds = req.query.subjectIds.split(",");
      subjectWhere.subjectId = { [Op.in]: subjectIds };
    }

    let response = await models.assessment.findAndCountAll({
      distinct: true,
      where: where,
      order: orderBy,
      offset: parseInt(Constant.DEFAULT_PAGE),
      limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
      include: [
        {
          model: models.class,
          attributes: ["className"],
        },
        {
          model: models.batchAssessmentMapping,
          attributes: ["batchId"],
          include: [
            {
              model: models.batch,
              attributes: ["batchName"],
            },
          ],
        },
        {
          model: models.groupAssessmentMapping,
          attributes: ["groupId"],
          include: [
            {
              model: models.group,
              attributes: ["groupName"],
            },
          ],
        },
        {
          model: models.subjectAssessmentMapping,
          required: true,
          where: subjectWhere,
          attributes: ["subjectId", "noOfQuestions"],
          include: [
            {
              model: models.subject,
              attributes: ["subjectName"],
            },
          ],
        },
      ],
    });
    if (response.rows.length > 0) {
      response.rows = JSON.parse(JSON.stringify(response.rows));
      response.rows.forEach((row, index) => {
        let totalCountOfQuestions = 0;
        row.subjectAssessmentMappings.forEach((subject) => {
          totalCountOfQuestions += subject.noOfQuestions;
        });
        response.rows[index].totalCountOfQuestions = totalCountOfQuestions;
        response.rows[index].maxMarks = totalCountOfQuestions * row.correctMark;
      });
    }
    let result = {
      rows: response.rows,
      total: response.count,
    };
    let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
    return res.send(await commonResponse.response(true, msg, result));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};
