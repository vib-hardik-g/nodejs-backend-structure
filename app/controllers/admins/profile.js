const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const { Message, Usertypes } = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const _ = require("lodash");

module.exports.getProfile = async (req, res) => {
  await models.user.sync({ force: false });
  await models.module.sync({ force: false });
  await models.userModuleMapping.sync({ force: false });

  let { data } = req.authData;
  let response;
  let userTypes = data.userTypes.split(",");
  if (_.includes(userTypes, Usertypes.SUPER_ADMIN)) {
    let user = await models.user.findOne({ where: { id: data.id } });
    response = {
      userName: user.name ? user.name : "",
      email: user.email,
      userType: Usertypes.SUPER_ADMIN,
    };
  } else if (
    _.includes(userTypes, Usertypes.ADMIN) ||
    _.includes(userTypes, Usertypes.STAFF)
  ) {
    // get data of the staff along with the institution
    let item = await models.userStaffMapping.findOne({
      where: { userId: data.id },
      include: [
        {
          model: models.user,
          include: [
            {
              model: models.userModuleMapping,
              attributes: ["permission"],
              include: [
                {
                  model: models.module,
                  attributes: ["moduleName"],
                },
              ],
            },
          ],
        },
        {
          model: models.staff,
          include: [
            {
              model: models.country,
            },
            {
              model: models.city,
            },
            {
              model: models.state,
            },
            {
              model: models.document,
            },
          ],
        },
        {
          model: models.institution,
          include: ["info", "organisation", "phones", "emails", "websites"],
        },
      ],
    });
    if (item) {
      // check if staff or admin
      let accessType;
      if (_.includes(userTypes, Usertypes.ADMIN)) {
        accessType = Usertypes.ADMIN;
      } else {
        accessType = Usertypes.STAFF;
      }
      let staffData = {
        userType: accessType,
        userName: item.user.userName
          ? item.user.userName
          : item.staff.firstName + " " + item.staff.lastName,
        staffId: item.staffId,
        userId: item.userId,
        userTypeId: item.user.userTypeId,
        email: item.user.email,
        firstName: item.staff.firstName,
        lastName: item.staff.lastName,
        designation: item.staff.designation,
        dob: item.staff.dob ? item.staff.dob : "",
        gender: item.staff.gender,
        bloodGroup: item.staff.bloodGroup,
        primaryPhoneNumber: item.staff.primaryPhoneNumber,
        address: item.staff.address,
        pincode: item.staff.pincode,
        aadharNumber: item.staff.aadharNumber,
        profilePic: item.staff.profilePic,
        bankName: item.staff.bankName,
        bankAccount: item.staff.bankAccount,
        bankIfsc: item.staff.bankIfsc,
        education: item.staff.education,
        experience: item.staff.experience,
        country: item.country ? item.country.countryName : "",
        state: item.state ? item.state.stateName : "",
        city: item.city ? item.city.cityName : "",
        institution: item.institution,
        profilePicture: item.staff.document ? item.staff.document : "",
      };
      let modules = item.user.userModuleMappings.map((x) => {
        return { moduleName: x.module.moduleName, permission: x.permission };
      });
      staffData.accessRights = modules;
      response = staffData;
    } else {
      return res
        .status(401)
        .json(await commonResponse.response(false, Message.UNAUTHORISED));
    }
  } else {
    return res
      .status(401)
      .json(await commonResponse.response(false, Message.UNAUTHORISED));
  }
  return res.send(
    await commonResponse.response(true, Message.DATA_FOUND, response)
  );
};
