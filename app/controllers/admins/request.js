const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.getStudentRequests = async (req, res) => {
    try {
        let { instituteId } = req.authData.data;
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let where = {
            status: true,
            instituteId
        };
        if (req.query.startDate && req.query.endDate) {
            where.updatedAt = { [Op.between]: [new Date(req.query.startDate).toISOString(), new Date(req.query.endDate + ' 23:59:59').toISOString()] };
        }
        if (req.query.classId) {

            let batchData = await models.userStudentMapping.findAll({
                attributes: ['batchId'],
                where: {
                    status: true,
                    classId: req.query.classId,
                    instituteId
                },
                group: ['batchId']
            })
            let mapData = batchData.map(val => {
                return val.batchId
            })
            where.batchId = mapData;
        }
        if (req.query.status) {
            where.approvedStatus = req.query.status;
        }
        if (req.query.searchKey) {
            where = {
                status: true,
                instituteId,
                [Op.or]: {
                    request: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$batch.batchName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    // '$topicDetails.chapter$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let response = await models.studentRequest.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            subQuery: false,
            include: [{
                model: models.studentRequestAttachment,
                include: [{
                    model: models.document
                }]
            }, {
                model: models.batch,
            }, {
                model: models.student,
                include: [{
                    model: models.studentBatchUniqueId,
                    attributes: ['uniqueId']
                }]
            }
            ],
        });
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        let result = {
            rows: response.rows,
            total: response.count
        }
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.getStudentRequestsById = async (req, res) => {
    try {
        let { instituteId } = req.authData.data;

        let response = await models.studentRequest.findOne({
            where: {
                id: req.params.id,
                instituteId
            },
            include: [{
                model: models.studentRequestAttachment,
                include: [{
                    model: models.document
                }]
            }, {
                model: models.batch,
            }, {
                model: models.student,
                include: [{
                    model: models.studentBatchUniqueId,
                    attributes: ['uniqueId']
                }]
            }
            ],
        });
        let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;

        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.approveOrRejectRequest = async (req, res) => {
    try {
        let { instituteId } = req.authData.data;
        let { requestType, approvedStatus, requestId } = req.body;
        if (requestType && requestType == "STUDENT") {

            let requestData = await models.studentRequest.findOne({
                where: {
                    id: requestId,
                    instituteId,
                    status: true
                }
            })
            if (requestData) {
                await requestData.update({ approvedStatus })
                if (approvedStatus == Generalstatus.APPROVED) {
                    return res.json(await commonResponse.response(true, Attendance.REQUEST_APPROVED));
                } else if (approvedStatus == Generalstatus.REJECTED) {
                    return res.json(await commonResponse.response(true, Attendance.REQUEST_REJECTED));
                } 

            } else {
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else if (requestType && requestType == "STAFF") {

            let requestData = await models.staffRequest.findOne({
                where: {
                    id: requestId,
                    instituteId,
                    status: true
                }
            })
            if (requestData) {
                await requestData.update({ approvedStatus })
                if (approvedStatus == Generalstatus.APPROVED) {
                    return res.json(await commonResponse.response(true, Attendance.REQUEST_APPROVED));
                } else if (approvedStatus == Generalstatus.REJECTED) {
                    return res.json(await commonResponse.response(true, Attendance.REQUEST_REJECTED));
                } 
            } else {
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else {
            return res.json(await commonResponse.response(false, Message.SOMETHING_WRONG));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));

    }
}

module.exports.getStaffRequests = async (req, res) => {
    try {
        let { instituteId } = req.authData.data;
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let where = {
            status: true,
            instituteId
        };
        if (req.query.startDate && req.query.endDate) {
            where.updatedAt = { [Op.between]: [new Date(req.query.startDate).toISOString(), new Date(req.query.endDate + ' 23:59:59').toISOString()] };
        }
        if (req.query.status) {
            where.approvedStatus = req.query.status;
        }
        if (req.query.searchKey) {
            where = {
                status: true,
                instituteId,
                [Op.or]: {
                    request: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$staff.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$staff.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    // '$topicDetails.chapter$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let response = await models.staffRequest.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            subQuery: false,
            include: [{
                model: models.staffRequestAttachment,
                include: [{
                    model: models.document
                }]
            }, {
                model: models.staff
            }
            ],
        });
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        let result = {
            rows: response.rows,
            total: response.count
        }
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.getStaffRequestsById = async (req, res) => {
    try {
        let { instituteId } = req.authData.data;

        let response = await models.staffRequest.findOne({
            where: {
                id: req.params.id,
                instituteId
            },
            include: [{
                model: models.staffRequestAttachment,
                include: [{
                    model: models.document
                }]
            }, {
                model: models.staff
            }
            ],
        });
        let msg = response ? Message.DATA_FOUND : Message.NO_DATA_FOUND;

        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}