const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;
const _ = require('lodash');
const excelGenerator = require('../../../utils/excelGenerator');
const fs = require('fs');
const path = require('path');
const xlsxFile = require('read-excel-file/node');
const { awsS3ServerToServerUpload } = require('../../../utils/awsS3Handler');
const htmlPdf = require('html-pdf');
const { excelToPDFConverter } = require('../../../utils/pdfGenerator');


module.exports.allQuestionBanks = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.subjectId) {
            where.subjectId = req.query.subjectId;
        }
        if (req.query.topicDetailsId) {
            where.topicDetailsId = req.query.topicDetailsId;
        }
        let where = {
            status: true,
            instituteId
        };
        if (req.query.searchKey) {
            where = {
                status: true,
                instituteId,
                [Op.or]: {
                    '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$topicDetails.chapter$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let response = await models.questionBank.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            subQuery: false,
            attributes: ['id', 'createdAt', 'updatedAt', [sequelize.fn('count', sequelize.col('questionBankQuestions.id')), 'noOfQuestions']],
            include: [{
                model: models.class,
                attributes: ['className']
            }, {
                model: models.subject,
                attributes: ['subjectName']
            }, {
                model: models.topicDetails,
                attributes: ['chapter']
            }, {
                model: models.questionBankQuestion,
                attributes: []
            }, {
                model: models.document,
                as: 'templateGenerated'
            }, {
                model: models.document,
                as: 'templateUploaded'
            }, {
                model: models.document,
                as: 'pdfGenerated'
            }],
            group: ['questionBank.id', 'class.id', 'subject.id', 'topicDetail.id', 'templateGenerated.id', 'templateUploaded.id', 'pdfGenerated.id']
        });
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        let result = {
            rows: response.rows,
            total: response.count.length
        }
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.createQuestionBank = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { classId, subjectId, topicDetailsId, noOfQuestions } = req.body;

        let [record, created] = await models.questionBank.findOrCreate({
            where: {
                instituteId,
                classId,
                subjectId,
                topicDetailsId,
                status: true
            },
            defaults: {
                instituteId,
                classId,
                subjectId,
                topicDetailsId
            }
        });
        if (created) {
            // generate excel template also : TODO
            let classDetails = await models.class.findOne({
                where: {
                    id: classId
                }
            });
            let subjectDetails = await models.subject.findOne({
                where: {
                    id: subjectId
                }
            });
            let topicDetails = await models.topicDetails.findOne({
                where: {
                    id: topicDetailsId
                }
            });
            let response = await excelGenerator.generateQuestionBankTemplate(noOfQuestions, classDetails.className, subjectDetails.subjectName, topicDetails.chapter);
            if (response) {
                // update the question bank with generated template id
                await models.questionBank.update({
                    generatedTemplate: response.id
                }, {
                    where: {
                        id: record.id
                    }
                });
                return res.json(await commonResponse.response(true, Message.DATA_CREATED, {
                    downloadUrl: response.fullPath
                }));
            } else {
                return res.status(400).json(await commonResponse.response(false, Message.EXCEL_GENERATION_FAILED));
            }
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.DATA_EXISTS));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteQuestionBank = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        // soft delete only
        await models.questionBank.update({
            status: false
        },
            {
                where: {
                    id
                }
            });
        await models.questionBankQuestion.update({
            status: false
        },
            {
                where: {
                    questionBankId: id
                }
            });
        let questionBankQuestions = await models.questionBankQuestion.findAll({
            where: {
                questionBankId: id
            },
            include: [{
                model: models.questionBankQuestionOption
            }]
        });
        questionBankQuestions.forEach(async (item) => {
            item.questionBankQuestionOptions.forEach(async (option) => {
                await models.questionBankQuestionOption.update({
                    status: false
                }, {
                    where: {
                        id: option.id
                    }
                })
            })
        });
        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.getQuestionBankById = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;
        let where = {
            questionBankId: id,
            status: true
        };
        if (req.query.searchKey) {
            where = {
                questionBankId: id,
                status: true,
                [Op.or]: {
                    question: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        let response = await models.questionBankQuestion.findAll({
            where: where,
            attributes: ['id', 'question'],
            include: [{
                model: models.questionBankQuestionOption,
                attributes: ['id', 'option', 'isCorrect']
            }, {
                model: models.questionBank,
                attributes: ['id'],
                include: [{
                    model: models.class,
                    attributes: ['id', 'className']
                }, {
                    model: models.subject,
                    attributes: ['id', 'subjectName']
                }, {
                    model: models.topicDetails,
                    attributes: ['id', 'chapter']
                }]
            }],
        });
        if (response.length > 0) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.createQuestions = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { questionBankId, questions } = req.body;

        questions.forEach(async (question) => {
            let questionCreated = await models.questionBankQuestion.create({
                questionBankId,
                question: question.question
            });
            question.options.forEach(async (option) => {
                let createData = {
                    questionBankQuestionId: questionCreated.id,
                    option: option.option,
                };
                if (option.isCorrect) {
                    createData.isCorrect = true;
                }
                await models.questionBankQuestionOption.create(createData);
            });
        });
        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteQuestion = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;

        await models.questionBankQuestionOption.destroy({
            where: {
                questionBankQuestionId: id
            }
        });
        await models.questionBankQuestion.destroy({
            where: {
                id
            }
        });
        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateQuestion = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;
        let { question, options } = req.body;

        await models.questionBankQuestion.update({
            question,
        }, {
            where: {
                id
            }
        });

        let existingOptions = await models.questionBankQuestionOption.findAll({
            where: {
                questionBankQuestionId: id
            }
        });
        if (existingOptions.length == options.length) {
            // update only
            options.forEach(async (option, index) => {
                models.questionBankQuestionOption.update({
                    option: option.option,
                    isCorrect: option.isCorrect
                }, {
                    where: {
                        id: existingOptions[index].id
                    }
                })
            });
        } else {
            // destroy & create
            await models.questionBankQuestionOption.destroy({
                where: {
                    questionBankQuestionId: id
                }
            });
            options.forEach(async (option) => {
                models.questionBankQuestionOption.create({
                    option: option.option,
                    isCorrect: option.isCorrect,
                    questionBankQuestionId: id
                })
            });
        }
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.uploadQuestionBankTemplate = async (req, res) => {
    try {
        await models.questionBank.sync({ force: false });
        await models.questionBankQuestion.sync({ force: false });
        await models.questionBankQuestionOption.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { questionBankId } = req.body;

        let findQuestionBank = await models.questionBank.findOne({
            where: {
                id: questionBankId,
                instituteId,
            },
            include: [{
                model: models.questionBankQuestion
            }]
        });
        if (!findQuestionBank) return res.status(404).json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        if (findQuestionBank.questionBankQuestions.length > 0) return res.status(400).json(await commonResponse.response(false, Message.ALREADY_UPLOADED_CANNOT_REUPLOAD));

        if (req.files.questionBankTemplate && req.files.questionBankTemplate.length > 0) {
            let questionBankExcel = req.files.questionBankTemplate[0];
            let filePath = path.join(questionBankExcel.destination, questionBankExcel.originalname);

            // parse the excel
            let schema = {
                'No.': {
                    prop: 'no',
                    type: Number,
                    required: true,
                },
                'Class': {
                    prop: 'className',
                    type: String,
                    required: true,
                },
                'Subject': {
                    prop: 'subjectName',
                    type: String,
                    required: true,
                },
                'Chapter': {
                    prop: 'chapter',
                    type: String,
                    required: true,
                },
                'Question Title': {
                    prop: 'question',
                    type: String,
                    required: true,
                },
                'Options': {
                    prop: 'options',
                    type: {
                        'Option 1': {
                            prop: 'option1',
                            type: String,
                            required: true,
                        },
                        'Option 2': {
                            prop: 'option2',
                            type: String,
                            required: true,
                        },
                        'Option 3': {
                            prop: 'option3',
                            type: String,
                            required: true,
                        },
                        'Option 4': {
                            prop: 'option4',
                            type: String,
                            required: true,
                        },
                    }
                },
                'Correct Answer': {
                    prop: 'answer',
                    type: String,
                    required: true,
                },
            };
            xlsxFile(filePath, { schema }).then(async ({ rows, errors }) => {
                if (errors.length == 0) {
                    if (rows && rows.length > 0) {
                        let errArr = [];
                        for (let i = 0; i < rows.length; i++) {
                            let optionsArr = [];
                            let options = rows[i].options;
                            Object.keys(options).map((key, index) => {
                                optionsArr.push(options[key]);
                            });
                            if (!_.includes(optionsArr, rows[i].answer)) {
                                errArr.push(`Row ${i + 1} column Answer doesn't match any of the options`);
                            }
                        }
                        if (errArr.length > 0) {
                            // unlink the file
                            fs.unlinkSync(filePath);

                            return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errArr));
                        }

                        // insert question answers now
                        for (let row of rows) {
                            let questionCreated = await models.questionBankQuestion.create({
                                questionBankId,
                                question: row.question,
                            });
                            let optionsArr = [];
                            let options = row.options;
                            Object.keys(options).map((key, index) => {
                                optionsArr.push(options[key]);
                            });
                            let mappings = optionsArr.map((option) => {
                                let isCorrect = false;
                                if (option == row.answer) {
                                    isCorrect = true;
                                }
                                return { questionBankQuestionId: questionCreated.id, option: option, isCorrect: isCorrect };
                            })
                            await models.questionBankQuestionOption.bulkCreate(mappings);
                        };

                        // upload the excel to server
                        const document = await awsS3ServerToServerUpload(filePath, 'question-bank-template-uploaded');
                        if (document) {
                            await models.questionBank.update({
                                uploadedTemplate: document.id
                            }, {
                                where: {
                                    id: questionBankId
                                }
                            });
                        }

                        // generate pdf from the excel and save to s3 and update the document id
                        excelToPDFConverter(filePath, 'question-bank-template-uploaded').then(async (doc)=>{
                            await models.questionBank.update({
                                generatedPDF: doc.id
                            }, {
                                where: {
                                    id: questionBankId
                                }
                            });
                        }).catch((err)=>{
                            console.log(err);
                        });

                        // unlink the file
                        fs.unlinkSync(filePath);

                        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
                    } else {
                        // unlink the file
                        fs.unlinkSync(filePath);

                        return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
                    }
                } else {
                    // unlink the file
                    fs.unlinkSync(filePath);

                    let errArr = [];
                    errors.forEach((err) => {
                        errArr.push(`Row ${err.row} column ${err.column} ${err.error}`)
                    })
                    return res.status(400).json(await commonResponse.response(false, Message.VALIDATION_ERROR, errArr));
                }
            });
        } else {
            return res.status(400).json(await commonResponse.response(false, Message.FILE_REQUIRED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}