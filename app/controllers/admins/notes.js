const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');

module.exports.createNote = async (req, res) => {
    try {
        await models.note.sync({ force: false });
        await models.batchNoteMapping.sync({ force: false });
        await models.noteAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { noteName, classId, subjectId, batchIds, noteFiles } = req.body;

        let note = await models.note.create({
            instituteId,
            classId,
            subjectId,
            noteName,
        });

        batchIds.forEach(async (batchId) => {
            await models.batchNoteMapping.create({
                instituteId,
                noteId: note.id,
                batchId
            });
        });

        noteFiles.forEach(async (noteFile) => {
            await models.noteAttachment.create({
                noteId: note.id,
                noteFile,
            })
        });
        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listAllNotes = async (req, res) => {
    try {
        await models.note.sync({ force: false });
        await models.batchNoteMapping.sync({ force: false });
        await models.noteAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                instituteId
            };
        } else {
            where = {
                noteName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                status: true,
                instituteId
            };
        }

        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.subjectId) {
            where.subjectId = req.query.subjectId;
        }

        let response = await models.note.findAndCountAll({
            distinct: true,
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            include: [{
                model: models.batchNoteMapping,
                attributes: ['batchId'],
                include: [{
                    model: models.batch,
                    attributes: ['batchName']
                }]
            }, {
                model: models.noteAttachment,
                attributes: ['noteFile'],
                include: [{
                    model: models.document
                }]
            }, {
                model: models.subject,
                attributes: ['subjectName'],
            }],
            attributes: ['id', 'noteName', 'publishStatus', 'createdAt', 'updatedAt']
        });
        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteNote = async (req, res) => {
    try {
        await models.note.sync({ force: false });
        await models.batchNoteMapping.sync({ force: false });
        await models.noteAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        // // delete the associated attachments
        // let attachments = await models.noteAttachment.findAll({
        //     where: {
        //         noteId: id,
        //     }
        // });
        // attachments.forEach(async (attachment) => {
        //     await awsS3Handler.deleteFile(attachment.noteFile);
        //     await models.noteAttachment.destroy({
        //         where: {
        //             id: attachment.id
        //         }
        //     })
        // });

        // // delete from mappings
        // await models.batchNoteMapping.destroy({
        //     where: {
        //         noteId: id
        //     }
        // });

        // // finally delete the note
        // await models.note.destroy({
        //     where: {
        //         id,
        //         instituteId
        //     }
        // });

        await models.note.update({
            status: false,
        }, {
            where: {
                instituteId,
                id
            }
        });

        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.publishNote = async (req, res) => {
    try {
        await models.note.sync({ force: false });
        await models.batchNoteMapping.sync({ force: false });
        await models.noteAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { noteId } = req.body;

        let response = await models.note.findOne({
            where: {
                id: noteId,
                instituteId
            }
        });
        let newStatus;
        if (response.publishStatus == Publishstatus.PENDING) {
            newStatus = Publishstatus.PUBLISHED;
        } else {
            newStatus = Publishstatus.REPUBLISHED;
        }
        await models.note.update({
            publishStatus: newStatus,
            updatedAt: new Date()
        }, {
            where: {
                instituteId,
                id: noteId,
            }
        });
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateNote = async (req, res) => {
    try {
        await models.note.sync({ force: false });
        await models.batchNoteMapping.sync({ force: false });
        await models.noteAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { noteName, classId, subjectId, batchIds, noteFiles } = req.body;
        let { id } = req.params;

        let existingNote = await models.note.findOne({
            where: {
                id,
                instituteId
            },
            include: [{
                model: models.noteAttachment
            }, {
                model: models.batchNoteMapping
            }]
        });

        if (existingNote) {
            let existingBatchIds = [], existingNoteFiles = [];
            existingNote.batchNoteMappings.forEach((item) => {
                existingBatchIds.push(item.batchId);
            });
            existingNote.noteAttachments.forEach((item) => {
                existingNoteFiles.push(item.noteFile);
            });

            // batch updation ===============
            batchIds.forEach(async (item) => {
                if (!_.includes(existingBatchIds, item)) {
                    // create new record
                    await models.batchNoteMapping.create({
                        noteId: id,
                        instituteId,
                        batchId: item
                    });
                }
            });
            existingBatchIds.forEach(async (item) => {
                if (!_.includes(batchIds, item)) {
                    // delete the record
                    await models.batchNoteMapping.destroy({
                        where: {
                            batchId: item,
                            instituteId
                        }
                    });
                }
            });

            // attachment updation ===============
            noteFiles.forEach(async (item) => {
                if (!_.includes(existingNoteFiles, item)) {
                    // create new record
                    await models.noteAttachment.create({
                        noteId: id,
                        noteFile: item
                    });
                }
            });
            existingNoteFiles.forEach(async (item) => {
                if (!_.includes(noteFiles, item)) {
                    // delete the record
                    await models.noteAttachment.destroy({
                        where: {
                            noteId: id,
                            noteFile: item
                        }
                    });
                    await awsS3Handler.deleteFile(item);
                }
            });

            // update the note
            existingNote.classId = classId;
            existingNote.subjectId = subjectId;
            existingNote.noteName = noteName;
            existingNote.updatedAt = new Date();
            existingNote.save();
        }
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}