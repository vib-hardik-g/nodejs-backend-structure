const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Staff, Constant, LoginStatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const asyncL = require('async');
const { Op } = require('sequelize');
const { staffInvitation } = require('../../mailTemplates');

/**
 * @description Create Staff
 * @param  {} req
 * @param  {} res
 */
module.exports.createStaff = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.country.sync({ force: false });
        await models.state.sync({ force: false });
        await models.city.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });
        await models.userModuleMapping.sync({ force: false });

        let { instituteId } = req.authData.data;

        let { roleName, salutation, email, firstName, lastName, designation, dateOfBirth, employeeCode,
            gender, contactNo, address, pincode, cityId, stateId, experience, dateOfJoining, profilePicture } = req.body;
        let body = req.body;


        let staffClassBatchSubject = JSON.parse(JSON.stringify(req.body.staffClassBatchSubject));
        let accessRights = JSON.parse(JSON.stringify(req.body.accessRights));

        // Check Unique User
        let checkUser = await models.user.count({ where: { email: body.email } });
        if (checkUser > 0) {
            return res.send(await commonResponse.response(false, Message.ALREADY_EXISTS));
        }

        // Find user type id
        let roleWhereClause = {
            status: true
        };

        if (roleName == Staff.SuperUser) {
            roleWhereClause.userType = Usertypes.ADMIN;
        } else if (roleName == Staff.Faculty) {
            roleWhereClause.userType = Usertypes.STAFF;
        }

        let userTypeOfStaff = await models.userType.findOne({
            where: roleWhereClause
        });

        if (!userTypeOfStaff) {
            return res.send(await commonResponse.response(false, Message.USER_TYPE_NOT_FOUND));
        }

        let staff = await models.staff.create({
            salutation, firstName, lastName, designation, dateOfBirth, employeeCode,
            gender, contactNo, address, pincode, cityId, stateId, experience, dateOfJoining, profilePicture
        });

        if (staff) {
            let user = await models.user.create({
                userTypeId: userTypeOfStaff.id,
                email
            });

            await models.userStaffMapping.create({
                userId: user.id,
                staffId: staff.id,
                instituteId
            });

            // User Module Mapping
            if (roleName == "Faculty" && accessRights && accessRights.length > 0) {
                let accessStaffRight = accessRights.map(data => {
                    return {
                        userId: user.id,
                        moduleId: data.moduleId,
                        permission: data.permission
                    }
                });

                await models.userModuleMapping.bulkCreate(accessStaffRight);
            }

            // Staff Class Batch Subject Mapping
            if (roleName == "Faculty" && staffClassBatchSubject && staffClassBatchSubject.length > 0) {
                asyncL.each(staffClassBatchSubject, (element, callback) => {
                    (async () => {
                        let classId = element.classId;

                        let checkStaffClass = await models.staffClassMapping.count({
                            where: {
                                classId,
                                staffId: staff.id
                            }
                        });

                        if (checkStaffClass == 0) {
                            let staffClassMapping = await models.staffClassMapping.create({
                                classId,
                                staffId: staff.id
                            });

                            if (staffClassMapping) {
                                let staffClassMappingId = staffClassMapping.id;
                                let batches = element.batches;

                                if (batches && batches.length > 0) {
                                    batches.forEach(async (batch) => {
                                        let checkStaffClassBatch = await models.staffClassBatchMapping.count({
                                            where: {
                                                staffClassMappingId,
                                                batchId: batch.batchId
                                            }
                                        });

                                        if (checkStaffClassBatch == 0) {
                                            let staffClassBatch = await models.staffClassBatchMapping.create({
                                                staffClassMappingId,
                                                batchId: batch.batchId
                                            });

                                            if (staffClassBatch) {
                                                let staffClassBatchMappingId = staffClassBatch.id;
                                                let subjectIds = batch.subjectIds;

                                                if (subjectIds && subjectIds.length > 0) {
                                                    let batchSubjectData = subjectIds.map(subjectId => {
                                                        return {
                                                            subjectId,
                                                            staffClassBatchMappingId
                                                        };
                                                    });

                                                    await models.staffClassBatchSubjectMapping.bulkCreate(batchSubjectData);
                                                }
                                            }
                                        }
                                    });

                                    callback();
                                } else {
                                    callback();
                                }
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                });
            } else {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get all the staff by institute
 * @param  {} req
 * @param  {} res
 */
module.exports.allStaffsByInstituteId = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.userType.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.country.sync({ force: false });
        await models.state.sync({ force: false });
        await models.city.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['createdAt', 'DESC']);
        }

        let staffWhereClause = {};
        if (req.query.searchKey) {
            staffWhereClause = {
                status: true,
                [Op.or]: {
                    'firstName': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    'lastName': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        if (req.query.gender) {
            staffWhereClause.gender = req.query.gender;
        }

        if (req.query.loginStatus) {
            staffWhereClause.loginStatus = req.query.loginStatus;
        }

        let classWhere = {};
        let classRequired = false;
        if (req.query.classId) {
            classWhere.classId = req.query.classId;
            classRequired = true;
        }

        let userWhere = {};
        if (req.query.role) {
            let roleWhereClause = {};
            if (req.query.role == Staff.SuperUser) {
                roleWhereClause.userType = Usertypes.ADMIN;
            } else if (req.query.role == Staff.Faculty) {
                roleWhereClause.userType = Usertypes.STAFF;
            }

            let userType = await models.userType.findOne({ where: roleWhereClause });

            if (userType) {
                userWhere.userTypeId = userType.id.toString();
            }
        }

        let response = await models.userStaffMapping.findAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: { instituteId, status: true },
            include: [
                {
                    model: models.staff,
                    where: staffWhereClause,
                    attributes: ['firstName', 'lastName', 'employeeCode', 'profilePicture', 'gender', 'loginStatus'],
                    include: [{
                        model: models.staffClassMapping,
                        where: classWhere,
                        required: classRequired
                    }],
                    required: true,
                },
                {
                    model: models.user,
                    where: userWhere,
                    attributes: ['userTypeId', 'email'],
                    required: true,
                }
            ]
        });

        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));
            let staffData = [];

            asyncL.each(response, (data, callback) => {
                (async () => {
                    let totalBatches = await models.staffClassMapping.count({ where: { staffId: data.staffId }, include: [{ model: models.staffClassBatchMapping }] });
                    let userType = await models.userType.findOne({ where: { id: data.user.userTypeId }, attributes: ['userType'] });

                    let noOfBatches, roleName;

                    if (userType.userType == Usertypes.ADMIN) {
                        noOfBatches = 'N/A';
                        roleName = Staff.SuperUser;
                    } else if (userType.userType == Usertypes.STAFF) {
                        noOfBatches = totalBatches;
                        roleName = Staff.Faculty;
                    }

                    staffData.push({
                        id: data.id,
                        userId: data.userId,
                        staffId: data.staffId,
                        loginStatus: data.staff.loginStatus,
                        staffName: `${data.staff.firstName} ${data.staff.lastName}`,
                        email: data.user.email,
                        employeeCode: data.staff.employeeCode,
                        gender: data.staff.gender,
                        noOfBatches: noOfBatches,
                        roleName: roleName,
                        createdAt: data.createdAt
                    });
                    
                    callback();
                })();
            }, async (err) => {
                let totalStaff = await models.userStaffMapping.count({
                    where: { instituteId, status: true },
                    attributes: [],
                    include: [
                        {
                            model: models.staff,
                            where: staffWhereClause,
                            attributes: [],
                            include: [{
                                model: models.staffClassMapping,
                                where: classWhere,
                                required: classRequired
                            }]
                        },
                        {
                            model: models.user,
                            where: userWhere,
                            attributes: [],
                        }
                    ],
                    group: ['userStaffMapping.staffId']
                });

                let result = {
                    total: totalStaff.length,
                    rows: staffData
                };

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            });
        } else {
            let result = {
                total: 0,
                rows: []
            }

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get a staff details by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getStaffById = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });
        await models.userModuleMapping.sync({ force: false });

        let { id } = req.params;

        let staff = await models.userStaffMapping.findOne({
            attributes: [],
            where: { staffId: id },
            include: [{
                model: models.staff,
                include: [
                    {
                        model: models.document,
                        attributes: [['fullPath', 'profilePicture']]
                    },
                    {
                        model: models.staffClassMapping,
                        attributes: ['id', 'classId'],
                        include: [
                            {
                                model: models.class,
                                attributes: ['className']
                            },
                            {
                                model: models.staffClassBatchMapping,
                                attributes: ['id', 'batchId']
                            }
                        ],
                    },
                    {
                        model: models.city,
                        attributes: ['cityName']
                    },
                    {
                        model: models.state,
                        attributes: ['stateName']
                    },
                ],
            },
            {
                model: models.user,
                attributes: ['email'],
                include: [
                    {
                        model: models.userModuleMapping,
                        attributes: ['moduleId', 'permission']
                    }
                ]
            }]
        });

        if (staff) {
            staff = JSON.parse(JSON.stringify(staff));

            if (staff.staff.document && staff.staff.document.profilePicture) {
                let profilePicture = staff.staff.document.profilePicture;
                staff.staff.profilePicturePath = profilePicture;

                delete staff.staff.document;
            } else {
                staff.staff.profilePicturePath = '';
            }

            staff.staff.email = staff.user.email;
            staff.staff.accessRights = staff.user.userModuleMappings;

            staff = staff.staff;

            if (staff.staffClassMappings && staff.staffClassMappings.length > 0) {

                asyncL.each(staff.staffClassMappings, (element, callback) => {
                    (async () => {
                        asyncL.each(element.staffClassBatchMappings, (batchElement, callback1) => {
                            (async () => {
                                let staffClassBatchMappingId = batchElement.id;
                                let batchId = batchElement.batchId;

                                let batch = await models.batch.findOne({ where: { id: batchId }, attributes: ['batchName'] });
                                batchElement.batchName = batch.batchName;

                                let staffClassBatchSubjectMapping = await models.staffClassBatchSubjectMapping.findAll({
                                    where: {
                                        staffClassBatchMappingId
                                    },
                                    attributes: ['subjectId'],
                                    include: [
                                        {
                                            model: models.subject,
                                            attributes: ['subjectName']
                                        }
                                    ]
                                });

                                batchElement.subjectIds = [];

                                if (staffClassBatchSubjectMapping && staffClassBatchSubjectMapping.length > 0) {
                                    batchElement.subjectIds = staffClassBatchSubjectMapping;
                                }

                                callback1();
                            })();
                        }, async (err) => {
                            element.batches = element.staffClassBatchMappings;
                            delete element.staffClassBatchMappings;

                            callback();
                        });
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, staff));
                });
            } else {
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, staff));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Update staff
 * @param  {} req
 * @param  {} res
 */
module.exports.updateStaff = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.country.sync({ force: false });
        await models.state.sync({ force: false });
        await models.city.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });
        await models.userModuleMapping.sync({ force: false });

        let { id } = req.params;
        let { salutation, email, firstName, lastName, designation, dateOfBirth, employeeCode,
            gender, contactNo, address, pincode, cityId, stateId, experience, dateOfJoining } = req.body;

        let staffClassBatchSubject = JSON.parse(JSON.stringify(req.body.staffClassBatchSubject));
        let accessRights = JSON.parse(JSON.stringify(req.body.accessRights));

        // Check Unique User
        let user = await models.userStaffMapping.findOne({
            where: { staffId: id }
        });

        if (!user) {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

        let checkUser = await models.user.count({
            where: {
                id: {
                    [Op.ne]: user.userId
                },
                email: email
            }
        });

        if (checkUser > 0) {
            return res.send(await commonResponse.response(false, Message.EMAIL_ALREADY_EXISTS));
        }

        let staff = await models.staff.update({
            salutation, firstName, lastName, designation, dateOfBirth, employeeCode,
            gender, contactNo, address, pincode, cityId, stateId, experience, dateOfJoining
        }, {
            where: { id }
        });

        if (staff) {
            await models.user.update({ email }, { where: { id: user.userId } });

            // User Module Mapping
            if (accessRights && accessRights.length > 0) {
                accessRights.map(async (data) => {
                    await models.userModuleMapping.update({
                        permission: data.permission
                    }, {
                        where: {
                            userId: user.userId,
                            moduleId: data.moduleId,
                        }
                    });
                });
            }

            // Staff Class Batch Subject Mapping
            if (staffClassBatchSubject && staffClassBatchSubject.length > 0) {
                // Remove class id if not exists in request data
                let staffClasses = await models.staffClassMapping.findAll({
                    where: {
                        staffId: id
                    },
                    attributes: ['id', 'classId'],
                    include: [{
                        model: models.staffClassBatchMapping,
                        attributes: [['id', 'staffClassBatchMappingId']]
                    }]
                });

                // Remove class, batch and subject
                if (staffClasses && staffClasses.length > 0) {
                    staffClasses = JSON.parse(JSON.stringify(staffClasses));

                    staffClasses.forEach(async (classeElement) => {
                        let staffClassBatchMappingIds = classeElement.staffClassBatchMappings.map(data => data.staffClassBatchMappingId);

                        // Delete subject
                        await models.staffClassBatchSubjectMapping.destroy({ where: { staffClassBatchMappingId: staffClassBatchMappingIds } });

                        // Delete batch
                        await models.staffClassBatchMapping.destroy({ where: { staffClassMappingId: classeElement.id } });

                        // Delete class
                        await models.staffClassMapping.destroy({ where: { staffId: id } });
                    });
                }

                await models.staffClassMapping.sync({ force: false });
                await models.staffClassBatchMapping.sync({ force: false });
                await models.staffClassBatchSubjectMapping.sync({ force: false });

                // Create class, batch and subject
                asyncL.each(staffClassBatchSubject, (element, callback) => {
                    (async () => {
                        let classId = element.classId;

                        let checkStaffClass = await models.staffClassMapping.count({
                            where: {
                                classId,
                                staffId: id
                            }
                        });

                        if (checkStaffClass == 0) {
                            let staffClassMapping = await models.staffClassMapping.create({
                                classId,
                                staffId: id
                            });

                            if (staffClassMapping) {
                                let staffClassMappingId = staffClassMapping.id;
                                let batches = element.batches;

                                if (batches && batches.length > 0) {
                                    batches.forEach(async (batch) => {
                                        let checkStaffClassBatch = await models.staffClassBatchMapping.count({
                                            where: {
                                                staffClassMappingId,
                                                batchId: batch.batchId
                                            }
                                        });

                                        if (checkStaffClassBatch == 0) {
                                            let staffClassBatch = await models.staffClassBatchMapping.create({
                                                staffClassMappingId,
                                                batchId: batch.batchId
                                            });

                                            if (staffClassBatch) {
                                                let staffClassBatchMappingId = staffClassBatch.id;
                                                let subjectIds = batch.subjectIds;

                                                if (subjectIds && subjectIds.length > 0) {
                                                    let batchSubjectData = subjectIds.map(subjectId => {
                                                        return {
                                                            subjectId,
                                                            staffClassBatchMappingId
                                                        };
                                                    });

                                                    let result = await models.staffClassBatchSubjectMapping.bulkCreate(batchSubjectData);
                                                }
                                            }
                                        }
                                    });

                                    callback();
                                } else {
                                    callback();
                                }
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
                });
            } else {
                return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_UPDATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Staff bulk upload
 * @param  {} req
 * @param  {} res
 */
module.exports.staffBulkUpload = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });

        let { staffs } = req.body;
        let { instituteId } = req.authData.data;

        let staffData = [];
        let duplicateUsers = [];

        if (req.files && req.files.staffs_attachment) {
            let csvData = req.files.staffs_attachment.data.toString('utf8');
            staffData = await csvtojson().fromString(csvData);
        }

        if (staffs && staffs.length > 0) {
            staffData = staffs;
        }

        if (staffData && staffData.length > 0) {

            // Find User Type
            let userTypeOfStaff = await models.userType.findOne({
                where: {
                    userType: Usertypes.STAFF,
                    status: true,
                }
            });

            if (!userTypeOfStaff) {
                return res.send(await commonResponse.response(false, Message.USER_TYPE_NOT_FOUND, []));
            }

            asyncL.each(staffData, (element, callback) => {
                (async () => {
                    if (element.email && element.firstName && element.lastName) {

                        // Check duplicate staff member
                        let checkUser = await models.user.count({ where: { email: element.email } });

                        if (checkUser > 0) {
                            // For check duplicate users
                            // duplicateUsers.push(element);
                            callback();
                        } else {
                            // Create Staff
                            let staff = await models.staff.create({
                                firstName: element.firstName,
                                lastName: element.lastName,
                                designation: element.designation,
                                dob: element.dob,
                                gender: element.gender,
                                bloodGroup: element.bloodGroup,
                                primaryPhoneNumber: element.primaryPhoneNumber,
                                address: element.address,
                                pincode: element.pincode,
                                cityId: null,
                                stateId: null,
                                countryId: null,
                                aadharNumber: element.aadharNumber,
                                bankAccount: element.bankAccount,
                                bankName: element.bankName,
                                bankIfsc: element.bankIfsc,
                                education: element.education,
                                experience: element.experience
                            });

                            if (staff) {
                                // Create Staff User
                                let user = await models.user.create({
                                    userTypeId: userTypeOfStaff.id,
                                    email: element.email
                                });

                                // Mapping Staff and User
                                await models.userStaffMapping.create({
                                    userId: user.id,
                                    staffId: staff.id,
                                    instituteId,
                                    subjectId: null,
                                    batchId: null
                                });

                                callback();
                            } else {
                                callback();
                            }
                        }
                    } else {
                        callback();
                    }
                })();
            }, async (err) => {
                return res.send(await commonResponse.response(true, Message.STAFFS_ADDED, []));
            });
        } else {
            return res.send(await commonResponse.response(false, Message.STAFFS_NOT_FOUND, []));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Assign subject to staff
 * @param  {} req
 * @param  {} res
 */
module.exports.assignSubjectToStaff = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { staffId, subjectId } = req.body;
        await models.userStaffMapping.update({ subjectId, updatedAt: new Date() }, { where: { staffId, instituteId } });
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete staff
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteStaff = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });

        let { id } = req.params;

        let userStaff = await models.userStaffMapping.findOne({ where: { staffId: id } });

        if (userStaff) {
            let userId = userStaff.userId;

            // Desable user staff mapping
            await models.userStaffMapping.update({ status: false }, { where: { id: userStaff.id } });

            // Desable user
            await models.user.update({ status: false }, { where: { id: userId } });

            // Desable staff
            await models.staff.update({ status: false }, { where: { id: id } });

            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.assignSubjectToStaff = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { staffId, subjectId } = req.body;
        await models.userStaffMapping.update({ subjectId, updatedAt: new Date() }, { where: { staffId, instituteId } });
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @param  {} req
 * @param  {} res
 */
module.exports.inviteStaff = async (req, res) => {
    try {
        await models.staff.sync({ force: false });
        await models.user.sync({ force: false });

        let { staffIds } = req.body;

        let staffUser = await models.userStaffMapping.findAll({
            where: {
                staffId: staffIds,
                status: true
            },
            attributes: ['id', 'staffId'],
            include: [
                {
                    model: models.user,
                    attributes: ['email']
                },
                {
                    model: models.staff,
                    attributes: ['firstName', 'lastName'],
                },
            ]
        });

        if (staffUser && staffUser.length > 0) {
            staffUser = JSON.parse(JSON.stringify(staffUser));

            asyncL.each(staffUser, (element, callback) => {
                (async () => {
                    let email = element.user.email;
                    let userName = `${element.staff.firstName} ${element.staff.lastName}`;

                    let isSent = await staffInvitation(email, userName);

                    if (isSent) {
                        // Update staff login status
                        await models.staff.update({
                            loginStatus: "Not Signed Up"
                        }, {
                            where: {
                                id: element.staffId
                            }
                        });
                    }

                    callback();
                })();
            }, async (err) => {
                return res.send(await commonResponse.response(true, Message.STAFF_INVITED));
            });
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @param  {} req
 * @param  {} res
 */
module.exports.sendRemindersToStaff = async (req, res) => {
    try {
        await models.userStaffMapping.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.user.sync({ force: false });

        let { instituteId } = req.authData.data;

        let staffUsers = await models.userStaffMapping.findAll({
            where: {
                instituteId,
                status: true,
            },
            attributes: ['id', 'staffId'],
            include: [
                {
                    model: models.user,
                    attributes: ['email']
                },
                {
                    model: models.staff,
                    attributes: ['firstName', 'lastName'],
                    where: {
                        loginStatus: [LoginStatus.NOT_SIGNED_UP, LoginStatus.NOT_YET_INVITED]
                    },
                    required: true,
                },
            ]
        });

        if (staffUsers && staffUsers.length > 0) {
            staffUsers = JSON.parse(JSON.stringify(staffUsers));

            asyncL.each(staffUsers, (element, callback) => {
                (async () => {
                    let email = element.user.email;
                    let userName = `${element.staff.firstName} ${element.staff.lastName}`;

                    let isSent = await staffInvitation(email, userName);

                    if (isSent) {
                        // Update staff login status
                        await models.staff.update({
                            loginStatus: "Not Signed Up"
                        }, {
                            where: {
                                id: element.staffId
                            }
                        });
                    }

                    callback();
                })();
            }, async (err) => {
                return res.send(await commonResponse.response(true, Message.STAFF_INVITED));
            });
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}