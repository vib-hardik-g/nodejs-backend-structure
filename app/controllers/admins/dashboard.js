const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');
const { administratorEmailOTP } = require('../../mailTemplates');

/**
 * @description get counts fro dashboard
 * @param  {} req
 * @param  {} res
 */
module.exports.dashboardCounts = async (req, res) => {
    try {

        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });

        let { instituteId } = req.authData.data;

        let classCount = await models.class.count({
            distinct: true,
            where: { status: true, instituteId: instituteId },
        });

        let batchCount = await models.batch.count({
            distinct: true,
            where: { status: true, instituteId: instituteId },
        });

        let studentCount = await models.student.findAndCountAll({
            distinct: true,
            where: { status: true },
            include: [{
                model: models.userStudentMapping,
                attributes: ['id', 'studentId', 'instituteId'],
                where: { status: true, instituteId: instituteId },
                require: true,
            }]
        });

        let staffCount = await models.staff.findAndCountAll({
            distinct: true,
            where: { status: true },
            include: [{
                model: models.userStaffMapping,
                attributes: ['id', 'staffId', 'instituteId'],
                where: { status: true, instituteId: instituteId },
                require: true,
            }]
        });

        let result = {
            classCount: classCount,
            batchCount: batchCount,
            studentCount: studentCount.count,
            staffCount: staffCount.count
        };
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description get student capacity request list epends on status
 * @param  {} req
 * @param  {} res
 */
module.exports.studentLeaveRequestList = async (req, res) => {
    try {

        await models.student.sync({ force: false });
        await models.studentLeave.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });

        let { instituteId } = req.authData.data;
        let where = { status: true };

        let response = await models.student.findAndCountAll({
            distinct: true,
            where: where,
            subQuery: false,
            attributes: ['id', 'firstName', 'lastName', 'createdAt', 'updatedAt'],
            include: [{
                model: models.studentLeave,
                attributes: ['reason', 'leaveStatus', 'startDate', 'endDate'],
                where: { status: true },
                require: true
            },
            {
                model: models.userStudentMapping,
                attributes: ['id', 'studentId', 'instituteId'],
                where: { status: true, instituteId: instituteId },
                require: true,
            }],
        });
        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.studentRequest = async (req, res) => {
}

module.exports.staffRequest = async (req, res) => {
}

module.exports.happeningList = async (req, res) => {
    try {

        await models.happening.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId } = req.authData.data;
        let where = { status: true, instituteId: instituteId };

        let response = await models.happening.findAndCountAll({
            distinct: true,
            where: where,
            subQuery: false,
            attributes: ['id', 'happening', 'happeningDescription', 'createdAt', 'updatedAt'],
            include: [{
                model: models.happeningAttachment,
                as: "attachments",
                attributes: ['id'],
                include: [{
                    model: models.document
                }]
            }],
        });
        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

