const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createHappening = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId, staffId } = req.authData.data;
        let { happening, happeningDescription,happeningThumnbnailImage, attachments } = req.body;

        let response = await models.happening.create({
            instituteId,
            staffId,
            happening,
            happeningDescription,
            happeningThumnbnailImage,
            happeningLikeCount: 0,
            happeningCommentCount: 0,
        });

        if (attachments.length > 0) {
            attachments.forEach(async (documnetId) => {
                await models.happeningAttachment.create({
                    happeningId: response.id,
                    happeningImage: documnetId
                });
            });
        }
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allHappenings = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                instituteId,
                status: true
            }
        } else {
            where = {
                instituteId,
                status: true,
                [Op.or]: {
                    happening: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    happeningDescription: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                },

            }
        }
        if (req.query.startDate && req.query.endDate) {
            where.createdAt = { [Op.between]: [new Date(req.query.startDate).toISOString(), new Date(req.query.endDate+ ' 23:59:59').toISOString()] };
        }

        let response = await models.happening.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            attributes: ["id", "happening","happeningThumnbnailImage", "happeningDescription", "happeningLikeCount", "happeningCommentCount", "createdAt", "updatedAt"],
            include: [
                {
                    model: models.happeningComment,
                    as: "comments",
                    attributes: ["id", "comment", "createdAt", "status"],
                    include: [
                        {
                            model: models.student,
                            attributes: ['firstName', 'lastName'],
                        },
                        {
                            model: models.staff,
                            as: "staff",
                            attributes: ['firstName', 'lastName'],
                        }
                    ]
                },
                {
                    model: models.happeningAttachment,
                    as: "attachments",
                    attributes: ["id"],
                    include: [
                        {
                            model: models.document
                        }
                    ]
                },{
                    model: models.document,
                    as:'thumnbnailImage'
                }
            ]
        });
        let formattedResponse = [];
        for (let i = 0; i < response.rows.length; i++) {
            let comments = response.rows[i].comments;
            let formattedComments = [];
            for (let j = 0; j < comments.length; j++) {
                let newComment = {
                    id: comments[j].id,
                    status: comments[j].status,
                    comment: comments[j].comment,
                    createdAt: comments[j].createdAt,
                    firstName: comments[j].student ? comments[j].student.firstName : comments[j].staff.firstName,
                    lastName: comments[j].student ? comments[j].student.lastName : comments[j].staff.lastName,
                }
                formattedComments.push(newComment);
            }
            formattedResponse.push({
                id: response.rows[i].id,
                happening: response.rows[i].happening,
                happeningThumnbnailImage: response.rows[i].thumnbnailImage,
                happeningDescription: response.rows[i].happeningDescription,
                happeningLikeCount: response.rows[i].happeningLikeCount,
                happeningCommentCount: response.rows[i].happeningCommentCount,
                createdAt: response.rows[i].createdAt,
                comments: formattedComments,
                attachments: response.rows[i].attachments,
            });
        }
        let result = {
            rows: formattedResponse,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.happeningById = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        let response = await models.happening.findOne({
            where: { instituteId, id, status: true },
            attributes: ["id", "happening","happeningThumnbnailImage", "happeningDescription", "happeningLikeCount", "happeningCommentCount", "createdAt", "updatedAt"],
            include: [
                {
                    model: models.happeningAttachment,
                    as: "attachments",
                    attributes: ["id"],
                    include: [
                        {
                            model: models.document
                        }
                    ]
                },{
                    model: models.document,
                    as:'thumnbnailImage'
                }
            ]
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateHappening = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        let response = await models.happening.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    instituteId,
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteHappening = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;
        let response = await models.happening.update({ status: false }, {
            where: { instituteId, id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.approveOrRejectHappeningComment = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { staffId } = req.authData.data;
        let { id } = req.params;
        let { approvalStatus } = req.body;

        let response = await models.happeningComment.update(
            { approverId: staffId, status: approvalStatus, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteHappeningComment = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.document.sync({ force: false });

        let { id } = req.params;
        let happeningComment = await models.happeningComment.findOne({ where: { id } });
        let response = await models.happeningComment.destroy({ where: { id } });
        if (response) {
            // ddecrease comment count
            await models.happening.decrement('happeningCommentCount', { by: 1, where: { id: happeningComment.happeningId } });
        }
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}