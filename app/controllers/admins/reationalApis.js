const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const _ = require('lodash');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports = {
    batchesByClasses: async (req, res) => {
        try {
            let { instituteId } = req.authData.data;
            let { classIds } = req.body;

            let response = await models.batch.findAll({
                where: {
                    classId: {
                        [Op.in]: classIds
                    },
                    instituteId
                },
                attributes: ['id', 'batchName']
            });
            let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            return res.json(await commonResponse.response(true, msg, response));
        } catch (err) {
            const { status, message, error } = errorHandle(err);
            return res.status(status).json(await commonResponse.response(false, message, error));
        }
    },

    subjectsByBatches: async (req, res) => {
        try {
            let { instituteId } = req.authData.data;
            let { batchIds } = req.body;

            let response = await models.batchSubjectMapping.findAll({
                where: {
                    batchId: {
                        [Op.in]: batchIds
                    },
                    instituteId,
                },
                attributes: [[sequelize.fn('COUNT', sequelize.col('subject.id')), 'subjectCount']],
                include: [{
                    model: models.subject,
                    required: true
                }],
                group: ['subject.id'],
                raw: true,
            });

            let finalResponse = [];
            if (batchIds.length > 1) {
                // intersection needed
                response.forEach((item) => {
                    if (item.subjectCount == batchIds.length) {
                        finalResponse.push({
                            id: item['subject.id'],
                            subjectName: item['subject.subjectName']
                        });
                    }
                });
            } else {
                // all results
                response.forEach((item) => {
                    finalResponse.push({
                        id: item['subject.id'],
                        subjectName: item['subject.subjectName']
                    });
                });
            }

            let msg = finalResponse.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            return res.json(await commonResponse.response(true, msg, finalResponse));
        } catch (err) {
            const { status, message, error } = errorHandle(err);
            return res.status(status).json(await commonResponse.response(false, message, error));
        }
    },

    groupsByBatchClass: async (req, res) => {
        try {
            let { instituteId } = req.authData.data;
            let { classId, batchId } = req.body;

            let response = await models.groupBatch.findAll({
                where: {
                    batchId
                },
                include: [{
                    model: models.group,
                    attributes: ['id', 'groupName'],
                    where: {
                        classId
                    },
                    required: true,
                }],
                attributes: [],
            });
            let msg = response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            return res.json(await commonResponse.response(true, msg, response));
        } catch (err) {
            const { status, message, error } = errorHandle(err);
            return res.status(status).json(await commonResponse.response(false, message, error));
        }
    },



    getEmployeecodeBySubject: async (req, res) => {
        try {
            let { subjectId } = req.body;
            let response = await models.staffClassBatchSubjectMapping.findAll({
                where: {
                    subjectId: subjectId
                },
                include: [{
                    model: models.staffClassBatchMapping,
                    required: true,
                    include: [{
                        model: models.staffClassMapping,
                        required: true,
                        include: [
                            {
                                model: models.staff,
                                attributes: ['id', 'firstName', 'lastName', 'employeeCode'],
                            }
                        ]
                    }]
                },
                ],
            })
            let staffData = [];
            for (let index1 = 0; index1 < response.length; index1++) {
                const element1 = response[index1];
                staffData.push(element1.staffClassBatchMapping.staffClassMapping.staff);

            }
            let msg = staffData.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            return res.json(await commonResponse.response(true, msg, staffData));

        } catch (err) {
            const { status, message, error } = errorHandle(err);
            return res.status(status).json(await commonResponse.response(false, message, error));
        }
    },

    getStaffAndSubjectByBatch: async (req, res) => {
        try {
            let { instituteId } = req.authData.data;
            let { batchId } = req.params;

            let sub = await models.batchSubjectMapping.findAll({
                where: {
                    batchId,
                    instituteId,
                },
                include: [{
                    model: models.subject
                }]
            });

            let mapping = sub.map(val => {
                return val.subjectId;
            });
            let staff = await models.staffClassBatchSubjectMapping.findAll({
                where: {
                    subjectId: mapping
                },
                include: [{
                    model: models.staffClassBatchMapping,
                    required: true,
                    include: [{
                        model: models.staffClassMapping,
                        required: true,
                        include: [
                            {
                                model: models.staff,
                                attributes: ['id', 'firstName', 'lastName'],
                            }
                        ]
                    }]
                },
                ],
            })


            let resultArr = [];

            for (let index = 0; index < sub.length; index++) {
                const element = sub[index];
                let staffData = [];
                for (let index1 = 0; index1 < staff.length; index1++) {
                    const element1 = staff[index1];
                    if (element.subject.id == element1.subjectId) {
                        staffData.push(element1.staffClassBatchMapping.staffClassMapping.staff);
                    }
                }
                resultArr.push({
                    id: element.subject.id,
                    subjectName: element.subject.subjectName,
                    icon: element.subject.icon,
                    staffData: staffData
                })

            }
            if (staff) {
                return res.json(await commonResponse.response(true, Message.DATA_FOUND, resultArr));
            } else {
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }
        } catch (err) {
            const { status, message, error } = errorHandle(err);
            return res.status(status).json(await commonResponse.response(false, message, error));
        }

    }
}