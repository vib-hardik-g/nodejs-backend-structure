const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');

module.exports.createAssignment = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });

        let { instituteId, staffId } = req.authData.data;
        let { assignmentName, classId, subjectId, batchIds, assignmentFiles, assignmentDueDate, assignmentDueTime } = req.body;

        let createData = {
            instituteId,
            classId,
            subjectId,
            assignmentName,
            staffId
        };
        if (assignmentDueDate && assignmentDueTime) {
            createData.assignmentDueDate = moment(assignmentDueDate).format('YYYY-MM-DD');
            createData.assignmentDueTime = assignmentDueTime;
        }
        let assignment = await models.assignment.create(createData);

        batchIds.forEach(async (batchId) => {
            await models.batchAssignmentMapping.create({
                instituteId,
                assignmentId: assignment.id,
                batchId
            });
        });

        assignmentFiles.forEach(async (assignmentFile) => {
            await models.assignmentAttachment.create({
                assignmentId: assignment.id,
                assignmentFile,
            })
        });
        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listAllAssignments = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                instituteId
            };
        } else {
            where = {
                assignmentName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                status: true,
                instituteId
            };
        }

        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.subjectId) {
            where.subjectId = req.query.subjectId;
        }

        let response = await models.assignment.findAndCountAll({
            distinct: true,
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            include: [{
                model: models.batchAssignmentMapping,
                attributes: ['batchId'],
                include: [{
                    model: models.batch,
                    attributes: ['id','batchName']
                }]
            }, {
                model: models.assignmentAttachment,
                attributes: ['assignmentFile'],
                include: [{
                    model: models.document
                }]
            }, {
                model: models.subject,
                attributes: ['id','subjectName'],
            }, {
                model: models.class,
                attributes: ['id','className'],
            }],
            attributes: ['id', 'assignmentName', 'assignmentDueDate', 'assignmentDueTime', 'publishStatus', 'createdAt', 'updatedAt']
        });
        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteAssignment = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        await models.assignment.update({
            status: false,
        }, {
            where: {
                instituteId,
                id
            }
        });

        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.publishAssignment = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { assignmentId } = req.body;

        let response = await models.assignment.findOne({
            where: {
                id: assignmentId,
                instituteId
            }
        });
        let newStatus;
        if (response.publishStatus == Publishstatus.PENDING) {
            newStatus = Publishstatus.PUBLISHED;
        } else {
            newStatus = Publishstatus.REPUBLISHED;
        }
        await models.assignment.update({
            publishStatus: newStatus,
            updatedAt: new Date()
        }, {
            where: {
                instituteId,
                id: assignmentId,
            }
        });
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateAssignment = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });

        let { instituteId, staffId } = req.authData.data;
        let { assignmentName, classId, subjectId, batchIds, assignmentFiles, assignmentDueDate, assignmentDueTime } = req.body;
        let { id } = req.params;

        let existingAssignment = await models.assignment.findOne({
            where: {
                id,
                instituteId
            },
            include: [{
                model: models.assignmentAttachment
            }, {
                model: models.batchAssignmentMapping
            }]
        });

        if (existingAssignment) {
            let existingBatchIds = [], existingAssignmentFiles = [];
            existingAssignment.batchAssignmentMappings.forEach((item) => {
                existingBatchIds.push(item.batchId);
            });
            existingAssignment.assignmentAttachments.forEach((item) => {
                existingAssignmentFiles.push(item.assignmentFile);
            });

            // batch updation ===============
            batchIds.forEach(async (item) => {
                if (!_.includes(existingBatchIds, item)) {
                    // create new record
                    await models.batchAssignmentMapping.create({
                        assignmentId: id,
                        instituteId,
                        batchId: item
                    });
                }
            });
            existingBatchIds.forEach(async (item) => {
                if (!_.includes(batchIds, item)) {
                    // delete the record
                    await models.batchAssignmentMapping.destroy({
                        where: {
                            batchId: item,
                            instituteId
                        }
                    });
                }
            });

            // attachment updation ===============
            assignmentFiles.forEach(async (item) => {
                if (!_.includes(existingAssignmentFiles, item)) {
                    // create new record
                    await models.assignmentAttachment.create({
                        assignmentId: id,
                        assignmentFile: item
                    });
                }
            });
            existingAssignmentFiles.forEach(async (item) => {
                if (!_.includes(assignmentFiles, item)) {
                    // delete the record
                    await models.assignmentAttachment.destroy({
                        where: {
                            assignmentId: id,
                            assignmentFile: item
                        }
                    });
                    await awsS3Handler.deleteFile(item);
                }
            });

            // update the assignment
            existingAssignment.classId = classId;
            existingAssignment.subjectId = subjectId;
            existingAssignment.assignmentName = assignmentName;
            existingAssignment.updatedAt = new Date();
            existingAssignment.staffId = staffId;
            if (assignmentDueDate && assignmentDueTime) {
                existingAssignment.assignmentDueDate = moment(assignmentDueDate).format('YYYY-MM-DD');
                existingAssignment.assignmentDueTime = assignmentDueTime;
            } else {
                existingAssignment.assignmentDueDate = null;
                existingAssignment.assignmentDueTime = null;
            }
            existingAssignment.save();
        }
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listStudentsByAssignmentId = async (req, res) => {
    try {
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });
        await models.assignmentSubmission.sync({ force: false });
        await models.student.sync({ force: false });

        let { assignmentId } = req.params;

        let assignmentDetails = await models.assignment.findOne({
            where: {
                id: assignmentId,
                status: true
            },
            include: [{
                model: models.class,
                attributes: ['className']
            }]
        });
        if(!assignmentDetails) return res.status(404).json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        let where = {};
        if(!req.query.searchKey){
            where = {
                status: true,
            };
        }else{
            where = {
                status: true,
                [Op.or]: {
                    firstName: { [Op.iLike]: '%'+ req.query.searchKey +'%' },
                    lastName: { [Op.iLike]: '%'+ req.query.searchKey +'%' },
                    '$studentBatchUniqueId.uniqueId$': req.query.searchKey
                }
            };
        }

        let response = await models.student.findAll({
            subQuery: false,
            distinct: true,
            attributes: ['id', 'firstName', 'lastName'],
            where: where,
            include: [{
                required: true,
                model: models.userStudentMapping,
                attributes: ['id'],
                where: {
                    classId: assignmentDetails.classId
                },
                include: [{
                    required: true,
                    model: models.classSubjectMapping,
                    attributes: ['id'],
                    where: {
                        classId: assignmentDetails.classId,
                        subjectId: assignmentDetails.subjectId,
                    },
                    include: [{
                        model: models.subject,
                        attributes: ['id', 'subjectName']
                    }]
                }, {
                    model: models.class,
                    attributes: ['id', 'className']
                }, {
                    model: models.batch,
                    attributes: ['id', 'batchName']
                }]
            }, {
                model: models.studentBatchUniqueId,
                attributes: ['uniqueId']
            }, {
                model: models.assignmentSubmission,
                required: false,
                where: {
                    assignmentId,
                },
                include: [{
                    model: models.document
                }],
                attributes: ['id', 'createdAt']
            }]
        });
        let result = {};
        result.className = assignmentDetails.class.className;
        result.assignmentName = assignmentDetails.assignmentName;
        result.students = response;

        let msg = response.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}