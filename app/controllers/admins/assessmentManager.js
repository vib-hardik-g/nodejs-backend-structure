const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, commonModuleType } = require('../../../utils/commonMessages');
const { uploadFileOnS3 } = require('../../../utils/commonHelper');
const errorHandle = require('../../../utils/errorHandler');
const { getFileFromS3AndStore } = require('../../../utils/awsS3Handler');
const { Op } = require('sequelize');
const moment = require('moment');
const asyncL = require('async');
const Excel = require('exceljs');
const fs = require('fs');
const path = require('path');
const xlsxFile = require('read-excel-file/node');
const JSZip = require("jszip");
const AWS = require('aws-sdk');

// AWS S3
const S3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION_NAME
});

/**
 * @description Create test result template
 * @param  {} req
 * @param  {} res
 */
module.exports.createTestResultTemplate = async (req, res) => {
    try {
        await models.examType.sync({ force: false });
        await models.testResultTemplate.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { examTypeId, batchId, subjects } = req.body;
        subjects = JSON.parse(JSON.stringify(subjects));

        let checkTemplate = await models.testResultTemplate.count({
            where: {
                instituteId,
                batchId,
                examTypeId
            }
        });

        if (checkTemplate > 0) {
            return res.json(await commonResponse.response(false, Message.TEST_TEMPLATE_ALREADY_EXIST));
        } else {
            let testResultTemplate = await models.testResultTemplate.create({
                instituteId,
                batchId,
                examTypeId,
            });

            if (testResultTemplate) {
                let testResultTemplateId = testResultTemplate.id;

                for (let i = 0; i < subjects.length; i++) {
                    let subject = subjects[i];

                    let checkSubject = await models.testResultTemplateSubject.count({
                        where: { subjectId: subject.subjectId, testResultTemplateId },
                    });

                    if (checkSubject == 0) {
                        await models.testResultTemplateSubject.create({
                            testResultTemplateId,
                            subjectId: subject.subjectId,
                            maximumMarks: subject.maximumMarks,
                        });
                    }
                }

                return res.json(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.json(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update test result template
 * @param  {} req
 * @param  {} res
 */
module.exports.updateTestResultTemplate = async (req, res) => {
    try {
        await models.examType.sync({ force: false });
        await models.testResultTemplate.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });

        let { id } = req.params;
        let { examTypeId, batchId, subjects } = req.body;
        subjects = JSON.parse(JSON.stringify(subjects));

        let checkTemplate = await models.testResultTemplate.count({
            where: {
                id: {
                    [Op.ne]: id
                },
                batchId,
                examTypeId
            }
        });

        if (checkTemplate > 0) {
            return res.json(await commonResponse.response(false, Message.TEST_TEMPLATE_ALREADY_EXIST));
        } else {
            await models.testResultTemplate.update({
                batchId,
                examTypeId,
            }, {
                where: {
                    id: id
                }
            });

            let testResultTemplateId = id;

            // Removing not needed subject from db
            await models.testResultTemplateSubject.destroy({
                where: { testResultTemplateId, status: true }
            });

            // Store new subjects
            for (let i = 0; i < subjects.length; i++) {
                let subject = subjects[i];

                let checkSubject = await models.testResultTemplateSubject.count({
                    where: { subjectId: subject.subjectId, testResultTemplateId },
                });

                if (checkSubject == 0) {
                    await models.testResultTemplateSubject.create({
                        testResultTemplateId,
                        subjectId: subject.subjectId,
                        maximumMarks: subject.maximumMarks,
                    });
                }
            }

            return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get a test template by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getTestResultTemplateById = async (req, res) => {
    try {
        await models.examType.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.document.sync({ force: false });
        await models.testResultTemplate.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });

        let { id } = req.params;

        let response = await models.testResultTemplate.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.testResultTemplateSubject,
                    attributes: ['id', 'subjectId', 'maximumMarks'],
                    include: [
                        {
                            model: models.subject,
                            attributes: ['subjectName']
                        }
                    ]
                },
                {
                    model: models.examType,
                    attributes: ['examType']
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'resultSheet']]
                }
            ]
        });

        if (response) {
            let result = JSON.parse(JSON.stringify(response));

            result.testType = result.examType.examType;
            delete result.examType;

            result.batchName = result.batch.batchName;
            delete result.batch;

            result.subjects = result.testResultTemplateSubjects;
            delete result.testResultTemplateSubjects;

            if (result.document && result.document.resultSheet) {
                result.resultSheetPath = result.document.resultSheet;
            } else {
                result.resultSheetPath = '';
            }
            delete result.document;

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all test result templates with pagination and search
 * @param  {} req
 * @param  {} res
 */
module.exports.allTestResultTemplateWithPagination = async (req, res) => {
    try {
        await models.examType.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.document.sync({ force: false });
        await models.testResultTemplate.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let examTypeWhere = {};

        if (req.query.searchKey) {
            examTypeWhere = {
                status: true,
                examType: { [Op.iLike]: '%' + req.query.searchKey + '%' },
            }
        }

        let where = {
            instituteId,
            status: true
        };

        // Batch Filter
        if (req.query.batchId) {
            where.batchId = req.query.batchId;
        }

        if (req.query.examTypeId) {
            where.examTypeId = req.query.examTypeId;
        }

        if (req.query.startDate && req.query.endDate) {
            let startDate = req.query.startDate + 'T00:00:00.0000';
            let endDate = req.query.endDate + 'T23:59:59.0000';

            where.createdAt = {
                [Op.between]: [startDate, endDate]
            };
        }

        let response = await models.testResultTemplate.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            include: [
                {
                    model: models.examType,
                    attributes: ['examType'],
                    where: examTypeWhere,
                },
                {
                    model: models.batch,
                    attributes: ['batchName'],
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'resultSheet']]
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                asyncL.each(response.rows, (data, callback) => {
                    (async () => {
                        if (data.document && data.document.resultSheet) {
                            data.resultSheet = data.document.resultSheet;
                        } else {
                            data.resultSheet = '';
                        }
                        delete data.document;

                        data.batchName = data.batch.batchName;
                        delete data.batch;

                        data.testType = data.examType.examType;
                        delete data.examType;

                        callback();
                    })();
                }, async (err) => {
                    let result = {
                        total: response.count,
                        rows: response.rows
                    }
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
                });
            } else {
                let result = {
                    total: response.count,
                    rows: response.rows
                }
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description delete a test result template
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteTestResultTemplate = async (req, res) => {
    try {
        await models.testResultTemplate.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });

        let { id } = req.params;

        await models.testResultTemplate.update({
            status: false
        }, {
            where: { id, status: true },
        });

        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Publish test result for student and staff
 * @param  {} req
 * @param  {} res
 */
module.exports.publishTestResult = async (req, res) => {
    try {
        await models.testResultTemplate.sync({ force: false });

        let { id } = req.params;
        let body = req.body;

        await models.testResultTemplate.update(body, {
            where: { id, status: true },
        });

        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Download test result template
 * @param  {} req
 * @param  {} res
 */
module.exports.downloadTestResultTemplate = async (req, res) => {
    try {
        await models.testResultTemplate.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.testResultTemplateSubject.sync({ force: false });
        await models.subject.sync({ force: false });

        let { id } = req.params;

        let testResultTemplate = await models.testResultTemplate.findOne({
            where: { id, status: true },
            attributes: ["instituteId", "batchId"],
            include: [
                {
                    model: models.batch,
                    attributes: ['batchName']
                },
                {
                    model: models.examType,
                    attributes: ['examType']
                },
                {
                    model: models.testResultTemplateSubject,
                    attributes: ['subjectId', 'maximumMarks'],
                    include: [
                        {
                            model: models.subject,
                            attributes: ['subjectName']
                        }
                    ]
                }
            ]
        });

        if (testResultTemplate) {
            testResultTemplate = JSON.parse(JSON.stringify(testResultTemplate));

            let students = await models.userStudentMapping.findAll({
                where: {
                    instituteId: testResultTemplate.instituteId,
                    batchId: testResultTemplate.batchId,
                    status: true
                },
                attributes: ['studentId', 'batchId'],
                include: [
                    {
                        model: models.student,
                        attributes: ['firstName', 'lastName']
                    }
                ]
            });

            if (students && students.length > 0) {
                students = JSON.parse(JSON.stringify(students));

                let templateResults = [];
                let headerTitles = [
                    { header: 'Batch', key: 'batchName', width: 10 },
                    { header: 'Test Type', key: 'examType', width: 20 },
                    { header: 'Student Name', key: 'studentName', width: 20 },
                    { header: 'Unique ID', key: 'uniqueId', width: 10 },
                ];

                for (let i = 0; i < testResultTemplate.testResultTemplateSubjects.length; i++) {
                    let element = testResultTemplate.testResultTemplateSubjects[i];

                    headerTitles.push({ header: `${element.subject.subjectName} Marks`, key: element.subjectId, width: 20 });
                    headerTitles.push({ header: element.subject.subjectName + ' Maximum Score', key: 'maximumMarks' + element.subjectId, width: 20 });
                }

                for (i = 0; i < students.length; i++) {
                    let student = students[i];

                    let getUniqueId = await models.studentBatchUniqueId.findOne({
                        where: {
                            studentId: student.studentId,
                            batchId: student.batchId,
                        },
                        attributes: ['uniqueId']
                    });

                    let marksData = {
                        batchName: testResultTemplate.batch.batchName,
                        examType: testResultTemplate.examType.examType,
                        uniqueId: getUniqueId.uniqueId,
                        studentName: `${student.student.firstName} ${student.student.lastName}`
                    };

                    for (let i = 0; i < testResultTemplate.testResultTemplateSubjects.length; i++) {
                        let element = testResultTemplate.testResultTemplateSubjects[i];

                        marksData[element.subjectId] = '';
                        marksData['maximumMarks' + element.subjectId] = element.maximumMarks;
                    }

                    templateResults.push(marksData)
                }

                // Create Excel workbook and worksheet
                const workbook = new Excel.Workbook();
                const worksheet = workbook.addWorksheet('Test Result Template');

                // Define columns in the worksheet, these columns are identified using a key.
                worksheet.columns = headerTitles;

                worksheet.getRow(1).font = {
                    size: 10,
                    bold: true,
                    wrapText: true,
                    vertical: 'center',
                    horizontal: 'center'
                };

                // Add rows from database to worksheet 
                for (const row of templateResults) {
                    worksheet.addRow(row).font = {
                        size: 11,
                        wrapText: true,
                        vertical: 'center',
                        horizontal: 'center'
                    };
                }

                // File name & Path
                let fileName = `Test_Result_Template_${moment().format("HH_mm_ss")}.xlsx`;
                let filePath = path.join(__dirname, '../../../', `/${fileName}`);

                // Finally save the worksheet into the folder from where we are running the code. 
                await workbook.xlsx.writeFile(fileName);

                // Download Template File
                res.download(filePath, fileName);

                // Remove file from directory
                setTimeout(function () {
                    fs.unlinkSync(filePath);
                }, 1000);
            } else {
                return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Upload student test results
 * @param  {} req
 * @param  {} res
 */
module.exports.uploadTestResult = async (req, res) => {
    try {
        await models.testResultTemplate.sync({ force: false });
        await models.studentTestResult.sync({ force: false });
        await models.student.sync({ force: false });
        await models.subject.sync({ force: false });

        let { id } = req.body;

        if (req.files.studentResultAttachment && req.files.studentResultAttachment.length > 0) {
            let studentResultAttachment = req.files.studentResultAttachment[0];
            let filePath = path.join(studentResultAttachment.destination, studentResultAttachment.originalname);
            let fileName = studentResultAttachment.originalname;

            // Parse a file
            xlsxFile(filePath).then(async (rows) => {
                if (rows && rows.length > 0) {
                    let key = rows[0];
                    let studentTestResultData = [];

                    for (let i = 1; i < rows.length; i++) {
                        let element = rows[i];
                        let studentObj = {};

                        for (let j = 0; j < element.length; j++) {
                            studentObj[key[j]] = element[j];
                        }

                        studentTestResultData.push(studentObj);
                    }

                    if (studentTestResultData && studentTestResultData.length > 0) {
                        let errors = [];
                        let subjectData = [];

                        // Check subjects
                        let keyElements = Object.keys(studentTestResultData[0]);
                        for (let index = 4; index < keyElements.length; index += 2) {
                            let headerSubjectName = keyElements[index];
                            let subjectName = headerSubjectName.replace('Marks', '')
                            subjectName = subjectName.trim();

                            let subjectMarks = await models.testResultTemplateSubject.findOne({
                                where: { testResultTemplateId: id },
                                attributes: ['maximumMarks'],
                                include: [{
                                    model: models.subject,
                                    attributes: ['id', 'subjectName'],
                                    where: {
                                        subjectName: subjectName
                                    },
                                    require: true
                                }]
                            });

                            if (subjectMarks) {
                                subjectMarks = JSON.parse(JSON.stringify(subjectMarks));

                                subjectData.push({
                                    subjectId: subjectMarks.subject.id,
                                    subjectName: subjectMarks.subject.subjectName,
                                    maximumMarks: subjectMarks.maximumMarks,
                                });
                            } else {
                                errors.push(`${subjectName} is not found in our records.`);
                            }
                        }

                        if (errors && errors.length > 0) {
                            let messages = errors.join(', ');
                            return res.send(await commonResponse.response(false, messages));
                        } else {
                            let studentDataErrors = [];
                            let studentsSubjectMarks = [];

                            asyncL.each(studentTestResultData, (element, callback) => {
                                (async () => {
                                    let keys = Object.keys(element);

                                    let student = await models.studentBatchUniqueId.findOne({
                                        where: {
                                            uniqueId: element['Unique ID']
                                        },
                                        attributes: ['studentId']
                                    });

                                    if (!student) {
                                        studentDataErrors.push(`${element['Student Name']} student is not exixts in our records.`);
                                    }

                                    let studentId = student.studentId;

                                    for (let index = 4; index < keys.length; index += 2) {
                                        const keyElement = keys[index];

                                        let subjectName = keyElement.replace('Marks', '')
                                        subjectName = subjectName.trim();

                                        let subjectMarks = parseFloat(element[keyElement]);

                                        let getSubjectInfo = subjectData.find(data => data.subjectName == subjectName);
                                        if (getSubjectInfo && getSubjectInfo != undefined) {
                                            let subjectMaximumMarks = parseFloat(getSubjectInfo.maximumMarks);

                                            if (subjectMarks <= subjectMaximumMarks) {
                                                studentsSubjectMarks.push({
                                                    testResultTemplateId: id,
                                                    studentId,
                                                    subjectId: getSubjectInfo.subjectId,
                                                    marks: subjectMarks
                                                });
                                            } else {
                                                studentDataErrors.push(`${element['Student Name']}'s ${subjectName} marks should be less than the maximum score.`);
                                            }
                                        } else {
                                            studentDataErrors.push(`${element['Student Name']}' subject details is not found.`);
                                        }
                                    }

                                    callback();
                                })();
                            }, async (err) => {
                                if (studentDataErrors && studentDataErrors.length > 0) {
                                    let messages = studentDataErrors.join(', ');

                                    // Remove Excel
                                    await fs.unlinkSync(filePath);

                                    return res.send(await commonResponse.response(false, messages));
                                } else {
                                    // Check and remove student test result
                                    let countStudentsResult = await models.studentTestResult.count({ where: { testResultTemplateId: id } });
                                    if (countStudentsResult > 0) {
                                        await models.studentTestResult.destroy({ where: { testResultTemplateId: id } });
                                    }

                                    // Store Student Test Result
                                    await models.studentTestResult.bulkCreate(studentsSubjectMarks);

                                    // Store Template
                                    let document = await uploadFileOnS3(filePath, fileName, 'student', true);
                                    await models.testResultTemplate.update({ documentId: document.documentId }, { where: { id: id } });

                                    return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                                }
                            });
                        }
                    } else {
                        return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
                    }
                } else {
                    return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
                }
            });
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Create test schedule
 * @param  {} req
 * @param  {} res
 */
module.exports.createTestSchedule = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });
        await models.testScheduleClassBatchGroup.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { testScheduleType, title, documentId, classId, groupIds, batchIds, classIds } = req.body;

        if (testScheduleType == commonModuleType.Class) {
            if (!classIds && classIds.length == 0) {
                return res.json(await commonResponse.response(false, "Class Ids are required"));
            }
        }

        if (testScheduleType == commonModuleType.Batch || testScheduleType == commonModuleType.Group) {
            if (!classId) {
                return res.json(await commonResponse.response(false, "Class Id is required"));
            }
        }

        if (testScheduleType == commonModuleType.Batch) {
            if (!batchIds && batchIds.length == 0) {
                return res.json(await commonResponse.response(false, "Batch Ids are required"));
            }
        }

        if (testScheduleType == commonModuleType.Group) {
            if (!groupIds && groupIds.length == 0) {
                return res.json(await commonResponse.response(false, "Group Ids are required"));
            }
        }

        // Create Test Schedule
        let testSchedule = await models.testSchedule.create({
            instituteId,
            testScheduleType,
            documentId,
            title
        });

        if (testSchedule) {
            let testScheduleId = testSchedule.id;

            if (testScheduleType == commonModuleType.Class) {
                classIds = JSON.parse(JSON.stringify(classIds));
                let classTestScheduleData = classIds.map(id => {
                    return {
                        classId: id,
                        testScheduleId
                    };
                });

                await models.testScheduleClassBatchGroup.bulkCreate(classTestScheduleData);
            }

            if (testScheduleType == commonModuleType.Batch) {
                batchIds = JSON.parse(JSON.stringify(batchIds));
                let batchTestScheduleData = batchIds.map(batchId => {
                    return {
                        testScheduleId,
                        classId,
                        batchId
                    };
                });

                await models.testScheduleClassBatchGroup.bulkCreate(batchTestScheduleData);
            }

            if (testScheduleType == commonModuleType.Group) {
                groupIds = JSON.parse(JSON.stringify(groupIds));
                let groupTestScheduleData = groupIds.map(groupId => {
                    return {
                        testScheduleId,
                        classId,
                        groupId
                    };
                });

                await models.testScheduleClassBatchGroup.bulkCreate(groupTestScheduleData);
            }

            return res.json(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.json(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all test schedules
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllTestSchedulesWithPagination = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });
        await models.testScheduleClassBatchGroup.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['updatedAt', 'DESC']);
        }

        let whereClause = {
            instituteId,
            status: true
        };

        if (req.query.searchKey) {
            whereClause.title = { [Op.iLike]: '%' + req.query.searchKey + '%' };
        }

        let classBatchGroupWhereClause = {};

        if (req.query.classId) {
            classBatchGroupWhereClause.classId = req.query.classId;
        }
        if (req.query.batchId) {
            classBatchGroupWhereClause.batchId = req.query.batchId;
        }
        if (req.query.groupId) {
            classBatchGroupWhereClause.groupId = req.query.groupId;
        }

        if (req.query.startDate && req.query.endDate) {
            let startDate = req.query.startDate + 'T00:00:00.0000';
            let endDate = req.query.endDate + 'T23:59:59.0000';

            whereClause.updatedAt = {
                [Op.between]: [startDate, endDate]
            };
        }

        let response = await models.testSchedule.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: whereClause,
            attributes: ['id', 'title', 'testScheduleType', 'updatedAt'],
            include: [
                {
                    model: models.document,
                    attributes: [['fullPath', 'testScheduleFile']]
                },
                {
                    model: models.testScheduleClassBatchGroup,
                    attributes: [],
                    where: classBatchGroupWhereClause
                }
            ],
            group: ['testSchedule.id']
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                asyncL.each(response.rows, (data, callback) => {
                    (async () => {
                        let testScheduleType = data.testScheduleType;

                        if (data.document && data.document.testScheduleFile) {
                            data.testScheduleFile = data.document.testScheduleFile;
                        } else {
                            data.testScheduleFile = '';
                        }
                        delete data.document;

                        // For class, batch and group
                        if (testScheduleType == commonModuleType.Class) {
                            let testScheduleClasses = await models.testScheduleClassBatchGroup.findAll({
                                where: { testScheduleId: data.id },
                                attributes: [],
                                include: [{
                                    model: models.class,
                                    attributes: ['className']
                                }]
                            });

                            let classes = '';
                            if (testScheduleClasses && testScheduleClasses.length > 0) {
                                classes = testScheduleClasses.map(data => data.class.className);
                                classes = classes.join(', ').trim();
                            }

                            data.classBatchSubject = classes;
                        }

                        if (testScheduleType == commonModuleType.Batch) {
                            let testScheduleClassBatches = await models.testScheduleClassBatchGroup.findAll({
                                where: { testScheduleId: data.id },
                                attributes: [],
                                include: [
                                    {
                                        model: models.class,
                                        attributes: ['className']
                                    },
                                    {
                                        model: models.batch,
                                        attributes: ['batchName']
                                    }
                                ]
                            });

                            let classBatchNames = '';
                            if (testScheduleClassBatches && testScheduleClassBatches.length > 0) {
                                let className = testScheduleClassBatches[0].class.className;
                                let classBatches = testScheduleClassBatches.map(data => {
                                    return className + ' - ' + data.batch.batchName;
                                });

                                classBatchNames = classBatches.join(', ').trim();
                            }

                            data.classBatchSubject = classBatchNames;
                        }

                        if (testScheduleType == commonModuleType.Group) {
                            let testScheduleClassGroups = await models.testScheduleClassBatchGroup.findAll({
                                where: { testScheduleId: data.id },
                                attributes: [],
                                attributes: [],
                                include: [
                                    {
                                        model: models.class,
                                        attributes: ['className']
                                    },
                                    {
                                        model: models.group,
                                        attributes: ['groupName']
                                    }
                                ]
                            });

                            let classGroupNames = '';
                            if (testScheduleClassGroups && testScheduleClassGroups.length > 0) {
                                let className = testScheduleClassGroups[0].class.className;
                                let classGroups = testScheduleClassGroups.map(data => {
                                    return className + ' - ' + data.group.groupName;
                                });

                                classGroupNames = classGroups.join(', ').trim();
                            }

                            data.classBatchSubject = classGroupNames;
                        }

                        callback();
                    })();
                }, async (err) => {
                    let result = {
                        total: response.count.length,
                        rows: response.rows
                    }
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
                });
            } else {
                let result = {
                    total: response.count.length,
                    rows: response.rows
                }
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, []));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get a test schedule by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getTestScheduleById = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });
        await models.testScheduleClassBatchGroup.sync({ force: false });
        await models.document.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let { id } = req.params;

        let response = await models.testSchedule.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.document,
                    attributes: [['fullPath', 'testScheduleFile']]
                }
            ]
        });

        if (response) {
            let result = JSON.parse(JSON.stringify(response));
            let id = result.id;
            let testScheduleType = result.testScheduleType;

            // For Document
            if (result.document && result.document.testScheduleFile) {
                result.testScheduleFile = result.document.testScheduleFile;
            } else {
                result.testScheduleFile = '';
            }
            delete result.document;

            // For class, batch and group
            if (testScheduleType == commonModuleType.Class) {
                let testScheduleClasses = await models.testScheduleClassBatchGroup.findAll({ where: { testScheduleId: id }, attributes: ['classId'] });

                let classIds = [];
                if (testScheduleClasses && testScheduleClasses.length > 0) {
                    classIds = testScheduleClasses.map(data => data.classId);
                }

                result.classIds = classIds;
            }

            if (testScheduleType == commonModuleType.Batch) {
                let testScheduleClassBatches = await models.testScheduleClassBatchGroup.findAll({ where: { testScheduleId: id }, attributes: ['classId', 'batchId'] });

                let batchIds = [];
                let classId;
                if (testScheduleClassBatches && testScheduleClassBatches.length > 0) {
                    batchIds = testScheduleClassBatches.map(data => data.batchId);
                    classId = testScheduleClassBatches[0].classId;
                }

                result.classId = classId;
                result.batchIds = batchIds;
            }

            if (testScheduleType == commonModuleType.Group) {
                let testScheduleClassGroups = await models.testScheduleClassBatchGroup.findAll({ where: { testScheduleId: id }, attributes: ['classId', 'groupId'] });

                let groupIds = [];
                let classId;
                if (testScheduleClassGroups && testScheduleClassGroups.length > 0) {
                    groupIds = testScheduleClassGroups.map(data => data.groupId);
                    classId = testScheduleClassGroups[0].classId;
                }

                result.classId = classId;
                result.groupIds = groupIds;
            }

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update test schedule
 * @param  {} req
 * @param  {} res
 */
module.exports.updateTestSchedule = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });
        await models.testScheduleClassBatchGroup.sync({ force: false });

        let { id } = req.params;
        let { testScheduleType, title, documentId, classId, groupIds, batchIds, classIds } = req.body;
        let testScheduleId = id;

        if (testScheduleType == commonModuleType.Class) {
            if (!classIds && classIds.length == 0) {
                return res.json(await commonResponse.response(false, "Class Ids are required"));
            }
        }

        if (testScheduleType == commonModuleType.Batch || testScheduleType == commonModuleType.Group) {
            if (!classId) {
                return res.json(await commonResponse.response(false, "Class Id is required"));
            }
        }

        if (testScheduleType == commonModuleType.Batch) {
            if (!batchIds && batchIds.length == 0) {
                return res.json(await commonResponse.response(false, "Batch Ids are required"));
            }
        }

        if (testScheduleType == commonModuleType.Group) {
            if (!groupIds && groupIds.length == 0) {
                return res.json(await commonResponse.response(false, "Group Ids are required"));
            }
        }

        // Update Test Schedule
        await models.testSchedule.update({
            testScheduleType,
            documentId,
            title
        }, {
            where: {
                id
            }
        });

        // Delete 
        await models.testScheduleClassBatchGroup.destroy({
            where: {
                testScheduleId
            }
        });

        if (testScheduleType == commonModuleType.Class) {
            classIds = JSON.parse(JSON.stringify(classIds));
            let classTestScheduleData = classIds.map(id => {
                return {
                    classId: id,
                    testScheduleId
                };
            });

            await models.testScheduleClassBatchGroup.bulkCreate(classTestScheduleData);
        }

        if (testScheduleType == commonModuleType.Batch) {
            batchIds = JSON.parse(JSON.stringify(batchIds));
            let batchTestScheduleData = batchIds.map(batchId => {
                return {
                    testScheduleId,
                    classId,
                    batchId
                };
            });

            await models.testScheduleClassBatchGroup.bulkCreate(batchTestScheduleData);
        }

        if (testScheduleType == commonModuleType.Group) {
            groupIds = JSON.parse(JSON.stringify(groupIds));
            let groupTestScheduleData = groupIds.map(groupId => {
                return {
                    testScheduleId,
                    classId,
                    groupId
                };
            });

            await models.testScheduleClassBatchGroup.bulkCreate(groupTestScheduleData);
        }

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Publish test schedule
 * @param  {} req
 * @param  {} res
 */
module.exports.publishTestSchedule = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });

        let { id } = req.params;

        await models.testSchedule.update({
            isPublished: true,
            publishedAt: moment().utc("+05:30").format("YYYY-MM-DD HH:mm:ss")
        }, {
            where: {
                id
            }
        });

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete test schedule
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteTestSchedule = async (req, res) => {
    try {
        await models.testSchedule.sync({ force: false });

        let { id } = req.params;

        await models.testSchedule.update({
            status: true,
        }, {
            where: {
                id
            }
        });

        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description Create test result report card
 * @param  {} req
 * @param  {} res
 */
module.exports.createTestResultReportCard = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.testResultReportCardStudent.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { term, batchId, zipFileName } = req.body;

        let getStudents = await models.userStudentMapping.findAll({
            where: {
                instituteId, batchId
            },
            attributes: ['studentId']
        });

        if (getStudents && getStudents.length > 0) {
            getStudents = JSON.parse(JSON.stringify(getStudents));
            let studentIds = getStudents.map(data => data.studentId);

            let getUniqueIds = await models.studentBatchUniqueId.findAll({
                where: {
                    studentId: studentIds,
                    batchId
                },
                attributes: ['uniqueId', 'studentId']
            });

            let uniqueIds = JSON.parse(JSON.stringify(getUniqueIds));

            if (req.files.zipFile && req.files.zipFile.length > 0) {

                let zipFile = req.files.zipFile[0];
                let zipFilePath = zipFile.path;
                let zipFileOriginalname = zipFile.originalname;

                let document = await uploadFileOnS3(zipFilePath, zipFileOriginalname, 'student', false);

                // Create report card
                let testResultReportCard = await models.testResultReportCard.create({
                    instituteId,
                    term,
                    batchId,
                    documentId: document.documentId,
                    zipFileName
                });

                if (testResultReportCard) {
                    let testResultReportCardId = testResultReportCard.id;

                    // read a zip file
                    fs.readFile(zipFilePath, async function (err, data) {
                        if (err) {
                            return res.json(await commonResponse.response(false, Message.FILE_REQUIRED));
                        } else {
                            JSZip.loadAsync(data).then(async (zip) => {
                                let files = Object.keys(zip.files);

                                let fileUniqueIds = files.map(file => {
                                    let uniqueId = file.replace('.pdf', '');
                                    return uniqueId;
                                });

                                asyncL.each(uniqueIds, (element, callback) => {
                                    (async () => {
                                        let studentObject = {
                                            testResultReportCardId,
                                            studentId: element.studentId,
                                            documentId: null
                                        };

                                        let uniqueId = fileUniqueIds.find(uniqueId => element.uniqueId == uniqueId);

                                        if (uniqueId && uniqueId != undefined) {
                                            let pdfFiles = zip.files[`${element.uniqueId}.pdf`];

                                            const params = {
                                                Bucket: process.env.AWS_BUCKET_NAME, // pass your bucket name
                                                Key: 'student/results/' + pdfFiles.name, // file will be saved as testBucket/contacts.csv
                                                Body: JSON.stringify(pdfFiles._data, null, 2),
                                                ACL: 'public-read',
                                            };

                                            S3.upload(params, async function (s3Err, data) {
                                                if (data) {
                                                    let s3Data = data;

                                                    let document = await models.document.create({
                                                        type: "student",
                                                        etag: s3Data.ETag.substring(1, s3Data.ETag.length - 1),
                                                        fileName: s3Data.key.split('/').pop(),
                                                        filePath: s3Data.key,
                                                        fullPath: s3Data.Location,
                                                        mimeType: s3Data.mimetype,
                                                        size: s3Data.size,
                                                        originalFileName: pdfFiles.name,
                                                        fileExtension: "pdf"
                                                    });

                                                    if (document) {
                                                        studentObject.documentId = document.id;
                                                    }

                                                    // Create multiple student report card data
                                                    await models.testResultReportCardStudent.create(studentObject);
                                                    callback();
                                                } else {
                                                    callback();
                                                }
                                            });
                                        } else {
                                            // Create multiple student report card data
                                            await models.testResultReportCardStudent.create(studentObject);
                                            callback();
                                        }
                                    })();
                                }, async (err) => {
                                    await fs.unlinkSync(zipFilePath);
                                    return res.json(await commonResponse.response(true, Message.DATA_CREATED));
                                });
                            });
                        }
                    });
                } else {
                    return res.json(await commonResponse.response(false, Message.DATA_NOT_CREATED));
                }
            } else {
                return res.json(await commonResponse.response(false, Message.FILE_REQUIRED));
            }
        } else {
            return res.json(await commonResponse.response(false, Message.STUDENTS_NOT_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all test result report cards
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllTestResultReportCardWithPagination = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.document.sync({ force: false });
        await models.batch.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['updatedAt', 'DESC']);
        }

        let whereClause = {
            instituteId,
            status: true
        };

        if (req.query.searchKey) {
            whereClause.term = { [Op.iLike]: '%' + req.query.searchKey + '%' };
        }

        if (req.query.batchId) {
            whereClause.batchId = req.query.batchId;
        }

        if (req.query.startDate && req.query.endDate) {
            let startDate = req.query.startDate + 'T00:00:00.0000';
            let endDate = req.query.endDate + 'T23:59:59.0000';

            whereClause.updatedAt = {
                [Op.between]: [startDate, endDate]
            };
        }

        let response = await models.testResultReportCard.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: whereClause,
            include: [
                {
                    model: models.document,
                    attributes: [['fullPath', 'zipFile']]
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ]
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                asyncL.each(response.rows, (data, callback) => {
                    (async () => {
                        if (data.document && data.document.zipFile) {
                            data.zipFile = data.document.zipFile;
                        } else {
                            data.zipFile = '';
                        }
                        delete data.document;

                        data.batchName = data.batch.batchName;
                        delete data.batch;

                        callback();
                    })();
                }, async (err) => {
                    let result = {
                        total: response.count,
                        rows: response.rows
                    }
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
                });
            } else {
                let result = {
                    total: response.count.length,
                    rows: response.rows
                }
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND, []));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all test result report card by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getTestResultReportCardById = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.batch.sync({ force: false });

        let { id } = req.params;

        let whereClause = {
            id,
            status: true
        };

        let response = await models.testResultReportCard.findOne({
            where: whereClause,
            attributes: ['id', 'term', 'zipFileName'],
            include: [
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ]
        });

        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update test result report card
 * @param  {} req
 * @param  {} res
 */
module.exports.updateTestResultReportCard = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.testResultReportCardStudent.sync({ force: false });
        await models.batch.sync({ force: false });

        let { id } = req.params;

        let whereClause = {
            id,
            status: true
        };

        let response = await models.testResultReportCard.findOne({
            where: whereClause,
            include: [
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ]
        });

        if (response) {
            let testResultReportCardId = id;

            if (req.files.zipFile && req.files.zipFile.length > 0) {
                let zipFile = req.files.zipFile[0];
                let zipFilePath = zipFile.path;
                let zipFileOriginalname = zipFile.originalname;

                let getStudents = await models.userStudentMapping.findAll({
                    where: {
                        instituteId: response.instituteId,
                        batchId: response.batchId
                    },
                    attributes: ['studentId']
                });

                if (getStudents && getStudents.length > 0) {
                    getStudents = JSON.parse(JSON.stringify(getStudents));
                    let studentIds = getStudents.map(data => data.studentId);

                    let getUniqueIds = await models.studentBatchUniqueId.findAll({
                        where: {
                            studentId: studentIds,
                            batchId: response.batchId
                        },
                        attributes: ['uniqueId', 'studentId']
                    });

                    let uniqueIds = JSON.parse(JSON.stringify(getUniqueIds));

                    let document = await uploadFileOnS3(zipFilePath, zipFileOriginalname, 'student', false);

                    // Update document
                    await models.testResultReportCard.update({
                        documentId: document.documentId,
                        isPublished: false,
                        isPublishedAt: null
                    }, { where: { id } });

                    // Delete all student document
                    await models.testResultReportCardStudent.destroy({ where: { testResultReportCardId } });

                    // read a zip file
                    fs.readFile(zipFilePath, async function (err, data) {
                        if (err) {
                            return res.json(await commonResponse.response(false, Message.FILE_REQUIRED));
                        } else {
                            JSZip.loadAsync(data).then(async (zip) => {
                                let files = Object.keys(zip.files);

                                let fileUniqueIds = files.map(file => {
                                    let uniqueId = file.replace('.pdf', '');
                                    return uniqueId;
                                });

                                asyncL.each(uniqueIds, (element, callback) => {
                                    (async () => {
                                        let studentObject = {
                                            testResultReportCardId,
                                            studentId: element.studentId,
                                            documentId: null
                                        };

                                        let uniqueId = fileUniqueIds.find(uniqueId => element.uniqueId == uniqueId);

                                        if (uniqueId && uniqueId != undefined) {
                                            let pdfFiles = zip.files[`${element.uniqueId}.pdf`];

                                            const params = {
                                                Bucket: process.env.AWS_BUCKET_NAME, // pass your bucket name
                                                Key: 'student/results/' + pdfFiles.name, // file will be saved as testBucket/contacts.csv
                                                Body: JSON.stringify(pdfFiles._data, null, 2),
                                                ACL: 'public-read',
                                            };

                                            S3.upload(params, async function (s3Err, data) {
                                                if (data) {
                                                    let s3Data = data;

                                                    let document = await models.document.create({
                                                        type: "student",
                                                        etag: s3Data.ETag.substring(1, s3Data.ETag.length - 1),
                                                        fileName: s3Data.key.split('/').pop(),
                                                        filePath: s3Data.key,
                                                        fullPath: s3Data.Location,
                                                        mimeType: s3Data.mimetype,
                                                        size: s3Data.size,
                                                        originalFileName: pdfFiles.name,
                                                        fileExtension: "pdf"
                                                    });

                                                    if (document) {
                                                        studentObject.documentId = document.id;
                                                    }

                                                    // Create multiple student report card data
                                                    await models.testResultReportCardStudent.create(studentObject);
                                                    callback();
                                                } else {
                                                    callback();
                                                }
                                            });
                                        } else {
                                            // Create multiple student report card data
                                            await models.testResultReportCardStudent.create(studentObject);
                                            callback();
                                        }
                                    })();
                                }, async (err) => {
                                    await fs.unlinkSync(zipFilePath);
                                    return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
                                });
                            });
                        }
                    });
                } else {
                    return res.json(await commonResponse.response(false, Message.STUDENTS_NOT_FOUND));
                }
            } else {
                return res.json(await commonResponse.response(false, Message.FILE_REQUIRED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete test result report card
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteTestResultReportCard = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });

        let { id } = req.params;

        // Delete
        await models.testResultReportCard.update({
            status: false
        }, { where: { id } });

        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all students's report card by test result report card id
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllStudentsReportCardById = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.testResultReportCardStudent.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.document.sync({ force: false });
        await models.student.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let { id } = req.params;

        let whereClause = {
            id,
            status: true
        };

        let response = await models.testResultReportCard.findOne({
            where: whereClause,
            attributes: ['term', 'batchId'],
            include: [
                {
                    model: models.batch,
                    attributes: ['batchName']
                },
                {
                    model: models.testResultReportCardStudent,
                    attributes: ['id', 'studentId', 'documentId', 'createdAt', 'isPublished', 'publishedAt'],
                    include: [
                        {
                            model: models.document,
                            attributes: [['fullPath', 'studentReportCard']]
                        },
                        {
                            model: models.student,
                            attributes: ['firstName', 'lastName'],
                            include: [{
                                model: models.studentBatchUniqueId,
                                attributes: ['uniqueId']
                            }]
                        }
                    ]
                }
            ]
        });

        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get student report card by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getStudentsReportCardById = async (req, res) => {
    try {
        await models.testResultReportCard.sync({ force: false });
        await models.testResultReportCardStudent.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.document.sync({ force: false });
        await models.student.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let { id } = req.params;

        let whereClause = {
            id,
            status: true
        };

        let response = await models.testResultReportCardStudent.findOne({
            where: whereClause,
            attributes: [],
            include: [
                {
                    model: models.student,
                    attributes: ['firstName', 'lastName'],
                    include: [{
                        model: models.studentBatchUniqueId,
                        attributes: ['uniqueId']
                    }]
                },
                {
                    model: models.testResultReportCard,
                    attributes: ['term'],
                    include: [
                        {
                            model: models.batch,
                            attributes: ['batchName']
                        }
                    ]
                }
            ]
        });

        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update student report card
 * @param  {} req
 * @param  {} res
 */
module.exports.updateStudentsReportCard = async (req, res) => {
    try {
        await models.testResultReportCardStudent.sync({ force: false });

        let { id } = req.params;
        let { documentId } = req.body;

        let whereClause = {
            id,
            status: true
        };

        await models.testResultReportCardStudent.update({
            documentId,
            isPublished: false,
            publishedAt: null
        }, {
            where: {
                id
            }
        });

        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update student report card
 * @param  {} req
 * @param  {} res
 */
module.exports.publishStudentsReportCard = async (req, res) => {
    try {
        await models.testResultReportCardStudent.sync({ force: false });

        let { id } = req.params;

        await models.testResultReportCardStudent.update({
            isPublished: true,
            publishedAt: moment().utc("+05:30").format("YYYY-MM-DD HH:mm:ss")
        }, {
            where: {
                id
            }
        });

        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete student report card
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteStudentsReportCard = async (req, res) => {
    try {
        await models.testResultReportCardStudent.sync({ force: false });

        let { id } = req.params;

        await models.testResultReportCardStudent.update({
            documentId: null,
            isPublished: false,
            publishedAt: null
        }, {
            where: {
                id
            }
        });

        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Create previous year test paper
 * @param  {} req
 * @param  {} res
 */
module.exports.createPreviousYearTestPaper = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { examTypeId, classId, subjectId, documentId, year } = req.body;

        let checkRecord = await models.previousYearTestPaper.count({
            where: { instituteId, examTypeId, classId, subjectId }
        });

        if (checkRecord > 0) {
            return res.json(await commonResponse.response(false, Message.ALREADY_EXISTS));
        } else {
            let previousYearTestPaper = await models.previousYearTestPaper.create({
                instituteId, examTypeId, classId, subjectId, documentId, year
            });

            if (previousYearTestPaper) {
                return res.json(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.json(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get all previous year test papers with pagination and search
 * @param  {} req
 * @param  {} res
 */
module.exports.allPreviousYearTestPaperWithPagination = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });
        await models.examType.sync({ force: false });
        await models.document.sync({ force: false });
        await models.class.sync({ force: false });
        await models.subject.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let subjectWhere = {};

        if (req.query.searchKey) {
            subjectWhere = {
                subjectName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
            }
        }

        let where = {
            instituteId,
            status: true
        };

        if (req.query.year) {
            where.year = req.query.year;
        }

        if (req.query.classId) {
            where.classId = req.query.classId;
        }

        if (req.query.examTypeId) {
            where.examTypeId = req.query.examTypeId;
        }

        if (req.query.startDate && req.query.endDate) {
            let startDate = req.query.startDate + 'T00:00:00.0000';
            let endDate = req.query.endDate + 'T23:59:59.0000';

            where.createdAt = {
                [Op.between]: [startDate, endDate]
            };
        }

        let response = await models.previousYearTestPaper.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            include: [
                {
                    model: models.examType,
                    attributes: ['examType'],
                },
                {
                    model: models.subject,
                    attributes: ['subjectName'],
                    where: subjectWhere
                },
                {
                    model: models.class,
                    attributes: ['className'],
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'testPaper']]
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                asyncL.each(response.rows, (data, callback) => {
                    (async () => {
                        if (data.document && data.document.testPaper) {
                            data.testPaper = data.document.testPaper;
                        } else {
                            data.testPaper = '';
                        }
                        delete data.document;

                        data.className = data.class.className;
                        delete data.class;

                        data.subjectName = data.subject.subjectName;
                        delete data.subject;

                        data.examType = data.examType.examType;
                        delete data.examType;

                        callback();
                    })();
                }, async (err) => {
                    let result = {
                        total: response.count,
                        rows: response.rows
                    }
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
                });
            } else {
                let result = {
                    total: response.count,
                    rows: response.rows
                }
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Get previous year test paper by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getPreviousYearTestPaperById = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });

        let { id } = req.params;

        let previousYearTestPaper = await models.previousYearTestPaper.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.examType,
                    attributes: ['examType'],
                },
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.class,
                    attributes: ['className'],
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'testPaper']]
                }
            ]
        });

        if (previousYearTestPaper) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, previousYearTestPaper));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Update previous year test paper
 * @param  {} req
 * @param  {} res
 */
module.exports.updatePreviousYearTestPaper = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });

        let { id } = req.params;
        let { documentId, year, subjectId } = req.body;

        await models.previousYearTestPaper.update({
            documentId, year, subjectId,
            isPublished: false,
            publishedAt: null,
            isRePublished: false,
            rePublishedAt: null
        }, {
            where: { id },
        });

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Publish previous year test paper
 * @param  {} req
 * @param  {} res
 */
module.exports.publishPreviousYearTestPaper = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });

        let { id } = req.params;
        let body = req.body;

        await models.previousYearTestPaper.update(body, {
            where: { id },
        });

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete previous year test paper
 * @param  {} req
 * @param  {} res
 */
module.exports.deletePreviousYearTestPaper = async (req, res) => {
    try {
        await models.previousYearTestPaper.sync({ force: false });

        let { id } = req.params;

        await models.previousYearTestPaper.update({
            status: false
        }, {
            where: { id },
        });

        return res.json(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}