const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormType, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const asyncL = require('async');

/**
 * @description create announcement for student
 * @param  {} req
 * @param  {} res
 */
module.exports.createAnnouncementStudent = async (req, res) => {
    try {
        await models.announcementStudent.sync({ force: false });
        await models.announcementStudentType.sync({ force: false });
        await models.announcementStaff.sync({ force: false });
        await models.announcementStaffType.sync({ force: false });
        let profileinfo = req.authData.data.instituteId;

        let {
            announcementType,
            title,
            description,
            announcementDocument,
            classId,
            classIds,
            batchIds,
            groupIds
        } = req.body;

        let response = await models.announcementStudent.create({
            instituteId: profileinfo,
            announcementType,
            title,
            description,
            announcementDocument,
        });
        if (response) {
            if (announcementType == "Class") {
                classIds.forEach(async (classes) => {
                    await models.announcementStudentType.create({
                        classId: classes,
                        announcementStudentId: response.id,
                    });
                });
            }
            if (announcementType == "Batch") {
                batchIds.forEach(async (batchId) => {
                    await models.announcementStudentType.create({
                        batchId,
                        classId,
                        announcementStudentId: response.id,
                    });
                });
            }
            if (announcementType == "Group") {
                groupIds.forEach(async (groupId) => {
                    await models.announcementStudentType.create({
                        groupId,
                        classId,
                        announcementStudentId: response.id,
                    });
                });
            }
            if (response) {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description get all student announcement List
 * @param  {} req
 * @param  {} res
 */
module.exports.allAnnouncementStudentList = async (req, res) => {
    try {
        await models.announcementStudent.sync({ force: false });
        await models.announcementStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }


        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['announcementStudentId', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            }
        } else {
            where = {
                status: true,
                instituteId: profileinfo,
                [Op.or]: {
                    '$announcementStudent.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$batch.batchName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$group.groupName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.batchId) {
            where.batchId = req.query.batchId;
        }
        if (req.query.groupId) {
            where.groupId = req.query.groupId;
        }


        let response = await models.announcementStudentType.findAll({
            attributes: ['announcementStudentId'],
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.announcementStudent,
                where: { status: true, instituteId: profileinfo },
                include: [{
                    model: models.document,
                    required: true,
                },
                ],
                required: true
            },
            {
                model: models.class,
                attributes: [],
            }, {
                model: models.batch,
                attributes: [],

            }, {
                model: models.group,
                attributes: [],

            },],
            group: ['announcementStudent.id', 'announcementStudentId', 'announcementStudent->document.id']
        });


        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));

            asyncL.each(response, (element, callback) => {
                (async () => {
                    let announcementStudentId = element.announcementStudentId;

                    let announcementStudentTypes = await models.announcementStudentType.findAll({
                        where: { announcementStudentId },
                        include: [
                            {
                                model: models.class,
                                attributes: ['className'],
                                required: false,

                            }, {
                                model: models.batch,
                                attributes: ['batchName'],

                            }, {
                                model: models.group,
                                attributes: ['groupName'],

                            }
                        ]
                    });
                    // element.data = announcement

                    let classes = '';
                    let classAndBatchAndSubject = '';

                    if (element.announcementStudent.announcementType == FeedbackFormType.Class) {
                        classAndBatchAndSubject = announcementStudentTypes.map(data => {
                            return data.class.className;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    } else if (element.announcementStudent.announcementType == FeedbackFormType.Batch) {
                        classAndBatchAndSubject = announcementStudentTypes.map(data => {
                            return `${data.class.className} - ${data.batch.batchName}`;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    } else if (element.announcementStudent.announcementType == FeedbackFormType.Group) {
                        classAndBatchAndSubject = announcementStudentTypes.map(data => {
                            return `${data.class.className} - ${data.group.groupName}`;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    }

                    element.classAndBatchAndSubject = classAndBatchAndSubject;

                    callback();
                })();
            }, async (err) => {
                let totalRecords = await models.announcementStudentType.findAll({
                    attributes: ['announcementStudentId'],
                    where: where,
                    include: [{
                        model: models.announcementStudent,
                        where: { status: true, instituteId: profileinfo },
                        include: [{
                            model: models.document,
                            required: true,
                        },
                        ],
                        required: true
                    },
                    {
                        model: models.class,
                        attributes: [],
                    }, {
                        model: models.batch,
                        attributes: [],

                    }, {
                        model: models.group,
                        attributes: [],

                    },],
                    group: ['announcementStudent.id', 'announcementStudentId', 'announcementStudent->document.id']
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                    "count": totalRecords.length,
                    "rows": response
                }));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                "count": 0,
                "rows": []
            }));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description create announcement for staff
 * @param  {} req
 * @param  {} res
 */
module.exports.createAnnouncementStaff = async (req, res) => {
    try {
        await models.announcementStaff.sync({ force: false });
        await models.announcementStaffType.sync({ force: false });
        let profileinfo = req.authData.data.instituteId;

        let {
            announcementType,
            title,
            description,
            announcementDocument,
            classIds,
        } = req.body;

        let response = await models.announcementStaff.create({
            instituteId: profileinfo,
            announcementType,
            description,
            title,
            announcementDocument,
        });
        if (response) {
            if (announcementType == "Class") {
                classIds.forEach(async (classes) => {
                    await models.announcementStaffType.create({
                        classId: classes.classId,
                        announcementStaffId: response.id,
                    });
                });
            }
            if (announcementType == "Subject") {
                classIds.forEach(async (classes) => {
                    classes.subjectIds.forEach(async (subjects) => {
                        await models.announcementStaffType.create({
                            classId: classes.classId,
                            subjectId: subjects,
                            announcementStaffId: response.id,
                        });
                    });

                });
            }

            if (response) {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description get all staff announcement List
 * @param  {} req
 * @param  {} res
 */
module.exports.allAnnouncementStaffList = async (req, res) => {
    try {
        await models.announcementStudent.sync({ force: false });
        await models.announcementStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['announcementStaffId', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            }
        } else {
            where = {
                status: true,
                [Op.or]: {
                    '$announcementStaff.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.subjectId) {
            where.subjectId = req.query.subjectId;
        }


        let response = await models.announcementStaffType.findAll({
            attributes: ['announcementStaffId'],
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.announcementStaff,
                where: { instituteId: profileinfo },
                required: true,
                include: [{
                    model: models.document,
                    required: true,
                },]
            }, {
                model: models.class,
                attributes: [],
                required: false,

            }, {
                model: models.subject,
                attributes: [],
                required: false,

            },],
            group: ['announcementStaff.id', 'announcementStaffId', 'announcementStaff->document.id']
        });

        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));

            asyncL.each(response, (element, callback) => {
                (async () => {
                    let announcementStaffId = element.announcementStaffId;

                    let announcementStaffTypes = await models.announcementStaffType.findAll({
                        where: { announcementStaffId },
                        include: [
                            {
                                model: models.class,
                                attributes: ['className'],
                                required: false,

                            }, {
                                model: models.subject,
                                attributes: ['subjectName'],
                                required: false,

                            }
                        ]
                    });
                    // element.data = announcement

                    let classes = '';
                    let classAndSubject = '';

                    if (element.announcementStaff.announcementType == FeedbackFormType.Class) {
                        classes = announcementStaffTypes.map(data => {
                            return data.class.className;
                        });

                        classes = classes.join(', ');
                    } else if (element.announcementStaff.announcementType == FeedbackFormType.Subject) {
                        classes = announcementStaffTypes.map(data => {
                            return data.class.className;
                        });

                        // filtering unique id from the array

                        classes = classes.filter((x, i, a) => a.indexOf(x) === i)
                        classAndSubject = announcementStaffTypes.map(data => {
                            return `${data.class.className} - ${data.subject.subjectName}`;
                        });
                        classes = classes.join(', ');
                        classAndSubject = classAndSubject.join(', ');
                    }

                    element.class = classes;
                    element.classAndSubject = classAndSubject;

                    callback();
                })();
            }, async (err) => {
                let totalRecords = await models.announcementStaffType.findAll({
                    attributes: ['announcementStaffId'],
                    where: where,
                    include: [{
                        model: models.announcementStaff,
                        required: true,
                        include: [{
                            model: models.document,
                            required: true,
                        },]
                    }, {
                        model: models.class,
                        attributes: [],
                        required: false,

                    }, {
                        model: models.subject,
                        attributes: [],
                        required: false,

                    },],
                    group: ['announcementStaff.id', 'announcementStaffId', 'announcementStaff->document.id']
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                    "count": totalRecords.length,
                    "rows": response
                }));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                "count": 0,
                "rows": []
            }));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description get  staff announcement by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getAnnouncementStaffById = async (req, res) => {
    try {
        await models.announcementStudent.sync({ force: false });
        await models.announcementStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;
        let { announcementId } = req.params;
        let where = {};
        where = {
            id: announcementId,
            status: true,
            instituteId: profileinfo
        }
        let response = await models.announcementStaff.findOne({
            where: where,
            include: [{
                model: models.announcementStaffType,
                // attributes: ['announcementStaffId', 'classId', 'subjectId'],
                required: false,
                include: [{
                    model: models.class,
                    attributes: ['className'],
                    required: false,

                }, {
                    model: models.subject,
                    attributes: ['subjectName'],
                    required: false,

                },]
            }, {
                model: models.document,
                required: false,
            }],
        });

        let msg = response ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description get  student announcement by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getAnnouncementStudentById = async (req, res) => {
    try {
        await models.announcementStudent.sync({ force: false });
        await models.announcementStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;
        let { announcementId } = req.params;

        let where = {};
        where = {
            id: announcementId,
            status: true,
            instituteId: profileinfo
        }



        let response = await models.announcementStudent.findOne({
            where: where,
            include: [{
                model: models.announcementStudentType,
                attributes: ['announcementStudentId', 'classId', 'batchId', 'groupId'],
                required: false,
                include: [{
                    model: models.class,
                    attributes: ['className'],
                    required: false,

                }, {
                    model: models.batch,
                    attributes: ['batchName'],
                    required: false,

                }, {
                    model: models.group,
                    attributes: ['groupName'],
                    required: false,

                }]
            }, {
                model: models.document,
                required: false,
            }],
        });

        let msg = response ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description delete announcement by id
 * @param  {} req
 * @param  {} res
 */

module.exports.deleteAnnouncement = async (req, res) => {
    try {
        let instituteId = req.authData.data.instituteId;
        let { announcementId } = req.params;
        let { type } = req.body;
        if (type == Usertypes.STUDENT) {

            let announcementtype = await models.announcementStudent.findOne({
                where: { id: announcementId, instituteId, status: true }
            })
            if (announcementtype) {
                await models.announcementStudent.update({ status: false }, {
                    where: {
                        id: announcementId,
                        instituteId
                    }
                });

                await models.announcementStudentType.update({ status: false }, {
                    where: {
                        announcementStudentId: announcementId
                    }
                })
                return res.send(await commonResponse.response(true, Message.DATA_DELETED))

            } else {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else if (type == Usertypes.STAFF) {

            let announcementtype = await models.announcementStaff.findOne({
                where: { id: announcementId, instituteId, status: true }
            })
            if (announcementtype) {
                await models.announcementStaff.update({ status: false }, {
                    where: {
                        id: announcementId,
                        instituteId
                    }
                });

                await models.announcementStaffType.update({ status: false }, {
                    where: {
                        announcementStaffId: announcementId
                    }
                })
                return res.send(await commonResponse.response(true, Message.DATA_DELETED))

            } else {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}