const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormType, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const asyncL = require('async');

/**
 * @description create notice for student
 * @param  {} req
 * @param  {} res
 */
module.exports.createNoticeStudent = async (req, res) => {
    try {
        await models.noticeStudent.sync({ force: false });
        await models.noticeStudentType.sync({ force: false });
        let profileinfo = req.authData.data.instituteId;

        let {
            noticeType,
            title,
            noticeDocument,
            classId,
            classIds,
            batchIds,
            groupIds
        } = req.body;

        let response = await models.noticeStudent.create({
            instituteId: profileinfo,
            noticeType,
            title,
            noticeDocument,
        });
        if (response) {
            if (noticeType == "Class") {
                classIds.forEach(async (classes) => {
                    await models.noticeStudentType.create({
                        classId: classes,
                        noticeStudentId: response.id,
                    });
                });
            }
            if (noticeType == "Batch") {
                batchIds.forEach(async (batchId) => {
                    await models.noticeStudentType.create({
                        batchId,
                        classId,
                        noticeStudentId: response.id,
                    });
                });
            }
            if (noticeType == "Group") {
                groupIds.forEach(async (groupId) => {
                    await models.noticeStudentType.create({
                        groupId,
                        classId,
                        noticeStudentId: response.id,
                    });
                });
            }
            if (response) {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description get all student Notice List
 * @param  {} req
 * @param  {} res
 */
module.exports.allNoticeStudentList = async (req, res) => {
    try {
        await models.noticeStudent.sync({ force: false });
        await models.noticeStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }


        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['noticeStudentId', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            }
        } else {
            where = {
                status: true,
                instituteId: profileinfo,
                [Op.or]: {
                    '$noticeStudent.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$batch.batchName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$group.groupName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.batchId) {
            where.batchId = req.query.batchId;
        }
        if (req.query.groupId) {
            where.groupId = req.query.groupId;
        }


        let response = await models.noticeStudentType.findAll({
            attributes: ['noticeStudentId'],
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.noticeStudent,
                where: { status: true, instituteId: profileinfo },
                include: [{
                    model: models.document,
                    required: true,
                },
                ],
                required: true
            },
            {
                model: models.class,
                attributes: [],
            }, {
                model: models.batch,
                attributes: [],

            }, {
                model: models.group,
                attributes: [],

            },],
            group: ['noticeStudent.id', 'noticeStudentId', 'noticeStudent->document.id']
        });


        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));

            asyncL.each(response, (element, callback) => {
                (async () => {
                    let noticeStudentId = element.noticeStudentId;

                    let noticeStudentTypes = await models.noticeStudentType.findAll({
                        where: { noticeStudentId },
                        include: [
                            {
                                model: models.class,
                                attributes: ['className'],
                                required: false,

                            }, {
                                model: models.batch,
                                attributes: ['batchName'],

                            }, {
                                model: models.group,
                                attributes: ['groupName'],

                            }
                        ]
                    });
                    // element.data = notice

                    let classes = '';
                    let classAndBatchAndSubject = '';

                    if (element.noticeStudent.noticeType == FeedbackFormType.Class) {
                        classAndBatchAndSubject = noticeStudentTypes.map(data => {
                            return data.class.className;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    } else if (element.noticeStudent.noticeType == FeedbackFormType.Batch) {
                        classAndBatchAndSubject = noticeStudentTypes.map(data => {
                            return `${data.class.className} - ${data.batch.batchName}`;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    } else if (element.noticeStudent.noticeType == FeedbackFormType.Group) {
                        classAndBatchAndSubject = noticeStudentTypes.map(data => {
                            return `${data.class.className} - ${data.group.groupName}`;
                        });

                        classAndBatchAndSubject = classAndBatchAndSubject.join(', ');
                    }

                    element.classAndBatchAndSubject = classAndBatchAndSubject;

                    callback();
                })();
            }, async (err) => {
                let totalRecords = await models.noticeStudentType.findAll({
                    attributes: ['noticeStudentId'],
                    where: where,
                    include: [{
                        model: models.noticeStudent,
                        where: { status: true, instituteId: profileinfo },
                        include: [{
                            model: models.document,
                            required: true,
                        },
                        ],
                        required: true
                    },
                    {
                        model: models.class,
                        attributes: [],
                    }, {
                        model: models.batch,
                        attributes: [],

                    }, {
                        model: models.group,
                        attributes: [],

                    },],
                    group: ['noticeStudent.id', 'noticeStudentId', 'noticeStudent->document.id']
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                    "count": totalRecords.length,
                    "rows": response
                }));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                "count": 0,
                "rows": []
            }));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description create notice for staff
 * @param  {} req
 * @param  {} res
 */
module.exports.createNoticeStaff = async (req, res) => {
    try {
        await models.noticeStaff.sync({ force: false });
        await models.noticeStaffType.sync({ force: false });
        let profileinfo = req.authData.data.instituteId;

        let {
            noticeType,
            title,
            noticeDocument,
            classIds,
            subjectIds,
        } = req.body;

        let response = await models.noticeStaff.create({
            instituteId: profileinfo,
            noticeType,
            title,
            noticeDocument,
        });
        if (response) {
            if (noticeType == "Class") {
                classIds.forEach(async (classes) => {
                    await models.noticeStaffType.create({
                        classId: classes.classId,
                        noticeStaffId: response.id,
                    });
                });
            }
            if (noticeType == "Subject") {
                classIds.forEach(async (classes) => {
                    classes.subjectIds.forEach(async (subjects) => {
                        await models.noticeStaffType.create({
                            classId: classes.classId,
                            subjectId: subjects,
                            noticeStaffId: response.id,
                        });
                    });

                });
            }

            if (response) {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description get all staff Notice List
 * @param  {} req
 * @param  {} res
 */
module.exports.allNoticeStaffList = async (req, res) => {
    try {
        await models.noticeStudent.sync({ force: false });
        await models.noticeStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['noticeStaffId', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            }
        } else {
            where = {
                status: true,
                [Op.or]: {
                    '$noticeStaff.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        if (req.query.classId) {
            where.classId = req.query.classId;
        }
        if (req.query.subjectId) {
            where.subjectId = req.query.subjectId;
        }


        let response = await models.noticeStaffType.findAll({
            attributes: ['noticeStaffId'],
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.noticeStaff,
                where: { instituteId: profileinfo },
                required: true,
                include: [{
                    model: models.document,
                    required: true,
                },]
            }, {
                model: models.class,
                attributes: [],
                required: false,

            }, {
                model: models.subject,
                attributes: [],
                required: false,

            },],
            group: ['noticeStaff.id', 'noticeStaffId', 'noticeStaff->document.id']
        });

        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));

            asyncL.each(response, (element, callback) => {
                (async () => {
                    let noticeStaffId = element.noticeStaffId;

                    let noticeStaffTypes = await models.noticeStaffType.findAll({
                        where: { noticeStaffId },
                        include: [
                            {
                                model: models.class,
                                attributes: ['className'],
                                required: false,

                            }, {
                                model: models.subject,
                                attributes: ['subjectName'],
                                required: false,

                            }
                        ]
                    });
                    // element.data = notice

                    let classes = '';
                    let classAndSubject = '';

                    if (element.noticeStaff.noticeType == FeedbackFormType.Class) {
                        classes = noticeStaffTypes.map(data => {
                            return data.class.className;
                        });

                        classes = classes.join(', ');
                    } else if (element.noticeStaff.noticeType == FeedbackFormType.Subject) {
                        classes = noticeStaffTypes.map(data => {
                            return data.class.className;
                        });

                        // filtering unique id from the array

                        classes = classes.filter((x, i, a) => a.indexOf(x) === i)
                        classAndSubject = noticeStaffTypes.map(data => {
                            return `${data.class.className} - ${data.subject.subjectName}`;
                        });
                        classes = classes.join(', ');
                        classAndSubject = classAndSubject.join(', ');
                    }

                    element.class = classes;
                    element.classAndSubject = classAndSubject;

                    callback();
                })();
            }, async (err) => {
                let totalRecords = await models.noticeStaffType.findAll({
                    attributes: ['noticeStaffId'],
                    where: where,
                    include: [{
                        model: models.noticeStaff,
                        required: true,
                        include: [{
                            model: models.document,
                            required: true,
                        },]
                    }, {
                        model: models.class,
                        attributes: [],
                        required: false,

                    }, {
                        model: models.subject,
                        attributes: [],
                        required: false,

                    },],
                    group: ['noticeStaff.id', 'noticeStaffId', 'noticeStaff->document.id']
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                    "count": totalRecords.length,
                    "rows": response
                }));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                "count": 0,
                "rows": []
            }));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description get  staff Notice by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getNoticeStaffById = async (req, res) => {
    try {
        await models.noticeStudent.sync({ force: false });
        await models.noticeStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;
        let { noticeId } = req.params;
        let where = {};
        where = {
            id: noticeId,
            status: true,
            instituteId: profileinfo
        }
        let response = await models.noticeStaff.findOne({
            where: where,
            include: [{
                model: models.noticeStaffType,
                // attributes: ['noticeStaffId', 'classId', 'subjectId'],
                required: false,
                include: [{
                    model: models.class,
                    attributes: ['className'],
                    required: false,

                }, {
                    model: models.subject,
                    attributes: ['subjectName'],
                    required: false,

                },]
            }, {
                model: models.document,
                required: false,
            }],
        });

        let msg = response ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
/**
 * @description get  student Notice by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getNoticeStudentById = async (req, res) => {
    try {
        await models.noticeStudent.sync({ force: false });
        await models.noticeStudentType.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.group.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;
        let { noticeId } = req.params;

        let where = {};
        where = {
            id: noticeId,
            status: true,
            instituteId: profileinfo
        }



        let response = await models.noticeStudent.findOne({
            where: where,
            include: [{
                model: models.noticeStudentType,
                attributes: ['noticeStudentId', 'classId', 'batchId', 'groupId'],
                required: false,
                include: [{
                    model: models.class,
                    attributes: ['className'],
                    required: false,

                }, {
                    model: models.batch,
                    attributes: ['batchName'],
                    required: false,

                }, {
                    model: models.group,
                    attributes: ['groupName'],
                    required: false,

                }]
            }, {
                model: models.document,
                required: false,
            }],
        });

        let msg = response ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description delete Notice by id
 * @param  {} req
 * @param  {} res
 */

module.exports.deleteNotice = async (req, res) => {
    try {
        let instituteId = req.authData.data.instituteId;
        let { noticeId } = req.params;
        let { type } = req.body;
        if (type == Usertypes.STUDENT) {

            let noticetype = await models.noticeStudent.findOne({
                where: { id: noticeId, instituteId, status: true }
            })
            if (noticetype) {
                await models.noticeStudent.update({ status: false }, {
                    where: {
                        id: noticeId,
                        instituteId
                    }
                });

                await models.noticeStudentType.update({ status: false }, {
                    where: {
                        noticeStudentId: noticeId
                    }
                })
                return res.send(await commonResponse.response(true, Message.DATA_DELETED))

            } else {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else if (type == Usertypes.STAFF) {

            let noticetype = await models.noticeStaff.findOne({
                where: { id: noticeId, instituteId, status: true }
            })
            if (noticetype) {
                await models.noticeStaff.update({ status: false }, {
                    where: {
                        id: noticeId,
                        instituteId
                    }
                });

                await models.noticeStaffType.update({ status: false }, {
                    where: {
                        noticeStaffId: noticeId
                    }
                })
                return res.send(await commonResponse.response(true, Message.DATA_DELETED))

            } else {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}