const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const {
  Message,
  Usertypes,
  Constant,
} = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const _ = require("lodash");

const monthsNameToNumber = {
  January: "01",
  February: "02",
  March: "03",
  April: "04",
  May: "05",
  June: "06",
  July: "07",
  August: "08",
  September: "09",
  October: "10",
  November: "11",
  December: "12",
};
const currentYearMonths = [
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const nextYearMonths = ["January", "February", "March"];

module.exports.createHoliday = async (req, res) => {
  try {
    await models.holidayList.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { holidayType, day, date, holidayName } = req.body;

    if (holidayType == "Weekend") {
      let currentYear = new Date().getFullYear();
      let nextYear = currentYear + 1;

      // loop for current year months
      let theDays = [];
      for (let index = 0; index < currentYearMonths.length; index++) {
        let daysInMonth = new Date(
          currentYear,
          monthsNameToNumber[currentYearMonths[index]],
          0
        ).getDate();
        for (let i = 1; i <= daysInMonth; i++) {
          if (i.toString().length == 1) {
            i = "0" + i;
          }
          let newDate = new Date(
            currentYearMonths[index] + " " + i + ", " + currentYear
          );
          if (day == "Saturday") {
            if (newDate.getDay() == 6) {
              theDays.push(
                currentYear +
                  "-" +
                  monthsNameToNumber[currentYearMonths[index]] +
                  "-" +
                  i
              );
            }
          }
          if (day == "Sunday") {
            if (newDate.getDay() == 0) {
              theDays.push(
                currentYear +
                  "-" +
                  monthsNameToNumber[currentYearMonths[index]] +
                  "-" +
                  i
              );
            }
          }
        }
      }
      // loop for next year months
      for (let index = 0; index < nextYearMonths.length; index++) {
        let daysInMonth = new Date(
          nextYear,
          monthsNameToNumber[nextYearMonths[index]],
          0
        ).getDate();
        for (let i = 1; i <= daysInMonth; i++) {
          if (i.toString().length == 1) {
            i = "0" + i;
          }
          let newDate = new Date(
            nextYearMonths[index] + " " + i + ", " + nextYear
          );
          if (day == "Saturday") {
            if (newDate.getDay() == 6) {
              theDays.push(
                nextYear +
                  "-" +
                  monthsNameToNumber[nextYearMonths[index]] +
                  "-" +
                  i
              );
            }
          }
          if (day == "Sunday") {
            if (newDate.getDay() == 0) {
              theDays.push(
                nextYear +
                  "-" +
                  monthsNameToNumber[nextYearMonths[index]] +
                  "-" +
                  i
              );
            }
          }
        }
      }

      theDays.forEach(async (day) => {
        let data = day.split("-");
        let month = new Date(data[0], data[1] - 1, data[2]).toLocaleString(
          "default",
          { month: "long" }
        );

        // toggle system delete / create
        let check = await models.holidayList.findOne({
          where: {
            instituteId,
            year: data[0],
            month: month,
            date: day,
            holidayType,
          },
        });
        if (check) {
          // delete
          models.holidayList.destroy({
            where: {
              instituteId,
              year: data[0],
              month: month,
              date: day,
              holidayType,
            },
          });
        } else {
          // create
          let [record, created] = await models.holidayList.findOrCreate({
            where: {
              instituteId,
              year: data[0],
              month: month,
              date: day,
              holidayType,
            },
            defaults: {
              instituteId,
              year: data[0],
              month: month,
              date: day,
              holidayType,
            },
          });
        }
      });
      return res.json(
        await commonResponse.response(true, Message.DATA_CREATED)
      );
    } else {
      // normal holiday
      let data = date.split("-");
      let month = new Date(data[0], data[1] - 1, data[2]).toLocaleString(
        "default",
        { month: "long" }
      );
      await models.holidayList.findOrCreate({
        where: {
          instituteId,
          year: data[0],
          month: month,
          date,
          holidayType,
        },
        defaults: {
          instituteId,
          year: data[0],
          month: month,
          date,
          holidayType,
          holidayName,
        },
      });
      return res.json(
        await commonResponse.response(true, Message.DATA_CREATED)
      );
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.allHolidays = async (req, res) => {
  try {
    await models.holidayList.sync({ force: false });
    let { instituteId } = req.authData.data;

    let list = await models.holidayList.findAll({
      attributes: ["holidayType", "holidayName", "year", "month", "date"],
      where: {
        instituteId,
        status: true,
      },
      order: [
        ["year", "ASC"],
        ["month", "ASC"],
        ["date", "ASC"],
      ],
    });

    let listOfYearMonths = await models.holidayList.findAll({
      attributes: ["year", "month"],
      where: {
        instituteId,
        status: true,
      },
      group: ["year", "month"],
      order: [["year"], ["month"]],
    });

    let filteredData = [];
    for (let index = 0; index < listOfYearMonths.length; index++) {
      // filter and insert
      let tempDates = _.filter(
        list,
        (finder) =>
          finder.month == listOfYearMonths[index].month &&
          finder.year == listOfYearMonths[index].year
      );
      filteredData.push({
        year: listOfYearMonths[index].year,
        month: listOfYearMonths[index].month,
        dates: tempDates,
        totalDays: new Date(
          listOfYearMonths[index].year,
          monthsNameToNumber[listOfYearMonths[index].month],
          0
        ).getDate(),
        totalHolidays: tempDates.length,
        totalWorkingDays:
          new Date(
            listOfYearMonths[index].year,
            monthsNameToNumber[listOfYearMonths[index].month],
            0
          ).getDate() - tempDates.length,
      });
    }
    return res.json(
      await commonResponse.response(true, Message.DATA_FOUND, filteredData)
    );
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.updateHoliday = async (req, res) => {
  try {
    await models.holidayList.sync({ force: false });
    let { instituteId } = req.authData.data;
    let { id } = req.params;
    let { date } = req.body;

    let data = date.split("-");
    let month = new Date(data[0], data[1] - 1, data[2]).toLocaleString(
      "default",
      { month: "long" }
    );
    await models.holidayList.update(
      { ...req.body, year: data[0], month, updatedAt: new Date() },
      {
        where: {
          instituteId,
          id,
        },
      }
    );

    return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.deleteHoliday = async (req, res) => {
  try {
    await models.holidayList.sync({ force: false });
    let { instituteId } = req.authData.data;
    let { id } = req.params;

    await models.holidayList.destroy({
      where: {
        instituteId,
        id,
      },
    });

    return res.json(await commonResponse.response(true, Message.DATA_DELETED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};
