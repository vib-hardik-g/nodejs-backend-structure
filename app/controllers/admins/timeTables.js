const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const {
  Message,
  Publishstatus,
  Constant,
} = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const { Op } = require("sequelize");
const _ = require("lodash");

const currentYear = new Date().getFullYear();

module.exports.allTimeTables = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;

    if (req.query.pageSize) {
      Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
    }
    if (req.query.pageNo) {
      Constant.DEFAULT_PAGE =
        parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
    }

    let orderBy = [];
    if (req.query.sortBy && req.query.orderBy) {
      orderBy.push([req.query.sortBy, req.query.orderBy]);
    } else {
      orderBy.push(["id", "DESC"]);
    }

    let where = {
      status: true,
      instituteId,
    };

    let classWhere = {};
    if (req.query.classId) {
      classWhere.id = req.query.classId;
    }

    if (req.query.startDate && req.query.endDate) {
      where.updatedAt = {
        [Op.between]: [
          new Date(req.query.startDate).toISOString(),
          new Date(req.query.endDate + " 23:59:59").toISOString(),
        ],
      };
    }

    let response = await models.timeTable.findAndCountAll({
      where: where,
      order: orderBy,
      offset: parseInt(Constant.DEFAULT_PAGE),
      limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
      attributes: ["id", "year", "publishStatus", "createdAt", "updatedAt"],
      distinct: true,
      include: [
        {
          model: models.timeTableType,
          attributes: ["timeTableType"],
        },
        {
          model: models.batch,
          attributes: ["id", "batchName"],
          required: true,
          include: [
            {
              model: models.class,
              attributes: ["className"],
              where: classWhere,
            },
          ],
        },
      ],
    });
    let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
    let result = {
      rows: response.rows,
      total: response.count,
    };
    return res.send(await commonResponse.response(true, msg, result));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.createTimeTable = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { batchId, timeTableTypeId, days } = req.body;

    let timetable = await models.timeTable.findOne({
      where: {
        timeTableTypeId,
        instituteId,
        batchId,
      },
    });

    if (timetable) {
      return res
        .status(400)
        .json(await commonResponse.response(false, Message.DATA_EXISTS));
    }

    let errors = [];

    // start and end time validation
    for (let day of days) {
      for (let [index, item] of day.data.entries()) {
        if (
          parseInt(item.startTime.toString().replace(":", "")) >
          parseInt(item.endTime.toString().replace(":", ""))
        ) {
          errors.push({
            day: day.day,
            error: "endTime is less than startTime",
            index,
            field: "endTime",
          });
        }
      }
    }

    // staff id check if same staff has multiple classes in overlapping time
    for (let day of days) {
      let groupedByStaff = _.chain(day.data)
        .groupBy("staffId")
        .map((value, key) => ({ staffId: key, data: value }))
        .value();
      for (let staff of groupedByStaff) {
        if (staff.staffId != "null" && staff.data.length > 1) {
          let timingArr = [];
          for (let item of staff.data) {
            timingArr.push({
              start: item.startTime,
              end: item.endTime,
              staffId: item.staffId,
            });
          }
          let n = timingArr.length;
          timingArr.sort(function (i1, i2) {
            return i1.start - i2.start;
          });
          for (let i = 1; i < n; i++) {
            if (timingArr[i - 1].end > timingArr[i].start) {
              // find index in main array
              let index = day.data.findIndex(
                (e) =>
                  e.startTime == timingArr[i].start &&
                  e.endTime == timingArr[i].end
              );
              errors.push({
                day: day.day,
                error: "Staff cannot have overlapping timing for same day",
                index: index,
                field: "staffId",
              });
            }
          }
        }
      }
    }

    // same day there cannot be overlapping timing
    for (let day of days) {
      let timingArr = [];
      for (let item of day.data) {
        timingArr.push({ start: item.startTime, end: item.endTime });
      }
      let n = timingArr.length;
      timingArr.sort(function (i1, i2) {
        return i1.start - i2.start;
      });

      let rowNumbers = [];
      for (let i = 1; i < n; i++) {
        if (timingArr[i - 1].end > timingArr[i].start) {
          rowNumbers.push(i);
        }
      }

      if (rowNumbers.length > 0) {
        for (let rowNumber of rowNumbers) {
          errors.push({
            day: day.day,
            error: "Cannot have overlapping timing for same day",
            index: rowNumber,
            field: "startTime",
          });
        }
      }
    }

    if (errors.length > 0) {
      let groupByErrors = _.chain(errors)
        .groupBy("day")
        .map((value, key) => ({ day: key, data: value }))
        .value();
      return res
        .status(400)
        .json(
          await commonResponse.response(
            false,
            Message.VALIDATION_ERROR,
            groupByErrors
          )
        );
    }

    let created = await models.timeTable.create({
      timeTableTypeId,
      instituteId,
      batchId,
      year: currentYear,
    });

    for (let day of days) {
      for (let item of day.data) {
        await models.timeTableDetail.create({
          subjectId: item.subjectId,
          staffId: item.staffId,
          centerId: item.centerId,
          day: day.day,
          room: item.room,
          startTime: item.startTime,
          endTime: item.endTime,
          timeTableId: created.id,
        });
      }
    }
    return res.json(await commonResponse.response(true, Message.DATA_CREATED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.deleteTimeTable = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { id } = req.params;

    await models.timeTableDetail.destroy({
      where: {
        timeTableId: id,
      },
    });

    await models.timeTable.destroy({
      where: {
        instituteId,
        id,
      },
    });

    return res.send(await commonResponse.response(true, Message.DATA_DELETED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.checkTimeTable = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { batchId } = req.body;

    // check batch exists or not
    let batch = await models.batch.findOne({ where: { id: batchId } });
    if (!batch) {
      return res
        .status(404)
        .json(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }
    let response = await models.timeTable.findOne({
      where: {
        batchId,
        instituteId,
      },
    });
    if (response) {
      return res.json(
        await commonResponse.response(false, Message.DATA_EXISTS)
      );
    } else {
      return res.json(
        await commonResponse.response(true, Message.NO_DATA_FOUND)
      );
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.updateTimeTable = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { days } = req.body;
    let { id } = req.params;

    let currentTimeTable = await models.timeTable.findOne({
      where: {
        id,
        instituteId,
      },
    });

    let errors = [];

    // start and end time validation
    for (let day of days) {
      for (let [index, item] of day.data.entries()) {
        if (
          parseInt(item.startTime.toString().replace(":", "")) >
          parseInt(item.endTime.toString().replace(":", ""))
        ) {
          errors.push({
            day: day.day,
            error: "endTime is less than startTime",
            index,
            field: "endTime",
          });
        }
      }
    }

    // staff id check if same staff has multiple classes in overlapping time
    for (let day of days) {
      let groupedByStaff = _.chain(day.data)
        .groupBy("staffId")
        .map((value, key) => ({ staffId: key, data: value }))
        .value();
      for (let staff of groupedByStaff) {
        if (staff.staffId != "null" && staff.data.length > 1) {
          let timingArr = [];
          for (let item of staff.data) {
            timingArr.push({
              start: item.startTime,
              end: item.endTime,
              staffId: item.staffId,
            });
          }
          let n = timingArr.length;
          timingArr.sort(function (i1, i2) {
            return i1.start - i2.start;
          });
          for (let i = 1; i < n; i++) {
            if (timingArr[i - 1].end > timingArr[i].start) {
              // find index in main array
              let index = day.data.findIndex(
                (e) =>
                  e.startTime == timingArr[i].start &&
                  e.endTime == timingArr[i].end
              );
              errors.push({
                day: day.day,
                error: "Staff cannot have overlapping timing for same day",
                index: index,
                field: "staffId",
              });
            }
          }
        }
      }
    }

    // same day there cannot be overlapping timing
    for (let day of days) {
      let timingArr = [];
      for (let item of day.data) {
        timingArr.push({ start: item.startTime, end: item.endTime });
      }
      let n = timingArr.length;
      timingArr.sort(function (i1, i2) {
        return i1.start - i2.start;
      });

      let rowNumbers = [];
      for (let i = 1; i < n; i++) {
        if (timingArr[i - 1].end > timingArr[i].start) {
          rowNumbers.push(i);
        }
      }

      if (rowNumbers.length > 0) {
        for (let rowNumber of rowNumbers) {
          errors.push({
            day: day.day,
            error: "Cannot have overlapping timing for same day",
            index: rowNumber,
            field: "startTime",
          });
        }
      }
    }

    if (errors.length > 0) {
      let groupByErrors = _.chain(errors)
        .groupBy("day")
        .map((value, key) => ({ day: key, data: value }))
        .value();
      return res
        .status(400)
        .json(
          await commonResponse.response(
            false,
            Message.VALIDATION_ERROR,
            groupByErrors
          )
        );
    }

    // delete existing records
    await models.timeTableDetail.destroy({
      where: {
        timeTableId: id,
      },
    });

    await models.timeTable.update(
      {
        publishStatus: Publishstatus.REUPLOADED,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      }
    );

    for (let day of days) {
      for (let item of day.data) {
        await models.timeTableDetail.create({
          subjectId: item.subjectId,
          staffId: item.staffId,
          centerId: item.centerId,
          day: day.day,
          room: item.room,
          startTime: item.startTime,
          endTime: item.endTime,
          timeTableId: currentTimeTable.id,
        });
      }
    }
    return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.publishTimeTable = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { timeTableId } = req.body;

    let response = await models.timeTable.findOne({
      where: {
        id: timeTableId,
        instituteId,
      },
    });
    let newStatus;
    if (response.publishStatus == Publishstatus.PENDING) {
      newStatus = Publishstatus.PUBLISHED;
    }
    if (response.publishStatus == Publishstatus.REUPLOADED) {
      newStatus = Publishstatus.REPUBLISHED;
    }
    await models.timeTable.update(
      {
        publishStatus: newStatus,
        updatedAt: new Date(),
      },
      {
        where: {
          id: timeTableId,
        },
      }
    );
    return res.json(
      await commonResponse.response(true, Message.DATA_UPDATED, {
        newStatus: newStatus,
      })
    );
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.getTimeTableDetails = async (req, res) => {
  try {
    await models.center.sync({ force: false });
    await models.class.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.student.sync({ force: false });
    await models.staff.sync({ force: false });
    await models.user.sync({ force: false });
    await models.timeTableType.sync({ force: false });
    await models.timeTable.sync({ force: false });
    await models.timeTableDetail.sync({ force: false });

    let { instituteId } = req.authData.data;
    let { id } = req.params;

    let days = await models.timeTableDetail.findAll({
      where: {
        timeTableId: id,
      },
      attributes: ["day"],
      group: ["day"],
    });

    let response = await models.timeTable.findOne({
      where: {
        instituteId,
        id,
      },
      attributes: ["id", "year", "publishStatus", "createdAt", "updatedAt"],
      include: [
        {
          model: models.batch,
          attributes: ["id", "batchName"],
          include: [
            {
              model: models.class,
              attributes: ["className"],
            },
          ],
        },
        {
          model: models.timeTableDetail,
          attributes: ["day", "room", "startTime", "endTime"],
          required: true,
          include: [
            {
              model: models.staff,
            },
            {
              model: models.subject,
              attributes: ["id", "subjectName"],
            },
            {
              model: models.center,
              attributes: ["id", "centerName"],
            },
          ],
        },
      ],
    });

    if (response) {
      let dataArr = {};
      dataArr.timeTableId = response.id;
      dataArr.batchId = response.batch.id;
      dataArr.batchName = response.batch.batchName;
      dataArr.days = [];
      days.forEach((day) => {
        dataArr.days.push({
          day: day.day,
          data: [],
        });
      });
      response.timeTableDetails.forEach((item) => {
        dataArr.days.forEach((day, index) => {
          if (day.day.toString() == item.day.toString()) {
            dataArr.days[index].data.push(item);
          }
        });
      });
      return res.json(
        await commonResponse.response(true, Message.DATA_FOUND, dataArr)
      );
    } else {
      return res
        .status(404)
        .send(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};
