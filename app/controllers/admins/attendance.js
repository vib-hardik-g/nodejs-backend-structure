const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const sequelize = models.sequelize;
const _ = require('lodash');

module.exports.listStudentsByBatchDate = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { batchId } = req.params;

        // date validation
        if(!req.query.date) return res.status(400).json(await commonResponse.response(false, 'Date is required'));
        let dateValidation = moment(req.query.date, 'YYYY-MM-DD', true);
        if (!dateValidation.isValid()) {
            return res.status(400).json(await commonResponse.response(false, 'Date must be in YYYY-MM-DD format'));
        }

        // check if the date is in holiday list or not
        let checkExistingHolidayCount = await models.holidayList.count({
            where: {
                date: req.query.date,
                instituteId
            }
        });
        if (checkExistingHolidayCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.ALREADY_HOLIDAY));

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        if(!req.query.searchKey){
            where = {
                status: true,
            };
        }else{
            let stringCondition = {
                '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
            };
            let intCondition = {
                '$student.uniqueId$': req.query.searchKey,
                '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
            }
            let condition;
            if (!isNaN(req.query.searchKey)) {
                condition = intCondition
            } else {
                condition = stringCondition;
            }
            where = {
                status: true,
                [Op.or]: condition
            }
        }

        let response = await models.userStudentMapping.findAndCountAll({
            distinct: true,
            subQuery: false,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: {
                instituteId,
                batchId
            },
            attributes: [],
            include: [
                {
                    model: models.student,
                    attributes: ['id', 'firstName', 'lastName', 'uniqueId'],
                    where: where,
                    required: true,
                    include: [
                        {
                            model: models.studentAbsentee,
                            where: {
                                date: req.query.date
                            },
                            required: false,
                            attributes: ['id', 'date', 'absenceType']
                        },
                        {
                            model: models.studentLeave,
                            where: {
                                startDate: {
                                    [Op.lte]: req.query.date
                                },
                                endDate: {
                                    [Op.gte]: req.query.date
                                },
                                leaveStatus: Generalstatus.PENDING
                            },
                            required: false,
                            attributes: ['id', 'leaveStatus', 'startDate', 'endDate'],
                            include: [
                                {
                                    model: models.leaveType,
                                    attributes: [['leaveType', 'type']]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.submitAttendance = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { data, date, batchId } = req.body;

        // check if the date is in holiday list or not
        let checkExistingHolidayCount = await models.holidayList.count({
            where: {
                date,
                instituteId
            }
        });
        if (checkExistingHolidayCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.ALREADY_HOLIDAY));

        // check if there are any pending leave application
        let studentsCount = await models.userStudentMapping.count({
            where: {
                instituteId,
                batchId
            },
            distinct: true,
            include: [{
                model: models.student,
                where: {
                    status: true,
                },
                include: [{
                    model: models.studentLeave,
                    where: {
                        startDate: {
                            [Op.lte]: date
                        },
                        endDate: {
                            [Op.gte]: date
                        },
                        leaveStatus: Generalstatus.PENDING
                    },
                    required: true,
                }]
            }]
        });

        if (studentsCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.LEAVE_REQUEST_PENDING));

        let batch = await models.batch.findOne({
            where: {
                id: batchId
            }
        });
        let classId = batch.classId;

        // only once create attendance manager entry for same date
        let checkForAttendanceManagerEntry = await models.studentAttendanceManager.findOne({
            where: {
                date,
                instituteId,
                classId,
                batchId
            }
        });
        let studentAttendanceManagerId;
        if (checkForAttendanceManagerEntry) {
            studentAttendanceManagerId = checkForAttendanceManagerEntry.id;
            checkForAttendanceManagerEntry.publishStatus = Publishstatus.PENDING;
            checkForAttendanceManagerEntry.updatedAt = new Date();
            checkForAttendanceManagerEntry.save();

            // delete existing records from absentee
            await models.studentAbsentee.destroy({
                where: {
                    date,
                    absenceType: Attendance.ABSENT,
                    studentAttendanceManagerId
                }
            });
        } else {
            let dayOfDate = new Date(date).toLocaleString('en-us', { weekday: 'long' });
            let response = await models.studentAttendanceManager.create({
                date,
                instituteId,
                classId,
                batchId,
                day: dayOfDate
            });
            studentAttendanceManagerId = response.id;
        }

        let absenteeArray = [];
        for (let item of data) {
            if (item.absent) {
                absenteeArray.push({
                    studentId: item.studentId,
                    date,
                });
            }
        }

        for (let item of absenteeArray) {
            // check if no leave request there
            let checkFlag = await models.studentAbsentee.findOne({
                where: {
                    studentId: item.studentId,
                    date,
                    absenceType: Attendance.LEAVE,
                    studentAttendanceManagerId
                }
            });
            if (!checkFlag) {
                // freshly insert new records of absent only
                await models.studentAbsentee.create({
                    studentId: item.studentId,
                    date,
                    absenceType: Attendance.ABSENT,
                    studentAttendanceManagerId
                });
            }
        }
        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.approveOrRejectLeave = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId, staffId } = req.authData.data;
        let { leaveId, leaveStatus } = req.body;

        let studentLeave = await models.studentLeave.findOne({
            where: {
                id: leaveId
            }
        });
        if (studentLeave) {
            // get batch & class of the student
            let studentMappingData = await models.userStudentMapping.findOne({
                where: {
                    studentId: studentLeave.studentId
                }
            });
            let batchId = studentMappingData.batchId;
            let classId = studentMappingData.classId;

            if (studentLeave.leaveStatus == Generalstatus.PENDING) {
                if (leaveStatus == Generalstatus.APPROVED) {
                    studentLeave.leaveStatus = Generalstatus.APPROVED;
                    studentLeave.approvedBy = staffId;
                    // update the leave table with leave data
                    studentLeave.save();

                    let start = moment(studentLeave.startDate);
                    let end = moment(studentLeave.endDate);
                    for (let m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
                        // check if attendancemanager exists or not
                        let checkForAttendanceManagerEntry = await models.studentAttendanceManager.findOne({
                            where: {
                                date: m.format('YYYY-MM-DD'),
                                instituteId,
                                classId,
                                batchId
                            }
                        });

                        // check holiday list 
                        let checkHoliday = await models.holidayList.findOne({
                            where: {
                                date: m.format('YYYY-MM-DD')
                            }
                        });
                        if (!checkHoliday) {
                            if (checkForAttendanceManagerEntry) {
                                await models.studentAbsentee.create({
                                    studentId: studentLeave.studentId,
                                    date: m.format('YYYY-MM-DD'),
                                    absenceType: Attendance.LEAVE,
                                    studentAttendanceManagerId: checkForAttendanceManagerEntry.id
                                });
                            } else {
                                let dayOfDate = new Date(m.format('YYYY-MM-DD')).toLocaleString('en-us', { weekday: 'long' });
                                let response = await models.studentAttendanceManager.create({
                                    date: m.format('YYYY-MM-DD'),
                                    instituteId,
                                    classId,
                                    batchId,
                                    day: dayOfDate
                                });
                                await models.studentAbsentee.create({
                                    studentId: studentLeave.studentId,
                                    date: m.format('YYYY-MM-DD'),
                                    absenceType: Attendance.LEAVE,
                                    studentAttendanceManagerId: response.id
                                });
                            }
                        }
                    }
                } else if (leaveStatus == Generalstatus.REJECTED) {
                    studentLeave.leaveStatus = Generalstatus.REJECTED;
                    studentLeave.save();
                }
            }
        }
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.publishAttendance = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { batchId, date } = req.body;

        await models.studentAttendanceManager.update({
            publishStatus: Publishstatus.PUBLISHED,
            updatedAt: new Date()
        }, {
            where: {
                date,
                instituteId,
                batchId
            }
        });
        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listAttendanceByClassBatch = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { classId, batchId } = req.params;

        if (!req.query.startDate || !req.query.endDate) {
            return res.status(400).json(await commonResponse.response(false, 'Start & End Date is required'));
        }
        // start & end date validation
        let startDateValidation = moment(req.query.startDate, 'YYYY-MM-DD', true);
        if (!startDateValidation.isValid()) {
            return res.status(400).json(await commonResponse.response(false, 'Start Date must be in YYYY-MM-DD format'));
        }
        let endDateValidation = moment(req.query.endDate, 'YYYY-MM-DD', true);
        if (!endDateValidation.isValid()) {
            return res.status(400).json(await commonResponse.response(false, 'End Date must be in YYYY-MM-DD format'));
        }

        let totalStudentCount = await models.userStudentMapping.count({
            where: {
                instituteId,
                batchId,
                classId
            },
            include: [
                {
                    model: models.student,
                    required: true
                }
            ]
        });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                date: {
                    [Op.gte]: req.query.startDate,
                    [Op.lte]: req.query.endDate
                }
            }
        } else {
            where = {
                status: true,
            }
            let date = moment(req.query.searchKey, 'DD-MM-YYYY', true);
            if (date.isValid()) {
                where.date = moment(date).format('YYYY-MM-DD');
            }
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }
        let response = await models.studentAttendanceManager.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            attributes: [
                'id', 'date', 'day', 'publishStatus', 'createdAt', 'updatedAt'
            ],
            include: [{
                model: models.studentAbsentee,
                attributes: ['date', 'absenceType'],
            }],
        });

        let newArr = [];
        for (item of response.rows) {
            let countAbsents = item.studentAbsentees.filter((x) => {
                return x.absenceType == Attendance.ABSENT;
            }).length;
            let countLeaves = item.studentAbsentees.filter((x) => {
                return x.absenceType == Attendance.LEAVE;
            }).length;
            newArr.push({
                id: item.id,
                date: item.date,
                day: item.day,
                publishStatus: item.publishStatus,
                totalStudentCount: totalStudentCount,
                totalLeaveCount: countLeaves,
                totalAbsentCount: countAbsents,
                totalPresentCount: parseInt(totalStudentCount - (countAbsents + countLeaves)),
                createdAt: item.createdAt,
                updatedAt: item.updatedAt
            });
        }
        let result = {
            rows: newArr,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listAttendanceByStudentAttendanceManagerId = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;

        let managerDetails = await models.studentAttendanceManager.findOne({
            where: {
                id,
                instituteId
            },
            attributes: ['batchId', 'classId', 'date']
        });

        if (managerDetails) {
            if (req.query.pageSize) {
                Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            }
            if (req.query.pageNo) {
                Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
            }
            let where = {};
            if (!req.query.searchKey) {
                where = {
                    batchId: managerDetails.batchId,
                    classId: managerDetails.classId,
                    instituteId,
                    status: true,
                };
            } else {
                let stringCondition = {
                    '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                };
                let intCondition = {
                    '$student.uniqueId$': req.query.searchKey,
                    '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
                let condition;
                if (!isNaN(req.query.searchKey)) {
                    condition = intCondition
                } else {
                    condition = stringCondition;
                }

                where = {
                    batchId: managerDetails.batchId,
                    classId: managerDetails.classId,
                    instituteId,
                    status: true,
                    [Op.or]: condition
                }
            }

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let response = await models.userStudentMapping.findAndCountAll({
                where: where,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                distinct: true,
                attributes: [],
                include: [{
                    model: models.student,
                    attributes: ['id', 'uniqueId', 'firstName', 'lastName'],
                    include: [{
                        model: models.studentAbsentee,
                        required: false,
                        attributes: ['absenceType'],
                        where: {
                            date: managerDetails.date
                        }
                    }]
                }],
                subQuery: false
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            return res.status(404).json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.listAttendanceByStudentId = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.leaveType.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        await models.studentLeave.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { classId, batchId, studentId } = req.params;

        if (!req.query.startDate || !req.query.endDate) {
            return res.status(400).json(await commonResponse.response(false, 'Start & End Date is required'));
        }
        // start & end date validation
        let startDateValidation = moment(req.query.startDate, 'YYYY-MM-DD', true);
        if (!startDateValidation.isValid()) {
            return res.status(400).json(await commonResponse.response(false, 'Start Date must be in YYYY-MM-DD format'));
        }
        let endDateValidation = moment(req.query.endDate, 'YYYY-MM-DD', true);
        if (!endDateValidation.isValid()) {
            return res.status(400).json(await commonResponse.response(false, 'End Date must be in YYYY-MM-DD format'));
        }

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let where = {
            instituteId,
            classId,
            batchId,
            date: {
                [Op.gte]: req.query.startDate,
                [Op.lte]: req.query.endDate
            },
            status: true,
        };

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['date', 'ASC']);
        }

        if(req.query.searchKey){
            let dateValidator = moment(req.query.searchKey, 'DD-MM-YYYY', true);
            if(dateValidator.isValid()){
                where.date = moment(req.query.searchKey, 'DD-MM-YYYY').format('YYYY-MM-DD');
            }
        }

        // get dates by studentAttendanceManager first to determine which dates attendance has been taken
        let response = await models.studentAttendanceManager.findAndCountAll({
            distinct: true,
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            attributes: ['date', 'day'],
            include: [{
                model: models.studentAbsentee,
                where: {
                    studentId,
                },
                required: false,
                attributes: ['absenceType']
            }]
        });

        let result = {
            rows: response.rows,
            total: response.count,
        }

        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}