const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');

module.exports.listStaffs = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {
            status: true,
        };
        if (req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: {
                    firstName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    lastName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    employeeCode: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        let response = await models.userStaffMapping.findAndCountAll({
            distinct: true,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: {
                instituteId,
                status: true,
            },
            attributes: [],
            include: [{
                model: models.staff,
                attributes: ['id', 'employeeCode', 'firstName', 'lastName'],
                where: where,
                required: true,
                include: [{
                    required: true,
                    model: models.staffClassMapping,
                    attributes: ['classId'],
                    include: [{
                        model: models.class,
                        attributes: ['className']
                    }, {
                        model: models.staffClassBatchMapping,
                        attributes: ['batchId'],
                        include: [{
                            model: models.batch,
                            attributes: ['batchName'],
                        }, {
                            model: models.staffClassBatchSubjectMapping,
                            attributes: ['subjectId'],
                            include: [{
                                model: models.subject,
                                attributes: ['subjectName'],
                                include: [{
                                    model: models.topic,
                                    attributes: ['id'],
                                    include: [{
                                        model: models.topicDetails,
                                        attributes: ['id', 'chapter'],
                                        include: [{
                                            model: models.subTopic,
                                            attributes: ['id', 'subTopicName', 'allotedHours']
                                        }]
                                    }]
                                }]
                            }]
                        }]
                    }]
                }]
            }]
        });

        // formatting
        if (response.rows && response.rows.length > 0) {
            response.rows = JSON.parse(JSON.stringify(response.rows));

            for (let staff of response.rows) {
                let numberOfBatches = 0;
                let totalAllottedHours = 0;
                let totalActualHours = 0;
                let numberOfSubjects = 0;
                for (let cls of staff.staff.staffClassMappings) {
                    numberOfBatches += cls.staffClassBatchMappings.length;

                    // loop through sub topics and get the allotted hours
                    for (let batch of cls.staffClassBatchMappings) {
                        numberOfSubjects += batch.staffClassBatchSubjectMappings.length;

                        for (let subject of batch.staffClassBatchSubjectMappings) {
                            for (let topic of subject.subject.topics) {
                                for (let chapter of topic.topicDetails) {
                                    for (let subTopic of chapter.subTopics) {
                                        // look for progress report submission entry
                                        let entry = await models.staffProgressReport.findOne({
                                            where: {
                                                instituteId,
                                                staffId: staff.staff.id,
                                                classId: cls.classId,
                                                batchId: batch.batchId,
                                                subjectId: subject.subjectId,
                                                topicDetailsId: chapter.id,
                                                subTopicId: subTopic.id,
                                                status: true,
                                            },
                                            attributes: ['date', 'actualHours']
                                        });
                                        subTopic.staffProgress = entry;
                                        totalAllottedHours += subTopic.allotedHours;
                                        if (entry) {
                                            totalActualHours += entry.actualHours;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                staff.numberOfBatches = numberOfBatches;
                staff.avgSlippage = (totalActualHours - totalAllottedHours) > 0 ? ((totalActualHours - totalAllottedHours) / numberOfSubjects).toFixed(0) : 0;
            }
        }

        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.batchProgressReportByStaffClassBatchSubject = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });
        await models.staffProgressReport.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.staffClassBatchMapping.sync({ force: false });
        await models.staffClassBatchSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { staffId, classId, batchId, subjectId } = req.params;

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {
            status: true,
        };
        if (req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: {
                    chapter: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        let response = await models.topicDetails.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            distinct: true,
            attributes: ['chapter', 'createdAt', 'updatedAt'],
            include: [
                {
                    required: true,
                    model: models.topic,
                    attributes: [],
                    where: {
                        instituteId,
                        classId,
                        subjectId,
                        status: true
                    },
                },
                {
                    model: models.subTopic,
                    attributes: ['allotedHours', 'subTopicName'],
                    include: [{
                        model: models.staffProgressReport,
                        attributes: ['actualHours', 'date'],
                        where: {
                            staffId,
                            classId,
                            batchId,
                            subjectId
                        },
                        required: false,
                    }]
                }
            ]
        });

        // formatting
        if (response.rows && response.rows.length > 0) {
            response.rows = JSON.parse(JSON.stringify(response.rows));

            for(let topic of response.rows){
                let totalActualHours = 0;
                let totalAllotedHours = 0;
                let completionFlag = true;
                let tempDates = [];
                for (let subTopic of topic.subTopics) {
                    totalAllotedHours += subTopic.allotedHours;
                    if (subTopic.staffProgressReports.length > 0) {
                        totalActualHours += subTopic.staffProgressReports[0].actualHours;
                        tempDates.push(subTopic.staffProgressReports[0].date);
                    }
                    // check completion status - if all subtopic has staffProgressReport
                    if (subTopic.staffProgressReports.length == 0) {
                        completionFlag = false;
                    }
                }
                if (!completionFlag) {
                    topic.completedOn = 'Not Yet Completed';
                }
                if (tempDates.length > 0) {
                    let maxDate = tempDates.reduce(function (a, b) { return a > b ? a : b; });
                    topic.completedOn = maxDate;
                }
                topic.totalAllotedHours = totalAllotedHours;
                topic.totalActualHours = totalActualHours;
                topic.slippage = (totalActualHours - totalAllotedHours) > 0 ? totalActualHours - totalAllotedHours : 0;
            }
        }

        let batch = await models.batch.findOne({
            where: {
                id: batchId
            },
            attributes: ['batchName']
        });
        let subject = await models.subject.findOne({
            where: {
                id: subjectId
            },
            attributes: ['subjectName']
        });
        let staff = await models.staff.findOne({
            where: {
                id: staffId
            },
            attributes: ['firstName', 'lastName']
        });

        let result = {
            batch,
            subject,
            staff,
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}