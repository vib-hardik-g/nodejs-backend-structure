const asyncL = require('async');
const sequelize = require('sequelize');
const { Op } = require('sequelize');
const Excel = require('exceljs');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const xlsxFile = require('read-excel-file/node');
const AWS = require('aws-sdk');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Constant, Uploadpaths, StudentAge } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { studentInvitation } = require('../../mailTemplates');
const { getAge } = require('../../../utils/commonHelper');

// AWS S3
const S3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION_NAME
});

/**
 * @description Create student
 * @param  {} req
 * @param  {} res
 * @created 2021-10-20
 */
module.exports.createStudent = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let body = req.body;
        let { instituteId } = req.authData.data;

        // Check Unique User Email
        let checkUser = await exports.checkUniqueEmail(body.email);

        if (checkUser > 0) {
            return res.send(await commonResponse.response(false, Message.STUDENT_EMAIL_ALREADY_EXISTS));
        }

        // Find User Type
        let userTypeOfStudent = await models.userType.findOne({
            where: {
                userType: Usertypes.STUDENT,
                status: true,
            }
        });

        if (!userTypeOfStudent) {
            return res.send(await commonResponse.response(false, Message.STUDENT_USER_TYPE_NOT_FOUND));
        }

        // Check Unique Id
        let uniqueId = await models.studentBatchUniqueId.count({ where: { instituteId, uniqueId: body.uniqueId, batchId: body.batchId } });

        if (uniqueId > 0) {
            return res.send(await commonResponse.response(false, Message.UNIQUE_ID_ALREADY_EXISTS));
        }

        // Create Student
        let student = await models.student.create({
            firstName: body.firstName,
            lastName: body.lastName,
            dateOfBirth: body.dateOfBirth,
            gender: body.gender,
            bloodGroup: body.bloodGroup,
            religion: body.religion,
            primaryPhoneNumber: body.primaryPhoneNumber,
            stateId: body.stateId,
            cityId: body.cityId,
            address: body.address,
            pincode: body.pincode,
            profilePicture: body.profilePicture,
            fatherName: body.fatherName,
            fatherPhoneNumber: body.fatherPhoneNumber,
            fatherEmailId: body.fatherEmailId,
            motherName: body.motherName,
            motherPhoneNumber: body.motherPhoneNumber,
            motherEmailId: body.motherEmailId,
            legalGuardianName: body.legalGuardianName,
            legalGuardianPhoneNumber: body.legalGuardianPhoneNumber,
            legalGuardianEmailId: body.legalGuardianEmailId
        });

        if (student) {
            let studentId = student.id;

            // Create Student User
            let user = await models.user.create({
                userTypeId: userTypeOfStudent.id,
                email: body.email
            });

            // Mapping Student and User
            await models.userStudentMapping.create({
                userId: user.id,
                studentId: studentId,
                instituteId,
                classId: body.classId,
                batchId: body.batchId,
                groupId: body.groupId
            });

            // Storing student uniqueId by batch wise
            await models.studentBatchUniqueId.create({
                studentId,
                instituteId,
                batchId: body.batchId,
                uniqueId: body.uniqueId
            });

            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Update student
 * @param  {} req
 * @param  {} res
 * @created 2021-10-21
 */
module.exports.updateStudent = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let body = req.body;
        let { id } = req.params;

        // Check Unique Id
        let uniqueId = await models.studentBatchUniqueId.count({
            where: {
                uniqueId: body.uniqueId,
                batchId: body.batchId,
                studentId: {
                    [Op.ne]: id
                }
            }
        });

        if (uniqueId > 0) {
            return res.send(await commonResponse.response(false, Message.UNIQUE_ID_ALREADY_EXISTS));
        }

        let updateObj = {
            firstName: body.firstName,
            lastName: body.lastName,
            dateOfBirth: body.dateOfBirth,
            gender: body.gender,
            bloodGroup: body.bloodGroup,
            religion: body.religion,
            primaryPhoneNumber: body.primaryPhoneNumber,
            fatherName: body.fatherName,
            fatherPhoneNumber: body.fatherPhoneNumber,
            fatherEmailId: body.fatherEmailId,
            motherName: body.motherName,
            motherPhoneNumber: body.motherPhoneNumber,
            motherEmailId: body.motherEmailId,
            stateId: body.stateId,
            cityId: body.cityId,
            address: body.address,
            pincode: body.pincode,
            profilePicture: body.profilePicture,
        };

        // Find student user details
        let studentUser = await models.userStudentMapping.findOne({
            where: {
                studentId: id,
            }
        });

        if (studentUser) {
            // Check Unique Email
            let checkUser = await models.user.findOne({
                where: {
                    email: body.email,
                    id: {
                        [Op.ne]: studentUser.userId
                    }
                }
            });

            if (checkUser) {
                return res.send(await commonResponse.response(false, Message.ALREADY_EXISTS));
            } else {
                // Update Student
                await models.student.update(updateObj, {
                    where: {
                        id: id
                    }
                });

                // Update Student Email
                await models.user.update({
                    email: body.email
                }, {
                    where: {
                        id: studentUser.userId
                    }
                });

                // Update Mapping Student and User
                await models.userStudentMapping.update({
                    classId: body.classId,
                    batchId: body.batchId,
                    groupId: body.groupId
                }, {
                    where: {
                        id: studentUser.id
                    }
                });

                // Update unique id
                await models.studentBatchUniqueId.update({
                    uniqueId: body.uniqueId
                }, {
                    where: {
                        batchId: body.batchId,
                        studentId: id
                    }
                });

                return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get all student with pagination and search
 * @param  {} req
 * @param  {} res
 */
module.exports.allStudentsWithPagination = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.document.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};

        if (!req.query.searchKey) {
            where = {
                status: true
            }
        } else {
            where = {
                status: true,
                [Op.or]: {
                    firstName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    lastName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        if (req.query.gender) {
            where.gender = req.query.gender;
        }

        if (req.query.loginStatus) {
            where.loginStatus = req.query.loginStatus;
        }

        let userStudentWhereClause = {
            instituteId
        };

        if (req.query.classId) {
            userStudentWhereClause.classId = req.query.classId;
        }

        if (req.query.batchId) {
            userStudentWhereClause.batchId = req.query.batchId;
        }

        let response = await models.student.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            attributes: ['id', 'firstName', 'lastName', 'gender', 'createdAt', 'updatedAt', 'loginStatus'],
            include: [
                {
                    model: models.userStudentMapping,
                    required: true,
                    where: userStudentWhereClause,
                    attributes: ['userId', 'batchId', 'instituteId'],
                    include: [
                        {
                            model: models.user,
                            attributes: ['email']
                        },
                        {
                            model: models.batch,
                            attributes: ['batchName']
                        }
                    ]
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'profilePicture']]
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                asyncL.each(response.rows, (data, callback) => {
                    (async () => {
                        let userStudentMappings = data.userStudentMappings;
                        delete data.profilePicture;
                        delete data.userStudentMappings;

                        if (data.document && data.document.profilePicture) {
                            data.profilePicture = data.document.profilePicture;
                            delete data.document;
                        } else {
                            data.profilePicture = '';
                        }

                        data.email = userStudentMappings[0].user.email;
                        data.batchName = userStudentMappings[0].batch.batchName;
                        data.instituteId = userStudentMappings[0].instituteId;

                        let batchId = userStudentMappings[0].batchId;
                        let uniqueId = await exports.getUniqueId(instituteId, batchId, data.id);
                        data.uniqueId = uniqueId;

                        callback();
                    })();
                }, async (err) => {
                    let result = {
                        total: response.count,
                        rows: response.rows
                    }
                    return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
                });
            } else {
                let result = {
                    total: response.count,
                    rows: response.rows
                }
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
            }
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @param  {} instituteId
 * @param  {} batchId
 * @param  {} studentId
 */
module.exports.getUniqueId = (instituteId, batchId, studentId) => {
    return new Promise(async (resolve, reject) => {
        let studentUniqueId = await models.studentBatchUniqueId.findOne({ where: { instituteId, batchId, studentId }, attributes: ['uniqueId'] });
        studentUniqueId = JSON.parse(JSON.stringify(studentUniqueId));

        let uniqueId = (studentUniqueId && studentUniqueId.uniqueId) ? studentUniqueId.uniqueId : null;

        resolve(uniqueId);
    });
}

/**
 * @param  {} req
 * @param  {} res
 */
module.exports.studentById = async (req, res) => {
    try {
        await models.student.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let response = await models.student.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.userStudentMapping,
                    attributes: ['classId', 'batchId', 'groupId'],
                    include: [
                        {
                            model: models.group,
                            attributes: ['groupName']
                        },
                        {
                            model: models.class,
                            attributes: ['className']
                        },
                        {
                            model: models.batch,
                            attributes: ['batchName']
                        },
                        {
                            model: models.user,
                            attributes: ['email']
                        },
                    ]
                },
                {
                    model: models.city,
                    attributes: ['cityName']
                },
                {
                    model: models.state,
                    attributes: ['stateName']
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'profilePicture']]
                }
            ]
        });

        if (response) {
            let result = JSON.parse(JSON.stringify(response));

            if (result.document && result.document.profilePicture) {
                result.profilePicturePath = result.document.profilePicture;
                delete result.document;
            } else {
                result.profilePicturePath = '';
            }

            let uniqueId = await exports.getUniqueId(instituteId, result.userStudentMappings[0].batchId, result.id);
            result.uniqueId = uniqueId;

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete student
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteStudent = async (req, res) => {
    try {
        await models.student.sync({ force: false });
        await models.user.sync({ force: false });

        let { id } = req.params;

        // Find student user details
        let studentUser = await models.userStudentMapping.findOne({
            where: {
                studentId: id,
            }
        });

        if (studentUser) {
            await models.student.update({ status: false }, {
                where: {
                    id: id
                }
            });

            await models.user.update({
                status: false,
            }, {
                where: {
                    id: studentUser.userId
                }
            });

            await models.userStudentMapping.update({
                status: false,
            }, {
                where: {
                    id: studentUser.id
                }
            });

            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @param  {} req
 * @param  {} res
 */
module.exports.inviteStudents = async (req, res) => {
    try {
        await models.student.sync({ force: false });
        await models.user.sync({ force: false });

        let { studentIds } = req.body;

        let studentUser = await models.userStudentMapping.findAll({
            where: {
                studentId: studentIds,
                status: true
            },
            attributes: ['id', 'studentId'],
            include: [
                {
                    model: models.user,
                    attributes: ['email']
                },
                {
                    model: models.student,
                    attributes: ['firstName', 'lastName'],
                },
            ]
        });

        if (studentUser && studentUser.length > 0) {
            studentUser = JSON.parse(JSON.stringify(studentUser));

            asyncL.each(studentUser, (element, callback) => {
                (async () => {
                    let email = element.user.email;
                    let userName = `${element.student.firstName} ${element.student.lastName}`;

                    let isSent = await studentInvitation(email, userName);

                    if (isSent) {
                        // Update student login status
                        await models.student.update({
                            loginStatus: "Not Signed Up"
                        }, {
                            where: {
                                id: element.studentId
                            }
                        });
                    }

                    callback();
                })();
            }, async (err) => {
                return res.send(await commonResponse.response(true, Message.STUDENTS_INVITED));
            });
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Create bulk upload template
 * @param  {} req
 * @param  {} res
 */
module.exports.createStudentBulkUploadTemplate = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });

        let { classId, batchId, noOfStudent } = req.body;
        let { instituteId } = req.authData.data;

        let checkData = await models.studentBulkUploadHistory.count({
            where: {
                instituteId,
                batchId,
                classId
            }
        });

        if (checkData > 0) {
            return res.send(await commonResponse.response(false, Message.ALREADY_EXISTS));
        } else {
            // Request students
            noOfStudent = parseInt(noOfStudent);

            // Total registered students
            let totalStudents = await models.userStudentMapping.count({
                where: { instituteId, status: true }
            });
            totalStudents = parseInt(totalStudents);

            // Institution student limit
            let institute = await models.institution.findOne({ where: { id: instituteId }, attributes: ['studentLimit'] });
            if (!institute) {
                return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

            let studentLimit = parseInt(institute.studentLimit);
            let totalinstituteStudents = totalStudents + noOfStudent;

            if (studentLimit < totalinstituteStudents) {
                let totalLeftStudent = studentLimit - totalStudents;
                return res.send(await commonResponse.response(false, `You have permission to add only ${totalLeftStudent} more students. Please request for increasing your Student Capacity.`));
            } else {
                let studentBulkUploadHistory = await models.studentBulkUploadHistory.create({
                    instituteId,
                    documentId: null,
                    noOfStudent: noOfStudent,
                    batchId,
                    classId
                });

                if (studentBulkUploadHistory) {
                    return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                } else {
                    return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
                }
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Download bulk upload template
 * @param  {} req
 * @param  {} res
 */
module.exports.downloadStudentBulkUploadTemplate = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });

        let { instituteId } = req.authData.data;

        // Find Batches
        let batches = await models.batch.findAll({
            where: { instituteId, status: true },
            attributes: ['batchName']
        });

        // Find Group
        let groups = await models.group.findAll({
            where: { instituteId, status: true },
            attributes: ['groupName']
        });

        // Create Excel workbook and worksheet
        const workbook = new Excel.Workbook();
        const worksheet = workbook.addWorksheet('Students');

        // Header Title
        let headerTitles = [
            { header: 'No', key: 'no', width: 5 },
            { header: 'Batch', key: 'batchName', width: 10 },
            { header: 'First Name', key: 'firstName', width: 10 },
            { header: 'Last Name', key: 'lastName', width: 15 },
            { header: 'Image File Name', key: 'file', width: 15 },
            { header: 'Unique ID', key: 'uniqueId', width: 15 },
            { header: 'Email ID', key: 'email', width: 25 },
            { header: 'Date of Birth', key: 'dateOfBirth', width: 15 },
            { header: 'Group Name', key: 'groupName', width: 15 },
            { header: 'Gender', key: 'gender', width: 15 },
            { header: 'Blood Group', key: 'bloodGroup', width: 15 },
            { header: 'Religion', key: 'religion', width: 10 },
            { header: 'Primary Phone Number', key: 'primaryPhoneNumber', width: 15 },
            { header: "Father's Name", key: 'fatherName', width: 15 },
            { header: "Father's Phone Number", key: 'fatherPhoneNumber', width: 15 },
            { header: "Father's Email ID", key: 'fatherEmailId', width: 25 },
            { header: "Mother's Name", key: 'motherName', width: 15 },
            { header: "Mother's Phone Number", key: 'motherPhoneNumber', width: 15 },
            { header: "Mother's Email ID", key: 'motherEmailId', width: 25 },
            { header: "Legal Guardian's Name", key: 'legalGuardianName', width: 15 },
            { header: "Legal Guardian's Phone Number", key: 'legalGuardianPhoneNumber', width: 15 },
            { header: "Legal Guardian's Email ID", key: 'legalGuardianEmailId', width: 25 },
            { header: "State", key: 'stateName', width: 15 },
            { header: "City", key: 'cityName', width: 15 },
            { header: "Address", key: 'address', width: 20 },
            { header: "Pincode", key: 'pincode', width: 15 },
        ];

        // Define columns in the worksheet, these columns are identified using a key.
        worksheet.columns = headerTitles;

        // Data
        let results = [];

        // Add rows from database to worksheet 
        for (const row of results) {
            worksheet.addRow(row);
        }

        // File name & Path
        let fileName = `STUDENT_${moment().format('YYYY_MM_DD_HH_mm_ss')}.xlsx`;
        let filePath = path.join(__dirname, '../../../', `/${fileName}`);

        // Gender
        worksheet.getCell('J2').dataValidation = {
            type: 'list',
            allowBlank: false,
            formulae: ['"Male, Female"']
        };

        // Blood Group
        worksheet.getCell('K2').dataValidation = {
            type: 'list',
            allowBlank: false,
            formulae: ['"A+, A-, B+, B-, O+, O-, AB+, AB-"']
        };

        // Batch
        if (batches && batches.length > 0) {
            batches = JSON.parse(JSON.stringify(batches));
            batches = batches.map(data => data.batchName);

            let batchList = "\"" + batches.join(',') + "\"";

            worksheet.getCell('B2').dataValidation = {
                type: 'list',
                allowBlank: false,
                formulae: [batchList]
            };
        }

        // Group
        if (groups && groups.length > 0) {
            groups = JSON.parse(JSON.stringify(groups));
            groups = groups.map(data => data.groupName);

            let groupList = "\"" + groups.join(',') + "\"";

            worksheet.getCell('I2').dataValidation = {
                type: 'list',
                allowBlank: false,
                formulae: [groupList]
            };
        }


        // Finally save the worksheet into the folder from where we are running the code. 
        await workbook.xlsx.writeFile(fileName);

        // Download file direct
        res.download(filePath, fileName);

        // Remove file from directory
        setTimeout(function () {
            fs.unlinkSync(filePath);
        }, 1000);
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Upload bulk student using CSV or Request data
 * @param  {} req
 * @param  {} res
 */
module.exports.studentBulkUpload = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBulkUploadHistory.sync({ force: false });
        await models.studentBatchUniqueId.sync({ force: false });

        let { id } = req.body;
        let { instituteId } = req.authData.data;

        // Institution student limit
        let institute = await models.institution.findOne({ where: { id: instituteId }, attributes: ['studentLimit'] });
        if (!institute) {
            return res.send(await commonResponse.response(false, Message.INSTITUTION_NOT_FOUND));
        }

        // Check bulk upload details
        let studentBulkUploadHistory = await models.studentBulkUploadHistory.findOne({
            where: { id }
        });

        if(!studentBulkUploadHistory){
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

        let classId = studentBulkUploadHistory.classId;
        let batchId = studentBulkUploadHistory.batchId;

        if (req.files.studentExcel && req.files.studentExcel.length > 0) {
            let studentExcel = req.files.studentExcel[0];
            let filePath = path.join(studentExcel.destination, studentExcel.originalname);

            // Parse a file
            xlsxFile(filePath).then(async (rows) => {
                if (rows && rows.length > 0) {
                    let key = rows[0];
                    let studentData = [];

                    for (let i = 1; i < rows.length; i++) {
                        let element = rows[i];
                        let studentObj = {};

                        for (let j = 0; j < element.length; j++) {
                            studentObj[key[j]] = element[j];
                        }

                        studentData.push(studentObj);
                    }

                    if (studentData && studentData.length > 0) {
                        // Total registered students
                        let totalInstituteStudents = await models.userStudentMapping.count({
                            where: { instituteId, status: true }
                        });
                        totalInstituteStudents = parseInt(totalInstituteStudents);
                        let studentLimit = parseInt(institute.studentLimit);
                        let totalStudents = totalInstituteStudents + studentData.length;

                        if (studentLimit < totalStudents) {
                            let totalLeftStudent = studentLimit - totalInstituteStudents;
                            return res.send(await commonResponse.response(false, `You have permission to add only ${totalLeftStudent} more students. Please request for increasing your Student Capacity.`));
                        } else {
                            let errors = [];

                            // Find User Type
                            let userTypeOfStudent = await models.userType.findOne({
                                where: {
                                    userType: Usertypes.STUDENT,
                                    status: true,
                                }
                            });

                            if (!userTypeOfStudent) {
                                return res.send(await commonResponse.response(false, Message.USER_TYPE_NOT_FOUND));
                            }

                            asyncL.each(studentData, (element, callback) => {
                                (async () => {
                                    let firstName = element['First Name'];
                                    let lastName = element['Last Name'];
                                    let email = element['Email ID'];
                                    let groupName = element['Group Name'];
                                    let uniqueId = element['Unique ID'];
                                    let dateOfBirth = element['Date of Birth'];

                                    if (firstName && lastName && email && groupName && uniqueId && dateOfBirth) {
                                        // Check duplicate staff member
                                        let checkUser = await models.user.count({ where: { email: email } });

                                        // Check Unique Id
                                        let checkStudentUniqueId = await models.studentBatchUniqueId.count({ where: { instituteId, uniqueId, batchId } });

                                        if (checkUser > 0) {
                                            errors.push(`${firstName} ${lastName}'s email ${email} already exists in our records.`);
                                            callback();
                                        } else if (checkStudentUniqueId > 0) {
                                            errors.push(`${firstName} ${lastName}'s unique id ${uniqueId} already exists in our records.`);
                                            callback();
                                        } else {
                                            callback();
                                        }
                                    } else {
                                        let rawNo = element['No'];
                                        let errorMessage = `Row no: ${rawNo}, `;
                                        let fields = [];

                                        if (firstName) {
                                            fields.push(`First name`);
                                        }
                                        if (lastName) {
                                            fields.push(`Last name`);
                                        }
                                        if (email) {
                                            fields.push(`Email`);
                                        }
                                        if (groupName) {
                                            fields.push(`Group`);
                                        }
                                        if (uniqueId) {
                                            fields.push(`Unique Id`);
                                        }

                                        if (fields && fields.length > 0) {
                                            let message = fields.join(', ');
                                            errors.push(errorMessage + message + ` are required.`);
                                        }

                                        callback();
                                    }
                                })();
                            }, async (err) => {
                                if (errors && errors.length > 0) {
                                    // Remove Excel
                                    await fs.unlinkSync(filePath);

                                    req.files.studentProfilePictures.forEach(async (element) => {
                                        let filePath = path.join(element.destination, element.originalname);
                                        await fs.unlinkSync(filePath);
                                    });

                                    return res.send(await commonResponse.response(true, Message.DATA_CREATED, { validationError: errors }));
                                } else {
                                    // Store Data
                                    asyncL.each(studentData, (element, callback) => {
                                        (async () => {
                                            let firstName = element['First Name'];
                                            let lastName = element['Last Name'];
                                            let email = element['Email ID'];
                                            let groupName = element['Group Name'];
                                            let uniqueId = element['Unique ID'];
                                            let fileName = element['Image File Name'];
                                            let stateName = element['State'];
                                            let cityName = element['City'];

                                            // Find City
                                            let city = await models.city.findOne({ where: { cityName }, attributes: ['id'] });
                                            let cityId = (city) ? city.id : null;

                                            // Find State
                                            let state = await models.state.findOne({ where: { stateName }, attributes: ['id'] });
                                            let stateId = (state) ? state.id : null;

                                            // Find Group
                                            let group = await models.group.findOne({ where: { groupName }, attributes: ['id'] });
                                            let groupId = (group) ? group.id : null;

                                            // Profile Picture
                                            let profilePicPath = path.join(studentExcel.destination, fileName);
                                            let document = await exports.uploadFileOnS3(profilePicPath, fileName);
                                            let documentId = document.documentId;

                                            // Create Student
                                            let studentData = await models.student.create({
                                                firstName: firstName,
                                                lastName: lastName,
                                                dateOfBirth: element['Date of Birth'],
                                                gender: element['Gender'],
                                                bloodGroup: element['Blood Group'],
                                                religion: element['Religion'],
                                                primaryPhoneNumber: element['Primary Phone Number'],
                                                stateId: stateId,
                                                cityId: cityId,
                                                address: element['Address'],
                                                pincode: element['Pincode'],
                                                profilePicture: documentId,
                                                fatherName: element["Father's Name"],
                                                fatherPhoneNumber: element["Father's Phone Number"],
                                                fatherEmailId: element["Father's Email ID"],
                                                motherName: element["Mother's Name"],
                                                motherPhoneNumber: element["Mother's Phone Number"],
                                                motherEmailId: element["Mother's Email ID"],
                                                legalGuardianName: element["Legal Guardian's Name"],
                                                legalGuardianPhoneNumber: element["Legal Guardian's Phone Number"],
                                                legalGuardianEmailId: element["Legal Guardian's Email ID"]
                                            });

                                            if (studentData) {
                                                let studentId = studentData.id;

                                                // Create Student User
                                                let user = await models.user.create({
                                                    userTypeId: userTypeOfStudent.id,
                                                    email: email
                                                });

                                                // Mapping Student and User
                                                await models.userStudentMapping.create({
                                                    userId: user.id,
                                                    studentId: studentId,
                                                    instituteId,
                                                    classId: classId,
                                                    batchId: batchId,
                                                    groupId: groupId
                                                });

                                                // Storing student uniqueId by batch wise
                                                await models.studentBatchUniqueId.create({
                                                    studentId,
                                                    instituteId,
                                                    batchId: batchId,
                                                    uniqueId: uniqueId
                                                });

                                                callback();
                                            } else {
                                                callback();
                                            }
                                        })();
                                    }, async (err) => {
                                        // Store excel and create logs for uploaded excel file
                                        let document = await exports.uploadFileOnS3(filePath, studentExcel.originalname);
                                        let documentId = document.documentId;

                                        await models.studentBulkUploadHistory.update({
                                            documentId,
                                            noOfStudent: studentData.length,
                                        }, {
                                            where: { id }
                                        });

                                        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                                    });
                                }
                            });
                        }
                    } else {
                        return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
                    }
                } else {
                    return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
                }
            });
        } else {
            return res.send(await commonResponse.response(false, Message.FILE_REQUIRED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Upload files on S3 and store details in document table and return documentId as profilePicture.
 * @param  {} filePath, fileName
 */
module.exports.uploadFileOnS3 = async (filePath, fileName) => {
    return new Promise(async (resolve, reject) => {
        try {
            let folderName = Uploadpaths.STUDENT;

            fs.readFile(filePath, (err, data) => {
                if (err) {
                    reject({
                        documentId: null
                    });
                } else {
                    const params = {
                        Bucket: process.env.AWS_BUCKET_NAME, // pass your bucket name
                        Key: folderName + '/' + fileName, // file will be saved as testBucket/contacts.csv
                        Body: JSON.stringify(data, null, 2),
                        ACL: 'public-read',
                    };

                    S3.upload(params, async function (s3Err, data) {
                        await fs.unlinkSync(filePath);

                        if (s3Err) {
                            reject({
                                documentId: null
                            });
                        } else {
                            let s3Data = data;

                            let document = await models.document.create({
                                type: folderName,
                                etag: s3Data.ETag.substring(1, s3Data.ETag.length - 1),
                                fileName: s3Data.key.split('/').pop(),
                                filePath: s3Data.key,
                                fullPath: s3Data.Location,
                                mimeType: s3Data.mimetype,
                                size: s3Data.size
                            });

                            resolve({
                                documentId: document.id
                            });
                        }
                    });
                }
            });
        } catch (error) {
            reject({
                documentId: null
            });
        }
    });
}

/**
 * @description Check unique email
 * @param  {} parentTypeName
 */
module.exports.checkUniqueEmail = async (emailId) => {
    await models.user.sync({ force: false });
    return await models.user.count({ where: { email: emailId } });
}

/**
 * @description Uploaded excel list
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllUploadedExcelByInstitute = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.document.sync({ force: false });
        await models.studentBulkUploadHistory.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {
            instituteId,
            status: true
        };

        // Class Filter
        if (req.query.classId) {
            where.classId = req.query.classId;
        }

        // Batch Filter
        let batchWhere = {};
        if (req.query.searchKey) {
            batchWhere = {
                [Op.or]: {
                    batchName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        let response = await models.studentBulkUploadHistory.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            include: [
                {
                    model: models.batch,
                    attributes: ['batchName'],
                    where: batchWhere,
                    required: true
                },
                {
                    model: models.document,
                    attributes: [['fullPath', 'template']]
                }
            ]
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            if (response.rows && response.rows.length > 0) {
                response.rows = response.rows.map(data => {
                    if (data.document && data.document.template) {
                        data.template = data.document.template;
                        delete data.document;
                    } else {
                        data.template = '';
                    }

                    return data;
                })
            }

            let result = {
                total: response.count,
                rows: response.rows
            }
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Delete Templates
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteTemplates = async (req, res) => {
    try {
        await models.studentBulkUploadHistory.sync({ force: false });

        let { templateIds } = req.body;

        await models.studentBulkUploadHistory.destroy({
            where: {
                id: templateIds
            },
        });

        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description Request to change student's capacity
 * @param  {} req
 * @param  {} res
 */
module.exports.requestStudentCapacity = async (req, res) => {
    try {
        await models.studentCapacity.sync({ force: false });

        let body = req.body;
        let { instituteId } = req.authData.data;

        let studentCapacity = await models.studentCapacity.create({
            instituteId: instituteId,
            studentLimit: body.studentLimit
        });

        if (studentCapacity) {
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}