const asyncL = require('async');
const { Op } = require('sequelize');
const sequelize = require('sequelize');
const Excel = require('exceljs');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
var htmlPdf = require('html-pdf');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, FeedbackFormType, FeedbackFormStatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

/**
 * @description Create Feedback Template
 * @param  {} req
 * @param  {} res
 */
module.exports.createFeedbackTemplate = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });

        let { instituteId } = req.authData.data;
        let body = req.body;

        let noOfCategories = parseInt(body.noOfCategories);
        let categories = JSON.parse(JSON.stringify(body.categories));

        if (categories.length != noOfCategories) {
            return res.send(await commonResponse.response(false, "Template category length is not match with the number of categories."));
        } else {

            // create feedback template 
            let feedbackTemplate = await models.feedbackTemplate.create({
                templateName: body.templateName,
                noOfCategories: body.noOfCategories,
                instituteId
            });

            if (feedbackTemplate) {
                let feedbackTemplateId = feedbackTemplate.id;

                asyncL.each(categories, (category, callback) => {
                    (async () => {
                        // Create category
                        let feedbackTemplateCategory = await models.feedbackTemplateCategory.create({
                            feedbackTemplateId,
                            categoryName: category.categoryName
                        });

                        // Store benchmark
                        if (feedbackTemplateCategory) {
                            let feedbackTemplateCategoryId = feedbackTemplateCategory.id;

                            if (category.benchmarks && category.benchmarks.length > 0) {
                                let benchmarks = category.benchmarks;
                                benchmarks = benchmarks.map(benchmark => {
                                    return {
                                        feedbackTemplateCategoryId,
                                        benchmark: benchmark.benchmark,
                                        points: benchmark.points
                                    };
                                });

                                await models.feedbackTemplateBenchmark.bulkCreate(benchmarks);
                            }

                            // Store Parameters
                            if (category.parameters && category.parameters.length > 0) {
                                let parameters = category.parameters;
                                parameters = parameters.map(parameter => {
                                    return {
                                        feedbackTemplateCategoryId,
                                        parameter: parameter.parameter,
                                    };
                                });

                                await models.feedbackTemplateCategoryParameter.bulkCreate(parameters);
                            }

                            callback();
                        } else {
                            callback();
                        }
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                });
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get feedback template by id
 * @param  {} req
 * @param  {} res
 */
module.exports.getFeedbackTemplateById = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });

        let { id } = req.params;

        let response = await models.feedbackTemplate.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.feedbackTemplateCategory,
                    attributes: ['id', 'categoryName'],
                    include: [
                        {
                            model: models.feedbackTemplateBenchmark,
                            attributes: ['id', 'benchmark', 'points']
                        }
                    ],
                    required: true
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            for (var i = 0; i < response.feedbackTemplateCategories.length; i++) {
                let element = response.feedbackTemplateCategories[i];
                let categoryId = element.id;

                response.feedbackTemplateCategories[i].feedbackTemplateCategoryParameters = await models.feedbackTemplateCategoryParameter.findAll({
                    where: {
                        feedbackTemplateCategoryId: categoryId
                    },
                    attributes: ['id', 'parameter']
                });
            }

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get all feedback templates
 * @param  {} req
 * @param  {} res
 */
module.exports.allFeedbackTemplates = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['updatedAt', 'DESC']);
        }

        let whereClause = {};
        if (req.query.searchKey) {
            whereClause = {
                status: true,
                [Op.or]: {
                    'templateName': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        whereClause.instituteId = instituteId;
        whereClause.status = true;

        let response = await models.feedbackTemplate.findAndCountAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: whereClause,
            attributes: ['id', 'templateName', 'updatedAt'],
        });

        if (response.rows && response.rows.length > 0) {
            response.rows = JSON.parse(JSON.stringify(response.rows));

            asyncL.each(response.rows, (element, callback) => {
                (async () => {
                    let feedbackTemplateId = element.id;

                    // Count Categories
                    let noOfCategories = await models.feedbackTemplateCategory.count({ where: { feedbackTemplateId } });
                    element.noOfCategories = noOfCategories;

                    let noOfParameters = await models.feedbackTemplateCategory.count({
                        where: { feedbackTemplateId },
                        include: [
                            {
                                model: models.feedbackTemplateCategoryParameter,
                                required: true
                            }
                        ]
                    });
                    element.noOfParameters = noOfParameters;

                    callback();
                })();
            }, async (err) => {
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get all feedback template
 * @param  {} req
 * @param  {} res
 */
module.exports.getFeedbackTemplates = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });

        let { instituteId } = req.authData.data;

        let response = await models.feedbackTemplate.findAll({
            where: {
                instituteId, status: true
            },
            attributes: ['id', 'templateName'],
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Update Feedback Template
 * @param  {} req
 * @param  {} res
 */
module.exports.updateFeedbackTemplate = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });

        let { id } = req.params;
        let body = req.body;

        let noOfCategories = parseInt(body.noOfCategories);
        let categories = JSON.parse(JSON.stringify(body.categories));

        if (categories.length != noOfCategories) {
            return res.send(await commonResponse.response(false, "Template category length is not match with the number of categories."));
        } else {
            // update feedback template 
            let feedbackTemplate = await models.feedbackTemplate.update({
                templateName: body.templateName,
                noOfCategories: body.noOfCategories
            }, {
                where: {
                    id: id
                }
            });

            if (feedbackTemplate) {
                let feedbackTemplateId = id;

                asyncL.each(categories, (category, callback) => {
                    (async () => {
                        // Remove Category
                        let requestCategories = categories.filter(data => {
                            if (data.id) {
                                return data.id;
                            }
                        });

                        let getCategories = await models.feedbackTemplateCategory.findAll({ where: { feedbackTemplateId }, attributes: ['id'] });

                        if (getCategories && getCategories.length > 0) {
                            getCategories.map(async (cElement) => {
                                let checkCategory = requestCategories.find(gcElement => gcElement.id == cElement.id);

                                if (!checkCategory && checkCategory == undefined) {
                                    await models.feedbackTemplateCategoryParameter.destroy({ where: { feedbackTemplateCategoryId: cElement.id } });
                                    await models.feedbackTemplateBenchmark.destroy({ where: { feedbackTemplateCategoryId: cElement.id } });
                                    await models.feedbackTemplateCategory.destroy({ where: { id: cElement.id } });
                                }
                            });
                        }

                        if (category.id) {
                            let feedbackTemplateCategoryId = category.id;

                            // Update category
                            await models.feedbackTemplateCategory.update({
                                categoryName: category.categoryName
                            }, {
                                where: { id: feedbackTemplateCategoryId }
                            });

                            if (category.benchmarks && category.benchmarks.length > 0) {
                                let benchmarks = category.benchmarks;

                                // Remove Benchmarks
                                let requestBenchmarks = benchmarks.filter(data => {
                                    if (data.id) {
                                        return data.id;
                                    }
                                });

                                let getBenchmarks = await models.feedbackTemplateBenchmark.findAll({ where: { feedbackTemplateCategoryId }, attributes: ['id'] });

                                if (getBenchmarks && getBenchmarks.length > 0) {
                                    getBenchmarks.map(async (cElement) => {
                                        let checkBenchmark = requestBenchmarks.find(gcElement => gcElement.id == cElement.id);

                                        if (!checkBenchmark && checkBenchmark == undefined) {
                                            await models.feedbackTemplateBenchmark.destroy({ where: { id: cElement.id } });
                                        }
                                    });
                                }

                                benchmarks.map(async (benchmark) => {
                                    if (benchmark.id) {
                                        // Update Benchmark
                                        await models.feedbackTemplateBenchmark.update({
                                            benchmark: benchmark.benchmark,
                                            points: benchmark.points
                                        }, {
                                            where: {
                                                id: benchmark.id
                                            }
                                        });
                                    } else {
                                        // Create Benchmark
                                        await models.feedbackTemplateBenchmark.create({
                                            feedbackTemplateCategoryId,
                                            benchmark: benchmark.benchmark,
                                            points: benchmark.points
                                        });
                                    }
                                });
                            }

                            // Store Parameters
                            if (category.parameters && category.parameters.length > 0) {
                                let parameters = category.parameters;

                                // Remove Parameters
                                let requestParameters = parameters.filter(data => {
                                    if (data.id) {
                                        return data.id;
                                    }
                                });

                                let getParameters = await models.feedbackTemplateCategoryParameter.findAll({ where: { feedbackTemplateCategoryId }, attributes: ['id'] });

                                if (getParameters && getParameters.length > 0) {
                                    getParameters.map(async (cElement) => {
                                        let checkParameters = requestParameters.find(gcElement => gcElement.id == cElement.id);
                                        if (!checkParameters && checkParameters == undefined) {
                                            await models.feedbackTemplateCategoryParameter.destroy({ where: { id: cElement.id } });
                                        }
                                    });
                                }

                                parameters.map(async (parameter) => {
                                    if (parameter.id) {
                                        // Update Parameter
                                        await models.feedbackTemplateCategoryParameter.update({
                                            parameter: parameter.parameter,
                                        }, {
                                            where: {
                                                id: parameter.id
                                            }
                                        });
                                    } else {
                                        // Create Parameter
                                        await models.feedbackTemplateCategoryParameter.create({
                                            feedbackTemplateCategoryId,
                                            parameter: parameter.parameter,
                                        });
                                    }
                                });
                            }

                            callback();
                        } else {
                            // Create category
                            let feedbackTemplateCategory = await models.feedbackTemplateCategory.create({
                                feedbackTemplateId,
                                categoryName: category.categoryName
                            });

                            // Store benchmark
                            if (feedbackTemplateCategory) {
                                let feedbackTemplateCategoryId = feedbackTemplateCategory.id;

                                if (category.benchmarks && category.benchmarks.length > 0) {
                                    let benchmarks = category.benchmarks;
                                    benchmarks = benchmarks.map(benchmark => {
                                        return {
                                            feedbackTemplateCategoryId,
                                            benchmark: benchmark.benchmark,
                                            points: benchmark.points
                                        };
                                    });

                                    await models.feedbackTemplateBenchmark.bulkCreate(benchmarks);
                                }

                                // Store Parameters
                                if (category.parameters && category.parameters.length > 0) {
                                    let parameters = category.parameters;
                                    parameters = parameters.map(parameter => {
                                        return {
                                            feedbackTemplateCategoryId,
                                            parameter: parameter.parameter,
                                        };
                                    });

                                    await models.feedbackTemplateCategoryParameter.bulkCreate(parameters);
                                }

                                callback();
                            } else {
                                callback();
                            }
                        }
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
                });
            } else {
                return res.send(await commonResponse.response(false, Message.DATA_NOT_UPDATED));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Delete Feedback Template
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteFeedbackTemplate = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });

        let { id } = req.params;
        await models.feedbackTemplate.update({ status: false }, { where: { id: id } });

        return res.send(await commonResponse.response(false, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Download Feedback Template
 * @param  {} req
 * @param  {} res
 */
module.exports.downloadFeedbackTemplate = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });

        let { id, fileType } = req.body;

        let response = await models.feedbackTemplate.findOne({
            where: { id, status: true },
            attributes: [],
            include: [
                {
                    model: models.feedbackTemplateCategory,
                    attributes: ['id', 'categoryName'],
                    include: [
                        {
                            model: models.feedbackTemplateBenchmark,
                            attributes: ['benchmark', 'points']
                        }
                    ],
                    required: true
                }
            ],
        });

        if (response) {
            response = JSON.parse(JSON.stringify(response));

            let templateResults = [];

            for (var i = 0; i < response.feedbackTemplateCategories.length; i++) {
                let element = response.feedbackTemplateCategories[i];
                let categoryId = element.id;

                let mainElement = {
                    index: 0,
                    no: "Sl. No.",
                    categoryName: element.categoryName,
                };

                element.feedbackTemplateBenchmarks.forEach((bElement, index) => {
                    let indexNo = index + 1;
                    mainElement[`benchmark` + indexNo] = `${bElement.benchmark} (${bElement.points})`;
                });

                templateResults.push(mainElement);

                let feedbackTemplateCategoryParameters = await models.feedbackTemplateCategoryParameter.findAll({
                    where: {
                        feedbackTemplateCategoryId: categoryId
                    },
                    attributes: ['parameter']
                });

                feedbackTemplateCategoryParameters.forEach((pElement, pIndex) => {
                    let pIndexNo = pIndex + 1;

                    let pObject = {
                        no: pIndexNo,
                        categoryName: pElement.parameter
                    };

                    element.feedbackTemplateBenchmarks.forEach((bElement, index) => {
                        let indexNo = index + 1;
                        pObject[`benchmark` + indexNo] = '';
                    });

                    templateResults.push(pObject);
                });
            }

            if (fileType == 'EXCEL') {
                // Create Excel workbook and worksheet
                const workbook = new Excel.Workbook();
                const worksheet = workbook.addWorksheet('Feedback Template');

                // Header Title
                let headerTitles = [
                    { header: '', key: 'no', width: 10 },
                    { header: '', key: 'categoryName', width: 100 },
                    { header: '', key: 'benchmark1', width: 20 },
                    { header: '', key: 'benchmark2', width: 20 },
                    { header: '', key: 'benchmark3', width: 20 },
                    { header: '', key: 'benchmark4', width: 20 },
                ];

                // Define columns in the worksheet, these columns are identified using a key.
                worksheet.columns = headerTitles;

                // Add rows from database to worksheet 
                let n = 0;
                for (const row of templateResults) {
                    if (row.index != undefined && row.index == 0 && n != 0) {
                        worksheet.addRow({});
                        worksheet.addRow({});
                    }

                    let fields = worksheet.addRow(row);
                    fields.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
                    if (row.index != undefined && row.index == 0) {
                        fields.font = {
                            size: 14,
                            bold: true,
                            wrapText: true,
                            vertical: 'center',
                            horizontal: 'center'
                        }
                    } else {
                        fields.font = {
                            size: 11,
                            bold: false,
                            wrapText: true,
                            vertical: 'center',
                            horizontal: 'center'
                        }
                    }

                    n++;
                }

                // File name & Path
                let fileName = `Feedback_Template_${moment().format("HH_MM_SS")}.xlsx`;
                let filePath = path.join(__dirname, '../../../', `/${fileName}`);

                // Finally save the worksheet into the folder from where we are running the code. 
                await workbook.xlsx.writeFile(fileName);

                // We replaced all the event handlers with a simple call to readStream.pipe()
                var readStream = fs.createReadStream(filePath);
                res.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                readStream.pipe(res);

                // Remove file from directory
                setTimeout(function () {
                    fs.unlinkSync(filePath);
                }, 1000);
            } else if (fileType == 'PDF') {

                let options = {
                    "format": "A4", // allowed units: A3, A4, A5, Legal, Letter, Tabloid
                    "orientation": "portrait", // portrait or landscape
                    "header": {
                        "height": "20px",
                    },
                    "footer": {
                        "height": "20px"
                    },
                };

                let html = `<style>
                                table {
                                    font-family: arial, sans-serif;
                                    border-collapse: collapse;
                                    width: 100%;
                                }
                                
                                td, th {
                                    border: 1px solid #282c34;
                                    text-align: left;
                                    padding: 8px;
                                }

                                * { /* this works for all but td */
                                    word-wrap:break-word;
                                }
                                
                                table { /* this somehow makes it work for td */
                                    table-layout:fixed;
                                    width:100%;
                                }
                            </style>
                            <body style="margin: 10px; padding:10px; height:100%;">`;

                for (var i = 0; i < response.feedbackTemplateCategories.length; i++) {
                    html += `<table style="margin-bottom: 20px;" border="1px">`;
                    let element = response.feedbackTemplateCategories[i];
                    let categoryId = element.id;

                    html += `
                        <thead><tr>
                            <th width="10%">Sl. No.</th>
                            <th width="50%">${element.categoryName}</th>
                    `;

                    element.feedbackTemplateBenchmarks.forEach((bElement, index) => {
                        let benchmarkAndPoints = `${bElement.benchmark} (${bElement.points})`;
                        html += `<th>${benchmarkAndPoints}</th>`;
                    });

                    html += `</tr></thead>`;
                    html += `<tbody>`;

                    let feedbackTemplateCategoryParameters = await models.feedbackTemplateCategoryParameter.findAll({
                        where: {
                            feedbackTemplateCategoryId: categoryId
                        },
                        attributes: ['parameter']
                    });

                    feedbackTemplateCategoryParameters.forEach((pElement, pIndex) => {
                        let pIndexNo = pIndex + 1;

                        html += `<tr>
                            <td>${pIndexNo}</td>
                            <td style="word-wrap: break-word;">${pElement.parameter}</td>`;

                        element.feedbackTemplateBenchmarks.forEach((bElement, index) => {
                            html += `<td>&nbsp;</td>`;
                        });
                        html += `</tr>`;
                    });
                    html += `</tbody></table>`;
                }

                html += `</body>`;

                htmlPdf.create(html, options).toStream(async (err, stream) => {
                    if (err) {
                        return res.send(await commonResponse.response(false, "Something went wrong with PDF creation."));
                    } else {
                        res.set('Content-type', 'application/pdf');
                        stream.pipe(res);
                    }
                });
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Create Feedback Form
 * @param  {} req
 * @param  {} res
 */
module.exports.createFeedbackForm = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackForm.sync({ force: false });
        await models.feedbackFormClassBatch.sync({ force: false });

        let { instituteId } = req.authData.data;
        let body = req.body;

        let feedbackForm = await models.feedbackForm.create({
            instituteId,
            formType: body.formType,
            feedbackCycle: body.feedbackCycle,
            feedbackTemplateId: body.feedbackTemplateId,
            deadlineDate: body.deadlineDate,
            deadlineTime: body.deadlineTime
        });

        if (feedbackForm) {
            let feedbackFormId = feedbackForm.id;

            if (body.formType == FeedbackFormType.Class) {
                body.classIds = JSON.parse(JSON.stringify(body.classIds));
                let feedbackFormClassBatchData = body.classIds.map(classId => {
                    return {
                        feedbackFormId,
                        classId
                    };
                });

                await models.feedbackFormClassBatch.bulkCreate(feedbackFormClassBatchData);
            } else if (body.formType == FeedbackFormType.Batch) {
                let classId = body.classIds[0];
                let feedbackFormClassBatchData = body.batchIds.map(batchId => {
                    return {
                        feedbackFormId,
                        classId,
                        batchId
                    };
                });

                await models.feedbackFormClassBatch.bulkCreate(feedbackFormClassBatchData);
            }

            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get All Feedback Forms
 * @param  {} req
 * @param  {} res
 */
module.exports.allFeedbackForms = async (req, res) => {
    try {
        await models.feedbackTemplate.sync({ force: false });
        await models.feedbackTemplateCategory.sync({ force: false });
        await models.feedbackTemplateBenchmark.sync({ force: false });
        await models.feedbackTemplateCategoryParameter.sync({ force: false });
        await models.feedbackForm.sync({ force: false });
        await models.feedbackFormClassBatch.sync({ force: false });

        let { instituteId } = req.authData.data;

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }

        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['updatedAt', 'DESC']);
        }

        let whereClause = {};
        if (req.query.searchKey) {
            whereClause = {
                status: true,
                [Op.or]: {
                    'feedbackCycle': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        if (req.query.status) {
            whereClause.formStatus = req.query.status;
        }

        whereClause.instituteId = instituteId;
        whereClause.status = true;

        // Class Filter
        let classWhereClause = {};

        if (req.query.classId) {
            classWhereClause.classId = req.query.classId;
        }

        let response = await models.feedbackForm.findAll({
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: whereClause,
            include: [
                {
                    model: models.feedbackFormClassBatch,
                    attributes: ['id'],
                    where: classWhereClause,
                    include: [
                        {
                            model: models.class,
                            attributes: ['className']
                        },
                        {
                            model: models.batch,
                            attributes: ['batchName']
                        }
                    ],
                    required: true,
                },
                {
                    model: models.feedbackTemplate,
                    attributes: ['id'],
                    include: [
                        {
                            model: models.feedbackTemplateCategory,
                            attributes: ['id']
                        }
                    ],
                    required: true,
                }
            ]
        });

        if (response && response.length > 0) {
            response = JSON.parse(JSON.stringify(response));

            asyncL.each(response, (element, callback) => {
                (async () => {
                    let feedbackTemplateId = element.feedbackTemplateId;

                    let noOfParameters = await models.feedbackTemplateCategory.count({
                        where: { feedbackTemplateId },
                        include: [
                            {
                                model: models.feedbackTemplateCategoryParameter,
                                required: true
                            }
                        ]
                    });
                    element.totalQuestions = noOfParameters;

                    let classAndBatch = '';

                    if (element.formType == FeedbackFormType.Class) {
                        classAndBatch = element.feedbackFormClassBatches.map(data => {
                            return data.class.className;
                        });

                        classAndBatch = classAndBatch.join(', ');
                    } else if (element.formType == FeedbackFormType.Batch) {
                        classAndBatch = element.feedbackFormClassBatches.map(data => {
                            return `${data.class.className} - ${data.batch.batchName}`;
                        });

                        classAndBatch = classAndBatch.join(', ');
                    }

                    element.classAndBatch = classAndBatch;

                    let deadlineDate = moment(element.deadlineDate).format('DD-MM-YYYY');
                    element.deadline = `${deadlineDate} ${element.deadlineTime}`;

                    delete element.feedbackFormClassBatches;
                    delete element.feedbackTemplate;
                    delete element.deadlineDate;
                    delete element.deadlineTime;

                    callback();
                })();
            }, async (err) => {
                let totalRecords = await models.feedbackForm.count({
                    where: whereClause,
                    include: [
                        {
                            model: models.feedbackFormClassBatch,
                            where: classWhereClause,
                            required: true,
                        },
                    ]
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                    "count": totalRecords,
                    "rows": response
                }));
            });
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, {
                "count": 0,
                "rows": []
            }));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get Feedback Cycle
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllFeedbackCycles = async (req, res) => {
    try {
        await models.feedbackForm.sync({ force: false });

        let { instituteId } = req.authData.data;

        let response = await models.feedbackForm.findAll({
            where: {
                instituteId,
                status: true,
            },
            attributes: [['id', 'feedbackFormId'], 'feedbackCycle']
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get All Feedback Reports
 * @param  {} req
 * @param  {} res
 */
module.exports.allFeedbackReports = async (req, res) => {
    try {
        await models.userStaffMapping.sync({ force: false });
        await models.submittedFeedbackForm.sync({ force: false });

        let { instituteId } = req.authData.data;

        let staffs = await models.userStaffMapping.findAll({
            where: { instituteId, status: true },
            attributes: ['staffId']
        });

        if (staffs && staffs.length > 0) {

            staffs = JSON.parse(JSON.stringify(staffs));
            let staffsIds = staffs.map(data => data.staffId);

            if (req.query.pageSize) {
                Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            }

            if (req.query.pageNo) {
                Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
            }

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['updatedAt', 'DESC']);
            }

            // Status
            let whereClause = {
                staffId: staffsIds,
                status: true
            };

            if (req.query.feedbackFormId) {
                whereClause.feedbackFormId = req.query.feedbackFormId;
            }

            if (req.query.status) {
                whereClause.isPublished = req.query.status;
            }

            // Data
            let results = await models.submittedFeedbackForm.findAll({
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                where: whereClause,
                attributes: ['id', 'staffId', 'feedbackFormId', 'isPublished', 'publishedAt'],
                include: [
                    {
                        model: models.staff,
                        attributes: ['firstName', 'lastName', 'employeeCode']
                    },
                    {
                        model: models.feedbackForm,
                        attributes: ['feedbackCycle']
                    }
                ],
                group: ['submittedFeedbackForm.staffId', 'staff.id', 'feedbackForm.id', 'submittedFeedbackForm.id']
            });

            // Count
            let count = await models.submittedFeedbackForm.count({
                where: whereClause
            });

            let responseData = {
                count,
                rows: results
            };

            return res.json(await commonResponse.response(true, Message.DATA_FOUND, responseData));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get All Feedback Reports
 * @param  {} req
 * @param  {} res
 */
module.exports.publishFeedbackResult = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });

        let { id } = req.params;

        // Data
        await models.submittedFeedbackForm.update({
            isPublished: true,
            publishedAt: moment().utcOffset("+05:30").format('YYYY-MM-DD HH:mm:ss'),
        }, {
            where: {
                id
            },
        });

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Get staff feedback report 
 * @param  {} req
 * @param  {} res
 */
module.exports.getAllFeedbackResultsByStaff = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });

        let { staffId } = req.body;

        let results = await models.submittedFeedbackForm.findAll({
            attributes: ['feedbackFormId', 'staffId', 'subjectId', 'batchId'],
            where: {
                staffId
            },
            include: [
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ],
            group: ['submittedFeedbackForm.feedbackFormId', 'submittedFeedbackForm.staffId', 'subjectId', 'batchId', 'subject.id', 'batch.id'],
        });

        return res.json(await commonResponse.response(true, Message.DATA_FOUND, results));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description Download feedback report of staff
 * @param  {} req
 * @param  {} res
 */
module.exports.downloadFeedbackResultOfStaff = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });

        let { feedbackFormId, subjectId, batchId, staffId } = req.body;

        // Staff Batch and Subject Details
        let submittedFeedback = await models.submittedFeedbackForm.findOne({
            where: {
                feedbackFormId, subjectId, batchId, staffId
            },
            attributes: [],
            include: [
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                },
                {
                    model: models.staff,
                    attributes: ['firstName', 'lastName', 'employeeCode']
                },
                {
                    model: models.feedbackForm,
                    attributes: ['feedbackTemplateId', 'feedbackCycle', 'createdAt']
                }
            ],
        });

        if (!submittedFeedback) {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

        // No. of Student who gave feedback
        let submittedFeedbacks = await models.submittedFeedbackForm.findAll({
            attributes: ['id'],
            where: {
                feedbackFormId,
                subjectId,
                batchId,
                staffId,
                status: true
            }
        });

        if (submittedFeedbacks && submittedFeedbacks.length > 0) {
            let totalStudent = submittedFeedbacks.length;
            let submittedFeedbackFormIds = submittedFeedbacks.map(data => data.id);

            let response = await models.feedbackTemplate.findOne({
                where: { id: submittedFeedback.feedbackForm.feedbackTemplateId, status: true },
                attributes: [],
                include: [
                    {
                        model: models.feedbackTemplateCategory,
                        attributes: ['id', 'categoryName'],
                        include: [
                            {
                                model: models.feedbackTemplateBenchmark,
                                attributes: ['id', 'benchmark', 'points']
                            }
                        ],
                        required: true
                    }
                ],
            });

            if (response) {
                let averagePercentage = 0;
                let totalCategoryParameters = 0;

                let options = {
                    "format": "A4", // allowed units: A3, A4, A5, Legal, Letter, Tabloid
                    "orientation": "portrait", // portrait or landscape
                    "header": {
                        "height": "20px",
                    },
                    "footer": {
                        "height": "20px"
                    },
                };

                let html = `<style>
                                table {
                                    font-family: arial, sans-serif;
                                    border-collapse: collapse;
                                    width: 100%;
                                }
                                
                                td, th {
                                    border: 1px solid #282c34;
                                    text-align: left;
                                    padding: 8px;
                                }

                                * { /* this works for all but td */
                                    word-wrap:break-word;
                                }
                                
                                table { /* this somehow makes it work for td */
                                    table-layout:fixed;
                                    width:100%;
                                }

                                .text-center{
                                    text-align: center;
                                }
                            </style>
                            <body style="margin: 10px; padding:10px; height:100%;">
                            <table style="margin-bottom: 20px;" border="1px">
                                <tbody>
                                    <tr>
                                        <th>Feedback Cycle</th>
                                        <td>${submittedFeedback.feedbackForm.feedbackCycle}</td>
                                        <th>Batch</th>
                                        <td>${submittedFeedback.batch.batchName}</td>
                                        <th>Faculty</th>
                                        <td>${submittedFeedback.staff.firstName} ${submittedFeedback.staff.lastName}</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <th>Employee CODE</th>
                                        <td colspan="2" class="text-center">${submittedFeedback.staff.employeeCode}</td>
                                        <th>Subject</th>
                                        <td>${submittedFeedback.subject.subjectName}</td>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <td>${moment(submittedFeedback.feedbackForm.createdAt).format("DD/MM/YYYY")}</td>
                                        <th colspan="3">No. of Student who gave feedback</th>
                                        <td>${totalStudent}</td>
                                    </tr>
                                </tbody>
                            </table>`;

                for (var i = 0; i < response.feedbackTemplateCategories.length; i++) {
                    html += `<table style="margin-bottom: 20px;" border="1px">`;
                    let element = response.feedbackTemplateCategories[i];
                    let categoryId = element.id;
                    let cateLength = element.feedbackTemplateBenchmarks.length;
                    let benchmarkPoints = element.feedbackTemplateBenchmarks.map(data => data.points);
                    let maxBenchmarkPoint = Math.max(...benchmarkPoints);
                    maxBenchmarkPoint = parseInt(maxBenchmarkPoint);

                    let securedMarks = 0;

                    html += `
                        <thead>
                        <tr style="background-color:#696969; font-size:18px;">
                            <th class="text-center" colspan="2">Queries</th>
                            <th class="text-center" colspan="${cateLength}">Parameters</th>
                        </tr>
                        <tr style="background-color:#696969; font-size:18px;">
                            <th width="10%">Sl. No.</th>
                            <th width="50%">${element.categoryName}</th>
                    `;

                    element.feedbackTemplateBenchmarks.forEach((bElement, index) => {
                        let benchmarkAndPoints = `${bElement.benchmark} (${bElement.points})`;
                        html += `<th>${benchmarkAndPoints}</th>`;
                    });

                    html += `</tr></thead>`;
                    html += `<tbody>`;

                    let feedbackTemplateCategoryParameters = await models.feedbackTemplateCategoryParameter.findAll({
                        where: {
                            feedbackTemplateCategoryId: categoryId
                        },
                        attributes: ['id', 'parameter']
                    });

                    for (var c = 0; c < feedbackTemplateCategoryParameters.length; c++) {
                        let pElement = feedbackTemplateCategoryParameters[c];
                        let parameterId = pElement.id;
                        let pIndexNo = c + 1;

                        html += `<tr>
                            <td>${pIndexNo}</td>
                            <td style="word-wrap: break-word;">${pElement.parameter}</td>`;

                        for (var b = 0; b < element.feedbackTemplateBenchmarks.length; b++) {
                            let bElement = element.feedbackTemplateBenchmarks[b];
                            let benchmarkId = bElement.id;

                            let sumOfBenchmarks = await models.submittedFeedbackResponse.findOne({
                                attributes: [[sequelize.fn('SUM', sequelize.col('feedbackTemplateBenchmark.points')), 'totalFeedbacks']],
                                where: {
                                    submittedFeedbackFormId: submittedFeedbackFormIds,
                                    categoryId,
                                    parameterId,
                                    benchmarkId,
                                },
                                include: [
                                    {
                                        model: models.feedbackTemplateBenchmark,
                                        attributes: []
                                    }
                                ],
                                group: ['categoryId', 'parameterId', 'benchmarkId', 'feedbackTemplateBenchmark.id']
                            });

                            let totalStudentFeedbacks = '';

                            if (sumOfBenchmarks) {
                                sumOfBenchmarks = JSON.parse(JSON.stringify(sumOfBenchmarks));
                                totalStudentFeedbacks = sumOfBenchmarks.totalFeedbacks;
                            }
                            html += `<td>${totalStudentFeedbacks}</td>`;
                        }
                        html += `</tr>`;
                    }

                    let totalOfBenchmarks = await models.submittedFeedbackResponse.findAll({
                        attributes: [[sequelize.fn('SUM', sequelize.col('feedbackTemplateBenchmark.points')), 'totalFeedbacks'], 'categoryId', 'benchmarkId'],
                        where: {
                            submittedFeedbackFormId: submittedFeedbackFormIds,
                            categoryId
                        },
                        include: [
                            {
                                model: models.feedbackTemplateBenchmark,
                                attributes: ["id", "points"]
                            }
                        ],
                        group: ['categoryId', 'benchmarkId', 'feedbackTemplateBenchmark.id']
                    });

                    totalOfBenchmarks = (totalOfBenchmarks && totalOfBenchmarks.length > 0) ? JSON.parse(JSON.stringify(totalOfBenchmarks)) : [];
                    totalOfBenchmarks.forEach(element => {
                        let totalFeedbacksPoints = parseInt(element.totalFeedbacks);
                        let points = parseInt(element.feedbackTemplateBenchmark.points);
                        securedMarks += (totalFeedbacksPoints * points);
                    });

                    averagePercentage += securedMarks;

                    let totalParameters = feedbackTemplateCategoryParameters.length;
                    totalCategoryParameters += parseFloat(totalParameters);

                    let percentage = (maxBenchmarkPoint * totalStudent * totalParameters) * 100 / securedMarks;

                    percentage = parseFloat(percentage).toFixed(2);

                    html += `<tr style="background-color:#696969; font-size:18px;">
                                <th class="text-center">Secured Marks</th>
                                <th class="text-center">${parseFloat(securedMarks / totalStudent).toFixed(2)}</th>
                                <th class="text-center">Percentage</th>
                                <th class="text-center" colspan="${cateLength - 1}">${percentage}</th>
                            </tr>`;
                    html += `</tbody></table>`;
                }

                averagePercentage = averagePercentage * 100 / (totalCategoryParameters * 5);

                html += `
                    <table style="margin-bottom: 20px;">
                        <tr style="background-color:black; color: white; font-size:20px;">
                            <th class="text-center">Average Percentage</th>
                            <th class="text-center">${parseFloat(averagePercentage).toFixed(2)}</th>
                        </tr>
                    </table>
                `;

                html += `</body>`;

                htmlPdf.create(html, options).toStream(async (err, stream) => {
                    if (err) {
                        return res.send(await commonResponse.response(false, "Something went wrong with PDF creation."));
                    } else {
                        res.set('Content-type', 'application/pdf');
                        stream.pipe(res);
                    }
                });
            } else {
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

/**
 * @description View feedback result
 * @param  {} req
 * @param  {} res
 */
module.exports.viewFacultyFeedback = async (req, res) => {
    try {
        await models.submittedFeedbackForm.sync({ force: false });

        let { staffId, feedbackFormId } = req.body;

        let submittedfeedbackForm = await models.submittedFeedbackForm.findOne({
            attributes: ['feedbackFormId', 'staffId', 'subjectId', 'batchId'],
            where: {
                staffId, feedbackFormId
            },
            include: [
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.batch,
                    attributes: ['batchName']
                }
            ],
            group: ['submittedFeedbackForm.feedbackFormId', 'submittedFeedbackForm.staffId', 'subjectId', 'batchId', 'subject.id', 'batch.id'],
        });

        if (submittedfeedbackForm) {
            submittedfeedbackForm = JSON.parse(JSON.stringify(submittedfeedbackForm));

            // No. of Student who gave feedback
            let submittedFeedbacks = await models.submittedFeedbackForm.findAll({
                attributes: ['id'],
                where: {
                    feedbackFormId,
                    subjectId: submittedfeedbackForm.subjectId,
                    batchId: submittedfeedbackForm.batchId,
                    staffId,
                    status: true
                }
            });

            let categoryPercentages = [];
            let submittedFeedbackFormIds = submittedFeedbacks.map(data => data.id);
            let totalStudents = submittedFeedbacks.length;

            let totalOfBenchmarks = await models.submittedFeedbackResponse.findAll({
                attributes: [[sequelize.fn('SUM', sequelize.col('feedbackTemplateBenchmark.points')), 'totalFeedbacks']],
                where: {
                    submittedFeedbackFormId: submittedFeedbackFormIds
                },
                include: [
                    {
                        model: models.feedbackTemplateBenchmark,
                        attributes: ["id", "points"]
                    },
                    {
                        model: models.feedbackTemplateCategory,
                        attributes: ["id", "categoryName"]
                    },
                    {
                        model: models.submittedFeedbackForm,
                        attributes: ["id"],
                        include: [
                            {
                                model: models.feedbackForm,
                                attributes: ["feedbackTemplateId"]
                            }
                        ]
                    }
                ],
                group: ['categoryId', 'benchmarkId', 'feedbackTemplateBenchmark.id', 'feedbackTemplateCategory.id', 'submittedFeedbackForm.id', 'submittedFeedbackForm->feedbackForm.id']
            });

            totalOfBenchmarks = JSON.parse(JSON.stringify(totalOfBenchmarks));

            for (var j = 0; j < totalOfBenchmarks.length; j++) {
                let catElement = totalOfBenchmarks[j];

                let categoryName = catElement.feedbackTemplateCategory.categoryName;
                let categoryId = catElement.feedbackTemplateCategory.id;
                let secureMarks = parseInt(catElement.totalFeedbacks) * parseInt(catElement.feedbackTemplateBenchmark.points);

                let findCategory = categoryPercentages.find((element) => element.categoryName == categoryName);

                if (findCategory && findCategory != undefined) {
                    let key = categoryPercentages.findIndex((element) => element.categoryName == categoryName);
                    key = Math.abs(key);

                    categoryPercentages[key].secureMarks += secureMarks;
                } else {
                    categoryPercentages.push({
                        categoryId,
                        categoryName,
                        secureMarks,
                        feedbackTemplateId: catElement.submittedFeedbackForm.feedbackForm.feedbackTemplateId
                    });
                }
            }

            let chartData = [];

            for (var i = 0; i < categoryPercentages.length; i++) {
                let element = categoryPercentages[i];

                let templateDetail = await models.feedbackTemplate.findAll({
                    attributes: ['id'],
                    where: {
                        id: element.feedbackTemplateId,
                    },
                    include: [{
                        model: models.feedbackTemplateCategory,
                        where: {
                            id: element.categoryId
                        },
                        attributes: ['id'],
                        include: [
                            {
                                model: models.feedbackTemplateBenchmark,
                                attributes: ['points']
                            },
                            {
                                model: models.feedbackTemplateCategoryParameter,
                                attributes: ['id']
                            }
                        ]
                    }]
                });

                templateDetail = JSON.parse(JSON.stringify(templateDetail));

                let benchmarkPoints = templateDetail.map(data => {
                    let element = data.feedbackTemplateCategories[0];
                    let parameters = element.feedbackTemplateCategoryParameters.length;
                    let points = element.feedbackTemplateBenchmarks.map(data => data.points);
                    return {
                        maxPoints: Math.max(...points),
                        totalParams: parameters
                    };
                });

                let maxPoints = parseInt(benchmarkPoints[0].maxPoints);
                let totalParameters = parseInt(benchmarkPoints[0].totalParams);

                let maximumMarks = (totalStudents * maxPoints * totalParameters);
                let securedMarks = parseFloat(element.secureMarks);

                let categoryPercentage = maximumMarks * 100 / securedMarks;

                chartData.push({
                    categoryName: element.categoryName,
                    percentage: parseFloat(parseFloat(categoryPercentage).toFixed(2))
                })
            }

            submittedfeedbackForm.chartResult = chartData;

            return res.json(await commonResponse.response(true, Message.DATA_FOUND, submittedfeedbackForm));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};