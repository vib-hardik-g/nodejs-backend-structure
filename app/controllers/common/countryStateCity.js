const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.getCountries = async (req, res, next) => {
    try{
        await models.country.sync({ force: false });
        await models.state.sync({ force: false });
        await models.city.sync({ force: false });

        let response = await models.country.findAll({
            where: { status: true},
            attributes: ["id", "countryName"],
            include: [{
                model: models.state,
                attributes: ["id", "stateName"],
                include: [{
                    model: models.city,
                    attributes: ["id", "cityName"]
                }]
            }]
        });
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    }catch(err){
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}