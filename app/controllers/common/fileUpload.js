const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const errorHandle = require('../../../utils/errorHandler');
const { Message } = require('../../../utils/commonMessages');
const { deleteFile } = require('../../../utils/awsS3Handler');

const mimeTypeToExtension = {
    'image/jpeg': 'jpg',
    'image/png': 'png',
    'image/gif': 'gif',
    'application/pdf': 'pdf',
    'application/vnd.ms-excel': 'xls',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
    'application/zip': 'zip',
    'application/gzip': 'gz',
    'application/msword': 'doc',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx'
};

module.exports.uploadFile = async (req, res) => {
    try {
        await models.document.sync({ force: false });
        let { type } = req.body;

        if (req.files && req.files.file && req.files.file.length > 0) {
            let documentIds = [];
            for (let i = 0; i < req.files.file.length; i++) {
                let s3Data = req.files.file[i];
                let create = await models.document.create({
                    type,
                    etag: s3Data.etag.substring(1, s3Data.etag.length - 1),
                    fileName: s3Data.key.split('/').pop(),
                    filePath: s3Data.key,
                    fullPath: s3Data.location,
                    mimeType: s3Data.mimetype,
                    size: s3Data.size,
                    originalFileName: s3Data.originalname.split('.').shift().toString().replace(/[_\-&\/\\#,+()$~%.'":*?<>{}]/g, ' ').replace(/\s\s+/g, ' '),
                    fileExtension: mimeTypeToExtension[s3Data.mimetype],
                });
                documentIds.push(create.id);
            }
            return res.send(await commonResponse.response(true, Message.FILE_UPLOADED, {
                documentIds: documentIds
            }));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.FILE_REQUIRED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteFile = async (req, res) => {
    try {
        let { id } = req.params;
        if (!id) return res.status(400).send(await commonResponse.response(false, Message.BAD_REQUEST));

        await deleteFile(id);
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        console.log(err);
        return res.status(500).send(await commonResponse.response(false, Message.SOMETHING_WRONG, err));
    }
}