const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');
const { administratorEmailOTP } = require('../../mailTemplates');

/**
 * @description get all students
 * @param  {} req
 * @param  {} res
 */
module.exports.allstudents = async (req, res) => {
    try {

        await models.user.sync({ force: false });
        await models.userType.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.document.sync({ force: false });
        await models.institution.sync({ force: false });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        let instituteWhere = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            };
        } else {
            let stringCondition = {
                '$student.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$student.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$student.primaryPhoneNumber$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$student.gender$': { [Op.iLike]: '%' + req.query.searchKey + '%' }
            };
            let condition;
            condition = stringCondition;
            where = {
                status: true,
                [Op.or]: condition
            }
        }

        if (req.query.gender) {
            where.gender = req.query.gender;
        }

        if (req.query.instituteId) {
            instituteWhere.instituteId = req.query.instituteId;
        }

        let response = await models.student.findAndCountAll({
            distinct: true,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            subQuery: false,
            attributes: ['id', 'firstName', 'lastName', 'primaryPhoneNumber', 'gender', 'createdAt'],
            include: [{
                model: models.userStudentMapping,
                attributes: ['id', 'studentId', 'userId', 'instituteId'],
                where: instituteWhere,
                require:true,
                include: [
                    {
                        model: models.user,
                        attributes: ['id','email']
                    },
                    {
                        model: models.institution,
                        attributes: ['id','institutionName'],
                    }
                ]
            }]
        });

        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description get all staffs
 * @param  {} req
 * @param  {} res
 */
module.exports.allstaffs = async (req, res) => {
    try {

        await models.user.sync({ force: false });
        await models.userType.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.document.sync({ force: false });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        let instituteWhere = {};
        let userWhere = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            };
        } else {
            let stringCondition = {
                '$staff.firstName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$staff.lastName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$staff.contactNo$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$staff.gender$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$staff.designation$': { [Op.iLike]: '%' + req.query.searchKey + '%' }
            };
            let condition;
            condition = stringCondition;
            where = {
                status: true,
                [Op.or]: condition
            }
        }

        if (req.query.gender) {
            where.gender = req.query.gender;
        }

        if (req.query.instituteId) {
            instituteWhere.instituteId = req.query.instituteId;
        }

        let userType = await models.userType.findOne({
            where: { userType: req.query.role }
        });

        if(req.query.role && userType){
            userWhere.userTypeId = userType.id.toString();
        }

        let response = await models.staff.findAndCountAll({
            distinct: true,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            subQuery: false,
            attributes: ['id', 'firstName', 'lastName', 'contactNo', 'gender', 'designation', 'createdAt'],
            include: [{
                model: models.userStaffMapping,
                attributes: ['id', 'staffId', 'userId', 'instituteId'],
                where: instituteWhere,
                require:true,
                include: [
                    {
                        model: models.user,
                        attributes: ['id','email','userTypeId'],
                        require:true,
                        where:userWhere,
                    },
                    {
                        model: models.institution,
                        attributes: ['id','institutionName'],
                    }
                ]
            }]
        });

        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


