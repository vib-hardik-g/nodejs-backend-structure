const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');

/**
 * @description get all student capacity request 
 * @param  {} req
 * @param  {} res
 */
module.exports.allRequestStudentCapacity = async (req, res) => {
    try {
        await models.studentCapacity.sync({ force: false });
        await models.institution.sync({ force: false });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        if (req.query.searchKey) {
            let stringCondition = {
                '$institution.institutionName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
            };
            let condition;
            condition = stringCondition;
            where = {
                [Op.or]: condition
            }
        }

        if (req.query.startDate && req.query.endDate) {
            where.createdAt = {
                [Op.gte]: req.query.startDate,
                [Op.lte]: req.query.endDate
            }
        }

        let response = await models.studentCapacity.findAndCountAll({
            distinct: true,
            subQuery: false,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            include: [{
                model: models.institution,
                attributes: ['institutionName', 'studentLimit']
            }],
            attributes: ['id', 'instituteId', 'studentLimit', 'requestStatus', 'createdAt', 'updatedAt']
        });

        let result = {
            rows: response.rows,
            total: response.count
        }

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description change student capacity request status
 * @param  {} req
 * @param  {} res
 */
module.exports.updateRequestStatus = async (req, res) => {
    try {
        await models.studentCapacity.sync({ force: false });
        await models.institution.sync({ force: false });

        let body = req.body;

        let response = await models.studentCapacity.update({
            requestStatus: body.requestStatus,
        }, {
            where: {
                id: body.id
            }
        });
        if (response) {
            if (body.requestStatus == Generalstatus.APPROVED) {
                let institution = await models.institution.findOne({ where: { id: body.instituteId } });

                if (institution) {
                    let newStudentLimit = institution.studentLimit + body.studentLimit;
                    // Desable user staff mapping
                    await models.institution.update({ studentLimit: newStudentLimit }, { where: { id: body.instituteId } });
                }
            }
        }

        return res.json(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description get student capacity request by id
 * @param  {} req
 * @param  {} res
 */
module.exports.studentCapacityById = async (req, res) => {
    try {
        await models.studentCapacity.sync({ force: false });
        await models.institution.sync({ force: false });

        let { id } = req.params;
        let response = await models.studentCapacity.findOne({
            where: { id },
            include: [{
                model: models.institution,
                attributes: ['institutionName', 'studentLimit']
            }]
        });

        if (response) {
            let result = JSON.parse(JSON.stringify(response));
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
