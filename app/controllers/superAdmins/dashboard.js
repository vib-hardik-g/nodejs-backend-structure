const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');
const { administratorEmailOTP } = require('../../mailTemplates');

/**
 * @description get counts fro dashboard
 * @param  {} req
 * @param  {} res
 */
module.exports.dashboardCounts = async (req, res) => {
    try {

        await models.user.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.student.sync({ force: false });

        let administratorCount = await models.user.count({
            distinct: true,
            where: { status: true, userTypeId: '1' },
        });

        let institutionCount = await models.institution.count({
            distinct: true,
            where: { status: true },
        });

        let studentCount = await models.student.count({
            distinct: true,
            where: { status: true },
        });

        let staffCount = await models.staff.count({
            distinct: true,
            where: { status: true },
        });

        let result = {
            administratorCount: administratorCount,
            institutionCount: institutionCount,
            studentCount: studentCount,
            staffCount: staffCount
        };
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description get student capacity request list epends on status
 * @param  {} req
 * @param  {} res
 */
module.exports.studentRequestCapacityList = async (req, res) => {
    try {

        await models.studentCapacity.sync({ force: false });
        await models.institution.sync({ force: false });

        let where = {};
        if(req.query.requestStatus){
            where.requestStatus = req.query.requestStatus;
        }
       

        let response = await models.studentCapacity.findAll({
            distinct: true,
            where: where,
            include: [{
                model: models.institution,
                attributes: ['institutionName', 'studentLimit']
            }],
            attributes: ['id', 'instituteId', 'studentLimit', 'requestStatus', 'createdAt', 'updatedAt']
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


