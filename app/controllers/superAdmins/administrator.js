const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Generalstatus, Attendance, Publishstatus, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const moment = require('moment');
const awsS3Handler = require('../../../utils/awsS3Handler');
const _ = require('lodash');
const { administratorEmailOTP } = require('../../mailTemplates');

/**
 * @description get all administrator
 * @param  {} req
 * @param  {} res
 */
module.exports.allAdministrators = async (req, res) => {
    try {

        await models.user.sync({ force: false });
        await models.userType.sync({ force: false });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
            };
        } else {
            let stringCondition = {
                '$user.userName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$user.email$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                '$user.designation$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
            };
            let condition;
            condition = stringCondition;
            where = {
                status: true,
                [Op.or]: condition
            }
        }

        let userTypeOfStaff = await models.userType.findOne({
            where: { userType: Usertypes.SUPER_ADMIN }
        });

        if (!userTypeOfStaff) {
            return res.send(await commonResponse.response(false, Message.USER_TYPE_NOT_FOUND));
        }

        where.userTypeId = userTypeOfStaff.id.toString();
        let response = await models.user.findAndCountAll({
            distinct: true,
            subQuery: false,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            attributes: ['id', 'userTypeId', 'email', 'userName', 'designation', 'createdAt']
        });

        let result = {
            rows: response.rows,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description add administrator
 * @param  {} req
 * @param  {} res
 */
module.exports.addAdministrator = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.userType.sync({ force: false });

        let body = req.body;

        // Check Unique User Email
        let checkUser = await exports.checkUniqueEmail(body.email);

        if (checkUser > 0) {
            return res.send(await commonResponse.response(false, Message.EMAIL_ALREADY_EXISTS));
        }

        let userTypeOfStaff = await models.userType.findOne({
            where: { userType: Usertypes.SUPER_ADMIN }
        });

        if (!userTypeOfStaff) {
            return res.send(await commonResponse.response(false, Message.USER_TYPE_NOT_FOUND));
        }

        let user = await models.user.create({
            userTypeId: userTypeOfStaff.id.toString(),
            email: body.email,
            userName: body.administratorName,
            designation: body.designation
        });

        if (user) {
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description get administrator by id
 * @param  {} req
 * @param  {} res
 */
module.exports.administratorById = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let { id } = req.params;
        let response = await models.user.findOne({
            where: { id }
        });

        if (response) {
            let result = JSON.parse(JSON.stringify(response));
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description update administrator
 * @param  {} req
 * @param  {} res
 */
module.exports.updateAdministrator = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let body = req.body;

        // Update Student Email
        let user = await models.user.update({
            userName: body.administratorName,
            designation: body.designation
        }, {
            where: {
                id: body.id
            }
        });

        if (user) {
            return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_UPDATED));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description delete administrator
 * @param  {} req
 * @param  {} res
 */
module.exports.deleteAdministrator = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let { id } = req.params;

        let authData = req.authData.data;

        let user = await models.user.findOne({ where: { id: id } });

        if (user.id != authData.id) {

            //Disable user
            await models.user.update({ status: false }, { where: { id: id } });

            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        } else {
            return res.send(await commonResponse.response(false, Message.LOGIN_USER_DELETED_FAILED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}


/**
 * @description Sending OTP to existing Email
 * @param  {} req
 * @param  {} res
 */
module.exports.otpSendToExistingEmail = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let body = req.body;
        let email = body.email;
        let userName = body.administratorName;
        let otp = Math.floor(100000 + Math.random() * 900000);

        await models.user.update({
            otp: otp
        }, {
            where: {
                id: body.id
            }
        });

        let isSent = await administratorEmailOTP(email, userName, otp);
        if (isSent) {
            return res.send(await commonResponse.response(true, Message.ADMINISTRATOR_EMAIL_OTP_SENT));
        } else {
            return res.send(await commonResponse.response(false, Message.OTP_NOT_SENT));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description verifing OTP to existing Email
 * @param  {} req
 * @param  {} res
 */
module.exports.otpVerifyToExistingEmail = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let body = req.body;

        let user = await models.user.findOne({
            where: {
                id: body.id,
                otp: body.otp
            },
        });
        if (user) {
            return res.json(await commonResponse.response(true, Message.OTP_VERIFY));
        } else {
            return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
        }
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG))
    }
}

/**
 * @description Sending OTP to new Email
 * @param  {} req
 * @param  {} res
 */
module.exports.otpSendToNewEmail = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let body = req.body;
        let email = body.email;
        let userName = body.administratorName;
        let otp = Math.floor(100000 + Math.random() * 900000);

        await models.user.update({
            otp: otp
        }, {
            where: {
                id: body.id
            }
        });

        let isSent = await administratorEmailOTP(email, userName, otp);
        if (isSent) {
            return res.send(await commonResponse.response(true, Message.ADMINISTRATOR_EMAIL_OTP_SENT));
        } else {
            return res.send(await commonResponse.response(false, Message.OTP_NOT_SENT));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

/**
 * @description verifing OTP to new Email and updating new email 
 * @param  {} req
 * @param  {} res
 */
module.exports.updateAdministratorEmail = async (req, res) => {
    try {
        await models.user.sync({ force: false });

        let body = req.body;

        let find = await models.user.findOne({
            where: {
                id: body.id,
                otp: body.otp
            },
        });
        if (find) {

            // Check Unique User Email
            let checkUser = await exports.checkUniqueEmail(body.email);

            if (checkUser > 0) {
                return res.send(await commonResponse.response(false, Message.EMAIL_ALREADY_EXISTS));
            }

            let user = await models.user.update({
                email: body.email
            }, {
                where: {
                    id: body.id
                }
            });
            if (user) {
                return res.json(await commonResponse.response(true, Message.ADMINISTRATOR_EMAIL_UPDATED));
            } else {
                return res.status(401).json(await commonResponse.response(false, Message.ADMINISTRATOR_EMAIL_NOT_UPDATED));
            }
        } else {
            return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
        }
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG))
    }
}


/**
 * @description Check unique email
 * @param  {} parentTypeName
 */
module.exports.checkUniqueEmail = async (emailId) => {
    await models.user.sync({ force: false });
    return await models.user.count({ where: { email: emailId, status: true } });
}