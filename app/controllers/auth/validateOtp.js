const errorHandle = require('../../../utils/errorHandler');
const { validateToken, verifyToken } = require('../../../utils/jwt');
const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Validation, Constant } = require('../../../utils/commonMessages');
const sequelize = models.sequelize;

module.exports = async (req, res) => {
    try {
        await models.userForgotPassword.sync({ force: false });
        let { otp, token } = req.body;
        if (otp && otp.toString().length < Constant.LENGTH) {
            return res.json(await commonResponse.response(false, Validation.OTP));
        }
        verifyToken(token, async (err, tokenData) => {
            if (err) {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
            }
            let account = await models.userForgotPassword.findOne({
                where: {
                    id: tokenData.data.id,
                    status: true
                },
            });
            if (!account || (account.otp != otp)) {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
            } else if (account.otp == otp) {
                const jwtToken = generateToken({ userId: account.userId });
                return res.json(await commonResponse.response(true, Message.OTP_VERIFY, { token: jwtToken }));
            } else {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
            }
        })
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG))
    }
}