const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const models = require('../../models');

module.exports = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.loginSession.sync({ force: false });

        await models.loginSession.update({
            destroyedOn: new Date()
        }, {
            where: {
                token: req.token
            }
        });

        return res.send(await commonResponse.response(true, Message.LOGOUT_SUCCESS));
        
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}