const commonResponse = require('../../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../../utils/commonMessages');
const { setPassword } = require('../../../mailTemplates');
const models = require('../../../models');
const { Op } = require("sequelize");
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const JWT_SECRET_TOKEN = process.env.JWT_SECRET_KEY;

module.exports = async (req, res) => {
    try {
        await models.userForgotPassword.sync({ force: false });
        await models.user.sync({ force: false });

        let { email } = req.body;
        let account = await models.user.findOne({
            where: { email, status: true }
        });
        if (!account) {
            return res.status(401).json(await commonResponse.response(false, Message.INVALID_MAIL));
        } else {
            let userTypeIdsArr = account.userTypeId.split(',');
            let getUserTypes = await models.userType.findAll({
                attributes: ['userType'],
                where: {
                    id: { [Op.in]: userTypeIdsArr }
                }
            });
            let userTypesArr = [];
            getUserTypes.forEach(userType => {
                userTypesArr.push(userType.userType);
            });
            if (_.includes(userTypesArr, Usertypes.SUPER_ADMIN) || _.includes(userTypesArr, Usertypes.ADMIN)) {
                // send email with set password link
                let token = jwt.sign({ userId: account.id }, JWT_SECRET_TOKEN, {
                    expiresIn: '15m'
                });
                let mailSent = await setPassword(account.email, token);
                return res.json(await commonResponse.response(true, Message.RECOVERY_EMAIL_SENT));
            } else {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_EMAIL));
            }
        }
    } catch (err) {
        console.error(err);
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}