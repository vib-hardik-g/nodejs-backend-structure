const errorHandle = require('../../../../utils/errorHandler');
const models = require('../../../models');
const bcrypt = require("bcrypt")
const commonResponse = require('../../../../utils/commonResponse');
const { Message } = require('../../../../utils/commonMessages');
const { verifyToken } = require('../../../../utils/jwt');

module.exports = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        let { password , token } = req.body;

        verifyToken(token, async (err, tokenData) => {
            if(err) {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_TOKEN));
            }

            let hashedPassword = await bcrypt.hash(password, 10);
            let account = await models.user.findOne({
                where: {
                    id: tokenData.userId,
                    status: true
                },
            });
            if (account) {
                await account.update({
                    password: hashedPassword
                });
                return res.json(await commonResponse.response(true, Message.PASSWORD_VERIFIED));
            } else {
                return res.status(404).json(await commonResponse.response(false, Message.NOT_EXISTS));
            }
        })
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}