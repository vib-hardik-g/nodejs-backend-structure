const bcrypt = require('bcryptjs');
const commonResponse = require('../../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../../utils/commonMessages');
const models = require('../../../models');
const { generateToken } = require('../../../../utils/jwt');
const { Op } = require("sequelize");
const _ = require('lodash');
const sequelize = models.sequelize;

module.exports = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.loginSession.sync({ force: false });

        const userAgent = req.useragent;
        let { email, password } = req.body;
        let account = await models.user.findOne({
            where: { email },
        });

        let validationResponse = {
            email: false,
            password: false
        };

        if (account) {
            if (account.status == false) {
                return res.status(401).json(await commonResponse.authenticationResponse(false, Message.IN_ACTIVATE, validationResponse));
            } else if (account.password == null) {
                validationResponse.email = true;
                return res.status(401).json(await commonResponse.authenticationResponse(false, Message.SET_PASS, validationResponse));
            } else {
                bcrypt.compare(password, account.password, async function (err, resultPassword) {
                    if (err) {
                        validationResponse.email = true;
                        validationResponse.password = false;
                        return res.status(401).json(await commonResponse.authenticationResponse(false, Message.INVALID_CRED, validationResponse));
                    } else if (resultPassword) {
                        let userTypeIdsArr = account.userTypeId.split(',');
                        let getUserTypes = await models.userType.findAll({
                            attributes: ['userType'],
                            where: {
                                id: { [Op.in]: userTypeIdsArr }
                            }
                        });

                        let userTypesArr = [];
                        getUserTypes.forEach(userType => {
                            userTypesArr.push(userType.userType);
                        });
                        if (_.includes(userTypesArr, Usertypes.SUPER_ADMIN) || _.includes(userTypesArr, Usertypes.ADMIN) || _.includes(userTypesArr, Usertypes.STAFF)) {
                            if (_.includes(userTypesArr, Usertypes.ADMIN)) {
                                // get instituteId of the admin
                                let detailsFromMapping = await models.userStaffMapping.findOne({
                                    where: { userId: account.id }
                                });
                                if (!detailsFromMapping) {
                                    validationResponse.email = true;
                                    validationResponse.password = true;
                                    return res.status(401).json(await commonResponse.authenticationResponse(false, Message.UNAUTHORISED, validationResponse));
                                } else {
                                    const jwtToken = generateToken({ id: account.id, userTypeId: account.userTypeId, userTypes: userTypesArr.join(','), instituteId: detailsFromMapping.instituteId, staffId: detailsFromMapping.staffId });

                                    validationResponse.email = true;
                                    validationResponse.password = true;

                                    /**
                                     * Collecting Login Session
                                     */
                                    await models.loginSession.create({
                                        userId: account.id,
                                        token: jwtToken,
                                        isMobile: userAgent.isMobile || userAgent.isMobileNative || userAgent.isiPhone || userAgent.isiPhoneNative || userAgent.isAndroid || userAgent.isAndroidNative,
                                        isDesktop: userAgent.isDesktop,
                                        browser: userAgent.browser,
                                        version: userAgent.version,
                                        os: userAgent.os,
                                        platform: userAgent.platform,
                                        source: userAgent.source,
                                    });

                                    return res.send(await commonResponse.authenticationResponse(true, Message.LOGIN_SUCCESS,
                                        validationResponse,
                                        {
                                            accessToken: jwtToken,
                                            userName: account.userName ? account.userName : "",
                                        }
                                    ));
                                }
                            } else if (_.includes(userTypesArr, Usertypes.STAFF)) {
                                let detailsFromMapping = await models.userStaffMapping.findOne({
                                    where: { userId: account.id }
                                });
                                if (!detailsFromMapping) {
                                    validationResponse.email = true;
                                    validationResponse.password = true;
                                    return res.status(401).json(await commonResponse.authenticationResponse(false, Message.UNAUTHORISED, validationResponse));
                                } else {
                                    const jwtToken = generateToken({ id: account.id, userTypeId: account.userTypeId, userTypes: userTypesArr.join(','), instituteId: detailsFromMapping.instituteId, staffId: detailsFromMapping.staffId });

                                    validationResponse.email = true;
                                    validationResponse.password = true;

                                    /**
                                     * Collecting Login Session
                                     */
                                    await models.loginSession.create({
                                        userId: account.id,
                                        token: jwtToken,
                                        isMobile: userAgent.isMobile || userAgent.isMobileNative || userAgent.isiPhone || userAgent.isiPhoneNative || userAgent.isAndroid || userAgent.isAndroidNative,
                                        isDesktop: userAgent.isDesktop,
                                        browser: userAgent.browser,
                                        version: userAgent.version,
                                        os: userAgent.os,
                                        platform: userAgent.platform,
                                        source: userAgent.source,
                                    });
                                    let permission = await models.userModuleMapping.findAll({
                                        attributes: ['id', 'moduleId', 'permission', [sequelize.col('module.moduleName'), 'moduleName'],],
                                        where: {
                                            userId: account.id
                                        },
                                        include: [{
                                            model: models.module,
                                            attributes: [],
                                            required: true,
                                        }]
                                    })

                                    return res.send(await commonResponse.authenticationResponse(true, Message.LOGIN_SUCCESS,
                                        validationResponse,

                                        {
                                            accessToken: jwtToken,
                                            userName: account.userName ? account.userName : "",
                                            permission: permission
                                        }
                                    ));
                                }
                            } else {
                                const jwtToken = generateToken({ id: account.id, userTypeId: account.userTypeId, userTypes: userTypesArr.join(',') });

                                validationResponse.email = true;
                                validationResponse.password = true;

                                return res.send(await commonResponse.authenticationResponse(true, Message.LOGIN_SUCCESS,
                                    validationResponse,
                                    {
                                        accessToken: jwtToken,
                                        userName: account.userName ? account.userName : "",
                                    })
                                );
                            }
                        } else {
                            validationResponse.email = false;
                            validationResponse.password = false;
                            return res.status(401).json(await commonResponse.authenticationResponse(false, Message.INVALID_CRED, validationResponse));
                        }
                    } else {
                        validationResponse.email = true;
                        validationResponse.password = false;
                        return res.status(401).json(await commonResponse.authenticationResponse(false, Message.INVALID_PASSWORD, validationResponse));
                    }
                });
            }
        } else {
            validationResponse.email = false;
            validationResponse.password = false;
            return res.status(401).json(await commonResponse.authenticationResponse(false, Message.INVALID_EMAIL, validationResponse));
        }
    } catch (err) {
        return res.status(500).json(await commonResponse.authenticationResponse(false, Message.SOMETHING_WRONG));
    }
}