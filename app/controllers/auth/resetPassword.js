const errorHandle = require('../../../utils/errorHandler');
const models = require('../../models');
const bcrypt = require("bcrypt")
const commonResponse = require('../../../utils/commonResponse');
const { Message, Validation ,Constant } = require('../../../utils/commonMessages');
const { validateToken, verifyToken } = require('../../../utils/jwt');
const sequelize = models.sequelize;

module.exports = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        let { password, token } = req.body;
        if (password && password.toString().length < Constant.LENGTH) {
            return res.json(await commonResponse.response(false, Validation.PASSWORD));
        }

        verifyToken(token, async (err, tokenData) => {
            if (err) {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_OTP));
            }

            let hashedPassword = await bcrypt.hash(password, 10);
            let account = await models.user.findOne({
                where: {
                    id: tokenData.data.userId,
                    status: true
                },
            });
            if (account) {
                await account.update({
                    password: hashedPassword
                });
                await models.userForgotPassword.destroy({
                    where: {
                        userId: tokenData.data.userId
                    }
                })
                return res.json(await commonResponse.response(true, Message.PASSWORD_VERIFIED))
            } else {
                return res.status(404).json(await commonResponse.response(false, Message.NOT_EXISTS))
            }
        })
    } catch (err) {
        console.log(err);
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG))
    }
}