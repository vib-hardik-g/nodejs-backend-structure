const bcrypt = require('bcryptjs');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const models = require('../../models');
const { generateToken } = require('../../../utils/jwt');
const { Op } = require("sequelize");
const _ = require('lodash');
const studentLeave = require('../../models/studentLeave');

module.exports = async (req, res) => {
    try {
        let { email, password, loginAs } = req.body;
        let hashedPassword = await bcrypt.hash(password, 10)
        let account = await models.user.findOne({
            where: { email }
        });

        if (account) {

            if (account && (account.password == null || account.password == "")) {
                let userTypeIdsArr = account.userTypeId.split(',');
                let getUserTypes = await models.userType.findAll({
                    attributes: ['userType'],
                    where: {
                        id: { [Op.in]: userTypeIdsArr }
                    }
                });
                let userTypesArr = [];
                getUserTypes.forEach(userType => {
                    userTypesArr.push(userType.userType);
                });


                if (loginAs == Usertypes.STUDENT && (_.includes(userTypesArr, Usertypes.STUDENT))) {
                    await account.update({
                        password: hashedPassword
                    });
                    let studentData = await models.userStudentMapping.findOne(
                        {
                            where: {
                                userId: account.id
                            }
                        })
                    await models.student.update({
                        loginStatus: 'Signed Up'
                    }, {
                        where: {
                            id: studentData.studentId
                        }
                    })
                    return res.json(await commonResponse.response(true, Message.PASSWORD_VERIFIED));
                } else if ((_.includes(userTypesArr, Usertypes.STAFF)) && loginAs == Usertypes.STAFF) {
                    await account.update({
                        password: hashedPassword
                    });
                    let studentData = await models.userStaffMapping.findOne(
                        {
                            where: {
                                userId: account.id
                            }
                        })
                    await models.staff.update({
                        loginStatus: 'Signed Up'
                    }, {
                        where: {
                            id: studentData.staffId
                        }
                    })
                    return res.json(await commonResponse.response(true, Message.PASSWORD_VERIFIED));
                } else {
                    return res.status(401).json(await commonResponse.response(false, Message.INVALID_MAIL));
                }
            } else {
                return res.status(401).json(await commonResponse.response(false, Message.PASSWORD_ALREADY_SET));
            }

        } else {
            return res.status(401).json(await commonResponse.response(false, Message.INVALID_MAIL));
        }
    } catch (err) {
console.log(err);
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}