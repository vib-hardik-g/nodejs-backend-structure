const { errorHandle } = require('../../../utils/errorHandler');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const models = require('../../models');

module.exports = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        console.log(req.authData);
        const { data: { id } } = req.authData;
        let account = await models.user.findOne({
            where: { id }
        });
        if (account) {
            if (account && account.status === false) {
                res.status(401).json(await commonResponse.response(false, Message.IN_ACTIVATE ));
            } else{
                res.send(await commonResponse.response(true, Message.VALID_USER));
            }
        } else {
            res.status(401).json(await commonResponse.response(false, Message.INVALID_TOKEN));
        }
    } catch (err) {
        res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}