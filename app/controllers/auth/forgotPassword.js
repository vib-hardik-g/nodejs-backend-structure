const bcrypt = require('bcryptjs');
const errorHandle = require('../../../utils/errorHandler');
const commonResponse = require('../../../utils/commonResponse');
const {Message} = require('../../../utils/commonMessages');
const { generateToken } = require('../../../utils/jwt');
const { forgotPassword } = require('../../mailTemplates');
const models = require('../../models');

module.exports = async (req, res) => {
    try {
        await models.userForgotPassword.sync({ force: false });
        await models.user.sync({ force: false });

        let { email } = req.body;
        let otp = Math.floor(100000 + Math.random() * 900000)
        let account = await models.user.findOne({
            where: { email, status: true }
        });
        if (!account) {
            return res.status(401).json(await commonResponse.response(false, Message.INVALID_MAIL));
        } else {
            let mailSent = await forgotPassword(email, otp);
            if (mailSent) {
                let otpId = await models.userForgotPassword.create({
                    userId: account.id,
                    type: 1,
                    otp,
                });
                const jwtToken = generateToken({ id: otpId.id });
                return res.json(await commonResponse.response(true, Message.OTP_SENT, { token: jwtToken }));
            } else {
                return res.status(500).json(await commonResponse.response(false, Message.OTP_NOT_SENT));
            }
        }
    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}