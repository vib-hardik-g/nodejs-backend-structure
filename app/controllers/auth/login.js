const bcrypt = require('bcryptjs');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const models = require('../../models');
const { generateToken } = require('../../../utils/jwt');
const { Op } = require("sequelize");
const _ = require('lodash');

module.exports = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        await models.parentType.sync({ force: false });
        await models.user.sync({ force: false });
        await models.parent.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.student.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.device.sync({ force: false });
        await models.loginSession.sync({ force: false });

        const userAgent = req.useragent;
        let { email, password, loginAs, deviceName, deviceMake, deviceModel, deviceToken, platform, systemVersion, deviceId } = req.body;
        let studentRequired = false;
        let staffRequired = false;
        if (loginAs == Usertypes.STUDENT) {
            studentRequired = true;
        } else if (loginAs == Usertypes.STAFF) {
            staffRequired = true;
        }
        let account = await models.user.findOne({
            include: [
                {
                    model: models.userStudentMapping,
                    attributes: ['studentId'],
                    required: studentRequired,
                },
                {
                    model: models.userStaffMapping,
                    attributes: ['staffId'],
                    required: staffRequired,
                }
            ],
            where: { email }
        });
        // 
        if (account) {
            if (account && account.status == false) {
                return res.status(401).json(await commonResponse.response(false, Message.IN_ACTIVATE));
            } else if (account && account.password == null) {
                return res.status(401).json(await commonResponse.response(false, Message.SET_PASS));
            } else {
                bcrypt.compare(password, account.password, async function (err, resultPassword) {
                    if (err) {

                        return res.status(401).json(await commonResponse.response(false, Message.INVALID_CRED));
                    } else if (resultPassword) {
                        await models.device.create({
                            userId: account.id,
                            deviceName,
                            deviceMake,
                            deviceModel,
                            deviceToken,
                            platform,
                            systemVersion,
                            deviceId
                        });
                        let userTypeIdsArr = account.userTypeId.split(',');
                        let getUserTypes = await models.userType.findAll({
                            attributes: ['userType'],
                            where: {
                                id: { [Op.in]: userTypeIdsArr }
                            }
                        });

                        let userTypesArr = [];
                        getUserTypes.forEach(userType => {
                            userTypesArr.push(userType.userType);
                        });
                        if (loginAs == Usertypes.STUDENT) {
                            if (_.includes(userTypesArr, Usertypes.STUDENT) && loginAs == Usertypes.STUDENT) {
                                //     let student = await models.student.findOne({
                                //         where: {
                                //             id: account.userStudentMappings[0].studentId, status: true
                                //         },
                                //         order: [['id', 'asc']]
                                //     });
                                //     // 
                                //     const jwtToken = generateToken({ id: account.id, userTypeId: account.userTypeId, userTypes: loginAs });
                                //     return res.send(await commonResponse.response(true, Message.LOGIN_SUCCESS, {

                                //         accessToken: jwtToken,
                                //         firstName: student.firstName ? student.firstName : "",
                                //         lastName: student.lastName ? student.lastName : "",
                                //         middleName: student.middleName ? student.middleName : "",
                                //         profilePic: student.profilePic ? student.profilePic : "",
                                //         isParent:false,
                                //         students: []
                                //     }));
                                // } else if (_.includes(userTypesArr, Usertypes.PARENT)) {
                                let mappings = account.userStudentMappings.map(val => {
                                    return val.studentId;
                                });

                                let student = await models.student.findAll({
                                    attributes: ['id', 'firstName', 'lastName', 'profilePicture'],
                                    where: {
                                        id: { [Op.in]: mappings }, status: true
                                    },
                                    order: [['id', 'asc']]
                                });
                                let parent = false
                                if (student && student.length > 1) {
                                    parent = true;
                                }
                                
                                // 
                                const jwtToken = generateToken({ id: account.id, studentId: account.userStudentMappings[0].studentId, userTypeId: account.userTypeId, userTypes: loginAs });

                                /**
                                 * Collecting Login Session
                                 */
                                await models.loginSession.create({
                                    userId: account.id,
                                    token: jwtToken,
                                    isMobile: userAgent.isMobile || userAgent.isMobileNative || userAgent.isiPhone || userAgent.isiPhoneNative || userAgent.isAndroid || userAgent.isAndroidNative,
                                    isDesktop: userAgent.isDesktop,
                                    browser: userAgent.browser,
                                    version: userAgent.version,
                                    os: userAgent.os,
                                    platform: userAgent.platform,
                                    source: userAgent.source,
                                });
                                return res.send(await commonResponse.response(true, Message.LOGIN_SUCCESS, {

                                    accessToken: jwtToken,
                                    firstName: student[0].firstName ? student[0].firstName : "",
                                    lastName: student[0].lastName ? student[0].lastName : "",
                                    profilePic: student[0].profilePicture ? student[0].profilePicture : "",
                                    isParent: parent,
                                    students: student.length > 0 ? student : ""
                                }));
                            }
                            else {
                                return res.status(401).json(await commonResponse.response(false, Message.INVALID_CRED));
                            }

                        } else if (_.includes(userTypesArr, Usertypes.STAFF) && loginAs == Usertypes.STAFF) {
                            let staff = await models.staff.findOne({
                                where: { id: parseInt(account.userStaffMappings[0].staffId), status: true },
                                include: [{
                                    model: models.document,
                                }]
                            });
                            const jwtToken = generateToken({ id: account.id, userTypeId: account.userTypeId, userTypes: loginAs });
                            /**
                             * Collecting Login Session
                             */
                            await models.loginSession.create({
                                userId: account.id,
                                token: jwtToken,
                                isMobile: userAgent.isMobile || userAgent.isMobileNative || userAgent.isiPhone || userAgent.isiPhoneNative || userAgent.isAndroid || userAgent.isAndroidNative,
                                isDesktop: userAgent.isDesktop,
                                browser: userAgent.browser,
                                version: userAgent.version,
                                os: userAgent.os,
                                platform: userAgent.platform,
                                source: userAgent.source,
                            });
                            return res.send(await commonResponse.response(true, Message.LOGIN_SUCCESS, {
                                accessToken: jwtToken,
                                firstName: staff.firstName ? staff.firstName : "",
                                lastName: staff.lastName ? staff.lastName : "",
                                profilePic: staff.document ? staff.document.fullPath : "",
                            }));
                        } else {

                            return res.status(401).json(await commonResponse.response(false, Message.INVALID_CRED));
                        }

                    } else {

                        return res.status(401).json(await commonResponse.response(false, Message.INVALID_CRED));
                    }
                });
            }
        } else {

            return res.status(401).json(await commonResponse.response(false, Message.INVALID_CRED));
        }
    } catch (err) {

        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}