const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const { Message, Publishstatus, Usertypes, typeOfQuestions } = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const { Op, Sequelize } = require("sequelize");
const _ = require("lodash");

module.exports.postDoubt = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });
    await models.doubtObjectiveOption.sync({ force: false });
    await models.doubtObjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveFile.sync({ force: false });
    await models.doubtSubjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveResponseAttachment.sync({ force: false });
    await models.userStudentMapping.sync({ force: false });
    await models.userStaffMapping.sync({ force: false });

    let body = req.body;
    let userData = req.authData.data; // userData { id: 47, studentId: 6, userTypeId: '5', userTypes: 'STUDENT' }
    let instituteId = null;
    let studentId = null;
    let staffId = null;

    // if(body.option.length < 1 &&  typeOfQuestions.Objective == body.typeOfQuestion){
    //   return res.send(await commonResponse.response(false, "Please add two or more options."));
    // }

    if (userData.userTypes == Usertypes.STUDENT) {
      let studentMapData = await models.userStudentMapping.findOne({
        where: {
          userId: userData.id,
          status: true,
        }
      });
      if (studentMapData) {
        studentId = studentMapData.studentId;
        instituteId = studentMapData.instituteId;
      }
    } else {
      let staffMapData = await models.userStaffMapping.findOne({
        where: {
          userId: userData.id,
          status: true,
        }
      });
      if (staffMapData) {
        staffId = staffMapData.staffId;
        instituteId = staffMapData.instituteId;
      }
    }

    let response = await models.doubt.create({
      instituteId: instituteId,
      studentId: studentId,
      staffId: staffId,
      subjectId: body.subjectId,
      topicDetailsId: body.topicId,
      typeOfQuestion: body.typeOfQuestion,
      title: body.title,
      question: body.question,
      likeCount: 0,
      commentCount: 0
    });

    if (response) {
      if (typeOfQuestions.Objective == body.typeOfQuestion) {
        for (const row of body.options) {
          await models.doubtObjectiveOption.create({
            doubtId: response.id,
            option: row,
            status: true
          });
        }
      } else {
        for (const row of body.documents) {
          await models.doubtSubjectiveFile.create({
            doubtId: response.id,
            doubtFile: row,
            status: true
          });
        }
      }
    }

    if (response) {
      return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } else {
      return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};

module.exports.postDoubtRespond = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });
    await models.doubtObjectiveOption.sync({ force: false });
    await models.doubtObjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveFile.sync({ force: false });
    await models.doubtSubjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveResponseAttachment.sync({ force: false });

    let body = req.body;
    let userData = req.authData.data;
    let response = {};

    // if(body.optionId == null &&  typeOfQuestions.Objective == body.typeOfQuestion){
    //   return res.send(await commonResponse.response(false, "Please select Option"));
    // }
    // if(!body.respond  &&  typeOfQuestions.Subjective == body.typeOfQuestion){
    //   return res.send(await commonResponse.response(false, "Respond is required"));
    // }

    if (typeOfQuestions.Objective == body.typeOfQuestion) {
      response = await models.doubtObjectiveResponse.create({
        doubtId: body.doubtId,
        studentId: userData.userTypes == Usertypes.STUDENT ? userData.studentId : null,
        staffId: userData.userTypes == Usertypes.STAFF ? userData.staffId : null,
        optionId: body.optionId,
        correct: false,
        status: true
      });
    } else {
      response = await models.doubtSubjectiveResponse.create({
        doubtId: body.doubtId,
        studentId: userData.userTypes == Usertypes.STUDENT ? userData.studentId : null,
        staffId: userData.userTypes == Usertypes.STAFF ? userData.staffId : null,
        response: body.respond,
        responseLikeCount: 0,
        correct: false,
        status: true
      });

      if (response) {
        for (const row of body.documents) {
          await models.doubtSubjectiveResponseAttachment.create({
            doubtSubjectiveResponseId: response.id,
            doubtFile: row,
            status: true
          });
        }
      }
    }

    if (response) {
      return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } else {
      return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};

module.exports.doubtLikeUnlike = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });
    await models.doubtObjectiveOption.sync({ force: false });
    await models.doubtObjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveFile.sync({ force: false });
    await models.doubtSubjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveResponseAttachment.sync({ force: false });

    let body = req.body;

    let doubtData = await models.doubt.findOne({
      where: {
        id: body.doubtId,
        status: true,
      }
    });

    if (doubtData) {
      var likeCount = doubtData.get('likeCount');
      likeCount = body.isLike ? likeCount + 1 : likeCount - 1;
      await models.doubt.update({
        "likeCount": likeCount,
      }, {
        where: {
          id: body.doubtId
        }
      });
    }

    if (body.isLike) {
      return res.send(await commonResponse.response(true, Message.LIKE_DOUBT));
    } else {
      return res.send(await commonResponse.response(false, Message.UNLIKE_DOUBT));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};

module.exports.doubtRespondLikeUnlike = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });
    await models.doubtObjectiveOption.sync({ force: false });
    await models.doubtObjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveFile.sync({ force: false });
    await models.doubtSubjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveResponseAttachment.sync({ force: false });

    let body = req.body;

    let respondData = await models.doubtSubjectiveResponse.findOne({
      where: {
        id: body.respondId,
        doubtId: body.doubtId,
        status: true,
      }
    });

    if (respondData) {
      var likeCount = respondData.get('responseLikeCount');

      likeCount = body.isLike ? likeCount + 1 : likeCount - 1;
      await models.doubtSubjectiveResponse.update({
        "responseLikeCount": likeCount,
      }, {
        where: {
          id: body.respondId
        }
      });
    };

    if (body.isLike) {
      return res.send(await commonResponse.response(true, Message.LIKE_DOUBT));
    } else {
      return res.send(await commonResponse.response(false, Message.UNLIKE_DOUBT));
    }
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};

module.exports.approveCorrectResponce = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });
    await models.doubtObjectiveOption.sync({ force: false });
    await models.doubtObjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveFile.sync({ force: false });
    await models.doubtSubjectiveResponse.sync({ force: false });
    await models.doubtSubjectiveResponseAttachment.sync({ force: false });

    let body = req.body;
    let userData = req.authData.data;

    if (typeOfQuestions.Objective == body.typeOfQuestion) {
      await models.doubtObjectiveResponse.update({
        correct: true,
      }, {
        where: { id: body.respondId, doubtId: body.doubtId, status: true },
      });

    } else {
      // for Subjective only
      // if any response already resolved so first false all response then latest one will resolved 
      await models.doubtSubjectiveResponse.update({
        correct: true
      }, {
        where: { id: { [Op.not]: body.respondId }, doubtId: body.doubtId, status: true },
      });

      await models.doubtSubjectiveResponse.update({
        correct: true
      }, {
        where: { id: body.respondId, doubtId: body.doubtId, status: true },
      });
    }

    await models.doubt.update({
      resolved: true
    }, {
      where: { id: body.doubtId, status: true },
    });

    return res.send(await commonResponse.response(true, Message.RESOLVE_DOUBT));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};


module.exports.batchWiseUnresolvedCount = async (req, res) => {
  try {
    await models.doubt.sync({ force: false });

    await models.staff.sync({ force: false });
    await models.batch.sync({ force: false });
    await models.subject.sync({ force: false });
    await models.userStaffMapping.sync({ force: false });
    await models.staffClassMapping.sync({ force: false });
    await models.staffClassBatchMapping.sync({ force: false });
    await models.staffClassBatchSubjectMapping.sync({ force: false });

    let body = req.body;
    let userData = req.authData.data;

    let staffUserMap = await models.userStaffMapping.findOne({
      where: {
        userId: userData.id,
        status: true,
      }
    });

    if (!staffUserMap) {
      return res.send(await commonResponse.response(true, Message.STAFFS_NOT_FOUND));
    }

    let response = await models.staff.findAll({
      distinct: true,
      subQuery: false,
      where: { id: staffUserMap.staffId, status: true },
      include: [
        {
          model: models.staffClassMapping,
          distinct: true,
          subQuery: false,
           where: { status: true },
          include: [
            {
              model: models.staffClassBatchMapping,
              distinct: true,
              subQuery: false,
              where: { status: true },
              include: [
                {
                  model: models.batch,
                  distinct: true,
                  subQuery: false,
                  where: { status: true },
                },
                {
                  model: models.staffClassBatchSubjectMapping,
                  distinct: true,
                  subQuery: false,
                  where: { status: true },
                  include: [
                    {
                      model: models.subject,
                      distinct: true,
                      subQuery: false,
                      where: { status: true },
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    });

    let batchWiseUnresolvedDoubts = [];

    if (response.length != 0) {
      for (item of response) {
        for (sub_item of item.staffClassMappings) {
          for (sub_sub_item of sub_item.staffClassBatchMappings) {
            for (sub_sub_sub_item of sub_sub_item.staffClassBatchSubjectMappings) {
              let unresolvedDoubtCount = await models.doubt.count({
                distinct: true,
                where: { status: true, subjectId: sub_sub_sub_item.subjectId, resolved: false },
              });
              let data = {
                subjectId : sub_sub_sub_item.subjectId,
                subjectName : sub_sub_sub_item.subject.subjectName,
                batchId : sub_sub_item.batch.id,
                batchName : sub_sub_item.batch.batchName,
                unresolvedDoubtCount : unresolvedDoubtCount
              };
              batchWiseUnresolvedDoubts.push(data);
            }
          }
        }
      }
    }

    let msg = batchWiseUnresolvedDoubts.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
    return res.send(await commonResponse.response(true, msg, batchWiseUnresolvedDoubts));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res.status(status).json(await commonResponse.response(false, message, error));
  }
};

