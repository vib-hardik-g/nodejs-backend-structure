const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Attendance, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;


// creating new request
module.exports.createRequest = async (req, res) => {
    try {
        await models.studentRequest.sync({ force: false });
        await models.staffRequest.sync({ force: false });
        await models.studentRequestAttachment.sync({ force: false });
        await models.staffRequestAttachment.sync({ force: false });

        let { studentId, staffId, batchId, instituteId } = req.authData.data;
        let { request, requestAttachment } = req.body


        //checking user login
        if (studentId) {

            let requestData = await models.studentRequest.create({
                studentId,
                instituteId,
                batchId,
                request

            });
            if (requestData && requestAttachment) {
                await models.studentRequestAttachment.create({
                    studentRequestId: requestData.id,
                    requestAttachment
                });
            }
            if (requestData) {
                return res.json(await commonResponse.response(true, Attendance.REQUEST_CREATE));
            } else {
                return res.json(await commonResponse.response(false, Attendance.REQUEST_FAILED));
            }


        } else if (staffId) {
            let requestData = await models.staffRequest.create({
                staffId,
                instituteId,
                request

            });
            if (requestData && requestAttachment) {
                await models.staffRequestAttachment.create({
                    staffRequestId: requestData.id,
                    requestAttachment
                });
            }
            if (requestData) {
                return res.json(await commonResponse.response(true, Attendance.REQUEST_CREATE));
            } else {
                return res.json(await commonResponse.response(false, Attendance.REQUEST_FAILED));
            }

        } else {
            return res.json(await commonResponse.response(false, Message.SOMETHING_WRONG));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}

//get request by user
module.exports.getRequest = async (req, res) => {
    try {
        await models.studentRequest.sync({ force: false });
        await models.staffRequest.sync({ force: false });
        await models.studentRequestAttachment.sync({ force: false });
        await models.staffRequestAttachment.sync({ force: false });

        let { studentId, staffId, batchId, instituteId } = req.authData.data;

        if (studentId) {
            let requestData = await models.studentRequest.findAll({
                where: {
                    instituteId,
                    studentId,
                    batchId,
                    status: true
                },
                include: [
                    {
                        model: models.studentRequestAttachment,
                        attributes:['requestAttachment'],
                        include:[
                            {model:models.document}
                        ]
                    },
                ]
            })
            if(requestData.length > 0){
                return res.json(await commonResponse.response(true, Message.DATA_FOUND, requestData));
            }else{
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }

        } else if (staffId) {
            let requestData = await models.staffRequest.findAll({
                where: {
                    instituteId,
                    staffId,
                    status: true
                },
                include: [
                    {
                        model: models.staffRequestAttachment
                    },
                ]
            })
            if(requestData.length > 0){
                return res.json(await commonResponse.response(true, Message.DATA_FOUND, requestData));
            }else{
                return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
            }
            
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}