const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');


module.exports.getStudentAnnouncementList = async (req, res) => {
    try {
        let profileinfo = req.authData.data;
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: [
                    { classId: profileinfo.classId },
                    { batchId: profileinfo.batchId, },
                    { groupId: profileinfo.groupId }
                ]

            }
        } else {
            where = {
                status: true,
                [Op.or]: [
                    { classId: profileinfo.classId },
                    { batchId: profileinfo.batchId, },
                    { groupId: profileinfo.groupId }
                ],
                [Op.or]: {
                    '$announcementStudent.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let results = await models.announcementStudentType.findAll({
            attributes: ['announcementStudentId'],
            where: where,
            order: [[models.announcementStudent, 'id', 'DESC']],
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.announcementStudent,
                where: { instituteId: profileinfo.instituteId, status: true },
                required: true,
                include: [
                    {
                        model: models.document,
                        attributes: [],
                        required: true,
                    }
                ]
            },],
            group: ['announcementStudentId', 'announcementStudent.id']
        });
        if (results && results.length > 0) {
            results = JSON.parse(JSON.stringify(results));

            for (let i = 0; i < results.length; i++) {

                let element = results[i];

                let announcementStudentId = element.announcementStudentId;

                let documents = await models.announcementStudent.findOne({
                    where: {
                        instituteId: profileinfo.instituteId,
                        id: announcementStudentId
                    },
                    include: [
                        {
                            model: models.document,
                        }
                    ]
                });


                results[i].announcementStudent.document = documents.document;
            }
            let resultsCount = await models.announcementStudentType.count({
                where: where,
                attributes: ['announcementStudentId'],
                include: [{
                    model: models.announcementStudent,
                    where: { instituteId: profileinfo.instituteId, status: true },
                    include: [
                        {
                            model: models.document,
                            attributes: []
                        }
                    ]
                },],
                group: ['announcementStudentId', 'announcementStudent.id'],
                subQuery: false,

            })
            let msg = results.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: results,
                total: resultsCount.length,
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.getStaffAnnouncementList = async (req, res) => {
    try {
        let profileinfo = req.authData.data;
        let response = await models.staffClassBatchSubjectMapping.findAll({
            include: [
                {
                    model: models.staffClassBatchMapping,
                    required: true,
                    include: [{
                        model: models.staffClassMapping,
                        where: {
                            staffId: profileinfo.staffId
                        },
                        required: true,
                    }]
                }
            ]
        })

        let subjectIds = response.map(data => {
            return data.subjectId;
        });

        let classIds = response.map(data => {
            return data.staffClassBatchMapping.staffClassMapping.classId;
        });
        classIds = classIds.filter((x, i, a) => a.indexOf(x) === i)
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: [
                    { classId: classIds },
                    { subjectId: subjectIds, },
                ]

            }
        } else {
            where = {
                status: true,
                [Op.or]: [
                    { classId: classIds },
                    { subjectId: subjectIds, },
                ],
                [Op.or]: {
                    '$announcementStaff.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let results = await models.announcementStaffType.findAll({
            attributes: ['announcementStaffId'],
            where: where,
            order: [[models.announcementStaff, 'id', 'DESC']],
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.announcementStaff,
                where: { instituteId: profileinfo.instituteId, status: true },
                required: true,
                include: [
                    {
                        model: models.document,
                        attributes: [],
                        required: true,
                    }
                ]
            },],
            group: ['announcementStaffId', 'announcementStaff.id']
        });
        if (results && results.length > 0) {
            results = JSON.parse(JSON.stringify(results));

            for (let i = 0; i < results.length; i++) {

                let element = results[i];

                let announcementStaffId = element.announcementStaffId;

                let documents = await models.announcementStaff.findOne({
                    where: {
                        instituteId: profileinfo.instituteId,
                        id: announcementStaffId
                    },
                    include: [
                        {
                            model: models.document,
                        }
                    ]
                });
                results[i].announcementStaff.document = documents.document;
            }
            let resultsCount = await models.announcementStaffType.count({
                where: where,
                attributes: ['announcementStaffId'],
                include: [{
                    model: models.announcementStaff,
                    where: { instituteId: profileinfo.instituteId, status: true },
                    include: [
                        {
                            model: models.document,
                            attributes: []
                        }
                    ]
                },],
                group: ['announcementStaffId', 'announcementStaff.id'],
                subQuery: false,

            })
            let msg = results.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: results,
                total: resultsCount.length,
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}