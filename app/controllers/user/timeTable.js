const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus, Timetable } = require('../../../utils/commonMessages');
const models = require('../../models');
const { Op } = require('sequelize');
const sequelize = models.sequelize;


module.exports.getStudentTimeTable = async (req, res) => {
    try {

        let { day } = req.body;
        let profileinfo = req.authData.data;

        if (profileinfo.batchId) {
            let tableData = await models.timeTable.findAll({
                attributes: ['year'],
                include: [{
                    model: models.timeTableDetail,
                    attributes: ['day', 'room', 'startTime', 'endTime',],
                    where: {
                        day: day
                    },
                    required: true,
                    include: [
                        {
                            model: models.subject,
                            attributes: ['subjectName', 'icon']
                        },
                        {
                            model: models.staff,
                            attributes: ['firstName', 'lastName']
                        },
                    ]
                },
                ],
                where: {
                    batchId: profileinfo.batchId,
                    instituteId: profileinfo.instituteId,
                    [Op.or]: [
                        { publishStatus: Publishstatus.PUBLISHED },
                        { publishStatus: Publishstatus.REPUBLISHED }
                    ]
                }

            });
            if (tableData.length > 0) {
                return res.json(await commonResponse.response(true, Message.DATA_FOUND, tableData));
            } else {

                return res.json(await commonResponse.response(false, Timetable.NO_CLASS_FOUND));
            }
        } else {

            return res.json(await commonResponse.response(false, Timetable.NO_CLASS_FOUND));
        }

    } catch (err) {

        return res.json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.getStaffRoutine = async (req, res) => {
    try {

        let { day } = req.body;
        let profileinfo = req.authData.data;
        
        if (profileinfo.instituteId) {
            let tableData = await models.timeTableDetail.findAll({
                attributes: ['day', 'room', 'startTime', 'endTime',],
                where: {
                    day: day
                },
                include: [{
                    model: models.timeTable,
                    attributes: ['year'],
                    where: {
                        instituteId: profileinfo.instituteId,
                        [Op.or]: [
                            { publishStatus: Publishstatus.PUBLISHED },
                            { publishStatus: Publishstatus.REPUBLISHED }
                        ]
                    },
                    required: true,
                    include: [
                        {
                            model: models.batch
                        },
                    ]
                },
                {
                    model: models.subject,
                    attributes: ['subjectName', 'icon']
                },
                ],
            });
            let resultArr = [];

            for (let index = 0; index < tableData.length; index++) {
                const element = tableData[index];

                resultArr.push({
                    day: element.day,
                    room: element.room ? element.room : "",
                    startTime: element.startTime,
                    endTime: element.endTime,
                    year: element.timeTable.year,
                    batchName: element.timeTable.batch.batchName,
                    subjectName: element.subject.subjectName,
                })
            }

            if (tableData.length > 0) {
                return res.json(await commonResponse.response(true, Message.DATA_FOUND, resultArr));
            } else {
                
                return res.json(await commonResponse.response(false, Timetable.NO_CLASS_FOUND));
            }
        } else {
            
            return res.json(await commonResponse.response(false, Timetable.NO_CLASS_FOUND));
        }

    } catch (err) {

        return res.json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}