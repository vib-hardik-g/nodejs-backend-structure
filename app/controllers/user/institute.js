const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.getInstituteInfo = async (req,res) =>{
    try {
        let profileinfo = req.authData.data;
        let result = await models.institution.findOne({
            attributes:['institutionName','legalName','institutionType','licenseNo',[sequelize.col('document.fullPath'),'instituteIcon'],[sequelize.col('info.content'),'content']],
            include:[
                {
                    model:models.institutionInfo,
                    as: "info",
                    attributes:['boardType','address','googleMap','googleImage']
                },
                {
                    model:models.institutionEmail,
                    as: "emails",
                    attributes:['emailType','emailId']
                },
                {
                    model:models.institutionPhone,
                    as: "phones",
                    attributes:['phoneNumber']
                },
                {
                    model:models.institutionWebsite,
                    as: "websites",
                    attributes:['website']
                },
                {
                    model:models.document,
                    attributes:[]
                }
            ],
            where:{
                id:profileinfo.instituteId
            }
        })
        if(result){
            result.instituteIcon = result.instituteIcon ? result.instituteIcon : "https://i.pinimg.com/474x/b7/c8/f9/b7c8f9c63e14e5d3a7e078f7109cb65c.jpg"
            result.info.googleImage = result.info.googleImage ? result.info.googleImage : "https://www.ydesignservices.com/wp-content/uploads/2016/07/Googlemap-600x551.jpg"
            return res.json(await commonResponse.response(true, Message.DATA_FOUND,result));
        }else{
            return res.json(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
        
    }
}