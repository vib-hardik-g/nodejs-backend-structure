const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.getHappeningWithPagination = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });

        let profileinfo = req.authData.data.instituteId;
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }

        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                instituteId: profileinfo
            }
        } else {
            where = {
                status: true,
                instituteId: profileinfo,
                [Op.or]: {
                    happening: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    happeningDescription: { [Op.iLike]: '%' + req.query.searchKey + '%' }
                }
            }
        }


        let response = await models.happening.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            attributes: [['id', 'happeningId'], ['happening', 'happeningTitle'],
            ['happeningDescription', 'description'], ['happeningLikeCount', 'totalLikeCount'],
            ['happeningCommentCount', 'totalCommentCount'], 'createdAt', 'updatedAt'],
            include: [
                {
                    model: models.happeningAttachment,
                    as: "attachments",
                    attributes: ['id'],
                    include: [{
                        model: models.document
                    }]
                },
                {
                    model: models.document,
                    as: 'thumnbnailImage'
                }
            ]
        });
        if (response) {
            let result = {
                data: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }

    } catch (err) {

        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.createHappeningComment = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });

        let { happeningId, comment } = req.body;
        let profileinfo = req.authData.data;
        if (profileinfo && profileinfo.studentId) {
            let response = await models.happeningComment.create({
                happeningId,
                studentId: profileinfo.studentId,
                comment
            });
            if (response) {
                // increase comment count
                await models.happening.increment('happeningCommentCount', { by: Constant.COUNT, where: { id: req.body.happeningId } });
            }
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else if (profileinfo && profileinfo.staffId) {
            let response = await models.happeningComment.create({
                happeningId,
                staffId: profileinfo.staffId,
                comment
            });
            if (response) {

                // increase comment count
                await models.happening.increment('happeningCommentCount', { by: Constant.COUNT, where: { id: req.body.happeningId } });
            }
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.status(400).send(await commonResponse.response(false, Message.BAD_REQUEST));
        }

    } catch (err) {

        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.getHappeningById = async (req, res) => {
    try {
        let response = await models.happening.findOne({
            where: {
                id: req.params.happeningId,
                status: true
            },
            attributes: [['id', 'happeningId'], ['happening', 'happeningTitle'],
            ['happeningDescription', 'description'], ['happeningLikeCount', 'totalLikeCount'],
            ['happeningCommentCount', 'totalCommentCount'], 'createdAt'],
            include: [
                {
                    model: models.happeningAttachment,
                    as: "attachments",
                    attributes: ['id'],
                    include: [{
                        model: models.document
                    }]
                },
                {
                    model: models.document,
                    as: 'thumnbnailImage'
                }
            ]
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, response));
        }

    } catch (err) {

        const { status, message, error } = errorHandle(err);
        return res.send(await commonResponse.response(true, Message.SOMETHING_WRONG));
    }
}

module.exports.getHappeningCommentsWithPagination = async (req, res) => {
    try {
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }


        let response = await models.happeningComment.findAndCountAll({
            where: {
                happeningId: req.query.happeningId,
                // approvalStatus:'true',
                status: true
            },
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            include: [
                {
                    model: models.student,
                    attributes: ['firstName', 'lastName'],
                },
                {
                    model: models.staff,
                    as: "staff",
                    attributes: ['firstName', 'lastName'],
                }
            ]
        });
        if (response) {
            let resultArr = [];
            response.rows = JSON.parse(JSON.stringify(response.rows));
            for (let index = 0; index < response.rows.length; index++) {
                const element = response.rows[index];
                resultArr.push({
                    happeningId: element.happeningId,
                    comment: element.comment,
                    createdAt: element.createdAt,
                    firstName: element.student ? element.student.firstName : element.staff.firstName,
                    lastName: element.student ? element.student.lastName : element.staff.lastName,

                })
            }
            let result = {
                data: resultArr,
                total: response.count
            }

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND));
        }

    } catch (err) {

        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.createHappeningLike = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.happening.sync({ force: false });
        await models.happeningComment.sync({ force: false });
        await models.happeningAttachment.sync({ force: false });
        await models.happeningLike.sync({ force: false });

        let { happeningId } = req.body;
        let profileinfo = req.authData.data;
        if (profileinfo && profileinfo.studentId) {
            let response = await models.happeningLike.create({
                happeningId,
                studentId: profileinfo.studentId
            });
            if (response) {
                // increase like count
                await models.happening.increment('happeningLikeCount', { by: Constant.COUNT, where: { id: req.body.happeningId } });
            }
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else if (profileinfo && profileinfo.staffId) {
            let response = await models.happeningLike.create({
                happeningId,
                staffId: profileinfo.staffId
            });
            if (response) {

                // increase like count
                await models.happening.increment('happeningLikeCount', { by: Constant.COUNT, where: { id: req.body.happeningId } });
            }
            return res.send(await commonResponse.response(true, Message.DATA_CREATED));
        } else {
            return res.status(400).send(await commonResponse.response(false, Message.BAD_REQUEST));
        }

    } catch (err) {

        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}