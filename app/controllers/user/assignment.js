const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.listAssignmentsByClassSubject = async (req, res) => {
    try{
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });
        await models.assignmentSubmission.sync({ force: false });

        let { studentId, classId } = req.authData.data;
        let { subjectId } = req.params;

        let where = {};

        if(!req.query.searchKey){
            where = {
                status: true,
                [Op.or]: [
                    { publishStatus: Publishstatus.PUBLISHED },
                    { publishStatus: Publishstatus.REPUBLISHED }
                ],
                classId,
                subjectId
            };
        }else{
            where = {
                status: true,
                [Op.or]: [
                    { publishStatus: Publishstatus.PUBLISHED },
                    { publishStatus: Publishstatus.REPUBLISHED }
                ],
                classId,
                subjectId,
                [Op.or]: {
                    assignmentName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    '$subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' }
                }
            };
        }

        let response = await models.assignment.findAll({
            where: where,
            attributes: ['id','assignmentName', 'assignmentDueDate', 'assignmentDueTime', 'createdAt', 'updatedAt'],
            include: [{
                model: models.assignmentAttachment,
                include: [{
                    model: models.document
                }],
                attributes: ['id']
            },{
                model: models.assignmentSubmission,
                where: {
                    studentId
                },
                required: false,
                include: [{
                    model: models.document
                }],
                attributes: ['id']
            }, {
                model: models.subject,
                attributes: ['id', 'subjectName']
            }]
        });
        let msg = response.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.json(await commonResponse.response(true, msg, response));
    }catch(err){
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.submitAssignment = async (req, res) => {
    try{
        await models.assignment.sync({ force: false });
        await models.batchAssignmentMapping.sync({ force: false });
        await models.assignmentAttachment.sync({ force: false });
        await models.assignmentSubmission.sync({ force: false });

        let { studentId } = req.authData.data;
        let { assignmentId, assignmentFile } = req.body;

        // TODO: check assignment submission date & validate

        await models.assignmentSubmission.create({
            assignmentId,
            studentId,
            assignmentFile
        });
        return res.json(await commonResponse.response(true, Message.DATA_CREATED));
    }catch(err){
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}