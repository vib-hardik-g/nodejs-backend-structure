const bcrypt = require('bcryptjs');
const errorHandle = require('../../../utils/errorHandler');
const { generateToken } = require('../../../utils/jwt');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const models = require('../../models');
const { Op, DATE } = require('sequelize');
const sequelize = models.sequelize;
let moment = require('moment');

module.exports.getProfile = async (req, res) => {

    try {

        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.country.sync({ force: false });
        await models.state.sync({ force: false });
        await models.city.sync({ force: false });
        await models.parentType.sync({ force: false });
        await models.parent.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });

        let { data } = req.authData;

        if (data.userTypes == Usertypes.STUDENT) {

            let userType = await models.user.findOne({
                include: [
                    {
                        model: models.userStudentMapping,
                        attributes: ['studentId'],
                        include: [
                            {
                                model: models.student,
                                include: [

                                    {
                                        model: models.state,
                                        include: [
                                            {
                                                model: models.country,
                                            }
                                        ]
                                    },
                                    {
                                        model: models.city,
                                    },
                                    {
                                        model: models.parent,
                                    },
                                ]
                            },
                            {
                                model: models.batch
                            },
                        ]
                    }
                ],
                where: {
                    id: data.id
                }
            });
            // let parents = await models.parent.findAll({
            //     attributes: ['parentName', 'parentPhoneNumber', 'parentEmail', [sequelize.col('parentType.parentType'), 'parentTypes']],
            //     include: [{
            //         model: models.parentType,
            //         attributes: []

            //     }],
            //     order: [['id', 'asc']],
            //     where: {
            //         studentId: userType.userStudentMappings[0].studentId
            //     }
            // })
            let studentData = userType.userStudentMappings[0].student;
            let result = {
                rollNo: studentData.uniqueId,
                batchId: userType.userStudentMappings[0].batch ? userType.userStudentMappings[0].batch.id : "",
                batchName: userType.userStudentMappings[0].batch ? userType.userStudentMappings[0].batch.batchName : "",
                firstName: studentData.firstName,
                lastName: studentData.lastName ? studentData.lastName : "",
                email: userType ? userType.email : "",
                dob: studentData.dateOfBirth ? studentData.dateOfBirth : "",
                bloodGroup: studentData.bloodGroup ? studentData.bloodGroup : "",
                gender: studentData.gender ? studentData.gender : "",
                primaryPhoneNumber: studentData.primaryPhoneNumber ? studentData.primaryPhoneNumber : "",
                address: studentData.address ? studentData.address : "",
                pincode: studentData.pincode ? studentData.pincode : "",
                fatherName: studentData.fatherName ? studentData.fatherName : "",
                fatherEmailId: studentData.fatherEmailId ? studentData.fatherEmailId : "",
                fatherPhoneNumber: studentData.fatherPhoneNumber ? studentData.fatherPhoneNumber : "",
                motherName: studentData.motherName ? studentData.motherName : "",
                motherPhoneNumber: studentData.motherPhoneNumber ? studentData.motherPhoneNumber : "",
                motherEmailId: studentData.motherEmailId ? studentData.motherEmailId : "",
                legalGuardianName: studentData.legalGuardianName ? studentData.legalGuardianName : "",
                legalGuardianPhoneNumber: studentData.legalGuardianPhoneNumber ? studentData.legalGuardianPhoneNumber : "",
                legalGuardianEmailId: studentData.legalGuardianEmailId ? studentData.legalGuardianEmailId : "",
                cityName: studentData.city ? studentData.city.cityName : "",
                countryName: studentData.state.country ? studentData.state.country.countryName : "",
                stateName: studentData.state ? studentData.state.stateName : "",
                profilePic: studentData.profilePic ? studentData.state.profilePic : "https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg",
                // parents: parents,
            }
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, result))
        } else if (data.userTypes == Usertypes.STAFF) {
            let userType = await models.user.findOne({
                include: [
                    {
                        model: models.userStaffMapping,
                        include: [
                            {
                                model: models.staff,
                                include: [
                                    {
                                        model: models.state,
                                        include: [
                                            {
                                                model: models.country,
                                            }
                                        ]
                                    },
                                    {
                                        model: models.city,
                                    },
                                    {
                                        model: models.document
                                    }
                                ]
                            },
                            // {
                            //     model: models.batch
                            // },
                        ]
                    }
                ],
                where: {
                    id: data.id
                }
            });
            let staffData = userType.userStaffMappings[0].staff;
            let result = {
                firstName: staffData.firstName,
                lastName: staffData.lastName ? staffData.lastName : "",
                email: userType ? userType.email : "",
                dob: staffData.dateOfBirth ? staffData.dateOfBirth : "",
                bloodGroup: staffData.bloodGroup ? staffData.bloodGroup : "",
                gender: staffData.gender ? staffData.gender : "",
                designation: staffData.designation ? staffData.designation : "",
                primaryPhoneNumber: staffData.contactNo ? staffData.contactNo : "",
                address: staffData.address ? staffData.address : "",
                pincode: staffData.pincode ? staffData.pincode : "",
                salutation: staffData.salutation ? staffData.salutation : "",
                empCode: staffData.employeeCode ? staffData.employeeCode : "",
                profilePic: staffData.document.fullPath ? staffData.document.fullPath : "https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg",
                cityName: staffData.city ? staffData.city.cityName : "",
                countryName: staffData.state.country ? staffData.state.country.countryName : "",
                stateName: staffData.state ? staffData.state.stateName : "",
            }
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }


    } catch (err) {

        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}

module.exports.getStaffDetails = async (req, res) => {

    try {
        let { staffId } = req.params;
        let staffData = await models.user.findOne({
            include: [
                {
                    model: models.userStaffMapping,
                    include: [
                        {
                            model: models.staff,
                            where: {
                                id: staffId,
                            },
                            required: true,
                            include: [{
                                model: models.document
                            }]
                        },
                    ]
                }
            ],
        });
        if (staffData) {
            let staffInfo = staffData.userStaffMappings[0].staff;
            let result = {
                firstName: staffInfo.firstName,
                lastName: staffInfo.lastName ? staffInfo.lastName : "",
                designation: staffInfo.designation ? staffInfo.designation : "",
                profilePic: staffInfo.document.fullPath ? staffInfo.document.fullPath : "",
                totalExperience: staffInfo.experience ? staffInfo.experience : "",
                email: staffData ? staffData.email : "",
                dob: staffInfo.dob ? staffInfo.dob : "",
                salutation: staffInfo.salutation ? staffInfo.salutation : "",
                empCode: staffInfo.employeeCode ? staffInfo.employeeCode : "",
                currentExperience: await calculateAge(staffInfo.createdAt),
            }
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {

        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.getStaffsBySubject = async (req, res) => {

    try {
        let { subjectId } = req.params;
        let staffData = await models.staff.findAll({
            attributes: [['id', 'staffId'], 'firstName', 'lastName', 'profilePic'],
            include: [
                {
                    model: models.userStaffMapping,
                    attributes: [],
                    where: {
                        subjectId: subjectId,
                    },
                    required: true
                }
            ],
        });
        if (staffData) {
            return res.json(await commonResponse.response(false, Message.DATA_FOUND, staffData));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {

        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.getSubjectByStudent = async (req, res) => {
    try {

        //get staff id
        let sub = await models.batchSubjectMapping.findAll({
            where: {
                batchId: req.authData.data.batchId
            },
            include: [{
                model: models.subject
            }]
        })
        let mapping = sub.map(val => {
            return val.subjectId;
        });
        let staff = await models.staffClassBatchSubjectMapping.findAll({
            where: {
                subjectId: mapping
            },
            include: [{
                model: models.staffClassBatchMapping,
                where: {
                    batchId: req.authData.data.batchId
                },
                required: true,
                include: [{
                    model: models.staffClassMapping,
                    where: {
                        classId: req.authData.data.classId
                    },
                    required: true,
                    include: [
                        {
                            model: models.staff,
                            attributes: ['id', 'firstName', 'lastName'],
                            include: [
                                { model: models.document }
                            ]
                        }
                    ]
                }]
            },
            ],
        })


        let resultArr = [];

        for (let index = 0; index < sub.length; index++) {
            const element = sub[index];
            let staffData = [];
            for (let index1 = 0; index1 < staff.length; index1++) {
                const element1 = staff[index1];
                if (element.subject.id == element1.subjectId) {
                    staffData.push(element1.staffClassBatchMapping.staffClassMapping.staff);
                }
            }
            resultArr.push({
                id: element.subject.id,
                event_message_type: element.subject.subjectName,
                icon: element.subject.icon,
                staffData: staffData
            })

        }
        if (staff) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, resultArr));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }



    } catch (err) {
        console.log(err);
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.switchProfile = async (req, res) => {
    try {
        let { studentId } = req.body;

        let account = await models.userStudentMapping.findOne({
            where: {
                studentId
            },
            include: [
                {
                    model: models.user
                }
            ]
        })

        const jwtToken = generateToken({ id: account.user.id, studentId: account.studentId, userTypeId: account.user.userTypeId, userTypes: 'STUDENT' });
        return res.status(500).json(await commonResponse.response(true, Message.DATA_FOUND, jwtToken));


    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
async function calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday;
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}