const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;
const asyncL = require('async');


module.exports.getStudentNoticeList = async (req, res) => {
    try {
        let profileinfo = req.authData.data;
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: [
                    { classId: profileinfo.classId },
                    { batchId: profileinfo.batchId, },
                    { groupId: profileinfo.groupId }
                ]

            }
        } else {
            where = {
                status: true,
                [Op.or]: [
                    { classId: profileinfo.classId },
                    { batchId: profileinfo.batchId, },
                    { groupId: profileinfo.groupId }
                ],
                [Op.or]: {
                    '$noticeStudent.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let results = await models.noticeStudentType.findAll({
            attributes: ['noticeStudentId'],
            where: where,
            order: [[models.noticeStudent, 'id', 'DESC']],
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.noticeStudent,
                where: { instituteId: profileinfo.instituteId, status: true },
                required: true,
                include: [
                    {
                        model: models.document,
                        attributes: [],
                        required: true,
                    }
                ]
            },],
            group: ['noticeStudentId', 'noticeStudent.id']
        });
        if (results && results.length > 0) {
            results = JSON.parse(JSON.stringify(results));

            for (let i = 0; i < results.length; i++) {

                let element = results[i];

                let noticeStudentId = element.noticeStudentId;

                let documents = await models.noticeStudent.findOne({
                    where: {
                        instituteId: profileinfo.instituteId,
                        id: noticeStudentId
                    },
                    include: [
                        {
                            model: models.document,
                        }
                    ]
                });


                results[i].noticeStudent.document = documents.document;
            }
            let resultsCount = await models.noticeStudentType.count({
                where: where,
                attributes: ['noticeStudentId'],
                include: [{
                    model: models.noticeStudent,
                    where: { instituteId: profileinfo.instituteId, status: true },
                    include: [
                        {
                            model: models.document,
                            attributes: []
                        }
                    ]
                },],
                group: ['noticeStudentId', 'noticeStudent.id'],
                subQuery: false,

            })
            let msg = results.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: results,
                total: resultsCount.length,
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}
module.exports.getStaffNoticeList = async (req, res) => {
    try {
        let profileinfo = req.authData.data;
        let response = await models.staffClassBatchSubjectMapping.findAll({
            include: [
                {
                    model: models.staffClassBatchMapping,
                    required: true,
                    include: [{
                        model: models.staffClassMapping,
                        where: {
                            staffId: profileinfo.staffId
                        },
                        required: true,
                    }]
                }
            ]
        })

        let subjectIds = response.map(data => {
            return data.subjectId;
        });

        let classIds = response.map(data => {
            return data.staffClassBatchMapping.staffClassMapping.classId;
        });
        classIds = classIds.filter((x, i, a) => a.indexOf(x) === i)
        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                [Op.or]: [
                    { classId: classIds },
                    { subjectId: subjectIds, },
                ]

            }
        } else {
            where = {
                status: true,
                [Op.or]: [
                    { classId: classIds },
                    { subjectId: subjectIds, },
                ],
                [Op.or]: {
                    '$noticeStaff.title$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let results = await models.noticeStaffType.findAll({
            attributes: ['noticeStaffId'],
            where: where,
            order: [[models.noticeStaff, 'id', 'DESC']],
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            distinct: true,
            subQuery: false,
            include: [{
                model: models.noticeStaff,
                where: { instituteId: profileinfo.instituteId, status: true },
                required: true,
                include: [
                    {
                        model: models.document,
                        attributes: [],
                        required: true,
                    }
                ]
            },],
            group: ['noticeStaffId', 'noticeStaff.id']
        });
        if (results && results.length > 0) {
            results = JSON.parse(JSON.stringify(results));

            for (let i = 0; i < results.length; i++) {

                let element = results[i];

                let noticeStaffId = element.noticeStaffId;

                let documents = await models.noticeStaff.findOne({
                    where: {
                        instituteId: profileinfo.instituteId,
                        id: noticeStaffId
                    },
                    include: [
                        {
                            model: models.document,
                        }
                    ]
                });
                results[i].noticeStaff.document = documents.document;
            }
            let resultsCount = await models.noticeStaffType.count({
                where: where,
                attributes: ['noticeStaffId'],
                include: [{
                    model: models.noticeStaff,
                    where: { instituteId: profileinfo.instituteId, status: true },
                    include: [
                        {
                            model: models.document,
                            attributes: []
                        }
                    ]
                },],
                group: ['noticeStaffId', 'noticeStaff.id'],
                subQuery: false,

            })
            let msg = results.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: results,
                total: resultsCount.length,
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}