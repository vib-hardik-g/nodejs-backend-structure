const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Constant, Attendance } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.applyLeave = async (req, res) => {
    try {
        await models.leaveType.sync({ force: false });
        await models.studentLeave.sync({ force: false });
        await models.studentLeaveAttachment.sync({ force: false });
        let { instituteId, studentId } = req.authData.data;

        let { startDate, endDate, reason, supportingDocument } = req.body

        // check if the date is in holiday list or not

        // let checkExistingHolidayCount = await models.holidayList.count({
        //     where: {
        //         date,
        //         instituteId
        //     }
        // });
        // if (checkExistingHolidayCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.ALREADY_HOLIDAY));


        //check for the date leave is already applied or not
        let checkAppliedLeaveCount = await models.studentLeave.count({
            where: {
                startDate: {
                    [Op.lte]: startDate
                },
                endDate: {
                    [Op.gte]: startDate
                },
                studentId,
                instituteId
            }
        });
        if (checkAppliedLeaveCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.LEAVE_REQUEST_PENDING));

        //create leave request
        let leave = await models.studentLeave.create({
            studentId,
            instituteId,
            startDate,
            endDate,
            leaveTypeId: 1,
            reason

        });
        if (leave && supportingDocument) {
            await models.studentLeaveAttachment.create({
                leaveId: leave.id,
                supportingDocument
            });
         }
        if(leave){
            return res.json(await commonResponse.response(true, Attendance.LEAVE_APPLY_SUCCESS));
        } else {
            return res.json(await commonResponse.response(false, Attendance.LEAVE_APPLY_FAILED));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }

}

module.exports.leaveList = async (req, res) => {
    try {
        await models.studentAttendanceManager.sync({ force: false });
        await models.studentAbsentee.sync({ force: false });
        let { instituteId, studentId } = req.authData.data;

        let where = {};
        if (!req.query.searchKey) {
            where = {
                status: true,
                studentId: studentId,
                instituteId: instituteId
            }
        } else {
            where = {
                status: true,
                studentId: studentId,
                instituteId: instituteId,
                [Op.or]: {
                    reason: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }

        let result = await models.studentLeave.findAll({
            attributes: ['id','reason', 'startDate', 'endDate', 'leaveStatus'],
            where: where,
            include: [
                {
                    model: models.studentLeaveAttachment,
                    as: 'attachments',
                    attributes: ['id'],
                    include: [
                        {
                            model: models.document
                        }
                    ]
                }
            ]

        })
        if (result.length > 0) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, result));
        } else {
            return res.json(await commonResponse.response(false, Attendance.LEAVE_LIST));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}

module.exports.attendanceHistory = async (req, res) => {
    try {

        let { startDate, endDate } = req.body
        let { instituteId, studentId, classId } = req.authData.data;
        let data = await models.studentAttendanceManager.findAll({
            where: {
                instituteId: instituteId,
                classId: classId,
                publishStatus: 'Published',
                [Op.and]: [
                    { date: { [Op.lte]: endDate } },
                    { date: { [Op.gte]: startDate } }
                ]
            },
            include: [
                {
                    model: models.studentAbsentee,
                    where: {
                        studentId
                    },
                    required:false
                }
            ]
        })
        let result = []
        data.forEach((att) => {
            if (att.studentAbsentees.length > 0) {
                result.push({
                    date: att.studentAbsentees[0].date,
                    type: att.studentAbsentees[0].absenceType
                })
            } else {
                result.push({
                    date: att.date,
                    type: 'Present'
                })
            }
        })
        let holiday = await models.holidayList.findAndCountAll(
            {
                attributes: ['date', ['holidayType', 'type']],
                where: {
                    instituteId: instituteId,
                    [Op.and]: [
                        { date: { [Op.lte]: endDate } },
                        { date: { [Op.gte]: startDate } }
                    ]
                },
                distinct: true,
            }
        )
        result.push(...holiday.rows)
        let leaveCount = 0;
        let absentCount = 0;
        let presentCount = 0;
        result.forEach((count) => {
            if (count && count.type == 'Leave') {
                leaveCount++;
            } else if (count && count.type == 'Present') {
                presentCount++;
            } else if (count && count.type == 'Absent') {
                absentCount++;
            }
        })
        let resultArr = {
            attendance: result,
            holidayCount: holiday.count,
            leaveCount: leaveCount,
            absentCount: absentCount,
            presentCount: presentCount
        }
        if (result.length > 0) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, resultArr));
        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND, resultArr));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}

module.exports.editLeave = async (req, res) => {
    try {
        await models.leaveType.sync({ force: false });
        await models.studentLeave.sync({ force: false });
        await models.studentLeaveAttachment.sync({ force: false });
        let { instituteId, studentId } = req.authData.data;

        let { leaveId, startDate, endDate, reason, supportingDocument } = req.body

        //check leave is exist or not
        let userLeave = await models.studentLeave.findOne({
            where: {
                id: leaveId,
                studentId,
                instituteId,
                leaveStatus: 'Pending'
            }
        })
        if (userLeave) {
            //  check for the date leave is already applied or not
            let checkAppliedLeaveCount = await models.studentLeave.count({
                where: {
                    startDate: {
                        [Op.lte]: startDate
                    },
                    endDate: {
                        [Op.gte]: endDate
                    },
                    studentId,
                    instituteId,
                    id:{[Op.ne]: leaveId},

                }
            });
            if (checkAppliedLeaveCount > 0) return res.status(400).json(await commonResponse.response(false, Attendance.LEAVE_REQUEST_PENDING));

            await models.studentLeave.update({
                startDate, endDate, reason
            },
                {
                    where: {
                        id: leaveId,
                        studentId,
                        instituteId,
                        leaveStatus: 'Pending'
                    }
                }
            )

            if (supportingDocument) {
                await models.studentLeaveAttachment.create({
                    leaveId: leaveId,
                    supportingDocument
                });
            }
            return res.json(await commonResponse.response(true, Attendance.LEAVE_UPDATE));
        } else {
            return res.json(await commonResponse.response(false, Attendance.LEAVE_LIST));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}