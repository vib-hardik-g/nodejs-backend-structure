const models = require("../../models");
const commonResponse = require("../../../utils/commonResponse");
const { Message, Publishstatus } = require("../../../utils/commonMessages");
const errorHandle = require("../../../utils/errorHandler");
const { Op, Sequelize } = require("sequelize");
const _ = require("lodash");

module.exports.allAssessments = async (req, res) => {
  try {
    await models.assessment.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });
    await models.assessmentQuestionAnswer.sync({ force: false });

    let { instituteId, studentId } = req.authData.data;
    // get student's class batch group
    let studentDetails = await models.userStudentMapping.findOne({
      where: {
        instituteId,
        studentId,
      },
    });
    let classId = studentDetails.classId;
    // get student's subjects by batch
    let batchSubjects = await models.batchSubjectMapping.findAll({
      where: {
        instituteId,
        batchId: studentDetails.batchId,
      },
    });
    let groupSubjects = await models.groupSubject.findAll({
      where: {
        groupId: studentDetails.groupId,
      },
    });
    let studentSubjects = [];
    batchSubjects.forEach((batchSubject) => {
      studentSubjects.push(batchSubject.subjectId);
    });
    groupSubjects.forEach((groupSubject) => {
      studentSubjects.push(groupSubject.subjectId);
    });
    studentSubjects = _.uniq(studentSubjects);

    let where = {
      classId,
      instituteId,
      status: true,
    };
    if (req.query.searchKey) {
      where.assessmentName = { [Op.iLike]: "%" + req.query.searchKey + "%" };
    }
    let assessments = await models.assessment.findAll({
      where: where,
      attributes: [
        "id",
        "assessmentName",
        "duration",
        "startDate",
        "startTime",
        "endDate",
        "endTime",
        "correctMark",
        "incorrectMark",
        [
          Sequelize.fn("COUNT", Sequelize.col("assessmentQuestions.id")),
          "questionCounts",
        ],
        [
          Sequelize.fn("COUNT", Sequelize.col("assessmentQuestionAnswers.id")),
          "answerCounts",
        ],
      ],
      include: [
        {
          model: models.class,
          attributes: ["className"],
        },
        {
          model: models.subjectAssessmentMapping,
          attributes: ["subjectId"],
          include: [
            {
              model: models.subject,
              attributes: ["subjectName"],
            },
          ],
        },
        {
          model: models.assessmentQuestion,
          attributes: ["id", "question", "status"],
          include: [
            {
              model: models.assessmentQuestionOption,
              attributes: ["id", "option", "isCorrect", "status"],
            },
          ],
        },
        {
          model: models.assessmentQuestionAnswer,
          attributes: [],
        },
      ],
      group: [
        "assessment.id",
        "class.id",
        "subjectAssessmentMappings.id",
        "subjectAssessmentMappings->subject.id",
        "assessmentQuestions.id",
        "assessmentQuestions->assessmentQuestionOptions.id",
      ],
    });
    let result = [];
    assessments.forEach((assessment) => {
      let assessmentSubjects = [];
      assessment.subjectAssessmentMappings.forEach((subject) => {
        assessmentSubjects.push(subject.subjectId);
      });
      // check if all assessment subjects includes student's subjects
      if (_.difference(assessmentSubjects, studentSubjects).length == 0) {
        result.push(assessment);
      }
    });

    return res.send(
      await commonResponse.response(
        true,
        result.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND,
        result
      )
    );
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.submitAssessment = async (req, res) => {
  try {
    await models.assessment.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });
    await models.assessmentQuestionAnswer.sync({ force: false });

    let { instituteId, studentId } = req.authData.data;
    let { assessmentId, answers } = req.body;

    // check if test has been submitted previously or not
    let assessmentAnswerCount = await models.assessmentQuestionAnswer.count({
      where: {
        instituteId,
        studentId,
        assessmentId,
      },
    });
    if (assessmentAnswerCount) {
      return res
        .status(400)
        .json(await commonResponse.response(false, Message.DATA_EXISTS));
    }

    // insert the assessment answers at once
    let mapData = await Promise.all(
      answers.map(async (answer) => {
        // fetch question Option to check if its correct or not
        let assessmentQuestionOption =
          await models.assessmentQuestionOption.findOne({
            where: { id: answer.assessmentSelectedOptionId },
          });
        let isCorrect = false;
        if (assessmentQuestionOption.isCorrect) isCorrect = true;
        return {
          instituteId,
          studentId,
          assessmentId,
          assessmentQuestionId: answer.assessmentQuestionId,
          assessmentSelectedOptionId: answer.assessmentSelectedOptionId,
          isCorrect,
        };
      })
    );
    await models.assessmentQuestionAnswer.bulkCreate(mapData);
    return res.json(await commonResponse.response(true, Message.DATA_CREATED));
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.getAnswersByAssessmentId = async (req, res) => {
  try {
    await models.assessment.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });
    await models.assessmentQuestionAnswer.sync({ force: false });

    let { instituteId, studentId } = req.authData.data;
    let { assessmentId } = req.params;

    let response = await models.assessmentQuestion.findAll({
      where: {
        assessmentId,
        status: true,
      },
      attributes: ["id", "question"],
      include: [
        {
          model: models.assessmentQuestionOption,
          attributes: ["id", "option", "isCorrect"],
        },
        {
          model: models.subject,
          attributes: ["id", "subjectName"],
        },
        {
          model: models.topicDetails,
          attributes: ["id", "chapter"],
        },
        {
          model: models.assessmentQuestionAnswer,
          where: {
            studentId,
          },
          required: false,
          attributes: ["isCorrect"],
          include: [
            {
              model: models.assessmentQuestionOption,
              as: "selectedOption",
              attributes: ["id", "option"],
            },
          ],
        },
      ],
    });
    let result = {};
    result.totalQuestions = response.length;
    let correctQuestionsCount = 0,
      incorrectQuestionsCount = 0;
    result.questionAnswers = [];
    response.forEach((question) => {
      if (question.assessmentQuestionAnswers.length > 0) {
        if (question.assessmentQuestionAnswers[0].isCorrect) {
          correctQuestionsCount += 1;
        } else {
          incorrectQuestionsCount += 1;
        }
      }
      let questionAnswerData = {
        subject: question.subject.subjectName,
        chapter: question.topicDetail.chapter,
        subject: question.subject.subjectName,
        question: question.question,
        options: question.assessmentQuestionOptions,
      };
      if (question.assessmentQuestionAnswers.length > 0) {
        // answer submitted
        questionAnswerData.options = JSON.parse(
          JSON.stringify(questionAnswerData.options)
        );
        questionAnswerData.options.forEach((option) => {
          if (option.isCorrect) {
            option.isAnswered = true;
          } else {
            option.isAnswered = false;
          }
        });
      } else {
        questionAnswerData.options = JSON.parse(
          JSON.stringify(questionAnswerData.options)
        );
        questionAnswerData.options.forEach((option) => {
          option.isAnswered = false;
        });
      }
      result.questionAnswers.push(questionAnswerData);
    });
    result.correctQuestions = correctQuestionsCount;
    result.incorrectQuestions = incorrectQuestionsCount;
    result.skippedQuestions =
      result.totalQuestions -
      (result.correctQuestions + result.incorrectQuestions);
    return res.json(
      await commonResponse.response(
        true,
        response.length > 0 ? Message.DATA_FOUND : Message.NO_DATA_FOUND,
        result
      )
    );
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};

module.exports.analysis = async (req, res) => {
  try {
    await models.assessment.sync({ force: false });
    await models.assessmentQuestion.sync({ force: false });
    await models.assessmentQuestionOption.sync({ force: false });
    await models.assessmentQuestionAnswer.sync({ force: false });

    let { instituteId, studentId } = req.authData.data;
    let { assessmentId } = req.params;

    let submittedAnswersCount = await models.assessmentQuestionAnswer.count({
      where: {
        instituteId,
        assessmentId,
        studentId,
        status: true,
      },
    });
    if (!submittedAnswersCount)
      return res
        .status(404)
        .json(
          await commonResponse.response(false, Message.ASSESSMENT_NOT_TAKEN)
        );

    let response = await models.assessment.findOne({
      where: {
        instituteId,
        id: assessmentId,
        status: true,
      },
      attributes: [
        "assessmentName",
        "duration",
        "startDate",
        "startTime",
        "endDate",
        "endTime",
        "correctMark",
        "incorrectMark",
      ],
      include: [
        {
          model: models.assessmentQuestion,
          attributes: ["id", "question"],
          include: [
            {
              model: models.subject,
              attributes: ["subjectName"],
            }
          ],
        },
        {
          model: models.assessmentQuestionAnswer,
          attributes: ['assessmentQuestionId', 'isCorrect'],
          where: {
            instituteId,
            assessmentId,
            studentId
          },
          required: false
        },
        {
          model: models.subjectAssessmentMapping,
          attributes: ['subjectId'],
          include: [{
            model: models.subject,
            attributes: ['subjectName']
          }]
        }
      ],
    });

    if(response) response = JSON.parse(JSON.stringify(response));
    response.noOfTotalQuestions = response.assessmentQuestions.length;
    response.noOfCorrectQuestions = response.assessmentQuestionAnswers.filter((x)=> x.isCorrect).length;
    response.noOfInCorrectQuestions = response.assessmentQuestionAnswers.filter((x)=> x.isCorrect == false).length;
    response.noOfSkippedQuestions = parseInt(response.noOfTotalQuestions - (response.noOfCorrectQuestions + response.noOfInCorrectQuestions));
    response.totalExamScore = parseInt(response.noOfTotalQuestions * response.correctMark);
    let markCalculation = (response.noOfCorrectQuestions * response.correctMark) - (response.noOfInCorrectQuestions * Math.abs(response.incorrectMark));
    response.totalScoreReceived = markCalculation > 0 ? markCalculation : 0;

    response.subjectWiseQuestionAnswers = [];
    let assessmentSubjects = response.subjectAssessmentMappings.map((x)=> x.subject.subjectName);
    assessmentSubjects.forEach((subject)=>{
      let questions = response.assessmentQuestions.filter((x) => x.subject.subjectName == subject);
      questions.forEach((question)=>{
        let questionId = question.id;
        question.isAnswered = false;
        question.isCorrect = false;
        response.assessmentQuestionAnswers.forEach((answer)=>{
          if(answer.assessmentQuestionId == questionId){
            question.isCorrect = answer.isCorrect;
            question.isAnswered = true;
          }
        });
      });
      // count & marking calculations
      let totalQuestions = questions.length;
      let isAnswered = questions.filter((x)=> x.isAnswered == true).length;
      let isSkipped = questions.filter((x)=> x.isAnswered == false).length;
      let isCorrect = questions.filter((x)=> x.isCorrect == true).length;
      let isIncorrect = questions.filter((x)=> x.isCorrect == false && x.isAnswered == true).length;

      let totalMarks = parseInt(totalQuestions * response.correctMark);
      let markObtained = (isCorrect * response.correctMark) - (isIncorrect * Math.abs(response.incorrectMark));
      
      response.subjectWiseQuestionAnswers.push({
        subjectName: subject,
        // questions: questions,
        totalQuestions: totalQuestions,
        totalAnswered: isAnswered,
        totalCorrect: isCorrect,
        totalIncorrect: isIncorrect,
        totalSkipped: isSkipped,
        totalkMarks: totalMarks,
        markObtained: markObtained
      });
      
    });
    delete response.assessmentQuestions;
    delete response.assessmentQuestionAnswers;
    delete response.subjectAssessmentMappings;
    return res.json(
      await commonResponse.response(true, Message.DATA_FOUND, response)
    );
  } catch (err) {
    const { status, message, error } = errorHandle(err);
    return res
      .status(status)
      .json(await commonResponse.response(false, message, error));
  }
};
