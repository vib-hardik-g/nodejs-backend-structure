const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Publishstatus, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;


module.exports.getNotes = async (req, res) => {
    try {
        let profileinfo = req.authData.data;
        let where = {};
        if (!req.query.searchKey) {
            where = {
                classId: profileinfo.classId,
                subjectId: req.params.subjectId,
                instituteId: profileinfo.instituteId,
                status: true,
                [Op.or]: [
                    { publishStatus: Publishstatus.PUBLISHED },
                    { publishStatus: Publishstatus.REPUBLISHED }
                ]

            }
        } else {
            where = {
                status: true,
                classId: profileinfo.classId,
                subjectId: req.params.subjectId,
                instituteId: profileinfo.instituteId,
                status: true,
                [Op.or]: [
                    { publishStatus: Publishstatus.PUBLISHED },
                    { publishStatus: Publishstatus.REPUBLISHED }
                ],
                [Op.or]: {
                    noteName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }
        }
        let notes = await models.note.findAll({
            attributes: ['id', 'noteName', 'createdAt'],
            where: where,
            include: [{
                model: models.noteAttachment,
                attributes: ['id'],
                include: [{
                    model: models.document
                }]
            }]
        })
        if (notes.length > 0) {
            return res.json(await commonResponse.response(true, Message.DATA_FOUND, notes));

        } else {
            return res.json(await commonResponse.response(false, Message.NO_DATA_FOUND, notes));

        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, Message.SOMETHING_WRONG));
    }
}