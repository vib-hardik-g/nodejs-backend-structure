const models = require('../../models');
const errorHandle = require('../../../utils/errorHandler');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');

module.exports.institutionListsByOrgId = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        
        
        let { orgId } = req.params;
        let response = await models.organisation.findOne({
            where: { id: orgId, status: true },
            include: [ "institutions" ]
        });
        if(response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        console.log(err)
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}