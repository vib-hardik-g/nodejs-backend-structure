const models = require('../../models');
const errorHandle = require('../../../utils/errorHandler');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');

module.exports.createOrgnisation = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });

        let response = await models.organisation.create(req.body)
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allOrganisations = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });

        let response = await models.organisation.findAll({
            where: { status: true },
            include: [ "institutions" ]
        })
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.organisationById = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        
        let { id } = req.params;
        let response = await models.organisation.findOne({
            where: { id, status: true },
            include: [ "institutions" ]
        });
        if(response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateOrganisation = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        
        let { id } = req.params;

        let response = await models.organisation.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteOrganisation = async (req, res) => {
    try {
        await models.organisation.sync({ force: false });
        await models.institution.sync({ force: false });
        
        let { id } = req.params;
        let response = await models.organisation.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}