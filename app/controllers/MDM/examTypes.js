const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createExamType = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { examType } = req.body;

        // Check for uniqueness
        let checkUnique = await models.examType.count({
            where: {
                examType: { [Op.iLike]: examType.trim() },
                instituteId,
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.EXAM_EXIST));


        let response = await models.examType.create({ ...req.body, instituteId });
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allExamTypes = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let where = {};
            if (!req.query.searchKey) {
                where = {
                    status: true
                }
            } else {
                where = {
                    status: true,
                    examType: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }

            where.instituteId = instituteId;

            let response = await models.examType.findAndCountAll({
                where: where,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                attributes: ['id', 'examType', 'weightage', 'createdAt', 'updatedAt'],
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            };
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            let response = await models.examType.findAll({
                where: { status: true, instituteId },
                attributes: ['id', 'examType', 'weightage', 'createdAt'],
            });
            if (response.length > 0) {
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
            } else {
                return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, []));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.examTypeById = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { id } = req.params;

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));

        let response = await models.examType.findOne({
            where: { id, status: true, instituteId },
            attributes: ['id', 'examType', 'weightage', 'createdAt'],
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateExamType = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;
        let { examType } = req.body;

        // Check for uniqueness
        let checkUnique = await models.examType.count({
            where: {
                examType: { [Op.iLike]: examType.trim() },
                instituteId,
                id: { [Op.ne]: id }
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.EXAM_EXIST));

        let response = await models.examType.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id,
                    instituteId
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteExamType = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let response = await models.examType.update({ status: false }, {
            where: { id, instituteId }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteExamTypeMsg = async (req, res) => {
    try {
        await models.examType.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;
        // Check if chapter is relation with another tables
        let relativeTables = [];
        // let Doubt = await models.doubt.count({ where: { topicId: id, status: true } });
        // if (Doubt) {
        //     relativeTables.push("Doubt");
        // }
        


        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This chapter is linked with ${modules} module(s). Are you sure you want to delete this chapter?`));
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}