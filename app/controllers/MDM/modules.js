const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createModule = async ( req, res ) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let response = await models.module.create(req.body);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error)); 
    }
}

module.exports.allModules = async ( req, res ) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let response = await models.module.findAll({ 
            where: { status: true },
            include: ['submodules']
        });
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error)); 
    }
}

module.exports.moduleById = async ( req, res ) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;
        let response = await models.module.findOne({ 
            where: { id, status: true },
            include: ['submodules']
        });
        if(response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error)); 
    }
}

module.exports.updateModule = async (req, res ) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;

        let response = await models.module.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteModule = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;
        let response = await models.module.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}