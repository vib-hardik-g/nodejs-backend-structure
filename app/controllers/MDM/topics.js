const asyncL = require('async');
const { Op } = require('sequelize');

const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const _ = require('lodash');

module.exports.createTopic = async (req, res) => {
    try {
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });

        let { classId, subjectId, topics } = req.body;
        let { instituteId } = req.authData.data;

        // Check for uniqueness
        for (let topic of topics) {
            // check for unique chapters only for creation
            let checkUniqueChapters = await models.topic.count({
                where: {
                    instituteId,
                    classId,
                    subjectId
                },
                include: [{
                    model: models.topicDetails,
                    where: {
                        chapter: { [Op.iLike]: topic.chapterName.trim() },
                    },
                    required: true
                }]
            });
            if (checkUniqueChapters) return res.status(400).json(await commonResponse.response(false, Batch.CHAPTER_EXIST, {
                chapterName: topic.chapterName
            }));
        }

        let topic = await models.topic.create({
            instituteId: instituteId,
            classId,
            subjectId
        });

        if (topic) {
            let topicId = topic.id;
            if (topics && topics.length > 0) {
                asyncL.each(topics, (chapter, callback) => {
                    (async () => {
                        let topicDetails = await models.topicDetails.create({
                            topicId,
                            chapter: chapter.chapterName
                        });

                        if (topicDetails) {
                            let topicDetailsId = topicDetails.id;

                            if (chapter.subTopics && chapter.subTopics.length > 0) {
                                let subTopics = chapter.subTopics.map((element) => {
                                    return {
                                        topicDetailsId,
                                        subTopicName: element.subTopicName,
                                        allotedHours: element.allotedHours
                                    };
                                });

                                await models.subTopic.bulkCreate(subTopics);
                                callback();
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    })();
                }, async (err) => {
                    return res.send(await commonResponse.response(true, Message.DATA_CREATED));
                });
            } else {
                return res.send(await commonResponse.response(true, Message.DATA_CREATED));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.DATA_NOT_CREATED));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allTopics = async (req, res) => {
    try {
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, []));

        let topics = await models.topic.findAll({ attributes: ['id'], where: { status: true, instituteId } });

        if (topics && topics.length > 0) {
            let topicIds = JSON.parse(JSON.stringify(topics));
            topicIds = topicIds.map(data => data.id);

            if (req.query.pageSize && req.query.pageNo) {
                Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
                Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

                let orderBy = [];
                if (req.query.sortBy && req.query.orderBy) {
                    orderBy.push([req.query.sortBy, req.query.orderBy]);
                } else {
                    orderBy.push(['updatedAt', 'DESC']);
                }

                let where = {};
                if (!req.query.searchKey) {
                    where = {
                        status: true
                    }
                } else {
                    where = {
                        status: true,
                        [Op.or]: {
                            chapter: { [Op.iLike]: '%' + req.query.searchKey + '%' }
                        }
                    }
                }

                where.topicId = {
                    [Op.in]: topicIds
                };

                let response = await models.topicDetails.findAndCountAll({
                    where: where,
                    order: orderBy,
                    offset: parseInt(Constant.DEFAULT_PAGE),
                    limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                    attributes: ['chapter', 'createdAt', 'updatedAt'],
                    include: [
                        {
                            model: models.topic,
                            attributes: [['id', 'topicId']],
                            required: true,
                            include: [
                                {
                                    model: models.class,
                                    attributes: ['className']
                                },
                                {
                                    model: models.subject,
                                    attributes: ['subjectName']
                                },
                            ]
                        }
                    ]
                });

                let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
                let result = {
                    rows: response.rows,
                    total: response.count
                }
                return res.send(await commonResponse.response(true, msg, result));
            } else {
                let response = await models.topicDetails.findAll({
                    where: {
                        topicId: {
                            [Op.in]: topicIds
                        },
                        status: true
                    },
                    attributes: ['id', 'chapter'],
                    order: [['updatedAt', 'DESC']]
                });

                return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
            }
        } else {
            return res.send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.topicById = async (req, res) => {
    try {
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });

        let { id } = req.params;

        let response = await models.topic.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.class,
                    attributes: ['className']
                },
                {
                    model: models.subject,
                    attributes: ['subjectName']
                },
                {
                    model: models.topicDetails,
                    attributes: [['id', 'topicDetailsId'], ['chapter', 'chapterName']],
                    required: false,
                    include: {
                        model: models.subTopic,
                        attributes: [['id', 'subTopicId'], 'subTopicName', 'allotedHours']
                    }
                }
            ]
        });

        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateTopic = async (req, res) => {
    try {
        await models.topic.sync({ force: false });
        await models.topicDetails.sync({ force: false });
        await models.subTopic.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { id } = req.params;
        let { topics } = req.body;

        let topicDetails = await models.topic.findOne({
            where: {
                id,
            }
        });
        if (!topicDetails) return res.status(404).json(await commonResponse.response(false, Message.NO_DATA_FOUND));

        for (let topic of topics) {
            if (topic.topicDetailsId) {
                // records needs to be updated
                await models.topicDetails.update({
                    chapter: topic.chapterName.trim(),
                    updatedAt: new Date(),
                }, {
                    where: {
                        id: topic.topicDetailsId,
                        topicId: id,
                    }
                });

                // loop through sub topics
                for (let subTopic of topic.subTopics) {
                    if (subTopic.subTopicId) {
                        // update
                        await models.subTopic.update({
                            subTopicName: subTopic.subTopicName,
                            allotedHours: subTopic.allotedHours,
                            updatedAt: new Date(),
                        }, {
                            where: {
                                id: subTopic.subTopicId,
                                topicDetailsId: topic.topicDetailsId
                            }
                        })
                    } else {
                        // create new sub topic
                        // only insert if unique
                        let checkUniqueCount = await models.subTopic.count({
                            where: {
                                subTopicName: { [Op.iLike]: subTopic.subTopicName },
                                topicDetailsId: topic.topicDetailsId
                            }
                        });
                        if (!checkUniqueCount) {
                            await models.subTopic.create({
                                subTopicName: subTopic.subTopicName,
                                allotedHours: subTopic.allotedHours,
                                topicDetailsId: topic.topicDetailsId
                            });
                        }
                    }
                }

            } else {
                // new records needs to be inserted
                // check for unique chapters only for creation
                let checkUniqueChapters = await models.topic.count({
                    where: {
                        instituteId,
                        classId: topicDetails.classId,
                        subjectId: topicDetails.subjectId
                    },
                    include: [{
                        model: models.topicDetails,
                        where: {
                            chapter: { [Op.iLike]: topic.chapterName.trim() },
                        },
                        required: true
                    }]
                });
                if (checkUniqueChapters) return res.status(400).json(await commonResponse.response(false, Message.NOT_UNIQUE, {
                    chapterName: topic.chapterName
                }));
                let nameOfSubTopics = topic.subTopics.map((item) => item.subTopicName);
                if (nameOfSubTopics.length != _.uniq(nameOfSubTopics).length) {
                    let duplicateItem = _.filter(nameOfSubTopics, (val, i, iteratee) => _.includes(iteratee, val, i + 1));
                    return res.status(400).json(await commonResponse.response(false, Message.NOT_UNIQUE, {
                        subTopicName: _.uniq(duplicateItem)
                    }));
                }
                let topicDetailsCreated = await models.topicDetails.create({
                    topicId: id,
                    chapter: topic.chapterName
                });

                if (topicDetailsCreated) {
                    let topicDetailsId = topicDetailsCreated.id;
                    if (topic.subTopics && topic.subTopics.length > 0) {
                        let subTopics = topic.subTopics.map((element) => {
                            return {
                                topicDetailsId,
                                subTopicName: element.subTopicName,
                                allotedHours: element.allotedHours
                            }
                        });
                        await models.subTopic.bulkCreate(subTopics);
                    }
                }


            }
        }
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteTopic = async (req, res) => {
    try {
        await models.topic.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if chapter is relation with another tables

        // let Doubt = await models.doubt.count({ where: { topicId: id, status: true } });
        // if (Doubt) {
        //     relativeTables.push("Doubt");
        // }



        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This chapter is linked with ${modules} module(s). Are you sure you want to delete this chapter?`));
        } else {
            let response = await models.topic.update({ status: false }, {
                where: { id, instituteId: instituteId }
            });
        }
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteTopicMsg = async (req, res) => {
    try {
        await models.topic.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if chapter is relation with another tables

        // let Doubt = await models.doubt.count({ where: { topicId: id, status: true } });
        // if (Doubt) {
        //     relativeTables.push("Doubt");
        // }



        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This chapter is linked with ${modules} module(s). Are you sure you want to delete this chapter?`));
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}