const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.createBatch = async (req, res) => {
    try {
        await models.batch.sync({ force: false });
        await models.batchSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { data } = req.body;

        // Check for uniqueness
        for (let item of data) {
            let checkUnique = await models.batch.count({
                where: {
                    batchName: { [Op.iLike]: item.batchName.trim() },
                    classId: item.classId,
                    instituteId,
                }
            });
            if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.BATCH_EXIST, {
                batchName: item.batchName
            }));
        }

        for (item of data) {
            let subject = [];
            let response = await models.batch.create({
                batchName: item.batchName,
                classId: item.classId,
                instituteId
            });

            item.subjectIds.forEach(subjectId => {
                subject.push({
                    instituteId,
                    batchId: response.id,
                    subjectId: subjectId,
                });
            });
            await models.batchSubjectMapping.bulkCreate(subject);
        }
        return res.send(await commonResponse.response(true, Batch.CREATED));

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allBatches = async (req, res) => {
    try {
        await models.batch.sync({ force: false });
        await models.batchSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id']
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND, []));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;


            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let whereClause = {};
            if (!req.query.searchKey) {
                whereClause = {
                    status: true
                }
            } else {
                whereClause = {
                    status: true,
                    [Op.or]: {
                        batchName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$subjects.subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    }
                }
            }

            whereClause.instituteId = instituteId;

            if (req.query.classId) {
                whereClause.classId = req.query.classId
            }
            let response = await models.batch.findAndCountAll({
                where: whereClause,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                distinct: true,
                attributes: ['id', 'batchName', 'updatedAt', 'createdAt'],
                include: [
                    {
                        model: models.batchSubjectMapping,
                        as: 'subjects',
                        attributes: ['id'],
                        include: [{
                            model: models.subject,
                            attributes: ['id', 'subjectName', 'icon'],
                        }]
                    },
                    {
                        model: models.class,
                        attributes: ['className', 'id', 'updatedAt', 'createdAt'],
                    }
                ],
                subQuery: false,
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            let response = await models.batch.findAll({
                where: {
                    status: true,
                    instituteId: instituteId
                },
                attributes: ['id', 'batchName'],
                order: [['updatedAt', 'DESC']]
            });
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        }
    } catch (err) {
        console.log(err);
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.batchById = async (req, res) => {
    try {
        await models.batch.sync({ force: false });
        await models.batchSubjectMapping.sync({ force: false });

        let { id } = req.params;

        let response = await models.batch.findOne({
            attributes: ['id', 'batchName', 'updatedAt'],
            where: { status: true, id },
            include: [
                {
                    model: models.batchSubjectMapping,
                    as: 'subjects',
                    attributes: ['id'],
                    include: [{
                        model: models.subject,
                        attributes: ['id', 'subjectName', 'icon']
                    }]
                },
                {
                    model: models.class,
                    attributes: ['className', 'id']
                }
            ]
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateBatch = async (req, res) => {
    try {
        await models.batch.sync({ force: false });

        let { id } = req.params;
        let subjectIds = req.body.subjectIds;
        let { instituteId } = req.authData.data;
        let { batchName } = req.body;
        let batchRes = await models.batch.findOne({
            where: { id }
        })
        let checkUnique = await models.batch.count({
            where: {
                batchName: { [Op.iLike]: batchName.trim() },
                classId: batchRes.classId,
                instituteId,
                id:{[Op.ne]:id}
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.BATCH_EXIST));

        let response = await models.batch.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        );
        await models.batchSubjectMapping.destroy({
            where: {
                batchId: id
            }
        });

        let mappings = subjectIds.map(val => {
            return { instituteId: instituteId, subjectId: val, batchId: id }
        });
        await models.batchSubjectMapping.bulkCreate(mappings);
        return res.send(await commonResponse.response(true, Batch.UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteBatch = async (req, res) => {
    try {
        await models.batch.sync({ force: false });
        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Batch is relation with another tables
        let checkBatchAssignmentMapping = await models.batchAssignmentMapping.count({ where: { batchId: id, status: true } });
        if (checkBatchAssignmentMapping) {
            relativeTables.push("Batch Assignment Mapping");
        }

        let BatchNoteMapping = await models.batchNoteMapping.count({ where: { batchId: id, status: true } });
        if (BatchNoteMapping) {
            relativeTables.push("Batch Note Mapping");
        }

        // let BatchSubjectMapping = await models.batchSubjectMapping.count({ where: { batchId: id, status: true } });
        // if (BatchSubjectMapping) {
        //     relativeTables.push("Batch Subject Mapping");
        // }

        let FeedbackFormClassBatch = await models.feedbackFormClassBatch.count({ where: { batchId: id, status: true } });
        if (FeedbackFormClassBatch) {
            relativeTables.push("Feedback Form Class Batch");
        }

        let GroupBatch = await models.groupBatch.count({ where: { batchId: id, status: true } });
        if (GroupBatch) {
            relativeTables.push("Group Batch");
        }

        // let ReportCard = await models.reportCard.count({ where: { batchId: id, status: true } });
        // if (ReportCard) {
        //     relativeTables.push("Report Card");
        // }

        let StaffProgressReport = await models.staffProgressReport.count({ where: { batchId: id, status: true } });
        if (StaffProgressReport) {
            relativeTables.push("Staff Progress Report");
        }

        let StudentBatchUniqueId = await models.studentBatchUniqueId.count({ where: { batchId: id, status: true } });
        if (StudentBatchUniqueId) {
            relativeTables.push("StudentBatchUniqueId");
        }

        // let SubmittedFeedbackForm = await models.submittedFeedbackForm.count({ where: { batchId: id, status: true } });
        // if (SubmittedFeedbackForm) {
        //     relativeTables.push("Submitted Feedback Form");
        // }

        let TimeTable = await models.timeTable.count({ where: { batchId: id, status: true } });
        if (TimeTable) {
            relativeTables.push("Time Table");
        }

        // let checkSubjectTopicBatchMapping = await models.subjectTopicBatchMapping.count({ where: { batchId: id, status: true } });
        // if (checkSubjectTopicBatchMapping) {
        //     relativeTables.push("Subject Topic Batch Mapping");
        // }

        let checkStudentAttendanceManager = await models.studentAttendanceManager.count({ where: { batchId: id, status: true } });
        if (checkStudentAttendanceManager) {
            relativeTables.push("Student Attandence Manager");
        }

        let StaffClassBatchMapping = await models.staffClassBatchMapping.count({ where: { batchId: id, status: true } });
        if (StaffClassBatchMapping) {
            relativeTables.push("Staff Class Batch Mapping");
        }

        let checkUserStudentMapping = await models.userStudentMapping.count({ where: { batchId: id, status: true } });
        if (checkUserStudentMapping) {
            relativeTables.push("User Sudent Mapping");
        }

        let checkStudentBulkUploadHistory = await models.studentBulkUploadHistory.count({ where: { batchId: id, status: true } });
        if (checkStudentBulkUploadHistory) {
            relativeTables.push("Student Bulk Upload Template History");
        }

        // let UserInstituteMapping = await models.userInstituteMapping.count({ where: { batchId: id, status: true } });
        // if (UserInstituteMapping) {
        //     relativeTables.push("User Institute Mapping");
        // }

        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This batch is linked with ${modules} module(s). Are you sure you want to delete this batch?`));
        } else {
            await models.batch.update({ status: false }, {
                where: { id, instituteId: instituteId }
            });
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteBatchMsg = async (req, res) => {
    try {
        await models.batch.sync({ force: false });
        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Batch is relation with another tables
        let checkBatchAssignmentMapping = await models.batchAssignmentMapping.count({ where: { batchId: id, status: true } });
        if (checkBatchAssignmentMapping) {
            relativeTables.push("Batch Assignment Mapping");
        }

        let BatchNoteMapping = await models.batchNoteMapping.count({ where: { batchId: id, status: true } });
        if (BatchNoteMapping) {
            relativeTables.push("Batch Note Mapping");
        }

        let FeedbackFormClassBatch = await models.feedbackFormClassBatch.count({ where: { batchId: id, status: true } });
        if (FeedbackFormClassBatch) {
            relativeTables.push("Feedback Form Class Batch");
        }

        let GroupBatch = await models.groupBatch.count({ where: { batchId: id, status: true } });
        if (GroupBatch) {
            relativeTables.push("Group Batch");
        }


        let StaffProgressReport = await models.staffProgressReport.count({ where: { batchId: id, status: true } });
        if (StaffProgressReport) {
            relativeTables.push("Staff Progress Report");
        }

        let StudentBatchUniqueId = await models.studentBatchUniqueId.count({ where: { batchId: id, status: true } });
        if (StudentBatchUniqueId) {
            relativeTables.push("StudentBatchUniqueId");
        }


        let TimeTable = await models.timeTable.count({ where: { batchId: id, status: true } });
        if (TimeTable) {
            relativeTables.push("Time Table");
        }


        let checkStudentAttendanceManager = await models.studentAttendanceManager.count({ where: { batchId: id, status: true } });
        if (checkStudentAttendanceManager) {
            relativeTables.push("Student Attandence Manager");
        }

        let StaffClassBatchMapping = await models.staffClassBatchMapping.count({ where: { batchId: id, status: true } });
        if (StaffClassBatchMapping) {
            relativeTables.push("Staff Class Batch Mapping");
        }

        let checkUserStudentMapping = await models.userStudentMapping.count({ where: { batchId: id, status: true } });
        if (checkUserStudentMapping) {
            relativeTables.push("User Sudent Mapping");
        }

        let checkStudentBulkUploadHistory = await models.studentBulkUploadHistory.count({ where: { batchId: id, status: true } });
        if (checkStudentBulkUploadHistory) {
            relativeTables.push("Student Bulk Upload Template History");
        }


        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This batch is linked with ${modules} module(s). Are you sure you want to delete this batch?`));
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}