const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createClass = async (req, res) => {
    try {
        await models.class.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { className } = req.body;

        // Check for uniqueness
        let checkUnique = await models.class.count({
            where: {
                className: { [Op.iLike]: className.trim() },
                instituteId,
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.CLASS_EXIST));

        let response = await models.class.create({ ...req.body, instituteId });
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allClasses = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.institution.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id']
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND, []));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let where = {};
            if (!req.query.searchKey) {
                where = {
                    status: true
                }
            } else {
                where = {
                    status: true,
                    [Op.or]: {
                        className: { [Op.iLike]: '%' + req.query.searchKey + '%' }
                    }
                }
            }

            where.instituteId = instituteId;

            let response = await models.class.findAndCountAll({
                where: where,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                include: [
                    {
                        model: models.institution,
                        attributes: ['institutionName']
                    }
                ]
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            let response = await models.class.findAll({
                where: {
                    instituteId, status: true
                },
                include: [
                    {
                        model: models.institution,
                        attributes: ['institutionName']
                    }
                ],
                order: [['createdAt', 'DESC']]
            });

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.classById = async (req, res) => {
    try {
        await models.class.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let response = await models.class.findOne({
            where: { id, status: true, instituteId: instituteId },
            include: {
                model: models.institution,
                attributes: ['institutionName']
            }
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateClass = async (req, res) => {
    try {
        await models.class.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;
        let { className } = req.body;
        let checkUnique = await models.class.count({
            where: {
                className: { [Op.iLike]: className.trim() },
                instituteId,
                id:{[Op.ne]:id}
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.CLASS_EXIST));


        let response = await models.class.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id,
                    instituteId: instituteId
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteClass = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.classSubjectMapping.sync({ force: false });
        await models.section.sync({ force: false });
        await models.group.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBulkUploadHistory.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.subjectTopicBatchMapping.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let relativeTables = [];

        // Check if class is relation with another tables
        let checkGroup = await models.group.count({ where: { classId: id, status: true } });
        if (checkGroup) {
            relativeTables.push("Group");
        }

        let checkTopic = await models.topic.count({ where: { classId: id, status: true } });
        if (checkTopic) {
            relativeTables.push("Topic");
        }

        let checkSubjectTopicBatchMapping = await models.subjectTopicBatchMapping.count({ where: { classId: id, status: true } });
        if (checkSubjectTopicBatchMapping) {
            relativeTables.push("Subject Topic Batch Mapping");
        }

        let checkStudentAttendanceManager = await models.studentAttendanceManager.count({ where: { classId: id, status: true } });
        if (checkStudentAttendanceManager) {
            relativeTables.push("Student Attandence Manager");
        }

        let checkClassSubjectMapping = await models.classSubjectMapping.count({ where: { classId: id, status: true } });
        if (checkClassSubjectMapping) {
            relativeTables.push("Class Subject Mapping");
        }

        let checkStaffClassMapping = await models.staffClassMapping.count({ where: { classId: id, status: true } });
        if (checkStaffClassMapping) {
            relativeTables.push("Staff Class Mapping");
        }

        let checkUserStudentMapping = await models.userStudentMapping.count({ where: { classId: id, status: true } });
        if (checkUserStudentMapping) {
            relativeTables.push("User Sudent Mapping");
        }

        let checkStudentBulkUploadHistory = await models.studentBulkUploadHistory.count({ where: { classId: id, status: true } });
        if (checkStudentBulkUploadHistory) {
            relativeTables.push("Student Bulk Upload Template History");
        }

        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This class is linked with ${modules} module(s). Are you sure you want to delete this class?`));
        } else {
            // Remove class (Update class status)
            await models.class.update({ status: false }, {
                where: { id, instituteId: instituteId }
            });
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteClassMsg = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.classSubjectMapping.sync({ force: false });
        await models.section.sync({ force: false });
        await models.group.sync({ force: false });
        await models.staffClassMapping.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.studentBulkUploadHistory.sync({ force: false });
        await models.studentAttendanceManager.sync({ force: false });
        await models.topic.sync({ force: false });
        await models.subjectTopicBatchMapping.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let relativeTables = [];

        // Check if class is relation with another tables
        let checkGroup = await models.group.count({ where: { classId: id, status: true } });
        if (checkGroup) {
            relativeTables.push("Group");
        }

        let checkTopic = await models.topic.count({ where: { classId: id, status: true } });
        if (checkTopic) {
            relativeTables.push("Topic");
        }

        let checkSubjectTopicBatchMapping = await models.subjectTopicBatchMapping.count({ where: { classId: id, status: true } });
        if (checkSubjectTopicBatchMapping) {
            relativeTables.push("Subject Topic Batch Mapping");
        }

        let checkStudentAttendanceManager = await models.studentAttendanceManager.count({ where: { classId: id, status: true } });
        if (checkStudentAttendanceManager) {
            relativeTables.push("Student Attandence Manager");
        }

        let checkClassSubjectMapping = await models.classSubjectMapping.count({ where: { classId: id, status: true } });
        if (checkClassSubjectMapping) {
            relativeTables.push("Class Subject Mapping");
        }

        let checkStaffClassMapping = await models.staffClassMapping.count({ where: { classId: id, status: true } });
        if (checkStaffClassMapping) {
            relativeTables.push("Staff Class Mapping");
        }

        let checkUserStudentMapping = await models.userStudentMapping.count({ where: { classId: id, status: true } });
        if (checkUserStudentMapping) {
            relativeTables.push("User Sudent Mapping");
        }

        let checkStudentBulkUploadHistory = await models.studentBulkUploadHistory.count({ where: { classId: id, status: true } });
        if (checkStudentBulkUploadHistory) {
            relativeTables.push("Student Bulk Upload Template History");
        }

        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This class is linked with ${modules} module(s). Are you sure you want to delete this class?`));
        } else {
            // Remove class (Update class status)
            // await models.class.update({ status: false }, {
            //     where: { id, instituteId: instituteId }
            // });
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}