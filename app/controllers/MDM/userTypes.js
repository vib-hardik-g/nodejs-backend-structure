const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createUserType = async (req, res) => {
    try {
        await models.userType.sync({ force: false });

        let response = await models.userType.create(req.body)
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allUserTypes = async (req, res) => {
    try {
        await models.userType.sync({ force: false });

        let response = await models.userType.findAll({
            where: { status: true }
        })
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.userTypeById = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        
        let { id } = req.params;
        let response = await models.userType.findOne({
            where: { id, status: true },
        });
        if(response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateUserType = async (req, res) => {
    try {
        await models.userType.sync({ force: false });

        let { id } = req.params;
        let response = await models.userType.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteUserType = async (req, res) => {
    try {
        await models.userType.sync({ force: false });
        
        let { id } = req.params;
        let response = await models.userType.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}