const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createSubject = async (req, res) => {
    try {
        await models.subject.sync({ force: false });
        await models.classSubjectMapping.sync({ force: false });
        let { instituteId } = req.authData.data;
        let { subjectName, icon, classIds } = req.body;

        // Check for uniqueness
        for (let classId of classIds) {
            let checkUnique = await models.classSubjectMapping.count({
                where: {
                    classId,
                    instituteId,
                },
                include: [{
                    model: models.subject,
                    where: {
                        subjectName: { [Op.iLike]: subjectName.trim() },
                    },
                }]
            });
            if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.SUBJECT_EXIST, {
                subjectName,
                classId,
            }));
        }

        let response = await models.subject.create({ subjectName, icon, instituteId: instituteId });
        let mappings = classIds.map(val => {
            return { instituteId: instituteId, classId: val, subjectId: response.id }
        });
        await models.classSubjectMapping.bulkCreate(mappings);

        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allSubjects = async (req, res) => {
    try {
        await models.subject.sync({ force: false });
        await models.classSubjectMapping.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, []));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['subjectId', 'DESC']);
            }

            let where = {};
            if (!req.query.searchKey) {
                where = {
                    status: true
                }
            } else {
                where = {
                    status: true,
                    [Op.or]: {
                        '$subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    }
                }
            }


            if (req.query.classId) {
                where.classId = req.query.classId
            }

            where.instituteId = instituteId;

            let results = await models.classSubjectMapping.findAll({
                where: where,
                attributes: ['subjectId'],
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                include: [{
                    model: models.subject,
                    where: { status: true }
                }, {

                    model: models.class,
                    // where: whereClass,
                    attributes: []
                }],
                // order: [['subjectId', 'asc']],
                group: ['subjectId', 'subject.id'],
                subQuery: false,

            })

            if (results && results.length > 0) {
                results = JSON.parse(JSON.stringify(results));

                for (let i = 0; i < results.length; i++) {

                    let element = results[i];

                    let subjectId = element.subjectId;

                    let classes = await models.classSubjectMapping.findAll({
                        where: {
                            instituteId: instituteId,
                            subjectId: subjectId
                        },
                        include: [{
                            model: models.class,
                            // where: whereClass,
                        }]
                    })


                    // Find class using institute and subjectId

                    results[i].subject.classSubjectMappings = classes;
                }
                let resultsCount = await models.classSubjectMapping.count({
                    where: where,
                    attributes: ['subjectId'],
                    include: [{
                        model: models.subject,
                        where: { status: true }
                    }, {

                        model: models.class,
                        attributes: []
                    }],
                    group: ['subjectId', 'subject.id'],
                    subQuery: false,

                })
                let msg = results.length ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
                let result = {
                    rows: results,
                    total: resultsCount.length,
                }
                return res.send(await commonResponse.response(true, msg, result));
            }
        } else {
            let response = await models.subject.findAll({
                where: {
                    instituteId: instituteId,
                    status: true
                },
                include: [
                    {
                        model: models.institution,
                        attributes: ['institutionName']
                    }
                ],
                order: [['createdAt', 'DESC']]
            });

            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.subjectById = async (req, res) => {
    try {
        await models.subject.sync({ force: false });
        await models.classSubjectMapping.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let response = await models.subject.findOne({
            where: { id, instituteId: instituteId, status: true },
            include: [{
                model: models.institution,
                attributes: ['institutionName']
            }, {
                model: models.classSubjectMapping,
                include: [
                    {
                        model: models.class
                    }
                ]
            }]
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateSubject = async (req, res) => {
    try {
        await models.subject.sync({ force: false });

        let { id } = req.params;
        let classIds = req.body.classIds;
        let { instituteId } = req.authData.data;
        let { subjectName } = req.body;

        for (let classId of classIds) {
            let checkUnique = await models.classSubjectMapping.count({
                where: {
                    classId,
                    instituteId,
                },
                include: [{
                    model: models.subject,
                    where: {
                        id: { [Op.ne]: id },
                        subjectName: { [Op.iLike]: subjectName.trim() },
                    },
                }]
            });
            if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.SUBJECT_EXIST, {
                subjectName,
                classId,
            }));
        }

        let response = await models.subject.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )

        await models.classSubjectMapping.destroy({
            where: {
                subjectId: id,
                instituteId: instituteId
            }
        });

        let mappings = classIds.map(val => {
            return { instituteId: instituteId, classId: val, subjectId: id }
        });
        await models.classSubjectMapping.bulkCreate(mappings);

        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteSubject = async (req, res) => {
    try {
        await models.subject.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Subject is relation with another tables
        let Assignment = await models.assignment.count({ where: { subjectId: id, status: true } });
        if (Assignment) {
            relativeTables.push("Assignment");
        }

        // let Doubt = await models.doubt.count({ where: { subjectId: id, status: true } });
        // if (Doubt) {
        //     relativeTables.push("Doubt");
        // }

        // let InstitutionSubject = await models.institutionSubject.count({ where: { subjectId: id, status: true } });
        // if (InstitutionSubject) {
        //     relativeTables.push("Institution Subject");
        // }

        let Note = await models.note.count({ where: { subjectId: id, status: true } });
        if (Note) {
            relativeTables.push("Note");
        }

        let BatchSubjectMapping = await models.batchSubjectMapping.count({ where: { subjectId: id, status: true } });
        if (BatchSubjectMapping) {
            relativeTables.push("Batch Subject Mapping");
        }

        let Topic = await models.topic.count({ where: { subjectId: id, status: true } });
        if (Topic) {
            relativeTables.push("Topic");
        }

        let GroupSubject = await models.groupSubject.count({ where: { subjectId: id, status: true } });
        if (GroupSubject) {
            relativeTables.push("Group Subject");
        }

        // let ReportCard = await models.reportCard.count({ where: { subjectId: id, status: true } });
        // if (ReportCard) {
        //     relativeTables.push("Report Card");
        // }

        let StaffProgressReport = await models.staffProgressReport.count({ where: { subjectId: id, status: true } });
        if (StaffProgressReport) {
            relativeTables.push("Staff Progress Report");
        }

        // let SubmittedFeedbackForm = await models.submittedFeedbackForm.count({ where: { subjectId: id, status: true } });
        // if (SubmittedFeedbackForm) {
        //     relativeTables.push("Submitted Feedback Form");
        // }

        let TimeTableDetail = await models.timeTableDetail.count({ where: { subjectId: id, status: true } });
        if (TimeTableDetail) {
            relativeTables.push("Time Table Detail");
        }

        // let checkSubjectTopicBatchMapping = await models.subjectTopicBatchMapping.count({ where: { subjectId: id, status: true } });
        // if (checkSubjectTopicBatchMapping) {
        //     relativeTables.push("Subject Topic Batch Mapping");
        // }


        let StaffClassBatchSubjectMapping = await models.staffClassBatchSubjectMapping.count({ where: { subjectId: id, status: true } });
        if (StaffClassBatchSubjectMapping) {
            relativeTables.push("Staff Class Batch Subject Mapping");
        }

        // let UserInstituteMapping = await models.userInstituteMapping.count({ where: { subjectId: id, status: true } });
        // if (UserInstituteMapping) {
        //     relativeTables.push("User Institute Mapping");
        // }

        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This subject is linked with ${modules} module(s). Are you sure you want to delete this subject?`));
        } else {

            await models.subject.update(
                { status: false },
                {
                    where: { id, instituteId: instituteId }
                }
            );
            await models.classSubjectMapping.update({ status: false }, {
                where: { subjectId: id }
            });
        }
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteSubjectMsg = async (req, res) => {
    try {
        await models.subject.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Subject is relation with another tables
        let Assignment = await models.assignment.count({ where: { subjectId: id, status: true } });
        if (Assignment) {
            relativeTables.push("Assignment");
        }

        // let Doubt = await models.doubt.count({ where: { subjectId: id, status: true } });
        // if (Doubt) {
        //     relativeTables.push("Doubt");
        // }


        let Note = await models.note.count({ where: { subjectId: id, status: true } });
        if (Note) {
            relativeTables.push("Note");
        }

        let BatchSubjectMapping = await models.batchSubjectMapping.count({ where: { subjectId: id, status: true } });
        if (BatchSubjectMapping) {
            relativeTables.push("Batch Subject Mapping");
        }

        let Topic = await models.topic.count({ where: { subjectId: id, status: true } });
        if (Topic) {
            relativeTables.push("Topic");
        }

        let GroupSubject = await models.groupSubject.count({ where: { subjectId: id, status: true } });
        if (GroupSubject) {
            relativeTables.push("Group Subject");
        }

        // let ReportCard = await models.reportCard.count({ where: { subjectId: id, status: true } });
        // if (ReportCard) {
        //     relativeTables.push("Report Card");
        // }

        let StaffProgressReport = await models.staffProgressReport.count({ where: { subjectId: id, status: true } });
        if (StaffProgressReport) {
            relativeTables.push("Staff Progress Report");
        }

        let TimeTableDetail = await models.timeTableDetail.count({ where: { subjectId: id, status: true } });
        if (TimeTableDetail) {
            relativeTables.push("Time Table Detail");
        }

        let StaffClassBatchSubjectMapping = await models.staffClassBatchSubjectMapping.count({ where: { subjectId: id, status: true } });
        if (StaffClassBatchSubjectMapping) {
            relativeTables.push("Staff Class Batch Subject Mapping");
        }
        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This subject is linked with ${modules} module(s). Are you sure you want to delete this subject?`));
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }

    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}