const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createSubModule = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { moduleId, subModuleName, userTypeId } = req.body;
        let response = await models.subModule.create({ moduleId, subModuleName });
        if(userTypeId) {
            await models.userTypeModuleMapping.create({ 
                userTypeId,
                moduleId, 
                subModuleId: response.id
            });
        }
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allSubModules = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let response = await models.subModule.findAll({
            where: { status: true },
            include: ['module']
        });
        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.subModuleById = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;

        let response = await models.subModule.findOne({
            where: { id, status: true },
            include: ['module']
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateSubModule = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;

        let response = await models.subModule.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        );
        if(response) {
            let { userTypeId, moduleId,  } = response;
            await models.userTypeModuleMapping.destroy({ where: { id } });
            await models.userTypeModuleMapping.create({ 
                userTypeId,
                moduleId, 
                subModuleId: id
            });
        }
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteSubModule = async (req, res) => {
    try {
        await models.module.sync({ force: false });
        await models.subModule.sync({ force: false });
        await models.userTypeModuleMapping.sync({ force: false });

        let { id } = req.params;
        let response = await models.subModule.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}