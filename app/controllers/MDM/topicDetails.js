const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createTopicDetails = async (req, res) => {
    try {
        await models.topicDetails.sync({ force: false });

        let response = await models.topicDetails.create(req.body);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allTopicDetails = async (req, res) => {
    try {
        await models.topicDetails.sync({ force: false });

        let whereClause = {
            status: true
        };

        if (req.body.institutionId) {
            whereClause.institutionId = req.body.institutionId;
        }

        let response = await models.topicDetails.findAll({
            where: whereClause,
            include: {
                model: models.topic
            }
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.topicDetailsById = async (req, res) => {
    try {
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;

        let response = await models.topicDetails.findOne({
            where: { id, status: true },
            include: {
                model: models.topic
            }
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateTopicDetails = async (req, res) => {
    try {
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;

        let response = await models.topicDetails.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteTopicDetails = async (req, res) => {
    try {
        await models.topicDetails.sync({ force: false });

        let { id } = req.params;
        let response = await models.topicDetails.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}