const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createSubTopic = async (req, res) => {
    try {
        await models.subTopic.sync({ force: false });

        let response = await models.subTopic.create(req.body);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allSubTopic = async (req, res) => {
    try {
        await models.subTopic.sync({ force: false });

        let whereClause = {
            status: true
        };

        if (req.body.institutionId) {
            whereClause.institutionId = req.body.institutionId;
        }

        let response = await models.subTopic.findAll({
            where: whereClause,
            include: {
                model: models.topicDetails
            }
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.subTopicById = async (req, res) => {
    try {
        await models.subTopic.sync({ force: false });

        let { id } = req.params;

        let response = await models.subTopic.findOne({
            where: { id, status: true },
            include: {
                model: models.topicDetails
            }
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateSubTopic = async (req, res) => {
    try {
        await models.subTopic.sync({ force: false });

        let { id } = req.params;

        let response = await models.subTopic.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteSubTopic = async (req, res) => {
    try {
        await models.subTopic.sync({ force: false });

        let { id } = req.params;
        let response = await models.subTopic.update({ status: false }, {
            where: { id }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}