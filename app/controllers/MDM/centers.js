const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createCenter = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { centerName } = req.body;

        let checkUnique = await models.center.count({
            where: {
                centerName: { [Op.iLike]: centerName.trim() },
                instituteId,
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.CENTER_EXIST));

        let response = await models.center.create({ ...req.body, instituteId });
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allCenters = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let where = {};
            if (!req.query.searchKey) {
                where = {
                    status: true
                }
            } else {
                where = {
                    status: true,
                    centerName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                }
            }

            where.instituteId = instituteId;

            let response = await models.center.findAndCountAll({
                where: where,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                attributes: ['id', 'centerName', 'createdAt', 'updatedAt'],
                include: [
                    {
                        model: models.institution,
                        attributes: ['institutionName']
                    }
                ]
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            let response = await models.center.findAll({
                where: { status: true, instituteId },
                attributes: ['id', 'centerName', 'createdAt', 'updatedAt'],
                include: {
                    model: models.institution,
                    attributes: ['institutionName']
                }
            });
            if (response.length > 0) {
                return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
            } else {
                return res.send(await commonResponse.response(true, Message.NO_DATA_FOUND, []));
            }
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.centerById = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { id } = req.params;

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id'];
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));

        let response = await models.center.findOne({
            where: { id, status: true, instituteId },
            attributes: ['id', 'centerName', 'createdAt'],
            include: {
                model: models.institution,
                attributes: ['institutionName']
            }
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateCenter = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data; let { centerName } = req.body;

        let checkUnique = await models.center.count({
            where: {
                centerName: { [Op.iLike]: centerName.trim() },
                instituteId,
                id: { [Op.ne]: id }
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.CENTER_EXIST));


        let response = await models.center.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id,
                    instituteId
                }
            }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteCenter = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let response = await models.center.update({ status: false }, {
            where: { id, instituteId }
        });
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteCenterMsg = async (req, res) => {
    try {
        await models.center.sync({ force: false });

        let { id } = req.params;
        let { instituteId } = req.authData.data;

        let relativeTables = [];
        let TimeTable = await models.timeTable.count({ where: { batchId: id, status: true } });
        if (TimeTable) {
            relativeTables.push("Time Table");
        }

        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This chapter is linked with ${modules} module(s). Are you sure you want to delete this chapter?`));
        } else {

            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}