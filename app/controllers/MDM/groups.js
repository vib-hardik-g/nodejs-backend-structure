const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Batch, Constant } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { Op } = require('sequelize');

module.exports.createGroup = async (req, res) => {
    try {
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { groupName, classId, subjectIds, batchIds } = req.body;

        // Check for uniqueness
        let checkUnique = await models.group.count({
            where: {
                groupName: { [Op.iLike]: groupName.trim() },
                classId,
                instituteId,
            }
        });
        if (checkUnique) return res.status(400).json(await commonResponse.response(false, Batch.GROUP_EXIST));

        let subject = [];
        let batch = [];

        let response = await models.group.create({
            groupName,
            classId,
            instituteId
        });
        batchIds.forEach(batchId => {
            batch.push({
                groupId: response.id,
                batchId: batchId,
            });
        });
        subjectIds.forEach(subjectId => {
            subject.push({
                groupId: response.id,
                subjectId: subjectId,

            });
        });
        await models.groupBatch.bulkCreate(batch);
        await models.groupSubject.bulkCreate(subject);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allGroups = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let { instituteId } = req.authData.data;
        if (!instituteId) instituteId = req.query['institute-id']
        if (!instituteId) return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND, []));

        if (req.query.pageSize && req.query.pageNo) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;

            let orderBy = [];
            if (req.query.sortBy && req.query.orderBy) {
                orderBy.push([req.query.sortBy, req.query.orderBy]);
            } else {
                orderBy.push(['id', 'DESC']);
            }

            let where = {};
            if (!req.query.searchKey) {
                where = {
                    status: true
                }
            } else {
                where = {
                    status: true,
                    [Op.or]: {
                        groupName: { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$groupBatches.batch.batchName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$groupBatches.batch.class.className$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                        '$groupSubjects.subject.subjectName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
                    }
                }
            }

            where.instituteId = instituteId;

            let response = await models.group.findAndCountAll({
                where: where,
                order: orderBy,
                offset: parseInt(Constant.DEFAULT_PAGE),
                limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
                distinct: true,
                attributes: ['id', 'groupName', 'updatedAt', 'createdAt'],
                include: [
                    {
                        model: models.groupBatch,
                        attributes: ['id'],
                        include: [
                            {
                                model: models.batch,
                                attributes: ['id', 'batchName'],
                                include: [{
                                    model: models.class,
                                    attributes: ['className']
                                }]
                            }
                        ]
                    },
                    {
                        model: models.groupSubject,
                        attributes: ['id'],
                        include: [
                            {
                                model: models.subject,
                                attributes: ['id', 'subjectName', 'updatedAt'],
                            }
                        ]
                    },
                    {
                        model: models.class,
                        attributes: ['className']
                    }
                ],
                subQuery: false,
            });
            let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
            let result = {
                rows: response.rows,
                total: response.count
            }
            return res.send(await commonResponse.response(true, msg, result));
        } else {
            let response = await models.group.findAll({
                where: {
                    status: true,
                    instituteId: instituteId
                },
                attributes: ['id', 'groupName'],
                order: [['updatedAt', 'DESC']]
            });
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        }


    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.groupById = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let { id } = req.params;

        let response = await models.group.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.class,
                    attributes: ['className']
                },
                {
                    model: models.groupBatch,
                    attributes: ['id'],
                    include: [
                        {
                            model: models.batch,
                            attributes: ['id', 'batchName'],
                        }
                    ]
                },
                {
                    model: models.groupSubject,
                    attributes: ['id'],
                    include: [
                        {
                            model: models.subject,
                            attributes: ['id', 'subjectName', 'updatedAt'],
                        }
                    ]
                }
            ]
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateGroup = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let { id } = req.params;
        let { subjectIds, batchIds } = req.body;

        let response = await models.group.update(
            { ...req.body, updatedAt: new Date() },
            {
                where: {
                    id
                }
            }
        );
        await models.groupBatch.destroy({
            where: {
                groupId: id
            }
        });
        await models.groupSubject.destroy({
            where: {
                groupId: id
            }
        });

        let subjectMappings = subjectIds.map(val => {
            return { subjectId: val, groupId: id }
        });
        await models.groupSubject.bulkCreate(subjectMappings);

        let batchMappings = batchIds.map(val => {
            return { batchId: val, groupId: id }
        });
        await models.groupBatch.bulkCreate(batchMappings);
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteGroup = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Group is relation with another tables

        let UserStudentMapping = await models.userStudentMapping.count({ where: { groupId: id, status: true } });
        if (UserStudentMapping) {
            relativeTables.push("User Student Mapping");
        }
        // let checkSubjectTopicBatchMapping = await models.subjectTopicBatchMapping.count({ where: { groupId: id, status: true } });
        // if (checkSubjectTopicBatchMapping) {
        //     relativeTables.push("Subject Topic Batch Mapping");
        // }


        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This group is linked with ${modules} module(s). Are you sure you want to delete this group?`));
        } else {
            let response = await models.group.update({ status: false }, {
                where: { id, instituteId: instituteId }
            });
            await models.groupSubject.update({ status: false },{ where: { groupId: id} });
            await models.groupBatch.update({ status: false },{ where: { groupId: id} });
        }
        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}
module.exports.deleteGroupMsg = async (req, res) => {
    try {
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.group.sync({ force: false });
        await models.groupBatch.sync({ force: false });
        await models.groupSubject.sync({ force: false });

        let relativeTables = [];
        let { id } = req.params;
        let { instituteId } = req.authData.data;

        // Check if Group is relation with another tables

        let UserStudentMapping = await models.userStudentMapping.count({ where: { groupId: id, status: true } });
        if (UserStudentMapping) {
            relativeTables.push("User Student Mapping");
        }
        if (relativeTables && relativeTables.length > 0) {
            let modules = relativeTables.join(", ");
            return res.send(await commonResponse.response(false, `This group is linked with ${modules} module(s). Are you sure you want to delete this group?`));
        } else {
            return res.send(await commonResponse.response(true, Message.DATA_DELETED));
        }
      
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}