const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Constant, Message, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');
const { generateToken } = require('../../../utils/jwt');
const { setPassword } = require('../../mailTemplates');
const { Op } = require('sequelize');
const sequelize = models.sequelize;

module.exports.createInstitution = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.subject.sync({ force: false });
        await models.class.sync({ force: false });
        await models.batch.sync({ force: false });

        await models.institutionInfo.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });
        await models.document.sync({ force: false });

        let { orgId, institutionType, institutionName, legalName, instituteIcon, licenseNo, panNo, gstNo, studentLimit, content, boardType, address, googleMap, website, contactPersonName, contactPersonPhone, contactPersonEmail, countryId, stateId, cityId, adminData } = req.body;

        // validation for duplicate email
        let duplicateEmailList = [];
        if (adminData && adminData.length != 0) {
            for (let admin of adminData) {
                let userFind = await models.user.count({
                    where: {
                        email: admin.email,
                        status: true
                    }
                });
                if (userFind) {
                    duplicateEmailList.push(admin.email);
                }
            }
        };
        if (duplicateEmailList.length != 0) {
            return res.status(400).json(await commonResponse.response(false, Message.NOT_UNIQUE, { emailList: duplicateEmailList }));
        }

        let response = await models.institution.create({
            orgId,
            institutionType,
            institutionName,
            legalName,
            instituteIcon,
            // licenseNo,
            panNo,
            gstNo,
            studentLimit
        });

        if (response) {
            await models.institutionInfo.create({
                instituteId: response.id,
                //content,
                //boardType,
                // address,
                //googleMap,
                //website,
                contactPersonName,
                contactPersonPhone,
                contactPersonEmail,
                //countryId,
                //stateId,
                //cityId,
            });
        }
        let adminType = await models.userType.findOne({
            where: {
                userType: Usertypes.ADMIN,
                status: true
            }
        });
        adminData.forEach(async (admin) => {
            let user = await models.user.create({
                userTypeId: adminType.id,
                email: admin.email,
                userName: admin.firstName + ' ' + admin.lastName,
            });
            let staff = await models.staff.create({
                firstName: admin.firstName,
                lastName: admin.lastName,
                countryId,
                stateId,
                cityId,
            });

            await models.userStaffMapping.create({
                instituteId: response.id,
                userId: user.id,
                staffId: staff.id,
            });


            // send email with invitation link
            //let token = generateToken({ userId: user.id });
            //let mailSent = await setPassword(admin.email, token);
        });
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.allInstitutions = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });
        await models.document.sync({ force: false });

        if (req.query.pageSize) {
            Constant.LIST_LIMIT_MINIMUM = req.query.pageSize;
        }
        if (req.query.pageNo) {
            Constant.DEFAULT_PAGE = parseInt(req.query.pageNo - 1) * Constant.LIST_LIMIT_MINIMUM;
        }
        let orderBy = [];
        if (req.query.sortBy && req.query.orderBy) {
            orderBy.push([req.query.sortBy, req.query.orderBy]);
        } else {
            orderBy.push(['id', 'DESC']);
        }

        let where = {};
        if (req.query.searchKey) {
            let stringCondition = {
                '$institution.institutionName$': { [Op.iLike]: '%' + req.query.searchKey + '%' },
            };
            let condition;
            condition = stringCondition;
            where = {
                [Op.or]: condition
            }
        }
        if (req.query.institutionType) {
            where.institutionType = req.query.institutionType;
        }
        if (req.query.status) {
            where.status = req.query.status == "true" ? true : false;
        }
        if (req.query.startDate && req.query.endDate) {
            where.createdAt = {
                [Op.gte]: req.query.startDate,
                [Op.lte]: req.query.endDate
            }
        }

        let response = await models.institution.findAndCountAll({
            distinct: true,
            order: orderBy,
            offset: parseInt(Constant.DEFAULT_PAGE),
            limit: parseInt(Constant.LIST_LIMIT_MINIMUM),
            where: where,
            include: [
                "info",
                {
                    model: models.userStaffMapping
                },
                {
                    model: models.userStudentMapping
                }
            ]
        });

        let newArr = [];
        for (item of response.rows) {
            newArr.push({
                id: item.id,
                institutionName: item.institutionName,
                institutionType: item.institutionType,
                studentCapacity: item.studentLimit,
                staffCount: item.userStaffMappings.length,
                studentCount: item.userStudentMappings.length,
                status: item.status,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
                contactPersonName: item.info.contactPersonName,
                contactPersonEmail: item.info.contactPersonEmail
            });
        }

        let result = {
            rows: newArr,
            total: response.count
        }
        let msg = response.count ? Message.DATA_FOUND : Message.NO_DATA_FOUND;
        return res.send(await commonResponse.response(true, msg, result));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.institutionById = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });
        await models.document.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });

        var loginUserType = req.authData.data.userTypes;
        let id = "";
        if (loginUserType == Usertypes.ADMIN) {
            let { instituteId } = req.authData.data;
            if (instituteId) id = instituteId;
        } else {
            id = req.params.id;
        }


        console.log("loginUserType ==> ", loginUserType);
        console.log("id ==> ", id);

        let response = await models.institution.findOne({
            where: { id, status: true },
            include: [
                {
                    model: models.institutionInfo,
                    as: "info",
                    include: [
                        {
                            model: models.city,
                            as: "city",
                            attributes: ['id', 'cityName'],
                        },
                        {
                            model: models.country,
                            as: "country",
                            attributes: ['id', 'countryName'],
                        },
                        {
                            model: models.state,
                            as: "state",
                            attributes: ['id', 'stateName'],
                        }
                    ]
                },
                "organisation",
                "phones",
                "emails",
                "websites",
                {
                    model: models.document,
                    attributes: ['fullPath']
                },
                {
                    model: models.userStaffMapping,
                    attributes: ['id', 'staffId', 'userId', 'instituteId'],
                    include: [
                        {
                            model: models.staff,
                            attributes: ['id', 'firstName', 'lastName']
                        },
                        {
                            model: models.user,
                            attributes: ['id', 'userName', 'email']
                        }
                    ]
                }
            ],
        });
        if (response) {
            return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
        } else {
            return res.status(404).send(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.updateInstitution = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.userStudentMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { id } = req.params;
        let { orgId, institutionName, legalName, institutionType, instituteIcon, studentLimit, licenseNo, panNo, gstNo, boardType, content, address, googleMap, website, contactPersonName, contactPersonEmail, contactPersonPhone, countryId, stateId, cityId, status, adminData } = req.body;

        let duplicateEmailList = [];
        let staffMapIds = [];
        if (adminData && adminData.length != 0) {
            for (let admin of adminData) {
                if (admin.id) {
                    staffMapIds.push(admin.id);
                }
                let userFind = await models.user.count({
                    where: {
                        email: admin.email,
                        status: true
                    },
                    include: [{
                        model: models.userStaffMapping,
                        where: {
                            instituteId: { [Op.not]: id },
                            status: true
                        },
                        required: true
                    }]
                });
                if (userFind) {
                    duplicateEmailList.push(admin.email);
                }
            }
        };

        if (duplicateEmailList.length != 0) {
            return res.status(400).json(await commonResponse.response(false, Message.NOT_UNIQUE, { emailList: duplicateEmailList }));
        }

        let response = await models.institution.update(
            {
                orgId,
                institutionName,
                legalName,
                institutionType,
                instituteIcon,
                studentLimit,
                licenseNo,
                panNo,
                gstNo,
                updatedAt: new Date()
            },
            { where: { id } }
        );

        if (content && response) {
            await models.institutionInfo.update(
                { content, boardType, address, googleMap, website, contactPersonName, contactPersonEmail, contactPersonPhone, countryId, stateId, cityId, status, updatedAt: new Date() },
                { where: { instituteId: id } }
            )
        };

        // delete user staff mapping data
        if (staffMapIds.length != 0) {
            let deleteUserStaffmap = await models.userStaffMapping.findAll({
                attributes: ['id', 'userId', 'staffId'],
                where: {
                    id: { [Op.notIn]: staffMapIds },
                    instituteId: id,
                    status: true
                }
            });
            if (deleteUserStaffmap && deleteUserStaffmap.length > 0) {
                for (let userstaff of deleteUserStaffmap) {
                    await models.user.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.userId,
                        }
                    });
                    await models.staff.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.staffId,
                        }
                    });
                    await models.userStaffMapping.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.id,
                        }
                    });
                };
            };
        }

        let adminType = await models.userType.findOne({
            where: {
                userType: Usertypes.ADMIN,
                status: true
            }
        });
        if (adminData && adminData.length != 0) {
            // insert and update user staff mapping data
            for (let admin of adminData) {
                if (admin.id) {
                    let userStaffMap = await models.userStaffMapping.findOne({
                        where: {
                            id: admin.id,
                        }
                    });
                    if (userStaffMap) {
                        await models.user.update({
                            email: admin.email,
                            userName: admin.firstName + ' ' + admin.lastName,
                        }, {
                            where: {
                                id: userStaffMap.userId,
                            }
                        });
                        await models.staff.update({
                            firstName: admin.firstName,
                            lastName: admin.lastName,
                        }, {
                            where: {
                                id: userStaffMap.staffId,
                            }
                        });
                    }
                } else {
                    let user = await models.user.create({
                        userTypeId: adminType.id,
                        email: admin.email,
                        userName: admin.firstName + ' ' + admin.lastName,
                    });
                    let staff = await models.staff.create({
                        firstName: admin.firstName,
                        lastName: admin.lastName,
                        countryId,
                        stateId,
                        cityId,
                    });

                    await models.userStaffMapping.create({
                        instituteId: id,
                        userId: user.id,
                        staffId: staff.id,
                    });
                }
            }
        } else {
            // alldelete user staff mapping data
            let deleteUserStaffmap = await models.userStaffMapping.findAll({
                attributes: ['id', 'userId', 'staffId'],
                where: {
                    instituteId: id,
                    status: true
                }
            });
            if (deleteUserStaffmap && deleteUserStaffmap.length != 0) {
                for (let userstaff of deleteUserStaffmap) {
                    await models.user.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.userId,
                        }
                    });
                    await models.staff.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.staffId,
                        }
                    });
                    await models.userStaffMapping.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.id,
                        }
                    });
                };
            }
        }

        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.deleteInstitution = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.userInstituteMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { id } = req.params;
        let response = await models.institution.update({ status: false }, {
            where: { id }
        });

        if (response) {
            await models.institutionInfo.update({ status: false }, {
                where: { instituteId: id }
            });
            let deleteUserStaffmap = await models.userStaffMapping.findAll({
                attributes: ['id', 'userId', 'staffId'],
                where: {
                    instituteId: id,
                    status: true
                }
            });
            if (deleteUserStaffmap && deleteUserStaffmap.length > 0) {
                for (let userstaff of deleteUserStaffmap) {
                    await models.user.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.userId,
                        }
                    });
                    await models.staff.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.staffId,
                        }
                    });
                    await models.userStaffMapping.update({
                        status: false
                    }, {
                        where: {
                            id: userstaff.id,
                        }
                    });
                };
            };
        }

        return res.send(await commonResponse.response(true, Message.DATA_DELETED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}

module.exports.getAllInstitutions = async (req, res) => {
    try {
        await models.institution.sync({ force: false });

        let response = await models.institution.findAll({
            where: {
                status: true
            },
            attributes: ['id', 'institutionName']
        });

        return res.send(await commonResponse.response(true, Message.DATA_FOUND, response));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}