const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.createUpdateInstitutionPhoneNumber = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { phoneNumbers } = req.body;
        let { instituteId } = req.authData.data;

        let mappings = phoneNumbers.map(number => {
            return { phoneNumber: number, instituteId };
        });
        await models.institutionPhone.destroy({
            where: { instituteId }
        });
        await models.institutionPhone.bulkCreate(mappings);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

module.exports.createUpdateInstitutionEmail = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { emails } = req.body;
        let { instituteId } = req.authData.data;
        let mappings = emails.map(email => {
            return { emailType: email.emailType, emailId: email.emailId, instituteId };
        });
        await models.institutionEmail.destroy({
            where: { instituteId }
        });
        await models.institutionEmail.bulkCreate(mappings);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};

module.exports.createUpdateInstitutionWebsite = async (req, res) => {
    try {
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { websites } = req.body;
        let { instituteId } = req.authData.data;
        let mappings = websites.map(website => {
            return { website, instituteId };
        });
        await models.institutionWebsite.destroy({
            where: { instituteId }
        });
        await models.institutionWebsite.bulkCreate(mappings);
        return res.send(await commonResponse.response(true, Message.DATA_CREATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
};