const models = require('../../models');
const commonResponse = require('../../../utils/commonResponse');
const { Message, Usertypes } = require('../../../utils/commonMessages');
const errorHandle = require('../../../utils/errorHandler');

module.exports.updateInstitutionDetailsByAdmin = async (req, res) => {
    try {
        await models.user.sync({ force: false });
        await models.staff.sync({ force: false });
        await models.institution.sync({ force: false });
        await models.organisation.sync({ force: false });
        await models.institutionInfo.sync({ force: false });
        await models.userStaffMapping.sync({ force: false });
        await models.institutionPhone.sync({ force: false });
        await models.institutionEmail.sync({ force: false });
        await models.institutionWebsite.sync({ force: false });

        let { instituteId } = req.authData.data;
        let { institutionName, legalName, institutionType, instituteIcon, boardType, content, address, googleMap, website, contactPersonName, contactPersonEmail, contactPersonPhone, countryId, stateId, cityId, status } = req.body;

        let response = await models.institution.update(
            {
                institutionName,
                legalName,
                institutionType,
                instituteIcon,
                updatedAt: new Date()
            },
            { where: { id: instituteId } }
        );
        await models.institutionInfo.update(
            { content, boardType, address, googleMap, website, contactPersonName, contactPersonEmail, contactPersonPhone, countryId, stateId, cityId, status, updatedAt: new Date() },
            { where: { instituteId: instituteId } }
        )
        return res.send(await commonResponse.response(true, Message.DATA_UPDATED));
    } catch (err) {
        const { status, message, error } = errorHandle(err);
        return res.status(status).json(await commonResponse.response(false, message, error));
    }
}