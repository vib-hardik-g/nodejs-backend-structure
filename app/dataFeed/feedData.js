const bcrypt = require("bcryptjs");
const models = require("../models");
const fs = require("fs");

exports.createSuperAdmin = async () => {
  try {
    await models.userType.sync({ force: false });
    await models.user.sync({ force: false });

    let [userTypeId, created] = await models.userType.findOrCreate({
      where: { userType: "SUPER_ADMIN" },
      defaults: {
        userType: "SUPER_ADMIN",
      },
    });

    if (created) {
      let userResponse = await models.user.create({
        userName: "Super Admin",
        email: "lingeswaran@vibrant-info.com",
        password: await bcrypt.hash("admin123", 10),
        userTypeId: userTypeId.id,
      });

      await models.userType.bulkCreate([
        { userType: "ADMIN" },
        { userType: "STAFF" },
        { userType: "PARENT" },
        { userType: "STUDENT" },
      ]);
    }
  } catch (error) {
    console.log("error", error);
  }
};

exports.feedCountryStateCity = async () => {
  try {
    await models.country.sync({ force: false });
    await models.state.sync({ force: false });
    await models.city.sync({ force: false });
    await models.document.sync({ force: false });
    await models.organisation.sync({ force: false });
    await models.document.sync({ force: false });
    await models.institution.sync({ force: false });
    await models.institutionInfo.sync({ force: false });

    const countriesJson = JSON.parse(
      fs.readFileSync("./jsonData/countries.json")
    );
    const statesJson = JSON.parse(fs.readFileSync("./jsonData/states.json"));
    const citiesJson = JSON.parse(fs.readFileSync("./jsonData/cities.json"));

    let countriesCount = await models.country.count({
      where: {
        status: true,
      },
    });

    if (countriesCount == 0) {
      if (countriesJson) {
        for (let i = 0; i < countriesJson.length; i++) {
          let [countryDetails, created] = await models.country.findOrCreate({
            where: { countryName: countriesJson[i].countryName },
            defaults: {
              countryName: countriesJson[i].countryName,
            },
          });
        }
      }
      if (statesJson) {
        for (let i = 0; i < statesJson.length; i++) {
          let [stateDetails, created] = await models.state.findOrCreate({
            where: {
              id: parseInt(statesJson[i].id),
              stateName: statesJson[i].stateName,
              countryId: parseInt(statesJson[i].countryId),
            },
            defaults: {
              id: parseInt(statesJson[i].id),
              stateName: statesJson[i].stateName,
              countryId: parseInt(statesJson[i].countryId),
            },
          });
        }
      }
      if (citiesJson) {
        for (let i = 0; i < citiesJson.length; i++) {
          let [cityDetails, created] = await models.city.findOrCreate({
            where: {
              id: parseInt(citiesJson[i].id),
              cityName: citiesJson[i].cityName,
              stateId: parseInt(citiesJson[i].stateId),
            },
            defaults: {
              id: parseInt(citiesJson[i].id),
              cityName: citiesJson[i].cityName,
              stateId: parseInt(citiesJson[i].stateId),
            },
          });
        }
      }
    }
  } catch (error) {
    console.log("error", error);
  }
};

exports.createTimeTableType = async () => {
  try {
    await models.timeTableType.sync({ force: false });

    let types = ["Class", "Exam", "Event"];
    types.forEach(async (type) => {
      let [typeDetails, created] = await models.timeTableType.findOrCreate({
        where: {
          timeTableType: type,
        },
        defaults: {
          timeTableType: type,
        },
      });
    });
  } catch (error) {
    console.log("error", error);
  }
};

exports.createAssessRights = async () => {
  try {
    await models.module.sync({ force: false });
    await models.subModule.sync({ force: false });
    await models.userModuleMapping.sync({ force: false });

    let data = [
      "All Student Section",
      "All Staffs Section",
      "Doubt Reporting",
      "All Classes, All Batches",
      "Request Manager",
      "Attendence Manager",
      "Timetable Manager",
      "Assessments Manager",
      "Online Assessment",
      "Notes Manager, Assignment Manager",
      "Feedback Manager",
      "Batch Progess Reports",
      "Notice Manager, Announcements",
      "Happenings",
      "Student Usage",
      "Master Management",
    ];
    data.forEach(async (single) => {
      let [found, created] = await models.module.findOrCreate({
        where: {
          moduleName: single,
        },
        defaults: {
          moduleName: single,
        },
      });
    });
  } catch (error) {
    console.log("error", error);
  }
};
