const commonResponse = require('./commonResponse');
const { Message, Usertypes } = require('./commonMessages');
const models = require('../app/models')

module.exports.checkStudentAndStaffDetail = async (req, res, next) => {
    try {
        let profileinfo = req.authData.data;
        if (req.authData.data.userTypes == Usertypes.STAFF) {
            let data = await models.userStaffMapping.findOne({
                where: {
                    userId: req.authData.data.id
                }
            });
            if (data) {


                profileinfo.staffId = data.staffId
                profileinfo.instituteId = data.instituteId
                next();
            }
            else {
                next()
            }
        } else if (req.authData.data.userTypes == Usertypes.STUDENT) {
            let data = await models.userStudentMapping.findOne({
                where: {
                    userId: req.authData.data.id,
                    studentId: req.authData.data.studentId
                }
            });
            if (data) {
                // profileinfo.studentId = data.studentId
                profileinfo.classId = data.classId
                profileinfo.instituteId = data.instituteId
                profileinfo.batchId = data.batchId
                profileinfo.groupId = data.groupId
                next();
            }
            else {
                next()
            }
        } else {
            return res.status(500).json(await commonResponse.response(false, Message.NO_DATA_FOUND));
        }

    } catch (err) {
        return res.status(500).json(await commonResponse.response(false, Message.NO_DATA_FOUND));
    }

}