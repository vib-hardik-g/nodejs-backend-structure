const Excel = require('exceljs');
const path = require('path');
const fs = require('fs');
const { awsS3ServerToServerUpload } = require('../utils/awsS3Handler');
const md5 = require('md5');
const models = require('../app/models');

module.exports.generateQuestionBankTemplate = async (noOfRows = 0, className = '', subjectName = '', chapter = '') => {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('Excel For Question Bank');

    let headers = [
        { header: 'No.', 'key': 'no', width: 5},
        { header: 'Class', 'key': 'className', width: 10},
        { header: 'Subject', 'key': 'subjectName', width: 15},
        { header: 'Chapter', 'key': 'chapter', width: 20},
        { header: 'Question Title', 'key': 'question', width: 25},
        { header: 'Option 1', 'key': 'option1', width: 15},
        { header: 'Option 2', 'key': 'option2', width: 15},
        { header: 'Option 3', 'key': 'option3', width: 15},
        { header: 'Option 4', 'key': 'option4', width: 15},
        { header: 'Correct Answer', 'key': 'answer', width: 15},
    ];
    worksheet.columns = headers;
    let data = [];
    for(let i = 1; i<= noOfRows; i++ ){
        let rowData = {
            no: i,
            className: className,
            subjectName: subjectName,
            chapter: chapter
        }
        data.push(rowData);
    }

    data.forEach((row) => {
        worksheet.addRow(row);
    });

    let headerRow = worksheet.getRow(1);
    headerRow.fill = {
        type: 'pattern',
        pattern: 'solid',
        bgColor: { argb: 'FF111111' },
        fgColor: { argb: 'FFA5A5A5' }
    };

    let fileName = md5('Question-bank-'+ new Date().getTime());
    let fullPath = path.join(__dirname, `../tempFiles/`, `${fileName}.xlsx`);
    await workbook.xlsx.writeFile(fullPath);
    const document = await awsS3ServerToServerUpload(fullPath, 'question-bank-template');
    if (document) {
        fs.unlinkSync(fullPath);
        return document;
    }else{
        return null;
    }
}