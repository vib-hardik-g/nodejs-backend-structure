const errorHandler = (err) => {
    console.log("err", err);
    
    let status = 500, message = "Internal server error", error = {};

    if (err && err.name === 'SequelizeValidationError') {
        error = {};
        err.errors.forEach(e => { 
             error[e.path] = e.message 
        });
        message = "Validation error!";
        status = 400
    } else if (err.name == 'SequelizeUniqueConstraintError') {
        error = {};
        err.errors.forEach(e => { 
             error[e.path] = e.message 
        });
        message = "Validation error!";
        status = 400
    }

    return { status, message, error }
};

module.exports = errorHandler;