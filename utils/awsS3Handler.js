const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const fs = require('fs');
const path = require('path');
const { Uploadpaths } = require('./commonMessages');
const models = require('../app/models');
const md5 = require('md5');
const mime = require('mime-types');

// AWS S3
const S3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION_NAME
});

module.exports.awsS3Handler = () => {
    return multer({
        storage: multerS3({
            s3: S3,
            bucket: process.env.AWS_BUCKET_NAME,
            acl: 'public-read',
            key: function (req, file, cb) {
                let { type } = req.body;
                let destinationPath;
                if (!type) cb(new Error('Type is required'), null);
                switch (type) {
                    case 'institution':
                        destinationPath = Uploadpaths.INSTITUTION;
                        break;
                    case 'student':
                        destinationPath = Uploadpaths.STUDENT;
                        break;
                    case 'staff':
                        destinationPath = Uploadpaths.STAFF;
                        break;
                    case 'happening':
                        destinationPath = Uploadpaths.HAPPENING;
                        break;
                    case 'leave':
                        destinationPath = Uploadpaths.LEAVE;
                        break;
                    case 'note':
                        destinationPath = Uploadpaths.NOTE;
                        break;
                    case 'assignment':
                        destinationPath = Uploadpaths.ASSIGNMENT;
                        break;
                    case 'assignment_submission':
                        destinationPath = Uploadpaths.ASSIGNMENT_SUBMISSION;
                        break;
                    default:
                        destinationPath = 'default';
                }
                let extArray = file.mimetype.split("/");
                let extension = extArray[extArray.length - 1];
                let fileName = `${new Date().getTime()}-${file.originalname}`;
                cb(null, `${destinationPath}/${md5(fileName)}.${extension}`);
            }
        }),
        fileFilter: function (req, file, cb) {
            let isValid = true;
            let { maxFileSize } = req.body; // in kb
            if (maxFileSize) {
                let size = parseInt(+req.rawHeaders.slice(-1)[0]);
                let sizeLimit = parseInt(maxFileSize * 1024);
                if (sizeLimit < size) {
                    isValid = false;
                }
            }
            let error = isValid ? null : new Error('File size limit exceeds!');
            cb(error, isValid);
        },
    });
}

module.exports.awsS3ServerToServerUpload = (localPath, destinationPath, maxFileSize = '200MB') => {
    return new Promise((resolve, reject) => {
        const fileContent = fs.readFileSync(localPath);
        const fileName = localPath.split(path.sep).pop();
        // make the filename Unique in s3
        let fileExt = fileName.split('.').pop();
        let fileOldName = fileName.split('.').shift();
        let uniqueFileName = md5(fileOldName + '-' + new Date().getTime()) + '.' + fileExt;
        const params = {
            ACL: 'public-read',
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: destinationPath + '/' + uniqueFileName,
            Body: fileContent,
            limits: { fileSize: maxFileSize }
        };
        S3.upload(params, async function (err, data) {
            if (err) {
                reject(err);
            }
            // insert data in documents table
            let s3Data = data;
            let fileExtension = s3Data.key.split('/').pop().split('.').pop();
            let fileSize = fs.statSync(localPath);
            fileSize = fileSize.size;
            let create = await models.document.create({
                type: destinationPath,
                etag: s3Data.ETag.substring(1, s3Data.ETag.length - 1),
                fileName: s3Data.key.split('/').pop(),
                filePath: s3Data.key,
                fullPath: s3Data.Location,
                mimeType: mime.lookup(fileExtension),
                size: fileSize,
                originalFileName: s3Data.key.split('/').pop().split('.').shift().toString().replace(/[_\-&\/\\#,+()$~%.'":*?<>{}]/g, ' ').replace(/\s\s+/g, ' '),
                fileExtension: fileExtension,
            });
            resolve(create);
        });
    });
}

module.exports.deleteFile = async (id) => {
    return new Promise(async (resolve, reject) => {
        let document = await models.document.scope('all').findOne({ where: { id } });
        if (document) {
            // delete the file from s3
            S3.deleteObject({ Bucket: process.env.AWS_BUCKET_NAME, Key: document.filePath }, async (err, data) => {
                if (err) reject(err);
                document.destroy();
                resolve(document);
            });
        } else {
            resolve(null);
        }
    });

}

/**
 * @param  {} filePath
 */
module.exports.getFileFromS3 = async (filePath) => {
    return new Promise((resolve, reject) => {
        try {
            var getParams = {
                Bucket: process.env.AWS_BUCKET_NAME, // your bucket name,
                Key: filePath // path to the object you're looking for
            }

            S3.getObject(getParams, function (err, data) {
                // Handle any error and exit
                if (err)
                    reject(error);
                else
                    resolve(data);
            });
        } catch (error) {
            reject(error);
        }
    });
}