const path = require("path");
const fs = require("fs");
const { awsS3ServerToServerUpload } = require("../utils/awsS3Handler");
const md5 = require("md5");
const models = require("../app/models");
const htmlPdf = require("html-pdf");
const xlsxFile = require("read-excel-file/node");

module.exports.excelToPDFConverter = async (
  localPathToExcel,
  destinationPath,
  orientation = "portrait"
) => {
  return new Promise((resolve, reject) => {
    if (!localPathToExcel || !destinationPath)
      reject("LocalPath & Destination Path is required");
    xlsxFile(localPathToExcel)
      .then(async (rows) => {
        let headers = rows[0];
        rows.shift();
        // generate pdf from excel
        let options = {
          format: "A4",
          orientation: orientation,
          header: {
            height: "20px",
          },
          footer: {
            height: "20px",
          },
        };
        let tableHead = "<thead><tr>";
        for (let header of headers) {
          tableHead += `<th>${header}</th>`;
        }
        tableHead += "</tr></thead>";

        let tableBody = "<tbody>";
        for (let row of rows) {
          tableBody += "<tr>";
          for (let item of row) {
            tableBody += `<td>${item}</td>`;
          }
          tableBody += "</tr>";
        }
        tableBody += "</tbody>";

        let html = `<style>
                    table {
                        font-family: arial, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    
                    td, th {
                        border: 1px solid #282c34;
                        text-align: left;
                        padding: 8px;
                    }
    
                    * { /* this works for all but td */
                        word-wrap:break-word;
                    }
                    
                    table { /* this somehow makes it work for td */
                        table-layout:fixed;
                        width:100%;
                    }
                </style>
                <body style="margin: 10px; padding:10px">`;

        html += `<table style="margin-bottom: 20px;" border="1px">`;
        html += tableHead;
        html += tableBody;
        html += `</table>`;
        html += `</body>`;

        // change extension of xls / xlsx to pdf
        let pdfFilePath = localPathToExcel
          .replace(".xlsx", ".pdf")
          .replace(".xls", ".pdf");
        htmlPdf.create(html, options).toFile(pdfFilePath, async (err, data) => {
          if (err) reject("Unable to Generate PDF");
          // upload the pdf to s3 and remove localfile
          const document = await awsS3ServerToServerUpload(
            pdfFilePath,
            destinationPath
          );
          fs.unlinkSync(pdfFilePath);
          resolve(document);
        });
      })
      .catch((err) => {
        reject("Unable to Generate PDF");
      });
  });
};

// html template generation
module.exports.generateAssessmentPdfHtmlTemplate = async (assessmentId) => {
  let assessment = await models.assessment.findOne({
    where: { id: assessmentId },
    include: [
      {
        model: models.class,
        attributes: ["className"],
      },
      {
        model: models.batchAssessmentMapping,
        attributes: ["batchId"],
        include: [
          {
            model: models.batch,
            attributes: ["batchName"],
          },
        ],
      },
      {
        model: models.groupAssessmentMapping,
        attributes: ["groupId"],
        include: [
          {
            model: models.group,
            attributes: ["groupName"],
          },
        ],
      },
      {
        model: models.subjectAssessmentMapping,
        attributes: ["subjectId", "noOfQuestions"],
        include: [
          {
            model: models.subject,
            attributes: ["subjectName"],
          },
        ],
      },
      {
        model: models.assessmentQuestion,
        attributes: ["question"],
        include: [
          {
            model: models.assessmentQuestionOption,
            attributes: ["option", "isCorrect"],
          },
        ],
      },
    ],
  });

  let subjects = [];
  assessment.subjectAssessmentMappings.forEach((subject) => {
    subjects.push(subject.subject.subjectName);
  });
  subjects.join(",");

  let batchesOrGroups = [];
  assessment.batchAssessmentMappings.forEach((batch) => {
    batchesOrGroups.push(batch.batch.batchName);
  });
  assessment.groupAssessmentMappings.forEach((group) => {
    batchesOrGroups.push(group.group.groupName);
  });
  batchesOrGroups.join(",");

  let questions = "";
  assessment.assessmentQuestions.forEach((question, index) => {
    questions += `<div class="question">
            <div class="question-title">
                ${index + 1}. ${question.question}
            </div>
            <div class="question-options">
                <div class="row">`;
    let answer = 1;
    question.assessmentQuestionOptions.forEach((option, i) => {
      if (option.isCorrect) answer += i;
      questions += `<div class="column">(${i + 1}) ${option.option}</div>`;
    });
    questions += `</div>
                <div class="row">
                    <div class="column">
                        Correct Answer: ${answer}
                    </div>
                </div>
            </div>
        </div>`;
  });

  let html = `<style>
        html, body{
            margin: auto; 
            padding:10px;
            font-size: 14px;
        }
        section.header {
            border:1px solid #282c34;
            margin: auto;
            padding:10px;
            font-size: 14px;
        }
        .column {
            float: left;
            width: 50%;
            margin-bottom: 5px;
        }
        
        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .text-right{
            text-align: right;
        }

        .questions{
            margin: 10px auto;
        }
        .question{
            margin: 10px auto;
        }
        .question .question-title{
            font-weight: bold;
            margin-bottom: 6px;
            font-size: 16px;
        }
        .question .question-options{
            font-weight: 400;
        }
        
    </style>
    <body>
        <section class="header">
            <div class="row">
                <div class="column"><strong>Class:</strong> ${
                  assessment.class.className
                }</div>
                <div class="column text-right"><strong>Subjects:</strong> ${subjects}</div>
                <div class="column"><strong>Batch/Group(s):</strong> ${batchesOrGroups}</div>
                <div class="column text-right"><strong>Duration:</strong> ${
                  assessment.duration
                } min</div>
                <div class="column"><strong>No. Of Questions:</strong> ${
                  assessment.assessmentQuestions.length
                }</div>
                <div class="column text-right"><strong>Maximum Marks:</strong> ${
                  assessment.assessmentQuestions.length * assessment.correctMark
                }</div>
            </div>
        </section>
        <section class="questions">`;
  html += questions;
  html += `</section></body>`;
  return html;
};

module.exports.generateAssessmentResultPdfHtmlTemplate = async (
  assessmentId
) => {
  // get all the students having same assessment subjects
  let assessment = await models.assessment.findOne({
    where: {
      id: assessmentId,
    },
    include: [
      {
        model: models.subjectAssessmentMapping,
        attributes: ["subjectId"],
        include: [
          {
            model: models.subject,
            attributes: ["subjectName"],
          },
        ],
      },
      {
        model: models.class,
        attributes: ["className"],
      },
      {
        model: models.assessmentQuestion,
        include: [
          {
            model: models.subject,
            attributes: ["id", "subjectName"],
          },
        ],
      },
    ],
  });
  let subjects = [];
  let subjectwiseQuestionCount = {};
  assessment.subjectAssessmentMappings.forEach((subject) => {
    subjects.push(subject.subjectId);
    subjectwiseQuestionCount[subject.subject.subjectName] = 0;
  });

  // getting question count
  let originalQuestions = assessment.assessmentQuestions;
  originalQuestions.forEach((question) => {
    subjectwiseQuestionCount[question.subject.subjectName] += 1;
  });
  let studentData = await models.student.findAll({
    attributes: ["firstName", "lastName"],
    include: [
      {
        model: models.studentBatchUniqueId,
        attributes: ["uniqueId"],
      },
      {
        model: models.userStudentMapping,
        attributes: ["batchId"],
        include: [
          {
            model: models.batch,
            attributes: ["batchName"],
          },
        ],
      },
      {
        model: models.assessmentQuestionAnswer,
        where: {
          assessmentId,
        },
        attributes: ["isCorrect"],
        include: [
          {
            model: models.assessmentQuestion,
            attributes: ["subjectId"],
            include: [
              {
                model: models.subject,
                attributes: ["subjectName"],
              },
            ],
          },
        ],
      },
    ],
  });

  let subjectNames = "";
  assessment.subjectAssessmentMappings.forEach(
    (subject) => (subjectNames += subject.subject.subjectName + ",")
  );
  subjectNames = subjectNames.slice(0, -1);

  let subjectWiseMaximumMarks = [];
  let totalFullMark = 0;
  for (let [key, value] of Object.entries(subjectwiseQuestionCount)) {
    let maxMarks = value * assessment.correctMark;
    totalFullMark += maxMarks;
    subjectWiseMaximumMarks.push({
      subjectName: key,
      noOfQuestions: value,
      maxMarks,
    });
  }

  // formatting student Data from assessment answers
  let formattedStudentData = [];
  studentData.forEach((student) => {
    // question answer marking work here
    let answers = [];
    student.assessmentQuestionAnswers.forEach((answer) => {
      answers.push({
        isCorrect: answer.isCorrect,
        subjectName: answer.assessmentQuestion.subject.subjectName,
      });
    });

    // subject wise filter
    let filteredData = [];
    let studentwiseTotalObtained = 0;
    subjectWiseMaximumMarks.forEach((subject) => {
      let totalAnswered = answers.filter(
        (x) => x.subjectName == subject.subjectName
      ).length;
      let correctAnswered = answers.filter(
        (x) => x.subjectName == subject.subjectName && x.isCorrect == true
      ).length;
      let incorrectAnswered = answers.filter(
        (x) => x.subjectName == subject.subjectName && x.isCorrect == false
      ).length;
      let markObtained =
        correctAnswered * assessment.correctMark -
        incorrectAnswered * Math.abs(assessment.incorrectMark);
      studentwiseTotalObtained += markObtained;
      filteredData.push({
        subjectName: subject.subjectName,
        totalAnswered,
        correctAnswered,
        incorrectAnswered,
        markObtained,
      });
    });

    let studentWiseTotalPerecentage = 0;
    if (studentwiseTotalObtained > 0) {
      studentWiseTotalPerecentage =
        (100 / totalFullMark) * studentwiseTotalObtained;
    }

    formattedStudentData.push({
      firstName: student.firstName,
      lastName: student.lastName,
      uniqueId: student.studentBatchUniqueId.uniqueId,
      batchName: student.userStudentMappings[0].batch.batchName,
      examData: filteredData,
      total: studentwiseTotalObtained,
      totalPercentage: studentWiseTotalPerecentage,
    });
  });

  let noOfStudents = formattedStudentData.length;
  let allTotalMark = 0;
  formattedStudentData.forEach((x) => {
    allTotalMark += x.total;
  });
  let avgMark = parseInt(allTotalMark / noOfStudents);
  let maximumMark = Math.max.apply(
    Math,
    formattedStudentData.map((o) => o.total)
  );

  let result = {
    assessmentName: assessment.assessmentName,
    startDate: assessment.startDate,
    className: assessment.class.className,
    subjects: subjectNames,
    subjectWiseMaximumMarks: subjectWiseMaximumMarks,
    students: formattedStudentData,
    avgMark,
    maximumMark,
  };

  let subjectWiseMaxMarksHtml = `<div class="row">`;
  let tableHeadingDynamicValues = ``;
  let i = 1;
  result.subjectWiseMaximumMarks.forEach((subject) => {
    let className;
    if (i % 2 == 0) {
      className = "text-right";
    } else {
      className = "";
    }
    subjectWiseMaxMarksHtml += `<div class="column ${className}"><strong>${subject.subjectName} Maximum Marks:</strong> ${subject.maxMarks}</div>`;
    tableHeadingDynamicValues += `<th>${subject.subjectName} Marks</th>`;
    i++;
  });
  subjectWiseMaxMarksHtml += `</div>`;

  let data = result.students;

  let tableHtml = `<table style="margin-bottom: 20px;" border="1px"><thead><tr><th>Batch</th><th>Student Name</th><th>Unique ID</th>${tableHeadingDynamicValues}<th>Total</th><th>%</th></tr></thead><tbody>`;
  data.forEach((item) => {
    let dynamicBatchData = ``;
    item.examData.forEach((exam) => {
      dynamicBatchData += `<td>${exam.markObtained}</td>`;
    });
    tableHtml += `<tr><td>${item.batchName}</td><td>${item.firstName} ${item.lastName}</td><td>${item.uniqueId}</td>${dynamicBatchData}<td>${item.total}</td><td>${item.totalPercentage}</td></tr>`;
  });
  tableHtml += `</tbody></table>`;

  let html = `<style>
        html, body{
            margin: auto; 
            padding:10px;
            font-size: 14px;
        }
        section.header {
            border:1px solid #282c34;
            margin: auto;
            padding:10px;
            font-size: 14px;
        }
        .column {
            float: left;
            width: 50%;
            margin-bottom: 5px;
        }
        
        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .text-right{
            text-align: right;
        }

        .data{
            margin: 10px auto;
        }
        .item{
            margin: 10px auto;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }
        
        td, th {
            border: 1px solid #282c34;
            text-align: left;
            padding: 8px;
        }

        * { /* this works for all but td */
            word-wrap:break-word;
        }
        
        table { /* this somehow makes it work for td */
            table-layout:fixed;
            width:100%;
        }
        
    </style>
    <body>
        <section class="header">
            <div class="row">
                <div class="column"><strong>Test Name:</strong> ${result.assessmentName}</div>
                <div class="column text-right"><strong>Date Of Test:</strong> ${result.startDate}</div>
                  <div class="column"><strong>Class:</strong> ${result.className}</div>
                <div class="column text-right"><strong>Subject(s):</strong> ${result.subjects}</div>
            </div>
            ${subjectWiseMaxMarksHtml}
            <div class="row">
                <div class="column"><strong>Heighest Marks:</strong> ${result.maximumMark}</div>
                <div class="column text-right"><strong>Avg Marks:</strong> ${result.avgMark}</div>
            </div>
        </section>
        <section class="data">`;
  html += tableHtml;
  html += `</section></body>`;
  return html;
};
