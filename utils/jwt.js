const jwt = require('jsonwebtoken');
const commonResponse = require('./commonResponse');
const { Message } = require('./commonMessages');
const models = require('../app/models');
const JWT_SECRET_TOKEN = process.env.JWT_SECRET_KEY;



/**
 * Generate jwt token for user data
 * data: user data
 * cb: callback function
 */
generateToken = (data, cb) => {
    return jwt.sign({ data }, JWT_SECRET_TOKEN);
};

/**
 * Verify data
 * token: jwt token
 * cb: callback function
 */
verifyToken = (token, cb) => {
    try {
        let data = jwt.verify(token, JWT_SECRET_TOKEN);
        cb(null, data);
    } catch (error) {
        cb(error, null);
    }
};
validateToken = async (req, res, next) => {
    let token = req.headers['authorization'];
    if (token && typeof token !== 'undefined') {
        verifyToken(token, async (err, data) => {
            if (err) {
                return res.status(401).json(await commonResponse.response(false, Message.INVALID_TOKEN));
            }
            req.token = token;
            req.authData = data;
            next();
        });
    } else {
        await models.user.sync({ force: false });
        await models.loginSession.sync({ force: false });

        await models.loginSession.update({
            destroyedOn: new Date()
        }, {
            where: {
                token: req.token
            }
        });
        
        return res.status(401).json(await commonResponse.response(false, Message.INVALID_TOKEN));
    }

};

module.exports = {
    generateToken,
    verifyToken,
    validateToken
}