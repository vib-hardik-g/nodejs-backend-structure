const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY; // get the key from env


// using Twilio SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs
// javascript
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(SENDGRID_API_KEY);
module.exports = async (config) => {
  const msg = {
    from: 'varun@vibrant-info.com', // Change to your verified sender
    ...config
  };
 
  let mailRes = await sgMail.send(msg);
  
  return true;
}