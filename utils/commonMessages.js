const StaffAge = {
    MIN: 16
};

const Message = {
    INVALID_TOKEN: "Token Invalid!",
    DATA_FOUND: "Data Found!",
    NO_DATA_FOUND: "No Data Found!",
    LOGIN_SUCCESS: "Login successful!",
    LOGOUT_SUCCESS: "Logged out successful!",
    SET_PASS: "Set your password first!",
    IN_ACTIVATE: "Your account is inactive. Click on 'Sign Up' to active your account.",
    LOGIN_FAILED: "Login Failed!",
    ALREADY_EXISTS: "Account already exist in our records!",
    EMAIL_ALREADY_EXISTS: "Email is already exist in our records !",
    STUDENT_EMAIL_ALREADY_EXISTS: "Student email already exist in our records !",
    FATHER_EMAIL_ALREADY_EXISTS: "Father email already exist in our records !",
    MOTHER_EMAIL_ALREADY_EXISTS: "Mother email already exist in our records !",
    GUARDIAN_EMAIL_ALREADY_EXISTS: "Guardian email already exist in our records !",
    NOT_EXISTS: "Account does not exist in our records !",
    SOMETHING_WRONG: "Something went wrong!",
    INVALID_EMAIL: "You have entered an email is not exist in our records.",
    INVALID_PASSWORD: "You have entered a password that does not match your email.",
    INVALID_CRED: "You have entered an invalid email address or password!",
    INVALID_MAIL: "You have entered an invalid email address!",
    OTP_SENT: "OTP has been sent to your registered email!",
    OTP_NOT_SENT: "OTP has been not sent !",
    INVALID_OTP: "You have entered an invalid OTP!",
    OTP_VERIFY: "OTP verified successfully!",
    PASSWORD_VERIFIED: "Password updated successfully!",
    BAD_REQUEST: "Invalid request!",
    VALID_USER: "Valid User",
    USER_TYPE_NOT_ACTIVE: "User type not active!",
    VALIDATION_ERROR: "Validation error!",
    DATA_CREATED: "Record created successfully!",
    DATA_NOT_CREATED: "Record has not been created!",
    DATA_NOT_UPDATED: "Record has not been updated!",
    DATA_UPDATED: "Record updated successfully!",
    DATA_DELETED: "Record deleted successfully!",
    LOGIN_USER_DELETED_FAILED: "You can't deleted you own login user!",
    UNAUTHORISED: "You don't have permission to perform this action!",
    UNIQUE_ID_ALREADY_EXISTS: "Uniques id is already in exists in our records.",
    STUDENT_USER_TYPE_NOT_FOUND: "Student user type has not been found.",
    PARENT_USER_TYPE_NOT_FOUND: "Parent user type has not been found.",
    USER_TYPE_NOT_FOUND: "User type has not been found.",
    STAFFS_NOT_FOUND: "Staff details has not been found.",
    STAFFS_ADDED: "Staff details has been saved successfully.",
    STUDENTS_NOT_FOUND: "Students has not been found.",
    STUDENTS_ADDED: "Students has been added successfully.",
    STUDENTS_INVITED: "Students has been invited successfully.",
    STAFF_INVITED: "Staff has been invited successfully.",
    RECOVERY_EMAIL_SENT: "Recovery link has been sent to the registered Email Id",
    FILE_REQUIRED: "Input file is required!",
    FILE_UPLOADED: "File uploaded successfully!",
    FILE_NOT_UPLOADED: "Error uploading file!",
    EXCEL_GENERATION_FAILED: "Excel generation failed",
    EXCEL_UPLOAD_FAILED: "Excel upload failed",
    DATA_EXISTS: "Record already exists",
    PASSWORD_ALREADY_SET: "Password already set for this account!",
    INSTITUTION_NOT_FOUND: "Institution has not been found!",
    NOT_UNIQUE: "Already exists with same value!",
    STAFF_DOB_VALIDATION: "Staff's date of birth is not valid! Staff age should be minimum " + StaffAge.MIN + " years.",
    ALREADY_SUBMITTED_RESPONSE: "You have already submitted a feedback response.",
    FEEDBACK_FORM_NOT_FOUND: "Feedback form is not exist in our records",
    ADMINISTRATOR_EMAIL_OTP_SENT: "OTP has been sent to your email!",
    ADMINISTRATOR_EMAIL_UPDATED: "Email updated successfully!",
    ADMINISTRATOR_EMAIL_NOT_UPDATED: "Record has not been updated!",
    ALREADY_SUBMITTED_RESPONSE: "You have already submitted a feedback response.",
    ALREADY_UPLOADED_CANNOT_REUPLOAD: "Template already uploaded and can't be reuploaded again!",
    TEST_TEMPLATE_ALREADY_EXIST: "Template for same Batch and Test Type already created!",
    ASSESSMENT_NOT_TAKEN: "Assessment Not Taken!",
    DOCUMNET_NOT_FOUND: "Document is not exixts in our records.",
    LIKE_DOUBT: "Doubt Liked",
    UNLIKE_DOUBT: "Doubt Unliked",
    RESOLVE_DOUBT: "Doubt resolved successfully",
    
}

const Usertypes = {
    // Master Admin
    SUPER_ADMIN: "SUPER_ADMIN",

    // Institute
    ADMIN: "ADMIN",

    // Staff
    SUPER_USER: "SUPER_USER",
    STAFF: "STAFF",

    // Student
    PARENT: "PARENT",
    STUDENT: "STUDENT",
}
const Validation = {
    OTP: "Otp should be minimum 6 digit",
    PASSWORD: "Password should be minimum 6 character",
    // OTP: "Otp is required",
}
const Constant = {
    LIST_LIMIT_MINIMUM: 25,
    DEFAULT_PAGE: 0,
    LENGTH: 6,
    COUNT: 1,
}
const Batch = {
    CREATED: "Batch has been created successfully",
    UPDATED: "Batch has been updated successfully",
    CLASS_EXIST: "Class already exists in our records",
    EXAM_EXIST: "Exam type already exists in our records",
    CENTER_EXIST: "Center already exists in our records",
    SUBJECT_EXIST: "Subject already exists in our records",
    GROUP_EXIST: "Group already exists in our records",
    BATCH_EXIST: "Batch already exists in our records",
    CHAPTER_EXIST: "Chapter already exists in our records",
}
const Group = {
    CREATED: "Group has been created successfully",
    UPDATED: "Group has been updated successfully",
}

const Staff = {
    Faculty: "Faculty",
    SuperUser: "Superuser",
};
const Uploadpaths = {
    STAFF: "staff",
    INSTITUTION: "institution",
    STUDENT: "student",
    HAPPENING: "happening",
    LEAVE: "leave",
    NOTE: "note",
    ASSIGNMENT: "assignment",
    ASSIGNMENT_SUBMISSION: "assignment_submission",
    REQUEST: "request",
    NOTICE: "notice",
    ANNOUNCEMENT: "announcement"
}

const Publishstatus = {
    PENDING: "Pending",
    PUBLISHED: "Published",
    REUPLOADED: "Reuploaded",
    REPUBLISHED: "Republished"
},
    Timetable = {
        NO_CLASS_FOUND: "No Classes Today"
    }

const Generalstatus = {
    PENDING: "Pending",
    APPROVED: "Approved",
    REJECTED: "Rejected"
}

const Attendance = {
    ABSENT: "Absent",
    LEAVE: "Leave",
    LEAVE_REQUEST_PENDING: "One or more leave request is pending",
    ALREADY_HOLIDAY: "The date is already a holiday",
    LEAVE_APPLY_SUCCESS: "Your Leave Application is submitted successfully",
    LEAVE_APPLY_FAILED: "Failed to submit Your Leave Application",
    LEAVE_LIST: "No Leaves Found !",
    LEAVE_UPDATE: "Leave has been updated successfully",
    REQUEST_CREATE: "Your Request is created successfully",
    REQUEST_FAILED: "Failed to create your Request",
    REQUEST_APPROVED: "Your Request is Approved successfully",
    REQUEST_REJECTED: "Your Request is Rejected successfully",
    REQUEST_PENDING: "Your Request is Pending successfully",
}

const FeedbackFormType = {
    Class: "Class",
    Batch: "Batch",
    Subject: "Subject",
    Group: "Group"
};

const commonModuleType = {
    Class: "Class",
    Batch: "Batch",
    Group: "Group"
};

const FeedbackFormStatus = {
    Completed: "Completed",
    Ongoing: "Ongoing"
};

const LoginStatus = {
    NOT_SIGNED_UP: "Not Signed Up",
    NOT_YET_INVITED: "Not Yet Invited",
    SIGNED_UP: "Signed Up"
};


module.exports = {
    Message,
    Usertypes,
    Validation,
    Constant,
    Batch,
    Group,
    Staff,
    Uploadpaths,
    Publishstatus,
    Timetable,
    Generalstatus,
    Attendance,
    StaffAge,
    FeedbackFormType,
    FeedbackFormStatus,
    LoginStatus,
    commonModuleType
}