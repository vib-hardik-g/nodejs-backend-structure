const commonResponse = require('./commonResponse');
const { Message, Usertypes} = require('./commonMessages');
const _ = require('lodash');

isSuperAdmin = async (req, res, next) => {
    let userData = req.authData.data;
    let userTypesArr = userData.userTypes.split(',');
    if(!_.includes(userTypesArr, Usertypes.SUPER_ADMIN)){
        return res.status(401).json(await commonResponse.response(false, Message.UNAUTHORISED));
    }else{
        next();
    }
};

isAdmin = async (req, res, next) => {
    let userData = req.authData.data;
    let userTypesArr = userData.userTypes.split(',');
    if(!_.includes(userTypesArr, Usertypes.ADMIN)){
        return res.status(401).json(await commonResponse.response(false, Message.UNAUTHORISED));
    }else{
        next();
    }
};


isAdminOrSuperAdmin = async (req, res, next) => {
    let userData = req.authData.data;
    let userTypesArr = userData.userTypes.split(',');
    if(_.includes(userTypesArr, Usertypes.SUPER_ADMIN) || _.includes(userTypesArr, Usertypes.ADMIN)){
        next();
    }else{
        return res.status(401).json(await commonResponse.response(false, Message.UNAUTHORISED));
    }
};

module.exports = { isSuperAdmin, isAdmin, isAdminOrSuperAdmin };