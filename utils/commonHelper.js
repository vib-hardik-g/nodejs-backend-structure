const AWS = require('aws-sdk');
const fs = require('fs');
const models = require('../app/models');

// AWS S3
const S3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION_NAME
});

/**
 * @param  {} DOB
 */
module.exports.getAge = (DOB) => {
    var today = new Date();
    var birthDate = new Date(DOB);

    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

/**
 * @description Upload files on S3 and store details in document table and return documentId as profilePicture.
 * @param  {} filePath, fileName
 */
module.exports.uploadFileOnS3 = async (filePath, fileName, folderName, isRemoved) => {
    return new Promise(async (resolve, reject) => {
        try {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    reject({
                        documentId: null
                    });
                } else {
                    const params = {
                        Bucket: process.env.AWS_BUCKET_NAME, // pass your bucket name
                        Key: folderName + '/' + fileName, // file will be saved as testBucket/contacts.csv
                        Body: JSON.stringify(data, null, 2),
                        ACL: 'public-read',
                    };

                    S3.upload(params, async function (s3Err, data) {
                        if (isRemoved) {
                            await fs.unlinkSync(filePath);
                        }

                        if (s3Err) {
                            reject({
                                documentId: null
                            });
                        } else {
                            let s3Data = data;

                            let document = await models.document.create({
                                type: folderName,
                                etag: s3Data.ETag.substring(1, s3Data.ETag.length - 1),
                                fileName: s3Data.key.split('/').pop(),
                                filePath: s3Data.key,
                                fullPath: s3Data.Location,
                                mimeType: s3Data.mimetype,
                                size: s3Data.size,
                                originalFileName: fileName,
                                fileExtension: null
                            });

                            resolve({
                                documentId: document.id
                            });
                        }
                    });
                }
            });
        } catch (error) {
            resolve({
                documentId: null
            });
        }
    });
}